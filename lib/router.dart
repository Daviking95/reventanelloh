import 'package:anelloh_mobile/UI/activity_screen.dart';
import 'package:anelloh_mobile/UI/all_listings_screen.dart';
import 'package:anelloh_mobile/UI/change_pin_screen.dart';
import 'package:anelloh_mobile/UI/dashboard_screen.dart';
import 'package:anelloh_mobile/UI/edit_order_screen.dart';
import 'package:anelloh_mobile/UI/edit_passport.dart';
import 'package:anelloh_mobile/UI/edit_profile_screen.dart';
import 'package:anelloh_mobile/UI/face_unlock_screen.dart';
import 'package:anelloh_mobile/UI/identity_details_screen.dart';
import 'package:anelloh_mobile/UI/instant_match_screen.dart';
import 'package:anelloh_mobile/UI/match_found_screen.dart';
import 'package:anelloh_mobile/UI/no_match_screen.dart';
import 'package:anelloh_mobile/UI/payment_info_screen.dart';
import 'package:anelloh_mobile/UI/payment_summary_screen.dart';
import 'package:anelloh_mobile/UI/pin_creation_screen.dart';
import 'package:anelloh_mobile/UI/pin_face_id_screen.dart';
import 'package:anelloh_mobile/UI/pin_unlock_screen.dart';
import 'package:anelloh_mobile/UI/profile_screen.dart';
import 'package:anelloh_mobile/UI/select_profile_image_screen.dart';
import 'package:anelloh_mobile/UI/settings_screen.dart';
import 'package:anelloh_mobile/UI/support_screen.dart';
import 'package:anelloh_mobile/UI/transaction_summary_screen.dart';
import 'package:anelloh_mobile/UI/update_password_screen.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_bloc.dart';
import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/change_pin/change_pin_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/listings/listings_bloc.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_bloc.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_bloc.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/protected_screen/protected_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/router/router_bloc.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/session_timer/session_timer_bloc.dart';
import 'package:anelloh_mobile/bloc/update_password/update_password_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_bloc.dart';
import 'package:anelloh_mobile/helpers/protected_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'UI/create_order_screen.dart';
import 'UI/forgot_password_screen.dart';
import 'UI/login_screen.dart';
import 'UI/notifications_screen.dart';
import 'UI/onboarding_screen.dart';
import 'UI/payment_options_screen.dart';
import 'UI/signup_screen.dart';
import 'bloc/dashboard/dashboard_bloc.dart';
import 'bloc/forgot_password/forgot_password_bloc.dart';
import 'bloc/login/login_bloc.dart';
import 'bloc/notification/notification_bloc.dart';
import 'bloc/onboarding/onboarding_bloc.dart';
import 'bloc/signup/signup_bloc.dart';
import 'bloc/user/user_bloc.dart';
import 'styles.dart';

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.name == "/") return child;
    if (animation.status == AnimationStatus.reverse ||
        settings.name == '/change_pin' ||
        settings.name == '/update_password') {
      return super
          .buildTransitions(context, animation, secondaryAnimation, child);
    } else {
      return FadeTransition(opacity: animation, child: child);
    }
  }
}

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    Map<String, dynamic> routeArgs =
        settings.arguments != null ? settings.arguments : {};

    switch (settings.name) {
      case '/onboarding':
        return new MyCustomRoute(
          builder: (context) => BlocProvider<OnboardingScreenBloc>(
            create: (context) => OnboardingScreenBloc(),
            child: WillPopScope(
              child: OnboardingScreen(),
              onWillPop: () async {
                return false;
              },
            ),
          ),
          settings: settings,
        );
      case '/sign_up':
        return new MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginScreenBloc>(
                create: (context) => LoginScreenBloc(),
              ),
              BlocProvider<SignUpScreenBloc>(
                create: (context) => SignUpScreenBloc(),
              ),
            ],
            child: SignUpScreen(),
          ),
          settings: settings,
        );
      case '/login':
        return new MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<LoginScreenBloc>(
                create: (context) => LoginScreenBloc(),
              ),
              BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
            ],
            child: WillPopScope(
              child: LoginScreen(
                  isSessionExpired: routeArgs['isSessionExpired'] != null
                      ? routeArgs['isSessionExpired']
                      : false,
                  email: routeArgs['email'],
                  token: routeArgs['token']),
              onWillPop: () async {
                return false;
              },
            ),
          ),
          settings: settings,
        );
      case '/forgot_password':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  child: ForgotPasswordScreen(
                      email: routeArgs['email'], token: routeArgs['token']),
                  providers: [
                    BlocProvider<ForgotPasswordScreenBloc>(
                      create: (context) => ForgotPasswordScreenBloc(),
                      child: ForgotPasswordScreen(
                          email: routeArgs['email'], token: routeArgs['token']),
                    )
                  ],
                ),
              )),
        );
        break;
      case '/edit_passport':
        return MyCustomRoute(builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
            ],
            child: ProtectedScreen(
              child: EditPassportScreen(),
            ),
          );
        });
        break;
      case '/dashboard':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
                BlocProvider<MarketRateBloc>(
                  create: (context) => MarketRateBloc(),
                ),
                BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<DashboardBloc>(
                        create: (context) => DashboardBloc()),
                    BlocProvider<MainUserOrderBloc>(
                      create: (context) => MainUserOrderBloc(),
                    ),
                    BlocProvider<NotificationBloc>(
                      create: (context) => NotificationBloc(),
                    ),
                  ],
                  child: WillPopScope(
                    child: MainDashboardScreen(),
                    onWillPop: () async {
                      return false;
                    },
                  ),
                ),
              )),
          settings: settings,
        );
      case '/create_order':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
            ],
            child: ProtectedScreen(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider<CreateOrderScreenBloc>(
                      create: (context) => CreateOrderScreenBloc()),
                  BlocProvider<MarketRateBloc>(
                    create: (context) => MarketRateBloc(),
                  ),
                  BlocProvider<AllBanksBloc>(
                    create: (context) => AllBanksBloc(),
                  ),
                  BlocProvider<CurrencyConversionBloc>(
                    create: (context) => CurrencyConversionBloc(),
                  ),
                ],
                child: CreateOrderScreen(
                  editOrderObj: routeArgs['editOrderObj'],
                ),
              ),
            ),
          ),
          settings: settings,
        );
        break;
      case '/edit_order':
        return MyCustomRoute(builder: (context) {
          return MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<CreateOrderScreenBloc>(
                        create: (context) => CreateOrderScreenBloc()),
                    BlocProvider<MarketRateBloc>(
                      create: (context) => MarketRateBloc(),
                    ),
                    BlocProvider<AllBanksBloc>(
                      create: (context) => AllBanksBloc(),
                    ),
                    BlocProvider<CurrencyConversionBloc>(
                      create: (context) => CurrencyConversionBloc(),
                    ),
                    BlocProvider<EditOrderScreenBloc>(
                        create: (context) => EditOrderScreenBloc()),
                  ],
                  child: EditOrderScreen(
                    editOrderObj: routeArgs['editOrderObj'],
                  ),
                ),
              ));
        });
        break;
      case '/profile':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<ProfileScreenBloc>(
                        create: (context) => ProfileScreenBloc()),
                    BlocProvider<SelectProfileImageScreenBloc>(
                        create: (context) => SelectProfileImageScreenBloc()),
                  ],
                  child: ProfileScreen(),
                ),
              )),
          settings: settings,
        );
        break;
      case '/update_password':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
            ],
            child: ProtectedScreen(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider<UpdatePasswordScreenBloc>(
                      create: (context) => UpdatePasswordScreenBloc()),
                ],
                child: UpdatePasswordScreen(),
              ),
            ),
          ),
          settings: settings,
        );
        break;
      case '/pin_face_id':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
            ],
            child: ProtectedScreen(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider<UpdatePasswordScreenBloc>(
                      create: (context) => UpdatePasswordScreenBloc()),
                ],
                child: PinAndFaceIdScreen(),
              ),
            ),
          ),
          settings: settings,
        );
        break;
      case '/identity':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
            ],
            child: ProtectedScreen(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider<UpdatePasswordScreenBloc>(
                      create: (context) => UpdatePasswordScreenBloc()),
                ],
                child: IdentityDetailsScreen(),
              ),
            ),
          ),
          settings: settings,
        );
        break;
      case '/change_pin':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
            ],
            child: ProtectedScreen(
              child: MultiBlocProvider(
                providers: [
                  BlocProvider<ChangePinScreenBloc>(
                      create: (context) => ChangePinScreenBloc()),
                ],
                child: ChangePinScreen(),
              ),
            ),
          ),
          settings: settings,
        );
        break;
      case '/upload_profile_image':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<SelectProfileImageScreenBloc>(
                        create: (context) => SelectProfileImageScreenBloc()),
                  ],
                  child: SelectProfileImageScreen(
                    imageList: routeArgs['imageList'],
                  ),
                ),
              )),
          settings: settings,
        );
        break;
      case '/edit_profile':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<ProfileScreenBloc>(
                        create: (context) => ProfileScreenBloc()),
                    BlocProvider<SelectProfileImageScreenBloc>(
                        create: (context) => SelectProfileImageScreenBloc()),
                  ],
                  child: EditProfileScreen(),
                ),
              )),
          settings: settings,
        );
        break;
      case '/match_found':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<MatchFoundScreenBloc>(
                        create: (context) => MatchFoundScreenBloc()),
                    BlocProvider<AllBanksBloc>(
                        create: (context) => AllBanksBloc()),
                    BlocProvider<MainUserOrderBloc>(
                        create: (context) => MainUserOrderBloc()),
                  ],
                  child: MatchFoundScreen(
                    matchFoundObj: routeArgs['matchFoundObj'],
                  ),
                ),
              )),
          settings: settings,
        );
        break;
      case '/no_match':
        return MyCustomRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider<ProtectedScreenBloc>(
                          create: (context) => ProtectedScreenBloc()),
                      BlocProvider<SessionTimerBloc>(
                          create: (context) => SessionTimerBloc()),
                    ],
                    child: ProtectedScreen(
                      child: MultiBlocProvider(
                          providers: [
                            BlocProvider<AllListingScreenBloc>(
                                create: (context) => AllListingScreenBloc()),
                            BlocProvider<ListingsActionBloc>(
                                create: (context) => ListingsActionBloc()),
                            BlocProvider<CreateOrderScreenBloc>(
                                create: (context) => CreateOrderScreenBloc()),
                            BlocProvider<AllBanksBloc>(
                              create: (context) => AllBanksBloc(),
                            ),
                            BlocProvider<MatchNotFoundBloc>(
                                create: (context) => MatchNotFoundBloc()),
                            BlocProvider<EditOrderScreenBloc>(
                                create: (context) => EditOrderScreenBloc()),
                          ],
                          child: MatchNotFoundScreen(
                              createOrderObj: routeArgs['createOrderObj'],
                              isEdit: routeArgs['isEdit'],
                              editOrderObj: routeArgs['editOrderObj'])),
                    )),
            settings: settings);
        break;
      case '/payment_info':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    //change
                    BlocProvider<MatchFoundScreenBloc>(
                        create: (context) => MatchFoundScreenBloc()),
                  ],
                  child: PaymentInfoScreen(
                    matchFoundObj: routeArgs['matchFoundObj'],
                  ),
                ),
              )),
          settings: settings,
        );
        break;
      case '/listings':
        return MyCustomRoute(
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider<ProtectedScreenBloc>(
                          create: (context) => ProtectedScreenBloc()),
                      BlocProvider<SessionTimerBloc>(
                          create: (context) => SessionTimerBloc()),
                      BlocProvider<RouterBloc>(
                          create: (context) => RouterBloc()),
                    ],
                    child: ProtectedScreen(
                      child: MultiBlocProvider(
                        providers: [
                          // change
                          BlocProvider<AllListingScreenBloc>(
                              create: (context) => AllListingScreenBloc()),
                          BlocProvider<ListingsActionBloc>(
                              create: (context) => ListingsActionBloc()),
                        ],
                        child: AllListingsScreen(),
                      ),
                    )),
            settings: settings);
        break;
      case '/instant_match':
        return MyCustomRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider<ProtectedScreenBloc>(
                        create: (context) => ProtectedScreenBloc()),
                    BlocProvider<SessionTimerBloc>(
                        create: (context) => SessionTimerBloc()),
                    BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
                  ],
                  child: ProtectedScreen(
                    child: MultiBlocProvider(
                      providers: [
                        BlocProvider<ListingsActionBloc>(
                            create: (context) => ListingsActionBloc()),
                        BlocProvider<InstantMatchScreenBloc>(
                          create: (context) => InstantMatchScreenBloc(),
                        ),
                        BlocProvider<MainUserOrderBloc>(
                            create: (context) => MainUserOrderBloc()),
                        BlocProvider<UserBloc>(
                            create: (context) => UserBloc()),
                        BlocProvider<AllBanksBloc>(
                            create: (context) => AllBanksBloc()),
                      ],
                      child: InstantMatchScreen(
                        orderId: routeArgs['orderId'],
                        orderOwnerId: routeArgs['orderOwnerId'],
                        orderListing: routeArgs['orderListing'],
                        currencyNeededCountryCode:
                            routeArgs['currencyNeededCountryCode'],
                      ),
                    ),
                  ),
                ),
            settings: settings);
        break;
      case '/settings':
        return MyCustomRoute(
            builder: (context) => MultiBlocProvider(
                  providers: [
                    BlocProvider<ProtectedScreenBloc>(
                        create: (context) => ProtectedScreenBloc()),
                    BlocProvider<SessionTimerBloc>(
                        create: (context) => SessionTimerBloc()),
                    BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
                    BlocProvider<UserBloc>(create: (context) => UserBloc()),
                    BlocProvider<ProfileScreenBloc>(
                        create: (context) => ProfileScreenBloc()),
                  ],
                  child: ProtectedScreen(
                    child: MultiBlocProvider(
                      providers: [
                        BlocProvider<ListingsActionBloc>(
                            create: (context) => ListingsActionBloc()),
                      ],
                      child: SettingsScreen(),
                    ),
                  ),
                ),
            settings: settings);
        break;
      case '/activities':
        return MyCustomRoute(
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
                BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<MainUserOrderBloc>(
                      create: (context) => MainUserOrderBloc(),
                    ),
                  ],
                  child: ActivityScreen(),
                ),
              )),
          settings: settings,
        );
        break;
      case '/payment_summary':
        return MyCustomRoute(
          settings: settings,
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
                BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<MainUserOrderBloc>(
                      create: (context) => MainUserOrderBloc(),
                    ),
                    BlocProvider<PaymentSummaryStateBloc>(
                      create: (context) => PaymentSummaryStateBloc(),
                    ),
                  ],
                  child: PaymentSummary(
                    matchFoundObj: routeArgs['matchFoundObj'],
                    editOrderObj: routeArgs['editOrderObj'],
                    paymentSummaryOrderCard:
                        routeArgs['paymentSummaryOrderCard'],
                    orderNo: routeArgs['orderNo'],
                    customerId: routeArgs['customerId'],
                    sessionId: routeArgs['sessionId'],
                    retryCount: routeArgs['retryCount'],
                    paymentType: routeArgs['payment_type'],
                  ),
                ),
              )),
        );
        break;
      case '/payment_option':
        return MyCustomRoute(
          settings: settings,
          builder: (context) => MultiBlocProvider(
              providers: [
                BlocProvider<ProtectedScreenBloc>(
                    create: (context) => ProtectedScreenBloc()),
                BlocProvider<SessionTimerBloc>(
                    create: (context) => SessionTimerBloc()),
                BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
                BlocProvider<EditOrderScreenBloc>(
                    create: (context) => EditOrderScreenBloc()),
              ],
              child: ProtectedScreen(
                child: MultiBlocProvider(
                  providers: [
                    BlocProvider<MainUserOrderBloc>(
                      create: (context) => MainUserOrderBloc(),
                    ),
                    BlocProvider<PaymentSummaryStateBloc>(
                      create: (context) => PaymentSummaryStateBloc(),
                    ),
                  ],
                  child: PaymentOptionsScreen(
                    editOrderObj: routeArgs['editOrderObj'],
                    paymentSummaryOrderCard:
                        routeArgs['paymentSummaryOrderCard'],
                    orderNo: routeArgs['orderNo'],
                    customerId: routeArgs['customerId'],
                    sessionId: routeArgs['sessionId'],
                    retryCount: routeArgs['retryCount'],
                    paymentType: routeArgs['payment_type'],
                  ),
                ),
              )),
        );
        break;
      case '/support':
        return MyCustomRoute(
            settings: settings,
            builder: (context) => MultiBlocProvider(
                    providers: [
                      BlocProvider<ProtectedScreenBloc>(
                          create: (context) => ProtectedScreenBloc()),
                      BlocProvider<SessionTimerBloc>(
                          create: (context) => SessionTimerBloc()),
                      BlocProvider<RouterBloc>(
                          create: (context) => RouterBloc()),
                    ],
                    child: ProtectedScreen(
                      child: MultiBlocProvider(
                        providers: [
                          BlocProvider<MainUserOrderBloc>(
                            create: (context) => MainUserOrderBloc(),
                          ),
                        ],
                        child: SupportScreen(),
                      ),
                    )));
        break;
      case '/pin_unlock':
        return MyCustomRoute(
            builder: (context) {
              return MultiBlocProvider(
                providers: [
                  // BlocProvider<UserBloc>(create: (context) => UserBloc()),
                  BlocProvider<ProtectedScreenBloc>(
                      create: (context) => ProtectedScreenBloc()),
                  BlocProvider<SessionTimerBloc>(
                      create: (context) => SessionTimerBloc()),
                ],
                child: ProtectedScreen(
                  child: MultiBlocProvider(
                    providers: [
                      BlocProvider<PinUnlockScreenBloc>(
                        create: (context) => PinUnlockScreenBloc(),
                      ),
                    ],
                    child: PinUnlockScreen(
                      sessionExpired: routeArgs['sessionExpired'],
                    ),
                  ),
                ),
              );
            },
            settings: settings);
        break;
      case '/face_id_unlock':
        return MyCustomRoute(
          builder: (context) {
            return MultiBlocProvider(
              providers: [
                BlocProvider<PinUnlockScreenBloc>(
                  create: (context) => PinUnlockScreenBloc(),
                ),
              ],
              child: FaceUnlockScreen(),
            );
          },
          settings: settings,
        );
        break;
      case '/create_pin':
        return MyCustomRoute(builder: (context) {
          return WillPopScope(
              child: PinCreationScreen(),
              onWillPop: () async {
                return false;
              });
        });
      case '/transaction_summary':
        return MyCustomRoute(builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
              BlocProvider<RouterBloc>(create: (context) => RouterBloc()),
              BlocProvider<MainUserOrderBloc>(create: (context) => MainUserOrderBloc()),
            ],
            child: ProtectedScreen(
              child: TransactionSummaryScreen(
                activityCardItem: routeArgs['activityCardItem'],
              ),
            ),
          );
        });
        break;
      case '/notifications':
        return MyCustomRoute(builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<ProtectedScreenBloc>(
                  create: (context) => ProtectedScreenBloc()),
              BlocProvider<SessionTimerBloc>(
                  create: (context) => SessionTimerBloc()),
              BlocProvider<NotificationBloc>(
                  create: (context) => NotificationBloc()),
            ],
            child: ProtectedScreen(
              child: NotificationsScreen(),
            ),
          );
        });
        break;
      default:
        return errorRoute();
    }
  }

  static Route<dynamic> errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          backgroundColor: appPrimaryColor,
          title: Text('Error'),
        ),
        body: Center(
          child: Text(
            'Error : Page Not Found',
            style: textStylePrimaryDarkColorBold,
          ),
        ),
      );
    });
  }
}
