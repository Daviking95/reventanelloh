import 'dart:io';
import 'dart:ui' as ui;

import 'package:anelloh_mobile/bloc/login/login_bloc.dart';
import 'package:anelloh_mobile/bloc/login/login_event.dart';
import 'package:anelloh_mobile/bloc/login/login_state.dart';
import 'package:anelloh_mobile/bloc/router/router_bloc.dart';
import 'package:anelloh_mobile/bloc/router/router_event.dart';
import 'package:anelloh_mobile/bloc/router/router_state.dart';
import 'package:anelloh_mobile/helpers/anelloh_dialog.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/font_util.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/showDialog.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:local_auth/auth_strings.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';
import '../styles.dart';

class LoginScreen extends StatefulWidget {
  final bool isSessionExpired;
  final String email;
  final String token;

  LoginScreen({this.isSessionExpired, this.email, this.token});
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LocalAuthentication auth = LocalAuthentication();

  bool _canCheckBiometrics;
  List<BiometricType> _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  bool showPassword = false;
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _emailAddressController =
      new TextEditingController();
  final TextEditingController _passwordController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    AppConstants.userData = null;
    // clearPref();

    _loginDeveloper();
    if (widget.email != null) {
      _emailAddressController.value = TextEditingValue(text: widget.email);
      // this._checkBiometrics();
    }
    _verifyEmail();
  }

  _loginDeveloper() {
    BlocProvider.of<LoginScreenBloc>(context)
        .add(LoginDeveloperEvent(name: env['DEVELOPER_LOGIN_NAME']));
  }

  _verifyEmail() async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    if (_pref.getString('_EMAIL_VERIFY_') != null &&
        _pref.getString('_EMAIL_VERIFY_') != '' &&
        widget.email != null) {
      BlocProvider.of<LoginScreenBloc>(context).add(
          VerifyEmailEvent(emailAddress: widget.email, token: widget.token));
    }
  }

  _signUpScreen() {
    BlocProvider.of<RouterBloc>(context)
        .add(RouterMoveToCreateAccountScreenEvent());
  }

  _login() async {
    if (_formKey.currentState.validate()) {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      await _pref.setString('_EMAIL_VERIFY_', '');
      LoginRequestData loginRequestData = LoginRequestData(
          emailAddress: _emailAddressController.text.trim(),
          password: _passwordController.text.trim());

      BlocProvider.of<LoginScreenBloc>(context)
          .add(LoginActionEvent(loginRequestData: loginRequestData));
    }
  }

  // Future<void> _checkBiometrics() async {
  //   bool canCheckBiometrics;
  //   try {
  //     canCheckBiometrics = await auth.canCheckBiometrics;
  //   } on PlatformException catch (e) {
  //     print(e);
  //   }
  //   if (!mounted) return;
  //   if (canCheckBiometrics) {
  //     _getAvailableBiometrics();
  //   }
  // }

  // Future<void> _getAvailableBiometrics() async {
  //   List<BiometricType> availableBiometrics;
  //   try {
  //     availableBiometrics = await auth.getAvailableBiometrics();
  //     if (availableBiometrics.contains(BiometricType.face)) {
  //       _authenticate();
  //     }else{
  //       displayAlert(context: context, title: 'Face Biometric', content: 'We could not detect any face biometric on your device');
  //     }
  //   } on PlatformException catch (e) {
  //     print(e);
  //   }
  //   if (!mounted) return;
  // }

  Future<void> _useFingerPrint({bool isFace = false}) async {
    try {
      if (_emailAddressController.text.isNotEmpty) {
        List<BiometricType> availableBiometrics =
            await auth.getAvailableBiometrics();

        // print('Available Biometrics ${availableBiometrics}');

        if (!(await auth.canCheckBiometrics)) {
          return displayAlert(
              context: context,
              title: "Fingerprint not supported",
              content:
                  "Your device does not support fingerprint authentication");
        }
        if (availableBiometrics.contains(BiometricType.fingerprint) ||
            availableBiometrics.contains(BiometricType.face)) {
          await _authenticate(isFace: true);
        } else {
          displayAlert(
              context: context,
              title: 'Finger Print Biometric',
              content:
                  'We could not detect any finger print biometric on your device');
        }
      } else {
        return anellohOverlay(
            overlayObject: OverlayObject(
              title: 'Warning',
              body: 'Email cannot be empty',
              hasBtn: false,
              hasCancelBtn: false,
              icon: Icon(
                Icons.warning,
                color: Colors.red,
              ),
            ),
            context: context);
      }
    } on PlatformException catch (e) {
      print(e);
    } catch (err) {
      print(err);
    }
  }

  Future<void> _useFaceID() async {
    try {
      await _authenticate();
    } catch (err) {}
  }

  Future<void> _authenticate({bool isFace = false}) async {
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });

      const andStrings = const AndroidAuthMessages(
        cancelButton: 'cancel',
        goToSettingsButton: 'Go to set',
        goToSettingsDescription: 'Please set the fingerprint.',
        signInTitle: 'fingerprint verification',
      );

      authenticated = await auth.authenticate(
          biometricOnly: isFace ? false : true,
          localizedReason: 'Scan to log in',
          androidAuthStrings: andStrings,
          useErrorDialogs: true,
          stickyAuth: true);
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    if (authenticated) {
      SharedPreferences _pref = await SharedPreferences.getInstance();
      await _pref.setString('_EMAIL_VERIFY_', '');

      BlocProvider.of<LoginScreenBloc>(context)
          .add(LoginWithDeviceIDEvent(email: _emailAddressController.text.trim()));
    }
  }

  _forgotPassword() {
    BlocProvider.of<LoginScreenBloc>(context)
        .add(MoveToForgotPasswordScreenEvent());
  }

  _determineLoadingState({@required LoginScreenState state}) {
    if (state is LoggingInDeveloper) return true;
    if (state is RequestingLogin) return true;
    if (state is VerifyingEmail) return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RouterBloc, RouterState>(
      listener: (context, state) {
        if (state is RouterMoveToCreateAccountScreen) {
          Navigator.of(context).pushNamed(RoutesConstants.signUpUrl);
        }
        if (state is RouterMoveToDashboardScreen) {
          Navigator.of(context).pushNamed(RoutesConstants.dashboardUrl);
        }
      },
      child: BlocListener<LoginScreenBloc, LoginScreenState>(
        listener: (context, state) {
          if (state is CompletedLoginAction) {
            BlocProvider.of<RouterBloc>(context)
                .add(RouterMoveToDashboardScreenEvent());
          }
          if (state is MoveToForgotPasswordScreen) {
            Navigator.of(context).pushNamed(RoutesConstants.forgotPasswordUrl);
          }

          if (state is FailedLoginException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Error',
                  body: state.error,
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
          if (state is LoginHttpException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Warning',
                  body: state?.message ?? 'Slow or poor internet connection',
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
          if (state is LoginNoInternetException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Warning',
                  body: state?.error ?? 'Not connected to the internet',
                  hasBtn: false,
                  hasCancelBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
          if (state is EmailVerificationSuccess) {
            return anellohDialog(context,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomText(
                        title:
                            'Email Verification Success, Please login to continue',
                      ),
                      AnellohRoundButtonSmall(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          childText: 'OK')
                    ],
                  ),
                ),
                canClick: true);
          }
          if (state is EmailVerificationFailed) {
            return anellohDialog(context,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomText(
                        title:
                            '${state.error}, Please contact our support team: support@anelloh.com',
                      ),
                      AnellohRoundButtonSmall(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          childText: 'OK')
                    ],
                  ),
                ),
                canClick: true);
          }
          if (state is DeveloperLoginFailed) {
            return anellohDialog(context,
                child: Container(
                  alignment: Alignment.center,
                  margin: EdgeInsets.all(10),
                  padding: EdgeInsets.all(10),
                  width: MediaQuery.of(context).size.width,
                  height: 200,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CustomText(
                        title: '${state.error}. \nClick OK to restart process',
                      ),
                      AnellohRoundButtonSmall(
                          onPressed: () {
                            Navigator.pop(context);
                            _loginDeveloper();
                          },
                          childText: 'OK')
                    ],
                  ),
                ),
                canClick: false);
          }
          if (state is DeveloperLoginHttpException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Warning',
                  body: state.error,
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
          if (state is DeveloperLoginSocketException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Warning',
                  body: state.error,
                  hasBtn: false,
                  hasCancelBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
        },
        child: BlocBuilder<LoginScreenBloc, LoginScreenState>(
          builder: (context, state) {
            return RefreshIndicator(
                child: LoadingOverlay(
                  isLoading: _determineLoadingState(state: state),
                  progressIndicator: ProgressWidget(),
                  color: ColorPalette().textBlack,
                  opacity: 0.4,
                  child: Scaffold(
                    resizeToAvoidBottomInset: true,
                    body: SafeArea(
                        child: SingleChildScrollView(
                      physics: AlwaysScrollableScrollPhysics(),
                      child: Center(
                        // alignment: Alignment.center,
                        child: Column(
                          mainAxisSize: MainAxisSize.max,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Container(
                              margin: EdgeInsets.only(top: 80),
                              alignment: Alignment.center,
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    child: Image.asset(
                                        'assets/images/anello_update_logo.png',
                                        fit: BoxFit.cover),
                                    width: mainSizer(context: context, size: 2),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  Container(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          child: Text(
                                            'Log In',
                                            style: googleFontTextStyle(
                                                fontSize: 20,
                                                fontWeight: FontWeight.w600),
                                          ),
                                        ),
                                        widget.isSessionExpired == true
                                            ? Container(
                                                margin:
                                                    EdgeInsets.only(top: 10),
                                                child: Text(
                                                  'Login session expired.',
                                                  style: googleFontTextStyle(
                                                    fontSize: 15,
                                                    color: ColorPalette()
                                                        .dangerRed,
                                                  ),
                                                ),
                                              )
                                            : SizedBox(),
                                        Container(
                                          margin: EdgeInsets.only(top: 10),
                                          child: Text(
                                            'Enter Login details',
                                            style: googleFontTextStyle(
                                              fontSize: 15,
                                              color: ColorPalette().main,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  top: 40, left: 20, right: 20, bottom: 10),
                              child: Form(
                                key: _formKey,
                                autovalidateMode:
                                    AutovalidateMode.onUserInteraction,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    AppTextInputs(
                                      color: mainColor,
                                      controller: _emailAddressController,
                                      textInputType: TextInputType.emailAddress,
                                      validateInput:
                                          AppConstants.validators.validateEmail,
                                      textInputTitle: "Email Address",
                                      autoFocus: false,
                                    ),
                                    AppPasswordTextInput(
                                      color: mainColor,
                                      controller: _passwordController,
                                      validateInput: AppConstants
                                          .validators.validatePassword,
                                      textInputTitle: "Password",
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      child: Flex(
                                        direction: Axis.horizontal,
                                        mainAxisAlignment:
                                            MainAxisAlignment.end,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          AnellohFlatButton(
                                            onPressed: _forgotPassword,
                                            childText: 'Forgot Password?',
                                            fontWeight: FontWeight.w300,
                                          )
                                        ],
                                      ),
                                    ),
                                    Padding(padding: EdgeInsets.all(10)),
                                    AnellohRoundNetworkButtonLarge(
                                        onPressed: _login,
                                        childText: 'Log In',
                                        isLoading: state is RequestingLogin
                                            ? true
                                            : false),
                                    Padding(padding: EdgeInsets.all(10)),
                                    Container(
                                      width: MediaQuery.of(context).size.width,
                                      alignment: Alignment.center,
                                      child: Flex(
                                        direction: Axis.horizontal,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            child: Text(
                                              'Don\'t have an account?',
                                              style: googleFontTextStyle(
                                                  fontSize: 13),
                                            ),
                                          ),
                                          AnellohFlatButton(
                                              onPressed: _signUpScreen,
                                              childText: 'Sign Up'),
                                        ],
                                      ),
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Container(
                                          margin: EdgeInsets.only(top: 40),
                                          child: InkWell(
                                            onTap: () {
                                              _useFingerPrint();
                                            },
                                            child: ShaderMask(
                                                blendMode: BlendMode.srcIn,
                                                shaderCallback: (Rect bounds) {
                                                  return ui.Gradient.linear(
                                                    Offset(0.0, 0.0),
                                                    Offset(200.0, 70.0),
                                                    [
                                                      ColorPalette().main,
                                                      ColorPalette().mainMedium
                                                    ],
                                                  );
                                                },
                                                child: Icon(
                                                  Icons.fingerprint,
                                                  size: 50,
                                                )),
                                          ),
                                        ),
                                        // Platform.isAndroid
                                        //     ? Container()
                                        //     : Container(
                                        //         margin:
                                        //             EdgeInsets.only(top: 40),
                                        //         child: InkWell(
                                        //           onTap: () {
                                        //             _useFingerPrint(
                                        //                 isFace: true);
                                        //           },
                                        //           child: ShaderMask(
                                        //               blendMode:
                                        //                   BlendMode.srcIn,
                                        //               shaderCallback:
                                        //                   (Rect bounds) {
                                        //                 return ui.Gradient
                                        //                     .linear(
                                        //                   Offset(0.0, 0.0),
                                        //                   Offset(200.0, 70.0),
                                        //                   [
                                        //                     ColorPalette().main,
                                        //                     ColorPalette()
                                        //                         .mainMedium
                                        //                   ],
                                        //                 );
                                        //               },
                                        //               child: Icon(
                                        //                 Icons.face,
                                        //                 size: 50,
                                        //               )),
                                        //         ),
                                        //       ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    )),
                  ),
                ),
                onRefresh: () {
                  _loginDeveloper();
                  return;
                });
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

