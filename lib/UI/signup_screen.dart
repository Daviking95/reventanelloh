import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:anelloh_mobile/UI/otp_screen.dart';
import 'package:anelloh_mobile/bloc/login/login_bloc.dart';
import 'package:anelloh_mobile/bloc/login/login_event.dart';
import 'package:anelloh_mobile/bloc/login/login_state.dart';
import 'package:anelloh_mobile/bloc/otp/otp_bloc.dart';
import 'package:anelloh_mobile/bloc/signup/signup_bloc.dart';
import 'package:anelloh_mobile/bloc/signup/signup_event.dart';
import 'package:anelloh_mobile/bloc/signup/signup_state.dart';
import 'package:anelloh_mobile/helpers/anelloh_dialog.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/image_size.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/showDialog.dart';
import 'package:anelloh_mobile/styles.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:country_code_picker/country_code_picker.dart';
import 'package:date_field/date_field.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_svg/svg.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../constants.dart';

class SignUpScreen extends StatefulWidget {
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {
  bool showPassword = false;
  int _currentScreen = 0;

  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _emailAddressController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  TextEditingController _confirmPasswordController = TextEditingController();
  TextEditingController _bvnController = TextEditingController();
  TextEditingController _ssnController = TextEditingController();
  TextEditingController _ninController = TextEditingController();
  TextEditingController _passportNumberController = TextEditingController();
  TextEditingController _phoneController = TextEditingController();
  ScrollController _scrollController = ScrollController();

  String countryCode = 'NG';

  // String phoneNumber = '';
  DateTime expiryDate;
  File preferredID;
  bool hasDocumentError = true;
  bool hasDocumentSizeError = true;
  String preferredIdType = 'NIN'; // NIN or INT-PASS;

  String bvnValidity = '';
  int hasVerifiedBvn = 0;
  var imageSize;

  String deviceCountry = '';

  Locale locale;

  final _formKey = GlobalKey<FormState>();
  final _bvnFormKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _loginDeveloper();
    // _firstNameController.text = 'zarvy';
    // _lastNameController.text = 'zarvy';
    // _userNameController.text = 'zarvy';
    // _emailAddressController.text = 'zarvilla16@gmail.com';
    // _passwordController.text = 'Zarvilla2018##';
    // _confirmPasswordController.text = 'Zarvilla2018##';
    // _phoneController.text = '08166340512';
    // _bvnController.text = '08166340512';
    // _ninController.text = '08888777777';
  }

  _removeDocumentImage() {
    setState(() {
      preferredID = null;
      hasDocumentError = true;
      hasDocumentSizeError = true;
    });
  }

  _loginDeveloper() {
    BlocProvider.of<LoginScreenBloc>(context)
        .add(LoginDeveloperEvent(name: env['DEVELOPER_LOGIN_NAME']));
  }

  showRegistrationDialog() {
    WidgetsFlutterBinding.ensureInitialized();
    SchedulerBinding.instance.addPostFrameCallback((_) {
      return anellohDialog(
        context,
        canClick: false,
        child: Center(
          child: Container(
            padding: EdgeInsets.all(10),
            alignment: Alignment.center,
            constraints: BoxConstraints(
                minWidth: MediaQuery.of(context).size.width,
                maxHeight: MediaQuery.of(context).size.height / 3),
            decoration: BoxDecoration(color: Colors.white),
            child: Center(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  CustomText(
                    title: 'Email Verification',
                    isBold: true,
                    textSize: mainSizer(context: context, size: 21),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  CustomText(
                    title:
                        'Click on the link sent to your email address, to verify your account',
                    textSize: mainSizer(context: context, size: 28),
                    isCenter: true,
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  SvgPicture.asset('assets/images/envelope.svg'),
                  SizedBox(
                    height: 10,
                  ),
                  AnellohRoundButtonSmall(
                      onPressed: () {
                        Navigator.of(context).pushReplacementNamed('/login');
                      },
                      childText: 'OK')
                ],
              ),
            ),
          ),
        ),
      );
    });
  }

  _continue() {
    if (_formKey.currentState.validate()) {
      BlocProvider.of<SignUpScreenBloc>(context).add(VerifyEmailAndUserName(
          email: _emailAddressController.text.trim(),
          userName: _userNameController.text));
    }
  }

  String _validateConfirmPassword(v) {
    if (_passwordController.text.isEmpty) {
      return 'Password cannot be empty';
    }
    if (v != _passwordController.text) {
      return 'Passwords do not match';
    }
    return null;
  }

  _createAccount() {
    if (_bvnFormKey.currentState.validate()) {
      SignUpRequestData requestData;
      if (this.preferredIdType == 'NIN') {
        if (hasDocumentError == true || hasDocumentSizeError == true) {
          return;
        } else {
          if (this.preferredID != null ||
              this.hasDocumentError == false ||
              this.hasDocumentSizeError == false) {
            String base64Image = base64Encode(preferredID.readAsBytesSync());

            // print('What is base64 $base64Image');
            // log('What is base64 $base64Image');

            requestData = SignUpRequestData(
              emailAddress: this._emailAddressController.text.trim(),
              firstName: this._firstNameController.text,
              userName: this._userNameController.text,
              lastName: this._lastNameController.text,
              countryCode: this.countryCode,
              bvn: this._bvnController.text.isNotEmpty
                  ? this._bvnController.text
                  : '',
              phoneNumber: this._phoneController.text,
              password: this._passwordController.text.trim(),
              documentType: 1,
              documentImage: base64Image,
              documentNumber: this._ninController.text,
              ssn: _ssnController.text,
              documentExpiryDate:
                  expiryDate != null ? expiryDate.toIso8601String() : null,
            );

            // print('What is preferredID $preferredID');

            BlocProvider.of<SignUpScreenBloc>(context)
                .add(CreateAccountEvent(signUpRequestData: requestData));
          }
        }
      } else {
        if (hasDocumentError == true || hasDocumentSizeError == true) {
          return;
        } else {
          if (this.preferredID != null ||
              this.expiryDate != null &&
                  hasDocumentError == false &&
                  hasDocumentSizeError == false) {
            String base64Image = base64Encode(preferredID.readAsBytesSync());

            // print('What is base64 $base64Image');
            // log('What is base64 $base64Image');

            requestData = SignUpRequestData(
              emailAddress: this._emailAddressController.text.trim(),
              firstName: this._firstNameController.text,
              userName: this._userNameController.text,
              lastName: this._lastNameController.text,
              countryCode: this.countryCode,
              bvn: this._bvnController.text.isNotEmpty
                  ? this._bvnController.text
                  : '',
              phoneNumber: this._phoneController.text,
              password: this._passwordController.text.trim(),
              documentType: 2,
              documentImage: base64Image,
              documentNumber: this._passportNumberController.text,
              documentExpiryDate:
                  expiryDate != null ? expiryDate.toIso8601String() : null,
            );
            BlocProvider.of<SignUpScreenBloc>(context)
                .add(CreateAccountEvent(signUpRequestData: requestData));
          } else {
            return;
          }
        }
      }
    } else {
      _scrollController.animateTo(0.0,
          duration: Duration(milliseconds: 1), curve: Curves.easeIn);
    }
  }

  _loginScreen() {
    BlocProvider.of<SignUpScreenBloc>(context).add(MoveToLoginScreenEvent());
  }

  _determineLoadingState({@required SignUpScreenState state}) {
    if (state is VerifyingEmailAndUsername) return true;
    if (state is RequestingSignUp) return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    // print('hasDocumentSizeError $hasDocumentSizeError');
    // print('hasDocumentError $hasDocumentError');

    return BlocListener<LoginScreenBloc, LoginScreenState>(
      listener: (context, state) {
        if (state is DeveloperLoginFailed) {
          return anellohDialog(context,
              child: Container(
                alignment: Alignment.center,
                margin: EdgeInsets.all(10),
                padding: EdgeInsets.all(4),
                width: MediaQuery.of(context).size.width,
                height: 200,
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    CustomText(
                      title: state.error.toString(),
                    ),
                    AnellohRoundButtonSmall(
                        onPressed: () {
                          Navigator.pop(context);
                          _loginDeveloper();
                        },
                        childText: 'OK')
                  ],
                ),
              ),
              canClick: false);
        }
        if (state is DeveloperLoginHttpException) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Warning',
                body: state?.error?.toString() ??
                    'Slow or poor internet connection',
                hasBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
        if (state is DeveloperLoginSocketException) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Warning',
                body: state?.error?.toString(),
                hasBtn: false,
                hasCancelBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
      },
      child: BlocListener<SignUpScreenBloc, SignUpScreenState>(
        listener: (context, state) {
          if (state is EmailAndUsernameVerifySuccess) {
            setState(() {
              _currentScreen = 1;
            });
            _scrollController.animateTo(0.0,
                duration: Duration(seconds: 1), curve: Curves.easeIn);
          }
          if (state is EmailAndUsernameVerifyFailed) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                    title: 'Error',
                    body: state?.error,
                    hasBtn: false,
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    )),
                context: context);
          }
          if (state is MoveToLoginScreen) {
            Navigator.of(context).pushNamed(RoutesConstants.loginUrl);
          }
          if (state is MoveToOTPScreen) {
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
              return BlocProvider<OTPScreenBloc>(
                create: (context) => OTPScreenBloc(),
                child: OTPScreen(
                  token: state.token,
                ),
              );
            }));
          }
          if (state is SignUpAccountCreated) {
            return showRegistrationDialog();
          }
          if (state is FailedSignUpException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                    title: 'Error',
                    body: state.error,
                    hasBtn: false,
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    )),
                context: context);
          }
          if (state is SignUpNoInternetException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Warning',
                  body: state.error,
                  hasBtn: false,
                  hasCancelBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
          if (state is SignUpHttpException) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                  title: 'Warning',
                  body: state?.error ?? 'Slow or poor internet connection',
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  ),
                ),
                context: context);
          }
        },
        child: BlocBuilder<SignUpScreenBloc, SignUpScreenState>(
          builder: (context, blocState) {
            return LoadingOverlay(
              isLoading: _determineLoadingState(state: blocState),
              progressIndicator: ProgressWidget(),
              color: ColorPalette().textBlack,
              opacity: 0.4,
              child: Scaffold(
                resizeToAvoidBottomInset: true,
                appBar: AppBar(
                  elevation: 0.0,
                  backgroundColor: Colors.transparent,
                  leading: _currentScreen == 1
                      ? IconButton(
                          icon: Icon(
                            Icons.arrow_back_ios,
                            color: ColorPalette().main,
                          ),
                          onPressed: () {
                            setState(() {
                              _currentScreen = 0;
                            });
                          })
                      : SizedBox(),
                ),
                body: SafeArea(
                    child: SingleChildScrollView(
                  controller: _scrollController,
                  child: Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: _currentScreen == 0
                          ? CrossAxisAlignment.center
                          : CrossAxisAlignment.start,
                      children: [
                        _currentScreen == 0
                            ? Container(
                                margin: EdgeInsets.only(top: 15),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    SizedBox(
                                      height: 10,
                                    ),
                                    Container(
                                      child: Image.asset(
                                          'assets/images/anello_update_logo.png',
                                          fit: BoxFit.cover),
                                      width:
                                          mainSizer(context: context, size: 2),
                                    ),
                                    SizedBox(
                                      height: 30,
                                    ),
                                    Container(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            child: Text(
                                              'Create Account',
                                              style: GoogleFonts.montserrat(
                                                fontSize: 20,
                                                color: ColorPalette().main,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 10),
                                            child: Text(
                                              'Enter details to get started',
                                              style: GoogleFonts.montserrat(
                                                fontSize: 15,
                                                color: ColorPalette().main,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                padding: EdgeInsets.symmetric(
                                    horizontal: 17, vertical: 5),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Container(
                                            child: Text(
                                              'Complete your Registration',
                                              style: GoogleFonts.montserrat(
                                                fontSize: 20,
                                                color: ColorPalette().main,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          Container(
                                            margin: EdgeInsets.only(top: 10),
                                            child: Text(
                                              'You are required to provide necessary documents to complete registration.',
                                              style: GoogleFonts.montserrat(
                                                fontSize: 15,
                                                color: ColorPalette().main,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                        Container(
                          margin: EdgeInsets.only(
                              top: 40, left: 20, right: 20, bottom: 20),
                          child: _currentScreen == 0
                              ? Form(
                                  key: _formKey,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  child: Column(
                                    children: [
                                      AppTextInputs(
                                        color: mainColor,
                                        controller: _firstNameController,
                                        textInputType: TextInputType.text,
                                        validateInput: AppConstants
                                            .validators.validateName,
                                        textInputTitle: "First name",
                                        maxLength: 20,
                                        autoFocus: true,
                                        textInputFormatter: [
                                          FilteringTextInputFormatter.allow(
                                              new RegExp('[A-Za-z- ]'))
                                        ],
                                      ),
                                      AppTextInputs(
                                        color: mainColor,
                                        controller: _lastNameController,
                                        textInputType: TextInputType.text,
                                        validateInput: AppConstants
                                            .validators.validateName,
                                        textInputTitle: "Last name",
                                        maxLength: 20,
                                        textInputFormatter: [
                                          FilteringTextInputFormatter.allow(
                                              new RegExp('[A-Za-z- ]'))
                                        ],
                                      ),
                                      AppTextInputs(
                                        color: mainColor,
                                        controller: _userNameController,
                                        textInputType: TextInputType.name,
                                        validateInput: AppConstants
                                            .validators.validateString,
                                        textInputTitle: "User name",
                                        maxLength: 20,
                                      ),
                                      AppTextInputs(
                                        color: mainColor,
                                        controller: _emailAddressController,
                                        textInputType:
                                            TextInputType.emailAddress,
                                        validateInput: AppConstants
                                            .validators.validateEmail,
                                        textInputTitle: "Email address",
                                      ),
                                      CustomText(
                                        title:
                                            'Customers outside Nigeria are advised to register with email address linked to their bank account.',
                                        textSize: mainSizer(
                                            context: context, size: 30),
                                        textColor: ColorPalette().dangerRed,
                                        isCenter: false,
                                      ),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      AppPasswordTextInput(
                                        color: mainColor,
                                        controller: _passwordController,
                                        validateInput: AppConstants
                                            .validators.validatePassword,
                                        textInputTitle: "Password",
                                      ),
                                      SizedBox(
                                        height: 20,
                                      ),
                                      AppPasswordTextInput(
                                        color: mainColor,
                                        controller: _confirmPasswordController,
                                        validateInput: (val) => MatchValidator(
                                                errorText:
                                                    'Passwords do not match')
                                            .validateMatch(
                                                val, _passwordController.text),
                                        textInputTitle: "Confirm Password",
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        mainAxisSize: MainAxisSize.max,
                                        children: [
                                          // Radio(
                                          //   value: true,
                                          //   groupValue: true,
                                          //   onChanged: (value) {},
                                          //   activeColor: ColorPalette().main,
                                          // ),
                                          Expanded(
                                            child: InkWell(
                                                onTap: 
                                                Platform.isIOS
                                                    ? () => _launchURL('https://www.anelloh.com/terms-and-conditions') :
                                                    () {
                                                  Navigator.push(context,
                                                      MaterialPageRoute(
                                                    builder:
                                                        (BuildContext context) {
                                                      return TermsAndConditionsScreen();
                                                    },
                                                  ));
                                                },
                                                child: RichText(
                                                  text: TextSpan(
                                                    text:
                                                        'By clicking the continue button, you have accepted our ',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 12),
                                                    children: <TextSpan>[
                                                      TextSpan(
                                                          text:
                                                              'Terms and Conditions',
                                                          style: TextStyle(
                                                              fontWeight:
                                                                  FontWeight
                                                                      .bold,
                                                              color: ColorPalette()
                                                                  .textSecondary)),
                                                    ],
                                                  ),
                                                )),
                                          )
                                        ],
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Padding(padding: EdgeInsets.all(2)),
                                      AnellohRoundButtonLarge(
                                        onPressed: _continue,
                                        childText: 'Continue',
                                      ),
                                      Padding(padding: EdgeInsets.all(10)),
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        alignment: Alignment.center,
                                        child: Flex(
                                          direction: Axis.horizontal,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            Container(
                                              child: Text(
                                                'Already have an account?',
                                                style: GoogleFonts.montserrat(
                                                  fontSize: 13,
                                                  color: ColorPalette().main,
                                                ),
                                              ),
                                            ),
                                            AnellohFlatButton(
                                                onPressed: _loginScreen,
                                                childText: 'Log in'),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                )
                              : Form(
                                  key: _bvnFormKey,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                CountryCodePicker(
                                                  onChanged: (CountryCode
                                                      countryCode) {
                                                    //TODO : manipulate the selected country code here
                                                    // print(
                                                    //     "New Country selected: " +
                                                    //         countryCode.code
                                                    //             .toString());
                                                    // print(
                                                    //     "New Country selected: " +
                                                    //         countryCode.name
                                                    //             .toString());
                                                    // print(
                                                    //     "New Country selected: " +
                                                    //         countryCode.dialCode
                                                    //             .toString());
                                                    this.countryCode =
                                                        countryCode.code
                                                            .toString();
                                                  },
                                                  // Initial selection and favorite can be one of code ('IT') OR dial_code('+39')
                                                  initialSelection: 'NG',
                                                  favorite: ['+234', 'NG'],
                                                  // optional. Shows only country name and flag
                                                  showCountryOnly: false,
                                                  // optional. Shows only country name and flag when popup is closed.
                                                  showOnlyCountryWhenClosed:
                                                      false,
                                                  // optional. aligns the flag and the Text left
                                                  alignLeft: false,
                                                ),
                                                SizedBox(
                                                  width: 20,
                                                ),
                                                Expanded(
                                                  flex: 2,
                                                  child: AppTextInputs(
                                                    color: mainColor,
                                                    controller:
                                                        _phoneController,
                                                    textInputType:
                                                        TextInputType.phone,
                                                    validateInput: (value) {
                                                      if (value.isEmpty)
                                                        return 'Phone number is required';
                                                      if (value.length < 10 ||
                                                          value.length > 15)
                                                        return 'Invalid Phone number';
                                                      return null;
                                                    },
                                                    textInputTitle:
                                                        "Phone Number",
                                                  ),
                                                ),
                                              ],
                                            ),

                                            // IntlPhoneField(
                                            //   onCountryChanged: (phone) {
                                            //     setState(() {
                                            //       this.countryCode = phone
                                            //           .countryISOCode
                                            //           .toString();
                                            //     });
                                            //   },
                                            //   keyboardType: TextInputType.phone,
                                            //   style: textStyleWhiteColorNormal
                                            //       .copyWith(
                                            //           color: Colors.black),
                                            //   countryCodeTextColor:
                                            //       ColorPalette().textBlack,
                                            //   decoration: InputDecoration(
                                            //     counterText: '',
                                            //     labelText: 'Phone Number',
                                            //     fillColor:
                                            //         ColorPalette().textBlack,
                                            //     border: OutlineInputBorder(
                                            //         borderSide: BorderSide(
                                            //           color:
                                            //               ColorPalette().main,
                                            //         ),
                                            //         borderRadius:
                                            //             BorderRadius.circular(
                                            //                 14)),
                                            //   ),
                                            //   initialValue: this.phoneNumber,
                                            //   initialCountryCode:
                                            //       this.countryCode.isEmpty
                                            //           ? 'NG'
                                            //           : this.countryCode,
                                            //   inputFormatters: [
                                            //     new FilteringTextInputFormatter
                                            //         .allow(RegExp("[0-9]")),
                                            //   ],
                                            //   validator: (value) {
                                            //     if (value.isEmpty)
                                            //       return 'Phone number is required';
                                            //     if (value.length < 10 ||
                                            //         value.length > 15)
                                            //       return 'Invalid Phone number';
                                            //     return null;
                                            //   },
                                            //   onChanged: (phone) {
                                            //     setState(() {
                                            //       log('Country Code ${phone.countryCode}');
                                            //       this.countryCode =
                                            //           phone.countryCode;
                                            //     });
                                            //     if (phone.countryISOCode !=
                                            //         this.countryCode) {
                                            //       this._setFormFieldValue(
                                            //           value:
                                            //               phone.countryISOCode,
                                            //           field: 'countryCode');
                                            //     }
                                            //     this._setFormFieldValue(
                                            //         value: phone.number,
                                            //         field: 'phoneNumber');
                                            //   },
                                            // ),
                                          ],
                                        ),
                                      ),
                                      this.countryCode == 'NG'
                                          ? Container(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                      padding:
                                                          EdgeInsets.all(3)),
                                                  AppTextInputs(
                                                    color: mainColor,
                                                    controller: _bvnController,
                                                    textInputType:
                                                        TextInputType.number,
                                                    suffixIcon: ConstrainedBox(
                                                      constraints:
                                                          BoxConstraints(
                                                              maxHeight: 20,
                                                              maxWidth: 20),
                                                      child: Icon(
                                                        Icons.create,
                                                        color:
                                                            ColorPalette().main,
                                                        size: mainSizer(
                                                            context: context,
                                                            size: 24),
                                                      ),
                                                    ),
                                                    maxLength: 11,
                                                    isOnlyDigits: true,
                                                    validateInput: AppConstants
                                                        .validators.validateBVN,
                                                    textInputTitle: "BVN",
                                                    onChange: (value) async {
                                                      try {
                                                        setState(() {
                                                          hasVerifiedBvn = 1;
                                                        });
                                                        if (value.length ==
                                                            11) {
                                                          if (await checkIfBvnExists(
                                                              value)) return;

                                                          log('I am getting here');
                                                          log('bvn data ${{
                                                            'bvn': value,
                                                            "first_name":
                                                                _firstNameController
                                                                    .text,
                                                            "last_name":
                                                                _lastNameController
                                                                    .text,
                                                            'account_number':
                                                                "0000000000",
                                                            'bank_code': '485'
                                                          }}');

                                                          Dio.Response
                                                              response =
                                                              await dio.post(
                                                                  'https://api.paystack.co/bvn/match',
                                                                  data: {
                                                                    'bvn':
                                                                        value,
                                                                    "first_name":
                                                                        _firstNameController
                                                                            .text
                                                                            .toLowerCase(),
                                                                    "last_name":
                                                                        _lastNameController
                                                                            .text
                                                                            .toLowerCase(),
                                                                    'account_number':
                                                                        "0000000000",
                                                                    'bank_code':
                                                                        '485'
                                                                  },
                                                                  options: Dio
                                                                      .Options(
                                                                          headers: {
                                                                        'Authorization':
                                                                            'Bearer ${env['PAYSTACK_LIVE_SECRET_KEY']}'
                                                                      }));
                                                          log('BVN Request ${value}');

                                                          log('BVN Response ${response}');

                                                          if (response
                                                                  .statusCode ==
                                                              200) {
                                                            if (response.data[
                                                                    'status'] ==
                                                                true) {
                                                              if (!response.data[
                                                                          'data']
                                                                      [
                                                                      'first_name'] ||
                                                                  !response.data[
                                                                          'data']
                                                                      [
                                                                      'last_name']) {
                                                                setState(() {
                                                                  bvnValidity =
                                                                      'BVN name mismatch. Ensure your first name and last name match your BVN record';
                                                                  hasVerifiedBvn =
                                                                      3;
                                                                  // hasVerifiedBvn = 2;
                                                                });
                                                                return;
                                                              }

                                                              setState(() {
                                                                bvnValidity =
                                                                    response.data[
                                                                        'message'];
                                                                hasVerifiedBvn =
                                                                    2;
                                                              });
                                                            }
                                                          } else {
                                                            setState(() {
                                                              bvnValidity =
                                                                  'Invalid BVN';
                                                              hasVerifiedBvn =
                                                                  3;
                                                              // hasVerifiedBvn = 2;
                                                            });
                                                          }
                                                        }
                                                      } catch (err) {
                                                        if (err
                                                            is Dio.DioError) {
                                                          print(err.response);
                                                          print(err
                                                              .response.data);
                                                          setState(() {
                                                            bvnValidity = err
                                                                    .response
                                                                    .data[
                                                                'message'];
                                                            hasVerifiedBvn = 3;
                                                            // hasVerifiedBvn = 2;
                                                          });
                                                        }
                                                      }
                                                    },
                                                  ),
                                                  Padding(
                                                    padding: EdgeInsets.only(
                                                        left: 10, right: 10),
                                                    child: Text(
                                                      '$bvnValidity',
                                                      style: TextStyle(
                                                          color:
                                                              hasVerifiedBvn ==
                                                                      2
                                                                  ? Colors.green
                                                                  : Colors.red),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            )
                                          : Container(),
                                      this.countryCode != 'US'
                                          ? Container()
                                          : Container(
                                              child: Column(
                                                mainAxisSize: MainAxisSize.max,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                      padding:
                                                          EdgeInsets.all(3)),
                                                  AppPasswordTextInput(
                                                    color: mainColor,
                                                    controller: _ssnController,
                                                    textInputType:
                                                        TextInputType.number,
                                                    isOnlyDigits: true,
                                                    maxLength: 9,
                                                    suffixIcon: ConstrainedBox(
                                                      constraints:
                                                          BoxConstraints(
                                                              maxHeight: 20,
                                                              maxWidth: 20),
                                                      child: Icon(
                                                        Icons.create,
                                                        color:
                                                            ColorPalette().main,
                                                        size: mainSizer(
                                                            context: context,
                                                            size: 24),
                                                      ),
                                                    ),
                                                    validateInput: AppConstants
                                                        .validators.validateSSN,
                                                    textInputTitle: "SSN",
                                                  ),
                                                ],
                                              ),
                                            ),
                                      CustomText(
                                        title: 'Select preferred ID:',
                                        textSize: 16,
                                      ),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          this.countryCode == 'NG'
                                              ? Expanded(
                                                  child: ListTile(
                                                    title: CustomText(
                                                      title: 'NIN',
                                                    ),
                                                    leading: Radio(
                                                      value: 'NIN',
                                                      activeColor:
                                                          ColorPalette().main,
                                                      focusColor:
                                                          ColorPalette().main,
                                                      groupValue:
                                                          this.preferredIdType,
                                                      onChanged: (value) {
                                                        setState(() {
                                                          preferredIdType =
                                                              value;
                                                        });
                                                      },
                                                    ),
                                                    onTap: () {
                                                      setState(() {
                                                        preferredIdType = 'NIN';
                                                      });
                                                    },
                                                  ),
                                                )
                                              : SizedBox(),
                                          Expanded(
                                            child: ListTile(
                                              title: CustomText(
                                                title: "Int'l Passport",
                                              ),
                                              leading: Radio(
                                                value: 'INT-PASS',
                                                activeColor:
                                                    ColorPalette().main,
                                                focusColor: ColorPalette().main,
                                                groupValue:
                                                    this.preferredIdType,
                                                onChanged: (value) {
                                                  setState(() {
                                                    preferredIdType = value;
                                                  });
                                                },
                                              ),
                                              onTap: () {
                                                setState(() {
                                                  preferredIdType = 'INT-PASS';
                                                });
                                              },
                                            ),
                                          ),
                                        ],
                                      ),
                                      SizedBox(
                                        height: 3,
                                      ),
                                      this.preferredIdType == 'NIN' &&
                                              this.countryCode == 'NG'
                                          ? Column(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                AppTextInputs(
                                                  controller: _ninController,
                                                  color: mainColor,
                                                  isOnlyDigits: true,
                                                  maxLength: 11,
                                                  textInputType:
                                                      TextInputType.number,
                                                  validateInput: AppConstants
                                                      .validators.validateNIN,
                                                  suffixIcon: ConstrainedBox(
                                                    constraints: BoxConstraints(
                                                        maxHeight: 20,
                                                        maxWidth: 20),
                                                    child: Icon(
                                                      Icons.create,
                                                      color:
                                                          ColorPalette().main,
                                                      size: mainSizer(
                                                          context: context,
                                                          size: 24),
                                                    ),
                                                  ),
                                                  textInputTitle: "NIN",
                                                ),
                                                this.preferredID != null
                                                    ? Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .max,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              SvgPicture.asset(
                                                                  'assets/images/document_text_outline.svg'),
                                                              SizedBox(
                                                                width: 10,
                                                              ),
                                                              Center(
                                                                child:
                                                                    Container(
                                                                  child: Image
                                                                      .file(
                                                                    this.preferredID,
                                                                    width:
                                                                        100.0,
                                                                    height:
                                                                        100.0,
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              CustomText(
                                                                title: imageSize
                                                                    .toString(),
                                                                textColor:
                                                                    Colors
                                                                        .black,
                                                                textSize: 18,
                                                              )
                                                            ],
                                                          ),
                                                          InkWell(
                                                            child: Icon(
                                                                Icons.clear),
                                                            onTap:
                                                                _removeDocumentImage,
                                                          ),
                                                        ],
                                                      )
                                                    : InkWell(
                                                        onTap: () =>
                                                            displayImagePickerDialog(
                                                                context,
                                                                () async {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  FocusNode());
                                                          Navigator.pop(
                                                              context);
                                                          PickedFile image =
                                                              await ImagePicker().getImage(
                                                                  imageQuality:
                                                                      5,
                                                                  source:
                                                                      ImageSource
                                                                          .camera);
                                                          if (image != null) {
                                                            log("File Size ${await getFileSize(image.path, 1)}");

                                                            setState(() {
                                                              preferredID =
                                                                  File(image
                                                                      .path);
                                                              hasDocumentSizeError =
                                                                  false;
                                                              hasDocumentError =
                                                                  false;
                                                            });

                                                            imageSize =
                                                                await getFileSize(
                                                                    image.path,
                                                                    1);
                                                            print(
                                                                'Image Size ${imageSize.toString().split(' ')[1] == 'KB' && double.parse(imageSize.toString().split(' ')[0]).toInt() > 1024}');

                                                            if (imageSize.toString().split(
                                                                            ' ')[
                                                                        1] !=
                                                                    'KB' ||
                                                                imageSize.toString().split(' ')[
                                                                            1] !=
                                                                        'B' &&
                                                                    double.parse(imageSize.toString().split(' ')[0])
                                                                            .toInt() >
                                                                        1024) {
                                                              setState(() {
                                                                hasDocumentSizeError =
                                                                    true;
                                                              });
                                                              return;
                                                            }
                                                          } else {
                                                            print(
                                                                'No image selected.');
                                                          }
                                                        }, () async {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  FocusNode());
                                                          Navigator.pop(
                                                              context);
                                                          PickedFile image =
                                                              await ImagePicker().getImage(
                                                                  imageQuality:
                                                                      5,
                                                                  source: ImageSource
                                                                      .gallery);
                                                          if (image != null) {
                                                            log("File Size ${await getFileSize(image.path, 1)}");

                                                            setState(() {
                                                              preferredID =
                                                                  File(image
                                                                      .path);
                                                              hasDocumentSizeError =
                                                                  false;
                                                              hasDocumentError =
                                                                  false;
                                                            });

                                                            imageSize =
                                                                await getFileSize(
                                                                    image.path,
                                                                    1);

                                                            print(
                                                                'Image Size ${imageSize.toString().split(' ')[1] == 'KB' && double.parse(imageSize.toString().split(' ')[0]).toInt() > 1024}');

                                                            if (imageSize.toString().split(
                                                                            ' ')[
                                                                        1] !=
                                                                    'KB' ||
                                                                imageSize.toString().split(' ')[
                                                                            1] !=
                                                                        'B' &&
                                                                    double.parse(imageSize.toString().split(' ')[0])
                                                                            .toInt() >
                                                                        1024) {
                                                              setState(() {
                                                                hasDocumentSizeError =
                                                                    true;
                                                              });
                                                              return;
                                                            }
                                                          } else {
                                                            print(
                                                                'No image selected.');
                                                          }
                                                        }),
                                                        child: Center(
                                                          child: DottedBorder(
                                                            color:
                                                                ColorPalette()
                                                                    .main,
                                                            dashPattern: [
                                                              10,
                                                              5
                                                            ],
                                                            borderType:
                                                                BorderType
                                                                    .RRect,
                                                            radius:
                                                                Radius.circular(
                                                                    3),
                                                            strokeWidth: 1,
                                                            child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 10),
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          5,
                                                                      horizontal:
                                                                          5),
                                                              width: 350,
                                                              height: 200,
                                                              child: Center(
                                                                child: Column(
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .max,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Icon(
                                                                      Icons
                                                                          .file_upload,
                                                                      color: ColorPalette()
                                                                          .main,
                                                                    ),
                                                                    CustomText(
                                                                      isCenter:
                                                                          true,
                                                                      title:
                                                                          'browse',
                                                                      textColor:
                                                                          ColorPalette()
                                                                              .dangerRed,
                                                                    ),
                                                                    this.preferredID !=
                                                                            null
                                                                        ? CustomText(
                                                                            title:
                                                                                this.preferredID.path.split('/').last,
                                                                            isCenter:
                                                                                true,
                                                                          )
                                                                        : SizedBox(),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                              ],
                                            )
                                          : Column(
                                              mainAxisSize: MainAxisSize.max,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: [
                                                AppTextInputs(
                                                  controller:
                                                      _passportNumberController,
                                                  color: mainColor,
                                                  textInputType:
                                                      TextInputType.text,
                                                  maxLength: 20,
                                                  validateInput: AppConstants
                                                      .validators
                                                      .validateInternationalPassportNumber,
                                                  textInputTitle:
                                                      "Passport Number",
                                                ),
                                                this.preferredID != null
                                                    ? Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .center,
                                                        mainAxisSize:
                                                            MainAxisSize.max,
                                                        children: [
                                                          Row(
                                                            mainAxisSize:
                                                                MainAxisSize
                                                                    .min,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .center,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .start,
                                                            children: [
                                                              SvgPicture.asset(
                                                                  'assets/images/document_text_outline.svg'),
                                                              SizedBox(
                                                                width: 10,
                                                              ),
                                                              Center(
                                                                child:
                                                                    Container(
                                                                  child: Image
                                                                      .file(
                                                                    this.preferredID,
                                                                    width:
                                                                        100.0,
                                                                    height:
                                                                        100.0,
                                                                    fit: BoxFit
                                                                        .cover,
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 20,
                                                              ),
                                                              CustomText(
                                                                title: imageSize
                                                                    .toString(),
                                                                textColor:
                                                                    Colors
                                                                        .black,
                                                                textSize: 18,
                                                              )
                                                            ],
                                                          ),
                                                          InkWell(
                                                            child: Icon(
                                                                Icons.clear),
                                                            onTap:
                                                                _removeDocumentImage,
                                                          ),
                                                        ],
                                                      )
                                                    : InkWell(
                                                        onTap: () =>
                                                            displayImagePickerDialog(
                                                                context,
                                                                () async {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  FocusNode());

                                                          Navigator.pop(
                                                              context);
                                                          PickedFile image =
                                                              await ImagePicker().getImage(
                                                                  imageQuality:
                                                                      5,
                                                                  source:
                                                                      ImageSource
                                                                          .camera);
                                                          if (image != null) {
                                                            log("File Size ${await getFileSize(image.path, 1)}");

                                                            setState(() {
                                                              preferredID =
                                                                  File(image
                                                                      .path);
                                                              hasDocumentSizeError =
                                                                  false;
                                                              hasDocumentError =
                                                                  false;
                                                            });

                                                            imageSize =
                                                                await getFileSize(
                                                                    image.path,
                                                                    1);
                                                            log('Image Size ${imageSize.toString().split(' ')[1] == 'KB' && double.parse(imageSize.toString().split(' ')[0]).toInt() > 1024}');

                                                            if (imageSize.toString().split(
                                                                            ' ')[
                                                                        1] !=
                                                                    'KB' ||
                                                                imageSize.toString().split(' ')[
                                                                            1] !=
                                                                        'B' &&
                                                                    double.parse(imageSize.toString().split(' ')[0])
                                                                            .toInt() >
                                                                        1024) {
                                                              setState(() {
                                                                hasDocumentSizeError =
                                                                    true;
                                                              });
                                                              return;
                                                            }
                                                          } else {
                                                            print(
                                                                'No image selected.');
                                                          }
                                                        }, () async {
                                                          FocusScope.of(context)
                                                              .requestFocus(
                                                                  FocusNode());

                                                          Navigator.pop(
                                                              context);
                                                          PickedFile image =
                                                              await ImagePicker().getImage(
                                                                  imageQuality:
                                                                      5,
                                                                  source: ImageSource
                                                                      .gallery);
                                                          if (image != null) {
                                                            log("File Size ${await getFileSize(image.path, 1)}");

                                                            setState(() {
                                                              preferredID =
                                                                  File(image
                                                                      .path);
                                                              hasDocumentSizeError =
                                                                  false;
                                                              hasDocumentError =
                                                                  false;
                                                            });

                                                            imageSize =
                                                                await getFileSize(
                                                                    image.path,
                                                                    1);
                                                            log('Image Size ${imageSize.toString().split(' ')[1] == 'KB' && double.parse(imageSize.toString().split(' ')[0]).toInt() > 1024}');

                                                            if (imageSize.toString().split(
                                                                            ' ')[
                                                                        1] !=
                                                                    'KB' ||
                                                                imageSize.toString().split(' ')[
                                                                            1] !=
                                                                        'B' &&
                                                                    double.parse(imageSize.toString().split(' ')[0])
                                                                            .toInt() >
                                                                        1024) {
                                                              setState(() {
                                                                hasDocumentSizeError =
                                                                    true;
                                                              });
                                                              return;
                                                            }
                                                          } else {
                                                            print(
                                                                'No image selected.');
                                                          }
                                                        }),
                                                        child: Center(
                                                          child: DottedBorder(
                                                            color:
                                                                ColorPalette()
                                                                    .main,
                                                            dashPattern: [
                                                              10,
                                                              5
                                                            ],
                                                            borderType:
                                                                BorderType
                                                                    .RRect,
                                                            radius:
                                                                Radius.circular(
                                                                    3),
                                                            strokeWidth: 1,
                                                            child: Container(
                                                              margin: EdgeInsets
                                                                  .only(
                                                                      top: 10),
                                                              padding: EdgeInsets
                                                                  .symmetric(
                                                                      vertical:
                                                                          5,
                                                                      horizontal:
                                                                          5),
                                                              width: 350,
                                                              height: 200,
                                                              child: Center(
                                                                child: Column(
                                                                  mainAxisSize:
                                                                      MainAxisSize
                                                                          .max,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .center,
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .center,
                                                                  children: [
                                                                    Icon(
                                                                      Icons
                                                                          .file_upload,
                                                                      color: ColorPalette()
                                                                          .main,
                                                                    ),
                                                                    CustomText(
                                                                      isCenter:
                                                                          true,
                                                                      title:
                                                                          'browse',
                                                                      textColor:
                                                                          ColorPalette()
                                                                              .dangerRed,
                                                                    ),
                                                                    this.preferredID !=
                                                                            null
                                                                        ? CustomText(
                                                                            title:
                                                                                this.preferredID.path.split('/').last,
                                                                            isCenter:
                                                                                true,
                                                                          )
                                                                        : SizedBox(),
                                                                  ],
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      ),
                                                SizedBox(
                                                  height: 20,
                                                ),
                                                DateTimeFormField(
                                                  decoration: InputDecoration(
                                                    labelText: 'Expiry Date',
                                                    border: OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              13),
                                                    ),
                                                  ),
                                                  validator: (value) {
                                                    if (hasDocumentError ==
                                                            true &&
                                                        value != null) {
                                                      return 'Document image is required';
                                                    }
                                                    if (value == null) {
                                                      return 'Expiry date cannot be empty';
                                                    }
                                                    if (value.isBefore(
                                                        new DateTime.now())) {
                                                      return 'Invalid passport - Expired';
                                                    }
                                                    return null;
                                                  },
                                                  mode: DateTimeFieldPickerMode
                                                      .date,
                                                  autovalidateMode:
                                                      AutovalidateMode.always,
                                                  onDateSelected:
                                                      (DateTime value) {
                                                    setState(() {
                                                      expiryDate = value;
                                                    });
                                                  },
                                                ),
                                              ],
                                            ),
                                      this.hasDocumentError == true ||
                                              hasDocumentSizeError == true
                                          ? Padding(
                                              padding:
                                                  EdgeInsets.only(left: 15),
                                              child: CustomText(
                                                title:
                                                    'Document Image is required and must be less than 1 MB',
                                                textSize: mainSizer(
                                                    context: context, size: 36),
                                                textColor: Colors.red,
                                              ),
                                            )
                                          : SizedBox(),
                                      SizedBox(
                                        height: 15,
                                      ),
                                      AnellohRoundNetworkButtonLarge(
                                        onPressed: hasVerifiedBvn != 2
                                            ? () {}
                                            : _createAccount,
                                        childText: hasVerifiedBvn == 2
                                            ? 'Create Account'
                                            : hasVerifiedBvn == 1
                                                ? 'Verifying BVN...'
                                                : 'Awaiting BVN verification...',
                                        isLoading: blocState is RequestingSignUp
                                            ? true
                                            : false,
                                      ),
                                    ],
                                  ),
                                ),
                        ),
                      ],
                    ),
                  ),
                )),
              ),
            );
          },
        ),
      ),
    );
  }

  Future<bool> checkIfBvnExists(dynamic value) async {
    bool doesBvnExist = false;
    HttpClientResponse response = await HttpService().post(
        url: 'Customers/bvncheck/$value',
        requestData: {"bvn": int.parse(value)});

    log('checkIfBvnExists Customers/bvncheck/$value');

    Map<String, dynamic> result =
        jsonDecode(await response.transform(utf8.decoder).join());

    log('checkIfBvnExists ${jsonEncode(result)}');
    log('checkIfBvnExists ${response.statusCode}');

    if (response.statusCode == 200) {
      if (result['succeeded'] == true) {
        setState(() {
          bvnValidity = result['message'] ?? result['messages'][0];
          hasVerifiedBvn = 2;
        });

        doesBvnExist = false;
      } else {
        setState(() {
          bvnValidity = result['message'] ?? result['messages'][0];
          hasVerifiedBvn = 3;
        });

        doesBvnExist = true;
      }
    } else {
      setState(() {
        bvnValidity = result['message'] ?? result['messages'][0];
        hasVerifiedBvn = 3;
      });

      doesBvnExist = true;
    }

    log('doesBvnExist $doesBvnExist');

    return doesBvnExist;
  }

  void _launchURL(String url) async =>
      await canLaunch(url) ? await launch(url) : throw 'Could not launch $url';
}

class TermsAndConditionsScreen extends StatefulWidget {
  @override
  TermsAndConditionsScreenState createState() =>
      TermsAndConditionsScreenState();
}

class TermsAndConditionsScreenState extends State<TermsAndConditionsScreen> {
  @override
  void initState() {
    super.initState();
    // Enable hybrid composition.
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child:  WebView(
              initialUrl: 'https://www.anelloh.com/terms-and-conditions',
              javascriptMode: JavascriptMode.unrestricted,
              gestureNavigationEnabled: true,
              gestureRecognizers: Set()
                ..add(Factory<OneSequenceGestureRecognizer>(
                    () => EagerGestureRecognizer())),
              onProgress: (_) => CircularProgressIndicator(),
            ),
    );
  }

}
