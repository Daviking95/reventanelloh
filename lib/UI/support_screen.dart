import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/text_casing.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intercom_plugin/intercom_plugin.dart';

class SupportScreen extends StatefulWidget {
  _SupportScreenState createState() => _SupportScreenState();
}

class _SupportScreenState extends State<SupportScreen> {
  @override
  void initState() {
    super.initState();
  }

  _talkToIntercom() {
    Intercom.displayMessenger();
  }

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
      bottomPageContent: _bottomPageContent(context: context),
      hasBottomNav: false,
      hasTopWidget: false,
      hasScrollChild: false,
      hasBackButton: true,
      hasRefreshOption: false,
      bgColor: ColorPalette().main,
      backBtnColor: ColorPalette().textWhite,
    );
  }

  Widget _bottomPageContent({@required BuildContext context}) {
    return SafeArea(
      child: Stack(
        children: [
          BlocBuilder<UserBloc, UserState>(builder: (context, state) {
            return Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  constraints: BoxConstraints(
                      maxHeight: 200,
                      minHeight: 200,
                      maxWidth: MediaQuery.of(context).size.width,
                      minWidth: MediaQuery.of(context).size.width),
                  padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
                  decoration: BoxDecoration(color: ColorPalette().main),
                  child: Column(
                    children: [
                      subPageTitle(
                          'Hello ${CustomTextCasing.toSentenceCase(text: state is SaveCurrentUser ? state.user.userName : '')} 👋', '',
                          context: context,
                          color: ColorPalette().textWhite),
                      SizedBox(
                        height: 8,
                      ),
                      CustomText(
                        title:
                            'Ask us anything. We will respond to your messages as soon as possible.',
                        textColor: ColorPalette().textWhite,
                      ),
                    ],
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                  ),
                ),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      SvgPicture.asset('assets/images/intercom.svg'),
                      SizedBox(
                        width: 15,
                      ),
                      CustomText(
                        title: 'We run on intercom',
                        isBold: true,
                      ),
                    ],
                  ),
                  width: MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(22),
                ),
              ],
            );
          }),
          Positioned(
              top: 110,
              width: MediaQuery.of(context).size.width,
              child: Container(
                margin: EdgeInsets.all(15),
                padding: EdgeInsets.all(15),
                constraints: BoxConstraints(
                  minHeight: MediaQuery.of(context).size.height / 3,
                  maxHeight: MediaQuery.of(context).size.height / 3,
                ),
                decoration: BoxDecoration(
                    color: ColorPalette().textWhite,
                    borderRadius: BorderRadius.circular(24),
                    boxShadow: [
                      BoxShadow(),
                    ]),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  children: [
                    CustomText(
                      title: 'Start a conversation',
                      isBold: true,
                      textSize: 17,
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    CustomText(
                      title: 'You will get a response in a few minutes',
                    ),
                    SizedBox(
                      height: 70,
                    ),
                    Center(
                      child: AnellohRoundButtonLarge(
                          onPressed: _talkToIntercom,
                          childText: 'New Conversation',
                          hasImage: true,
                          assetImage: SvgPicture.asset(
                            'assets/images/send.svg',
                            fit: BoxFit.contain,
                          )),
                    ),
                  ],
                ),
              )),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
