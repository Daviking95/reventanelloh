
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinCreationScreen extends StatefulWidget {
  @override
  _PinCreationScreenState createState() => _PinCreationScreenState();
}

class _PinCreationScreenState extends State<PinCreationScreen> {

  TextEditingController _textPinOne = TextEditingController();
  TextEditingController _textPinTwo = TextEditingController();
  TextEditingController _textPinThree = TextEditingController();
  TextEditingController _textPinFour = TextEditingController();
  final _formKey = GlobalKey<FormState>();


  FocusNode _focusInputOne = FocusNode();
  FocusNode _focusInputTwo = FocusNode();
  FocusNode _focusInputThree = FocusNode();
  FocusNode _focusInputFour = FocusNode();




  @override
  void initState() {
    super.initState();
  }
  
  _handleChange({@required String name, @required String value}) {
    if (value.isNotEmpty) {
      _moveToNext(name: name);
    } else {
      _moveToPrev(name: name);
    }
  }
  
  _moveToPrev({@required String name}) {
    if(name == 'one') {
      _focusInputOne.unfocus();
    }
    if (name == 'two') {
      _focusInputTwo.previousFocus();
    }
    if (name == 'three') {
      _focusInputThree.previousFocus();
    }

    if (name == 'four') {
      _focusInputFour.previousFocus();
    }
  }

  _moveToNext({@required String name}) {
    if(name == 'one') {
      _focusInputOne.nextFocus();
    }
    if (name == 'two') {
      _focusInputTwo.nextFocus();
    }
    if (name == 'three') {
      _focusInputThree.nextFocus();
    }

    if (name == 'four') {
      _focusInputFour.unfocus();
    }
  }
  _setPin() async{
    try {
      if(_formKey.currentState.validate()) {
        var value = '${_textPinOne.text}${_textPinTwo.text}${_textPinThree.text}${_textPinFour.text}';
        SharedPreferences pref = await SharedPreferences.getInstance();
        pref.setString('__anelloh__user__pin__', value);

        Navigator.of(context).pushNamed(RoutesConstants.dashboardUrl);
      }
    }catch(err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minWidth: MediaQuery.of(context).size.width,
            minHeight: MediaQuery.of(context).size.height
          ),
          child: Center(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CustomText(
                  title: 'Create your pin',
                  textSize: mainSizer(context: context, size: 20),
                  isBold: true,
                ),
                SizedBox(
                  height: 30,
                ),
                Container(
                    constraints: BoxConstraints(
                        minHeight: 100
                    ),
                    margin: EdgeInsets.only(top: 40),
                    alignment: Alignment.center,
                    child: Form(
                      key: _formKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ConstrainedBox(
                            constraints: BoxConstraints(
                                maxWidth: 55,
                                minWidth: 55,
                                maxHeight: 55,
                                minHeight: 55
                            ), child: TextFormField(
                            autofocus: true,
                            obscureText: true,
                            maxLength: 1,
                            style: TextStyle(
                              fontSize: 18,
                            ),
                            controller: _textPinOne,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.phone,
                            onChanged: (v) {
                              _handleChange(name: 'one', value: v);
                            },
                            focusNode: _focusInputOne,
                            decoration: InputDecoration(
                              counterText: '',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)
                                )
                            ),
                          ),),
                          Padding(padding:EdgeInsets.all(10)),
                          ConstrainedBox(
                            constraints: BoxConstraints(
                                maxWidth: 55,
                                minWidth: 55,
                                maxHeight: 55,
                                minHeight: 55
                            ), child: TextFormField(
                            autofocus: true,
                            obscureText: true,
                            maxLength: 1,
                            style: TextStyle(
                              fontSize: 18,
                            ),
                            controller: _textPinTwo,
                            textAlign: TextAlign.center,
                            keyboardType: TextInputType.phone,
                            focusNode: _focusInputTwo,
                            onChanged: (v) {
                              _handleChange(name: 'two', value: v);
                            },
                            decoration: InputDecoration(
                                counterText: '',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)
                                )
                            ),
                          ),),
                          Padding(padding:EdgeInsets.all(10)),
                          ConstrainedBox(
                            constraints: BoxConstraints(
                                maxWidth: 55,
                                minWidth: 55,
                                maxHeight: 55,
                                minHeight: 55
                            ), child: TextFormField(
                            autofocus: true,
                            obscureText: true,
                            maxLength: 1,
                            style: TextStyle(
                              fontSize: 18,
                            ),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            focusNode: _focusInputThree,
                            controller: _textPinThree,
                            onChanged: (v) {
                              _handleChange(name: 'three', value: v);
                            },
                            decoration: InputDecoration(
                                counterText: '',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)
                                )
                            ),
                          ),),
                          Padding(padding:EdgeInsets.all(10)),
                          ConstrainedBox(
                            constraints: BoxConstraints(
                                maxWidth: 55,
                                minWidth: 55,
                                maxHeight: 55,
                                minHeight: 55
                            ), child: TextFormField(
                            autofocus: true,
                            obscureText: true,
                            maxLength: 1,
                            style: TextStyle(
                              fontSize: 18,
                            ),
                            keyboardType: TextInputType.phone,
                            textAlign: TextAlign.center,
                            focusNode: _focusInputFour,
                            controller: _textPinFour,
                            onChanged: (v) {
                              _handleChange(name: 'four', value: v);
                            },
                            decoration: InputDecoration(
                                counterText: '',
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16)
                                )
                            ),
                          ),),
                        ],
                      ),
                    )
                ),
               Padding(
                 padding: EdgeInsets.all(14),
                 child:  AnellohRoundButtonLarge(
                     onPressed: _setPin,
                     childText: 'Set Pin'
                 ),
               ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}