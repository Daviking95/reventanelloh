import 'package:anelloh_mobile/bloc/onboarding/onboarding_bloc.dart';
import 'package:anelloh_mobile/bloc/onboarding/onboarding_event.dart';
import 'package:anelloh_mobile/bloc/onboarding/onboarding_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class OnboardingScreen extends StatefulWidget {
  _OnboardingScreenState createState() => _OnboardingScreenState();
}

class _OnboardingScreenState extends State<OnboardingScreen>
    with TickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 5),
      vsync: this,
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<OnboardingScreenBloc, OnboardingScreenState>(
      listener: (context, state) {
        if (state is SkippedOnboardingScreen) {
          // move to register page;
          Navigator.of(context).pushNamed(RoutesConstants.signUpUrl);
        }
        if (state is MoveToSignUpScreen) {
          Navigator.of(context).pushNamed(RoutesConstants.signUpUrl);
        }
      },
      child: BlocBuilder<OnboardingScreenBloc, OnboardingScreenState>(
        builder: (context, state) {
          if (state is InitialOnboardingScreen) {
            return Scaffold(
              body: SafeArea(
                  maintainBottomViewPadding: true,
                  child: _onboardingScreen(context, _controller,
                      page: state.onboardingPage)),
            );
          }
          if (state is NextOnboardingScreen) {
            return Scaffold(
              body: SafeArea(
                  maintainBottomViewPadding: true,
                  child: _onboardingScreen(context, _controller,
                      page: state.onboardingPage)),
            );
          } else {
            return Scaffold(
              body: SafeArea(
                  maintainBottomViewPadding: true,
                  child: _onboardingScreen(context, _controller,
                      page: BlocProvider.of<OnboardingScreenBloc>(context)
                          .onboardingPage)),
            );
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }
}

Widget _onboardingScreen(context, AnimationController animationController,
    {int page}) {
  _nextScreen() {
    if (page < 2) {
      BlocProvider.of<OnboardingScreenBloc>(context)
          .add(NextOnboardingScreenEvent());
    }
    return false;
  }

  _prevScreen() {
    if (page > 0) {
      BlocProvider.of<OnboardingScreenBloc>(context)
          .add(PrevOnboardingScreenEvent());
    } else {
      return false;
    }
  }

  _skipOnboarding() async {
    BlocProvider.of<OnboardingScreenBloc>(context)
        .add(SkipOnboardingScreenEvent());
  }

  return GestureDetector(
      onHorizontalDragEnd: (details) {
        if (details.primaryVelocity < 0) {
          // Right Swipe
          _nextScreen();
        } else if (details.primaryVelocity > 0) {
          // Left swipe
          _prevScreen();
        }
      },
      child: SingleChildScrollView(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(bottom: 15),
          height: MediaQuery.of(context).size.width >
                  MediaQuery.of(context).size.height
              ? null
              : MediaQuery.of(context).size.height / 1.1,
          margin: EdgeInsets.only(left: 15, right: 15),
          // decoration: BoxDecoration(
          //     border: Border.all()
          // ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            margin: EdgeInsets.only(bottom: 45, top: 75),
                            child: Stack(
                              alignment: AlignmentDirectional.topCenter,
                              children: [
                                SvgPicture.asset(
                                  '${pageScreenData[page]['illustration']}',
                                  // width: 200,
                                  fit: BoxFit.contain,
                                )
                              ],
                            ),
                          ),
                          Container(
                              child: Column(
                            children: [
                              Container(
                                  child: Text(
                                      '${pageScreenData[page]['page_header']}',
                                      style: GoogleFonts.montserrat(
                                          fontSize: 23,
                                          fontWeight: FontWeight.w600,
                                          color: ColorPalette().main)),
                                  margin: EdgeInsets.only(bottom: 18)),
                              Container(
                                width: MediaQuery.of(context).size.width,
                                alignment: Alignment.center,
                                child: Container(
                                  width: 360,
                                  child: Text(
                                      '${pageScreenData[page]['page_description']}',
                                      textAlign: TextAlign.center,
                                      style: GoogleFonts.montserrat(
                                          fontSize: 16,
                                          color: ColorPalette().main)),
                                ),
                              )
                            ],
                          ))
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                alignment: Alignment.center,
                width: MediaQuery.of(context).size.width,
                margin: MediaQuery.of(context).size.width >
                        MediaQuery.of(context).size.height
                    ? EdgeInsets.only(top: 120)
                    : null,
                child: Slider(
                  controller: animationController,
                  page: page,
                ),
              ),
              page != 2
                  ? Container(
                      width: MediaQuery.of(context).size.width,
                      margin: MediaQuery.of(context).size.width >
                              MediaQuery.of(context).size.height
                          ? EdgeInsets.only(top: 190)
                          : null,
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          AnellohFlatButton(
                            childText: 'Skip',
                            onPressed: _skipOnboarding,
                          ),
                          AnellohRoundButtonSmall(
                              onPressed: _nextScreen, childText: 'Next')
                        ],
                      ))
                  : Column(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: MediaQuery.of(context).size.width >
                                  MediaQuery.of(context).size.height
                              ? EdgeInsets.only(top: 190)
                              : null,
                          child: AnellohRoundButtonLarge(
                            onPressed: _skipOnboarding,
                            childText: 'Create Account',
                          ),
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width,
                          margin: MediaQuery.of(context).size.width >
                                  MediaQuery.of(context).size.height
                              ? EdgeInsets.only(top: 190)
                              : null,
                          child: AppOutlineButton(
                            functionToRun: () => Navigator.of(context)
                                .pushNamed(RoutesConstants.loginUrl),
                            textTitle: 'Log In',
                            context: context,
                            borderRadius: 10,
                            width: MediaQuery.of(context).size.width,
                          ),
                        ),
                      ],
                    ),
            ],
          ),
        ),
      ));
}

class Slider extends AnimatedWidget {
  final int page;

  const Slider({Key key, this.page, AnimationController controller})
      : super(key: key, listenable: controller);

  @override
  Widget build(BuildContext context) {
    return Transform.translate(
      offset: Offset(2.0, 1.0),
      child: Container(
        alignment: Alignment.center,
        child: Flex(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          direction: Axis.horizontal,
          children: [
            sliderItem(isActive: this.page == 0 ? true : false),
            sliderItem(isActive: this.page == 1 ? true : false),
            sliderItem(isActive: this.page == 2 ? true : false),
          ],
        ),
      ),
    );
  }
}

Widget sliderItem({bool isActive}) {
  return Container(
    width: 14,
    height: 14,
    margin: EdgeInsets.only(left: 5, right: 5),
    decoration: BoxDecoration(
        color: isActive ? ColorPalette().main : Colors.grey.withOpacity(0.4),
        borderRadius: BorderRadius.circular(100)),
  );
}
