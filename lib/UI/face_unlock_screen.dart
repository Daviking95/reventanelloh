import 'dart:async';
import 'dart:ui' as ui;

import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/helpers/showDialog.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:local_auth/local_auth.dart';

class FaceUnlockScreen extends StatefulWidget {
  _FaceUnlockScreenState createState() => _FaceUnlockScreenState();
}

class _FaceUnlockScreenState extends State<FaceUnlockScreen> {
  LocalAuthentication auth = LocalAuthentication();
  bool _canCheckBiometrics;
  List<BiometricType> _availableBiometrics;
  String _authorized = 'Not Authorized';
  bool _isAuthenticating = false;

  @override
  void initState() {
    super.initState();
    this._checkBiometrics();
  }

  Future<void> _checkBiometrics() async {
    bool canCheckBiometrics;
    try {
      canCheckBiometrics = await auth.canCheckBiometrics;
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    if (canCheckBiometrics) {
      _getAvailableBiometrics();
    }
  }

  Future<void> _getAvailableBiometrics() async {
    List<BiometricType> availableBiometrics;
    try {
      availableBiometrics = await auth.getAvailableBiometrics();
      if (availableBiometrics.contains(BiometricType.face)) {
        _authenticate();
      } else {
        displayAlert(
            context: context,
            title: 'Face Biometric',
            content: 'We could not detect any face biometric on your device');
      }
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
  }

  Future<void> _useFingerPrint() async {
    try {
      List<BiometricType> availableBiometrics =
          await auth.getAvailableBiometrics();
      if (availableBiometrics.contains(BiometricType.fingerprint)) {
        _authenticate();
      } else {
        displayAlert(
            context: context,
            title: 'Finger Print Biometric',
            content:
                'We could not detect any finger print biometric on your device');
      }
    } on PlatformException catch (e) {
      print(e);
    } catch (err) {
      print(err);
    }
  }

  Future<void> _authenticate() async {
    bool authenticated = false;
    try {
      setState(() {
        _isAuthenticating = true;
        _authorized = 'Authenticating';
      });
      authenticated = await auth.authenticateWithBiometrics(
          localizedReason: 'Scan to log in',
          useErrorDialogs: true,
          stickyAuth: true);
      setState(() {
        _isAuthenticating = false;
        _authorized = 'Authenticating';
      });
    } on PlatformException catch (e) {
      print(e);
    }
    if (!mounted) return;
    if (authenticated) {
      if (await getFromPref('current_route') != null) {
        Navigator.of(context).popAndPushNamed('/dashboard');
      } else {
        Navigator.of(context).popAndPushNamed('/dashboard');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PinUnlockScreenBloc, PinUnlockScreenState>(
      listener: (context, state) {},
      child: BlocBuilder<PinUnlockScreenBloc, PinUnlockScreenState>(
        builder: (context, blocState) {
          return Scaffold(
            body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Container(
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  child: ClipRRect(
                                    child: Image.asset(
                                      ImageConstants.anellohLogo,
                                      fit: BoxFit.contain,
                                      width: 250,
                                      height: 250,
                                    ),
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                ),
                                Container(
                                  child: Text(
                                    'Log In',
                                    style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      color: ColorPalette().main,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 30,
                                ),
                                Container(
                                  child: ClipRRect(
                                    child: SvgPicture.asset(
                                        'assets/images/face_id_icon.svg'),
                                  ),
                                  width: MediaQuery.of(context).size.width,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                      width: MediaQuery.of(context).size.width,
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.of(context).pushNamed('/pin_unlock');
                      },
                      child: CustomText(
                        title: 'Use Pin instead?',
                      ),
                    ),
                    SizedBox(
                      height: 40,
                    ),
                    InkWell(
                      onTap: () {
                        _useFingerPrint();
                      },
                      child: ShaderMask(
                          blendMode: BlendMode.srcIn,
                          shaderCallback: (Rect bounds) {
                            return ui.Gradient.linear(
                              Offset(0.0, 0.0),
                              Offset(200.0, 70.0),
                              [Color(0xff4181F9), Color(0xff0047CC)],
                            );
                          },
                          child: Icon(
                            Icons.fingerprint,
                            size: 90,
                          )),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
