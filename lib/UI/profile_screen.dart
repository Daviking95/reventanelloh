import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/profile/profile_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_event.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_state.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_state.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/selet_profile_image_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/helpers/showDialog.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:photo_manager/photo_manager.dart';

class ProfileScreen extends StatefulWidget {
  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _activateUser();
  }

  _activateUser() async {
    try {
      var resultEntity;
      if (await readFromPref('__anelloh_current_user__')) {
        resultEntity =
            jsonDecode(await getFromPref('__anelloh_current_user__'));
      }

      String customerId = resultEntity['customerId'].toString();
      BlocProvider.of<UserBloc>(context)
          .add(GetCurrentCustomerByIdEvent(customerId: customerId));
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelectProfileImageScreenBloc,
        SelectProfileImageScreenState>(
      listener: (context, state) {
        if (state is UploadingUserImage) {
          setState(() {
            _isLoading = true;
          });
        }
        if (state is ImageUploadFailed) {
          showToast(
              toastMsg: 'Image upload failed',
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
        }
        if (state is ImageUploadSuccess) {
          showToast(
              toastMsg: 'Image upload successful',
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          Navigator.of(context).popAndPushNamed('/dashboard');
        }
      },
      child: BlocBuilder<SelectProfileImageScreenBloc,
          SelectProfileImageScreenState>(
        builder: (context, buildState) =>
            BlocListener<ProfileScreenBloc, ProfileScreenState>(
          listener: (context, state) {
            if (state is MoveToUpdatePasswordScreen) {
              Navigator.of(context).pushNamed('/update_password');
            }
            if (state is MoveToChangePinScreen) {
              Navigator.of(context).pushNamed('/change_pin');
            }
            if (state is MoveToEditProfileScreen) {
              Navigator.of(context).pushNamed('/edit_profile');
            }
            if (state is MoveToSelectProfileImageScreen) {
              Navigator.of(context).pushReplacementNamed(
                  '/upload_profile_image',
                  arguments: {'imageList': state.imageList});
            }
          },
          child: BlocBuilder<ProfileScreenBloc, ProfileScreenState>(
            builder: (context, state) {
              return BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                return LoadingOverlay(
                  isLoading: _isLoading
                      ? true
                      : state is RequestingLogout
                          ? true
                          : false,
                  child: StarterWidget(
                    hasBottomNav: false,
                    hasBackButton: true,
                    hasRefreshOption: false,
                    isExitApp: true,
                    isBgAllowed: false,
                    bgColor: ColorPalette().main,
                    backBtnColor: ColorPalette().textWhite,
                    bottomPageContent: _bottomPageContent(context: context),
                  ),
                );
              });
            },
          ),
        ),
      ),
    );
  }
}

Widget _bottomPageContent({@required BuildContext context}) {
  _logout() {
    BlocProvider.of<UserBloc>(context)
        .add(LogOutUserEvent(user: AppConstants.userData));
  }

  return BlocListener<UserBloc, UserState>(
    listener: (context, state) {
      if (state is LogOutSuccessful) {
        Navigator.of(context).popAndPushNamed('/login');
      }
    },
    child: SingleChildScrollView(
      padding: EdgeInsets.only(bottom: isPortrait(context) ? 0 : 250),
      child: Container(
          width: Get.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _profileHeader(context: context),
              Container(
                margin: EdgeInsets.only(top: 25),
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    // Container(
                    //   child: CustomText(
                    //     title: 'Refer a friend',
                    //     textColor: ColorPalette().textBlack,
                    //     textSize: 19,
                    //     fontWeight: FontWeight.w600,
                    //     hasFontWeight: true,
                    //   ),
                    //   margin: EdgeInsets.only(bottom: 10),
                    // ),

                    // _customProfileNavButton(
                    //     context: context,
                    //     onPressed: () {},
                    //     title: 'Share Link',
                    //     icon: Icon(Icons.vpn_key_outlined, size: 20.0)),

                    Container(
                      child: CustomText(
                        title: 'Security Settings',
                        textColor: ColorPalette().textBlack,
                        textSize: 19,
                        fontWeight: FontWeight.w600,
                        hasFontWeight: true,
                      ),
                      margin: EdgeInsets.only(bottom: 10),
                    ),
                    _customProfileNavButton(
                        onPressed: () {
                          Navigator.of(context).pushNamed('/update_password');
                        },
                        title: 'Password',
                        icon: Icon(MdiIcons.lockOutline,
                            size: 20.0, color: ColorPalette().main),
                        context: context),
                    // _customProfileNavButton(
                    //     onPressed: () {
                    //       Navigator.of(context).pushNamed('/pin_face_id');
                    //     },
                    //     title: 'Pin and Face ID',
                    //     icon: Icon(Icons.vpn_key_outlined,
                    //         size: 20.0, color: ColorPalette().main),
                    //     context: context),

                    // _customProfileNavButton(
                    //     onPressed: _logout,
                    //     title: 'Log out',
                    //     icon: Icon(Icons.vpn_key_outlined,
                    //         size: 20.0, color: ColorPalette().main),
                    //     context: context),
                  ],
                ),
              ),
            ],
          )),
    ),
  );
}

Widget _customProfileNavButton({
  @required BuildContext context,
  @required Function onPressed,
  @required String title,
  @required Icon icon,
}) {
  return GestureDetector(
    onTap: onPressed,
    child: Container(
      margin: EdgeInsets.only(top: 12, bottom: 12),
      decoration: BoxDecoration(
          color: ColorPalette().textWhite,
          boxShadow: [
            BoxShadow(
                offset: Offset(1, 2),
                blurRadius: 10.4,
                spreadRadius: 6.1,
                color: Colors.grey.withOpacity(0.1))
          ],
          borderRadius: BorderRadius.circular(10)),
      child: Ink(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: ColorPalette().textWhite,
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(1, 2),
                        blurRadius: 10.4,
                        spreadRadius: 6.1,
                        color: Colors.grey.withOpacity(0.1))
                  ]),
              padding: EdgeInsets.all(10),
              child: icon,
            ),
            Container(
              child: CustomText(
                title: title,
                textSize: 17,
              ),
            ),
            Container(
              child: Icon(Icons.arrow_forward_ios_outlined,
                  color: ColorPalette().main, size: 14.0),
            )
          ],
        ),
      ),
    ),
  );
}

Widget _profileHeader({@required BuildContext context}) {
  _moveToEditScreen() {
    BlocProvider.of<ProfileScreenBloc>(context)
        .add(MoveToEditProfileScreenEvent());
  }

  return Container(
    width: MediaQuery.of(context).size.width,
    padding: EdgeInsets.only(left: 20, right: 20, bottom: 10, top: 10),
    decoration: BoxDecoration(
        color: ColorPalette().main,
        borderRadius: BorderRadius.only(
            bottomLeft: Radius.circular(35), bottomRight: Radius.circular(35))),
    child: Column(
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          child: subPageTitle('Profile', 'View your profile information',
              color: ColorPalette().textWhite, context: context),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.only(bottom: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                margin: EdgeInsets.only(top: 25),
                alignment: Alignment.center,
                child: BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                    return _buildUserProfileImage(
                        state: state, context: context);
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 10),
                child: BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                    return _buildUserProfileData(
                        state: state, context: context);
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 13),
                alignment: Alignment.center,
                child: AnellohRoundButtonSmall(
                    onPressed: _moveToEditScreen,
                    childText: 'Edit Profile',
                    bgColor: ColorPalette().textWhite,
                    textColor: ColorPalette().main),
              )
            ],
          ),
        ),
      ],
    ),
  );
}

Widget _buildUserProfileData(
    {@required UserState state, @required BuildContext context}) {
  if (state is InitialUserState) {
    User _user = state.user;
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CustomText(
          title: '${_user.fullNames}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 20,
          hasFontWeight: true,
          fontWeight: FontWeight.w600,
        ),
        CustomText(
          title: '${_user.emailAddress}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 14,
        ),
        CustomText(
          title: '${_user.phoneNumber}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 14,
        ),
      ],
    );
  } else if (state is SaveCurrentUser) {
    User _user = state.user;
    return Column(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        CustomText(
          title: '${_user.fullNames}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 20,
          hasFontWeight: true,
          fontWeight: FontWeight.w600,
        ),
        CustomText(
          title: '@${_user.userName}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 14,
        ),
        CustomText(
          title: '${_user.emailAddress}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 14,
        ),
        CustomText(
          title: '${_user.phoneNumber}',
          isCenter: true,
          textColor: ColorPalette().textWhite,
          textSize: 14,
        ),
      ],
    );
  } else {
    return Center(
      child: Container(
        width: Get.width / 3,
        child: PlaceholderLines(
          count: 3,
          animate: true,
          color: Colors.grey.withOpacity(0.003),
          lineHeight: 5,
        ),
      ),
    );
    // return CustomText(
    //   title: 'Loading...',
    //   textSize: 10,
    //   textColor: ColorPalette().textWhite,
    // );
  }
}

Widget _buildUserProfileImage(
    {@required UserState state, @required BuildContext context}) {
  String selectedAssetName = Platform.isAndroid ? 'Recent' : 'Recents';

  File _fileImage2;
  final TextEditingController _profileImageController =
      new TextEditingController();

  _handleImageUpload() async {
    var result = await PhotoManager.requestPermission();
    if (result) {
      List<AssetPathEntity> assetPath =
          await PhotoManager.getAssetPathList(type: RequestType.image);
      if (assetPath.length > 0) {
        AssetPathEntity assetPathEntity = assetPath
            .where(
                (AssetPathEntity element) => element.name == selectedAssetName)
            .single;
        if (assetPathEntity != null) {
          List<AssetEntity> imageList =
              await assetPathEntity.getAssetListRange(start: 0, end: 500);
          BlocProvider.of<ProfileScreenBloc>(context)
              .add(MoveToSelectProfileImageScreenEvent(imageList: imageList));
        }
      }
    }
  }

  if (state is SaveCurrentUser) {
    User _user = state.user;
    return Stack(
      alignment: AlignmentDirectional.bottomEnd,
      children: [
        ClipRRect(
          child: _user.profileImage != null && _user.profileImage.length > 0
              ? Image.network('${_user.profileImage}',
                  fit: BoxFit.cover,
                  width: 110,
                  height: 110, errorBuilder: (context, _, __) {
                  return Container(
                      width: 110,
                      height: 110,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(100)),
                      child: Center(
                        child: Platform.isIOS
                            ? CupertinoActivityIndicator()
                            : CircularProgressIndicator(),
                      ));
                })
              : Image(
                  width: 110,
                  height: 110,
                  image: AssetImage(ImageConstants.profilePicture),
                  fit: BoxFit.cover,
                ),
          borderRadius: BorderRadius.circular(100),
        ),
        Positioned(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
            decoration: BoxDecoration(
                color: Colors.white.withOpacity(.9),
                borderRadius: BorderRadius.circular(100),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey.withOpacity(0.6),
                      blurRadius: 1.4,
                      offset: Offset(1, 1),
                      spreadRadius: 1.2)
                ]),
            child: InkWell(
              child: Icon(
                MdiIcons.pencilOutline,
                color: ColorPalette().main,
                size: 20,
              ),
              onTap: () => displayImagePickerDialog(context, () async {
                FocusScope.of(context).requestFocus(FocusNode());
                Navigator.pop(context);
                String _fileName;
                PickedFile image = await ImagePicker().getImage(
                  source: ImageSource.camera,
                  imageQuality: 10,
                );
                if (image != null) {
                  _fileImage2 = File(image.path);

                  String base64Image =
                      base64Encode(_fileImage2.readAsBytesSync());
                  // _filePath = fileImage.path;
                  _fileName = _fileImage2.path.split("/").last;

                  _profileImageController.text = base64Image;

                  print(_profileImageController.text);

                  User _user = BlocProvider.of<UserBloc>(context).state.user;
                  BlocProvider.of<SelectProfileImageScreenBloc>(context).add(
                      UploadUserImageEvent(
                          imageFile: _fileImage2,
                          customerId: _user.customerId));
                } else {
                  print('No image selected.');
                }
              }, () async {
                FocusScope.of(context).requestFocus(FocusNode());
                Navigator.pop(context);
                String _fileName;
                PickedFile image = await ImagePicker().getImage(
                  source: ImageSource.gallery,
                  imageQuality: 5,
                );
                if (image != null) {
                  _fileImage2 = File(image.path);

                  String base64Image =
                      base64Encode(_fileImage2.readAsBytesSync());
                  // _filePath = fileImage.path;
                  _fileName = _fileImage2.path.split("/").last;

                  _profileImageController.text = base64Image;

                  print(_profileImageController.text);

                  User _user = BlocProvider.of<UserBloc>(context).state.user;
                  BlocProvider.of<SelectProfileImageScreenBloc>(context).add(
                      UploadUserImageEvent(
                          imageFile: _fileImage2,
                          customerId: _user.customerId));
                } else {
                  print('No image selected.');
                }
              }),
            ),
          ),
        ),
      ],
    );
  } else {
    return ClipRRect(
      child: Image(
        width: 110,
        height: 110,
        image: AssetImage(ImageConstants.profilePicture),
        fit: BoxFit.cover,
      ),
      borderRadius: BorderRadius.circular(100),
    );
    // return Container(
    //   child: Text('loading...'),
    // );
  }
}
