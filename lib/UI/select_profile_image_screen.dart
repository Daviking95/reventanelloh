import 'dart:io';

import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_state.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/selet_profile_image_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:photo_manager/photo_manager.dart';

class SelectProfileImageScreen extends StatefulWidget {
  final List<AssetEntity> imageList;
  SelectProfileImageScreen({Key key, @required this.imageList})
      : super(key: key);
  _SelectProfileImageScreen createState() => _SelectProfileImageScreen();
}

class _SelectProfileImageScreen extends State<SelectProfileImageScreen> {
  List<File> imageFiles = [];

  @override
  void initState() {
    super.initState();
    _handleGetAssetFiles();
  }

  _handleGetAssetFiles() async {
    try {
      List<File> tempFiles = [];
      widget.imageList.forEach((AssetEntity entity) async {
        File file = await entity.file;
        if (tempFiles.length == widget.imageList.length - 1) {
          tempFiles.add(file);
          setState(() {
            imageFiles = tempFiles;
          });
        } else {
          tempFiles.add(file);
        }
      });
    } catch (err) {
      print(err);
      return;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelectProfileImageScreenBloc,
        SelectProfileImageScreenState>(
      listener: (context, state) {
        if (state is ImageUploadFailed) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                  title: 'Error',
                  body: 'Image upload failed',
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  )),
              context: context);
        }
        if (state is ImageUploadSuccess) {
          showToast(
              toastMsg: 'Image upload successful',
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          Navigator.of(context).popAndPushNamed('/profile');
        }
      },
      child: BlocBuilder<SelectProfileImageScreenBloc,
          SelectProfileImageScreenState>(
        builder: (context, buildState) {
          return LoadingOverlay(
              isLoading: buildState is UploadingUserImage ? true : false,
              progressIndicator: ProgressWidget(),
              color: ColorPalette().textBlack,
              opacity: 0.4,
              child: StarterWidget(
                hasBottomNav: false,
                hasBackButton: true,
                hasRefreshOption: false,
                hasScrollChild: true,
                isExitApp: true,
                bgColor: ColorPalette().main,
                backBtnColor: ColorPalette().textWhite,
                bottomPageContent: _bottomPageContent(
                  context: context,
                  isProcessingImage:
                      buildState is UploadingUserImage ? true : false,
                  imageFiles: imageFiles,
                ),
              ));
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

Widget _bottomPageContent({
  @required BuildContext context,
  @required List<File> imageFiles,
  @required bool isProcessingImage,
}) {
  _handleImageUpload({@required File imageFile}) async {
    User _user = BlocProvider.of<UserBloc>(context).state.user;
    BlocProvider.of<SelectProfileImageScreenBloc>(context).add(
        UploadUserImageEvent(
            imageFile: imageFile, customerId: _user.customerId));
  }

  return ConstrainedBox(
    constraints: BoxConstraints(
      maxHeight: MediaQuery.of(context).size.height / 1.2,
    ),
    child: GridView.builder(
        padding: EdgeInsets.only(top: 8, right: 8, bottom: 8),
        itemCount: imageFiles.length <= 0 ? 1 : imageFiles.length + 1,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount:
              MediaQuery.of(context).orientation == Orientation.portrait
                  ? 3
                  : 4,
        ),
        itemBuilder: (context, int index) {
          if (index == 0) {
            return GestureDetector(
              onTap: () {},
              child: Container(
                child: Center(
                  child: Icon(
                    Icons.camera,
                    color: ColorPalette().main,
                    size: 32,
                  ),
                ),
                padding: EdgeInsets.all(3),
                margin: EdgeInsets.only(left: 3),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Colors.black),
              ),
            );
          } else {
            return Container(
              padding: EdgeInsets.all(3),
              margin: EdgeInsets.all(3),
              child: Image.file(
                imageFiles[index - 1],
                frameBuilder: (BuildContext context, _, __, ___) {
                  return GestureDetector(
                      onTap: () {
                        if (isProcessingImage == false) {
                          File image = imageFiles[index - 1];
                          _handleImageUpload(imageFile: image);
                        }
                        // widget.setProfileImage(image);
                        // Navigator.of(context).pop();
                      },
                      child: Image.file(
                        imageFiles[index - 1],
                        width: 200,
                        height: 200,
                        fit: BoxFit.cover,
                      ));
                },
                errorBuilder: (BuildContext context, _, __) {
                  return Container(
                    child: Center(
                      child: Icon(
                        MdiIcons.alertOutline,
                        color: ColorPalette().dangerRed,
                      ),
                    ),
                    padding: EdgeInsets.all(3),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Colors.grey.withOpacity(0.3)),
                  );
                },
              ),
            );
          }
        }),
  );
}
