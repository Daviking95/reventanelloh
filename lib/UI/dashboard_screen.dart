import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/dashboard/dashboard_bloc.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_bloc.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_events.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_state.dart';
import 'package:anelloh_mobile/bloc/router/router_bloc.dart';
import 'package:anelloh_mobile/bloc/router/router_event.dart';
import 'package:anelloh_mobile/bloc/router/router_state.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_event.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/classes/order_listing.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/order_status_funcs.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/helpers/text_casing.dart';
import 'package:anelloh_mobile/styles.dart';
import 'package:anelloh_mobile/widgets/bottom_sheet_widgets.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/header_card.dart';
import 'package:anelloh_mobile/widgets/market_rate_chart.dart';
import 'package:anelloh_mobile/widgets/notify_prof_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/top_listings_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_countdown_timer/flutter_countdown_timer.dart';
import 'package:flutter_countdown_timer/index.dart';
import 'package:flutter_placeholder_textlines/flutter_placeholder_textlines.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

class MainDashboardScreen extends StatefulWidget {
  _MainDashboardScreenState createState() => _MainDashboardScreenState();
}

class _MainDashboardScreenState extends State<MainDashboardScreen> {
  Timer _timer;

  @override
  void initState() {
    super.initState();
    _activateUser();
    _getAllOrders();
    _fetchMarketRates();
    _checkUserPendingOrderState();
    _setupAutoRefresh();

    BlocProvider.of<UserBloc>(context).add(GetAllTransactionLimitEvent());

    // log('AppConstants.userLatestOrderStatus ${AppConstants.userLatestOrderStatus}');
  }

  _setupAutoRefresh() {
    _timer = Timer.periodic(Duration(seconds: 60), (timer) {
      _autoGetAllOrder();
      _autoCheckUserPendingOrder();
    });
  }

  _refresh() {
    _activateUser();
    _getAllOrders();
    _fetchMarketRates();
    _checkUserPendingOrderState();
  }

  _autoGetAllOrder() async {
    var resultEntity;
    if (await readFromPref('__anelloh_current_user__')) {
      resultEntity = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    String customerId = resultEntity['customerId'].toString();
    BlocProvider.of<DashboardBloc>(context)
        .add(AutoFetchAllOrdersEvent(customerId: customerId));
  }

  _autoCheckUserPendingOrder() async {
    var user;
    if (await readFromPref('__anelloh_current_user__')) {
      user = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    BlocProvider.of<MainUserOrderBloc>(context)
        .add(AutoCheckUserPendingOrderEvent(customerId: user['customerId']));
  }

  _getAllOrders() async {
    var resultEntity;
    if (await readFromPref('__anelloh_current_user__')) {
      resultEntity = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    String customerId = resultEntity['customerId'].toString();
    BlocProvider.of<DashboardBloc>(context)
        .add(FetchAllOrdersEvent(customerId: customerId));
  }

  _fetchMarketRates() {
    BlocProvider.of<MarketRateBloc>(context).add(FetchMarketRateEvent());
  }

  _moveToAllListingScreen() {
    BlocProvider.of<RouterBloc>(context)
        .add(RouterMoveToAllListingScreenEvent());
  }

  _checkUserPendingOrderState() async {
    var user;
    if (await readFromPref('__anelloh_current_user__')) {
      user = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    BlocProvider.of<MainUserOrderBloc>(context)
        .add(CheckUserPendingOrderEvent(customerId: user['customerId']));
  }

  void _updatePendingOrderState(String orderNo) async {

    log('AutoCalling _updatePendingOrderState');
    var user;
    if (await readFromPref('__anelloh_current_user__')) {
      user = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    BlocProvider.of<MainUserOrderBloc>(context).add(UpdateUserPendingOrderEvent(
        customerId: user['customerId'], orderNo: orderNo));
  }

  _activateUser() async {
    try {
      var resultEntity;
      if (await readFromPref('__anelloh_current_user__')) {
        resultEntity =
            jsonDecode(await getFromPref('__anelloh_current_user__'));
      }

      String customerId = resultEntity['customerId'].toString();

      log('What is customerId $customerId');

      BlocProvider.of<UserBloc>(context)
          .add(GetCurrentCustomerByIdEvent(customerId: customerId));
    } catch (err) {
      print(err);
    }
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<RouterBloc, RouterState>(
          listener: (context, state) {
            if (state is RouterMoveToCreateOrderScreen) {
              Navigator.of(context).pushNamed('/create_order');

              // Navigator.pushNamed(context, '/edit_order',
              //     arguments: {'editOrderObj': state.editOrderObj});
            }
            if (state is RouterMoveToAllListingScreen) {
              Navigator.of(context).pushNamed('/listings');
            }
            if (state is RouterMoveToPaymentSummaryScreen) {
              EditOrderObj _editOrderObj = EditOrderObj(
                  customerId: state.editOrderObj.customerId,
                  orderId: state.editOrderObj.orderId,
                  myAmount: state.editOrderObj.myAmount,
                  myCurrency: state.editOrderObj.myCurrency,
                  rate: state.editOrderObj.rate,
                  convertedAmount: state.editOrderObj.convertedAmount,
                  convertedCurrency: state.editOrderObj.convertedCurrency,
                  myPaymentChannelId: state.editOrderObj.myPaymentChannelId,
                  myBankName: state.editOrderObj.myBankName,
                  myAccountNumber: state.editOrderObj.myAccountNumber,
                  bankRouteNo: state.editOrderObj.bankRouteNo,
                  transactionFee: state.editOrderObj.transactionFee,
                  paymentStatus: 1,
                  loggedInDevice: state.editOrderObj.loggedInDevice,
                  receipt: "",
                  paymentReference: state.editOrderObj.paymentReference);

              Navigator.of(context).pushNamed('/payment_summary', arguments: {
                'matchFoundObj': state.matchFoundObj,
                "editOrderObj": _editOrderObj,
                'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                  customerId: state.matchFoundObj.customerId,
                  orderNo: state.matchFoundObj.orderNo,
                  myAmount: state.matchFoundObj.myAmount,
                  totalAmount: state.matchFoundObj.totalAmount,
                  myCurrency: state.matchFoundObj.myCurrency,
                  rate: state.matchFoundObj.exchangeRate,
                  convertedCurrency: state.matchFoundObj.convertedCurrency,
                  convertedAmount: state.matchFoundObj.convertedAmount,
                  transactionFee: state.matchFoundObj.processingFee,
                )
              });
            }
          },
        ),
      ],
      child: BlocBuilder<DashboardBloc, DashboardScreenState>(
        builder: (context, state) {
          return StarterWidget(
              isBgAllowed: false,
              hasNavDrawer: false,
              hasTopWidget: true,
              isExitApp: true,
              hasRefreshOption: true,
              onRefreshFunction: _refresh,
              hasBottomNav: true,
              bottomNavIndex: 0,
              topPageContent: _topPageContent(context: context),
              bottomPageContent: _bottomPageContent(context: context));
        },
      ),
    );
  }

  Widget _topPageContent({@required BuildContext context}) {
    return NotificationProfileWidget();
  }

  Widget _bottomPageContent({@required BuildContext context}) {
    return SingleChildScrollView(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: ConstrainedBox(
        constraints: BoxConstraints(
          minWidth: Get.width,
          minHeight: MediaQuery.of(context).size.height,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            _greetingRow(),
            // Expanded(),
            SizedBox(
              height: 15,
            ),
            _createOrderRow(context: context),
            SizedBox(
              height: 15,
            ),
            _topListingsRow(),
            SizedBox(
              height: 10,
            ),
            BlocBuilder<MarketRateBloc, MarketRateState>(
                builder: (context, state) {
                  if (state is SaveMarketRateData) {
                    return _marketRatesRow(marketRates: state.marketRates);
                  } else {
                    return _buildAnimatedMarketRates();
                  }
                }),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  _buildAnimatedMarketRates() {
    return Container(
      decoration: BoxDecoration(color: Colors.white),
      height: 200,
      width: Get.width,
      margin: EdgeInsets.only(top: 20),
      child: PlaceholderLines(
        count: 5,
        animate: true,
        color: Colors.grey.withOpacity(0.003),
        lineHeight: 7,
      ),
    );
  }

  _greetingRow() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          AppConstants?.userData?.userName == null
              ? Container()
              : CustomText(
            title:
            "Hello, ${CustomTextCasing.toSentenceCase(text: AppConstants?.userData?.userName ?? "")}!",
            hasFontWeight: true,
            fontWeight: FontWeight.w800,
            textSize: 18,
          ),
          SizedBox(
            width: 10,
          ),
          CustomText(
            title: AppConstants.howCanWeHelp,
            hasFontWeight: true,
            textColor: appPrimaryColor,
            fontWeight: FontWeight.w400,
            textSize: 15,
          ),
          Padding(
            padding: EdgeInsets.all(7),
          ),
        ],
      ),
    );
  }

  _buildAnimatedPendingOrder() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
      decoration: BoxDecoration(
          color: Color(0xFFF9968B), borderRadius: BorderRadius.circular(17)),
      child: PlaceholderLines(
        count: 4,
        animate: true,
        color: Colors.grey.withOpacity(0.003),
        lineHeight: 10,
      ),
    );
  }

  _createOrderRow({@required BuildContext context}) {
    return BlocBuilder<MainUserOrderBloc, MainUserOrderState>(
      builder: (context, state) {
        log('MainUserOrderBloc State $state');

        if (state is CheckingUserPendingOrderState ||
            state is UpdatingUserPendingOrderState) {
          return _buildAnimatedPendingOrder();
        } else {
          return HeaderCard(
            bodyWidget: _determineOrderBodyWidget(state: state),
            headerWidget: _dertermineOrderHeaderText(state: state),
            subChild: _determineHeaderCardSubAction(state: state),
            onTap: () {
              // BlocProvider.of<SessionTimerBloc>(context)
              //     .add(SessionTimerRestartEvent());
            },
          );
        }
      },
    );
  }

  Widget _determineHeaderCardSubAction({@required MainUserOrderState state}) {
    if (state is CheckingUserPendingOrderSuccessful) {
      if (state.pendingOrders.length > 0) {
        if (state?.pendingOrders[0]?.order?.orderStatus != null &&
            state?.pendingOrders[0]?.order?.orderStatus?.toString() ==
                '7' &&
            state?.pendingOrders[0]?.matchedOrder == null ||
            state?.pendingOrders[0]?.matchedOrder?.matchedOrderStatus
                ?.toString() ==
                '1' &&
                state?.pendingOrders[0]?.order?.orderStatus?.toString() == '7')
          return SvgPicture.asset('assets/images/waiting.svg');

        if (state.pendingOrders[0].order == null) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: SizedBox(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  AnellohRoundButtonSmall(
                      childText: 'Create Order',
                      onPressed: () {
                        BlocProvider.of<RouterBloc>(context)
                            .add(RouterMoveToCreateOrderScreenEvent());
                      }),
                ],
              ),
            ],
          );
        }

        if (state?.pendingOrders[0].isMatched && state.pendingOrders[0]?.matchedOrder?.matchedOrderStatus
            .toString() ==
            '1' &&
            state.pendingOrders[0].order.orderStatus.toString() == '1') {
          CountdownTimerController controller = CountdownTimerController(
              endTime: DateTime.parse(
                  state.pendingOrders[0].matchedOrder.matchedDate)
                  .millisecondsSinceEpoch +
                  // 1000 * 60 * 60 * 1,
                  1000 * 60 * 60 * 12,
              onEnd: () {
                // todo : Updating status
                this._updatePendingOrderState(
                    state.pendingOrders[0].order.orderNo);
              });
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SvgPicture.asset('assets/images/timer.svg'),
                    SizedBox(
                      width: 10,
                    ),
                    CountdownTimer(
                      controller: controller,
                      endWidget: CustomText(
                        title: 'Expired',
                        textSize: 7,
                      ),
                      onEnd: () {
                        // this._updatePendingOrderState(
                        //     state.pendingOrders[0].order.orderNo);
                      },
                      endTime: DateTime.parse(state
                          .pendingOrders[0].matchedOrder.matchedDate)
                          .millisecondsSinceEpoch +
                          // 1000 * 60 * 60 * 1,
                          1000 * 60 * 60 * 12,
                    ),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.max,
                children: [
                  AnellohRoundButtonSmall(
                      childText: 'Pay Now',
                      onPressed: () async {

                        OrderListing2 order = state.pendingOrders[0];

                        var resultEntity;
                        var deviceId;

                        if (await readFromPref('__anelloh_current_user__')) {
                          resultEntity = jsonDecode(
                              await getFromPref('__anelloh_current_user__'));
                        }

                        if (await readFromPref('__device__token')) {
                          deviceId = await getFromPref('__device__token') ??
                              await getDeviceToken();
                        } else {
                          deviceId = await getDeviceToken();
                        }

                        MatchFoundObj matchFoundObj = MatchFoundObj(
                            orderNo: state.pendingOrders[0].order.orderNo,
                            entityId:
                            state.pendingOrders[0].order.id.toString(),
                            customerId: state.pendingOrders[0].order.customerId ?? AppConstants.userData.customerId,
                            rates: [],
                            userEmail: '',
                            // add to api
                            orderOwnerId:
                            state.pendingOrders[0].order.customerId,
                            myCurrency: state.pendingOrders[0].order.myCurrency,
                            myAmount: state.pendingOrders[0].order.myAmount
                                .toString(),
                            orderStatus:
                            state.pendingOrders[0].order.orderStatus,
                            userName: 'test',
                            totalAmount: state
                                .pendingOrders[0].order.totalAmount
                                .toString(),
                            isRateAscending: true,
                            processingFee: state
                                .pendingOrders[0].order.transactionFee
                                .toString(),
                            ascDecValue: state
                                .pendingOrders[0].order.convertedAmount
                                .toDouble(),
                            subTotal: state.pendingOrders[0].order.totalAmount
                                .toString(),
                            exchangeRate:
                            state.pendingOrders[0].order.rate.toString(),
                            convertedAmount: state
                                .pendingOrders[0].order.convertedAmount
                                .toString(),
                            convertedCurrency:
                            state.pendingOrders[0].order.convertedCurrency);

                        EditOrderObj editOrderObj = EditOrderObj(
                          orderId: order.order.id,
                          myAmount: order.order.myAmount,
                          myCurrency: order.order.myCurrency,
                          rate: order.order.rate,
                          convertedAmount: order.order.convertedAmount,
                          convertedCurrency: order.order.convertedCurrency,
                          customerId: resultEntity['customerId'].toString(),
                          myAccountNumber:
                          order.order.myAccountNumber.toString(),
                          myBankName: order.order.myBankName.toString(),
                          bankRouteNo: order.order.bankRouteNo.toString(),
                          myPaymentChannelId: order.order.myPaymentChannelId,
                          transactionFee: order.order.transactionFee,
                          paymentStatus: order.order.paymentStatus,
                          loggedInDevice: deviceId,
                        );

                        // Navigator.pushNamed(context, '/edit_order',
                        //     arguments: {'editOrderObj': editOrderObj});

                        BlocProvider.of<RouterBloc>(context).add(
                            RouterMoveToPaymentSummaryScreenEvent(
                                matchFoundObj: matchFoundObj,
                                editOrderObj: editOrderObj));
                      })
                ],
              ),
            ],
          );
        } else if (state.pendingOrders[0].order.orderStatus.toString() == '1' ||
            state.pendingOrders[0].order.orderStatus.toString() == '5') {
          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: SizedBox(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  AnellohRoundButtonSmall(
                      childText: 'Edit Order',
                      onPressed: () async {
                        var resultEntity;
                        if (await readFromPref('__anelloh_current_user__')) {
                          resultEntity = jsonDecode(
                              await getFromPref('__anelloh_current_user__'));
                        }

                        var deviceId;
                        if (await readFromPref('__device__token')) {
                          deviceId = await getFromPref('__device__token') ??
                              await getDeviceToken();
                        } else {
                          deviceId = await getDeviceToken();
                        }
                        OrderListing2 order = state.pendingOrders[0];
                        EditOrderObj editOrderObj = EditOrderObj(
                          orderId: order.order.id.toString(),
                          myAmount: order.order.myAmount.toString(),
                          myCurrency: order.order.myCurrency.toString(),
                          rate: order.order.rate.toString(),
                          convertedAmount:
                          order.order.convertedAmount.toString(),
                          convertedCurrency:
                          order.order.convertedCurrency.toString(),
                          customerId: resultEntity['customerId'].toString(),
                          myAccountNumber:
                          order.order.myAccountNumber.toString(),
                          myBankName: order.order.myBankName.toString(),
                          bankRouteNo: order.order.bankRouteNo.toString(),
                          myPaymentChannelId:
                          order.order.myPaymentChannelId.toString(),
                          transactionFee: order.order.transactionFee.toString(),
                          paymentStatus: order.order.paymentStatus.toString(),
                          loggedInDevice: deviceId,
                        );

                        Navigator.pushNamed(context, '/edit_order',
                            arguments: {'editOrderObj': editOrderObj});

                        // BlocProvider.of<RouterBloc>(context).add(
                        //     RouterMoveToCreateOrderScreenEvent(
                        //         editOrderObj: editOrderObj));
                      }),
                ],
              ),
            ],
          );
        } else {

          return Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
              Expanded(
                child: SizedBox(),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.min,
                children: [
                  AnellohRoundButtonSmall(
                      childText: 'Create Order',
                      onPressed: () {
                        BlocProvider.of<RouterBloc>(context)
                            .add(RouterMoveToCreateOrderScreenEvent());
                      }),
                ],
              ),
            ],
          );
        }
      } else {

        return Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Expanded(
              child: SizedBox(),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisSize: MainAxisSize.min,
              children: [
                AnellohRoundButtonSmall(
                    childText: 'Create Order',
                    onPressed: () {
                      BlocProvider.of<RouterBloc>(context)
                          .add(RouterMoveToCreateOrderScreenEvent());
                    }),
              ],
            ),
          ],
        );
      }
    } else {
      log('This is the state I am in $state');

      return Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Expanded(
            child: SizedBox(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.end,
            mainAxisSize: MainAxisSize.min,
            children: [
              AnellohRoundButtonSmall(
                  childText: 'Create Order',
                  onPressed: () {
                    BlocProvider.of<RouterBloc>(context)
                        .add(RouterMoveToCreateOrderScreenEvent());
                  }),
            ],
          ),
        ],
      );
    }
  }

  _determineCreateOrderRowAction({@required MainUserOrderState state}) {
    if (state is CheckingUserPendingOrderSuccessful) {
      if (state.pendingOrders.length > 0) {
        if (state.pendingOrders[0].isMatched) {
          switch (state.pendingOrders[0].matchedOrder.matchedOrderStatus) {
            case 1:
              return '';
              break;
            default:
              return BlocProvider.of<RouterBloc>(context)
                  .add(RouterMoveToCreateOrderScreenEvent());
          }
        } else {
          return BlocProvider.of<RouterBloc>(context)
              .add(RouterMoveToCreateOrderScreenEvent());
        }
      } else {
        return BlocProvider.of<RouterBloc>(context)
            .add(RouterMoveToCreateOrderScreenEvent());
      }
    } else {
      return null;
    }
  }

  _determineOrderBodyWidget({@required MainUserOrderState state}) {
    if (state is CheckingUserPendingOrderSuccessful) {
      if (state.pendingOrders.length > 0) {
        if (state.pendingOrders[0].order == null) {
          return CustomText(
            title: 'Start here to swap currencies',
            hasFontWeight: true,
            fontWeight: FontWeight.w400,
            textColor: ColorPalette().mainMedium,
            textSize: 14,
          );
        }

        // if (state.pendingOrders[0].order.paymentStatus == 6)
        //   return CustomText(
        //     title:
        //         'You can continue swapping while your order is being processed',
        //     hasFontWeight: true,
        //     fontWeight: FontWeight.w400,
        //     textColor: ColorPalette().mainMedium,
        //     textSize: 14,
        //   );
        return CustomText(
          title: determineOrderBodyTextString(
              matchedOrderStatus: state
                  .pendingOrders[0]?.matchedOrder?.matchedOrderStatus
                  .toString(),
              orderStatus: state.pendingOrders[0].order.orderStatus.toString()),
          hasFontWeight: true,
          fontWeight: FontWeight.w400,
          textColor: ColorPalette().mainMedium,
          textSize: 14,
        );
      } else {
        return CustomText(
          title: 'Start here to swap currencies',
          hasFontWeight: true,
          fontWeight: FontWeight.w400,
          textColor: ColorPalette().mainMedium,
          textSize: 14,
        );
      }
    } else {
      return CustomText(
        title: 'Start here to swap currencies',
        hasFontWeight: true,
        fontWeight: FontWeight.w400,
        textColor: ColorPalette().mainMedium,
        textSize: 14,
      );
    }
  }

  _dertermineOrderHeaderText({@required MainUserOrderState state}) {
    if (state is CheckingUserPendingOrderSuccessful) {
      if (state.pendingOrders.length > 0) {
        // log('state.pendingOrders[0].order ${state.pendingOrders[0].order}');
        if (state.pendingOrders[0].order == null) {
          return CustomText(
            title: 'Create Order',
            hasFontWeight: true,
            fontWeight: FontWeight.w800,
            textColor: ColorPalette().mainMedium,
            textSize: 18,
          );
        }
        return CustomText(
          title: determineOrdeerHeaderTextString(
              matchedOrderStatus: state
                  .pendingOrders[0]?.matchedOrder?.matchedOrderStatus
                  .toString(),
              orderStatus: state.pendingOrders[0].order.orderStatus.toString()),
          hasFontWeight: true,
          fontWeight: FontWeight.w800,
          textColor: ColorPalette().mainMedium,
          textSize: 18,
        );
      } else {
        return CustomText(
          title: 'Create Order',
          hasFontWeight: true,
          fontWeight: FontWeight.w800,
          textColor: ColorPalette().mainMedium,
          textSize: 18,
        );
      }
    } else {
      return CustomText(
        title: 'Create Order',
        hasFontWeight: true,
        fontWeight: FontWeight.w800,
        textColor: ColorPalette().mainMedium,
        textSize: 18,
      );
    }
  }

  _topListingsRow() {
    return Align(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: [
          SizedBox(
            height: 10,
          ),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                Row(
                  children: [
                    CustomText(
                      title: "Top Listing",
                      hasFontWeight: true,
                      fontWeight: FontWeight.w700,
                      textColor: appPrimaryColor,
                      textSize: 15,
                    ),
                    SizedBox(width: 10,),
                    MyTooltip(message: "View top listings here", child: Icon(Icons.info_outline_rounded, color: Color(0xffb6b6b6), size: 17,))
                  ],
                ),
                InkWell(
                  onTap: _moveToAllListingScreen,
                  child: CustomText(
                    title: 'View more',
                    textSize: 15,
                  ),
                )
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          BlocBuilder<DashboardBloc, DashboardScreenState>(
            builder: (context, state) {
              if (state is FetchingOrdersSuccessful) {
                if(state.allOrders.length <= 0){
                  return Center(
                    child: Container(
                      height: 150,
                      child: Column(
                        children: [
                          SvgPicture.asset('assets/images/error.svg', width: 100,),
                          SizedBox(height: 10,),
                          CustomText(
                            title: 'No Listings available',
                            textSize: 15,
                          )
                        ],
                      ),
                    ),
                  );
                }
                return Container(
                  height: state.allOrders.length <= 0 ? 10 : 180,
                  child: ListView.builder(
                    itemCount: state.allOrders.length,
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (context, index) => Flex(
                      direction: Axis.vertical,
                      children: [
                        Expanded(
                            child: topListingsWidget(context,
                                orderListing: state.allOrders[index]))
                      ],
                    ),
                  ),
                );
              } else {
                return Container(
                  height: 180,
                  child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    shrinkWrap: true,
                    itemBuilder: (context, index) => _buildAnimatedTopListing(),
                    itemCount: 8,
                  ),
                );
              }
            },
          )
        ],
      ),
    );
  }

  _buildAnimatedTopListing() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      margin: EdgeInsets.only(right: 10, top: 10, bottom: 10),
      constraints: BoxConstraints(maxHeight: 180, minWidth: 250),
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [
            BoxShadow(
                offset: Offset(2, 3),
                blurRadius: 10.4,
                spreadRadius: 0.2,
                color: Colors.grey.withOpacity(0.2)),
          ],
          borderRadius: BorderRadius.circular(10)),
      child: SizedBox(
        height: 380,
        width: 200,
        child: PlaceholderLines(
          count: 5,
          animate: true,
          color: Colors.grey.withOpacity(0.003),
          lineHeight: 7,
        ),
      ),
    );
  }

  _marketRatesRow({
    @required List<MarketRateObj> marketRates,
  }) {
    return Align(
      alignment: Alignment.centerLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              CustomText(
                title: "Market Rates",
                hasFontWeight: true,
                fontWeight: FontWeight.w700,
                textColor: appPrimaryColor,
                textSize: 15,
              ),
              SizedBox(width: 10,),
              MyTooltip(message: "View latest market rates here", child: Icon(Icons.info_outline_rounded, color: Color(0xffb6b6b6), size: 17,))
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Container(
            height: 200,
            width: Get.width,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(10)),
            child: StackedAreaLineChart(
              marketRates: marketRates,
            ),
          )
        ],
      ),
    );
  }
}

