import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_state.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinUnlockScreen extends StatefulWidget {
  final bool sessionExpired;
  PinUnlockScreen({Key key, this.sessionExpired: false}):super(key: key);
  _PinUnlockScreenState createState() => _PinUnlockScreenState();
}

class _PinUnlockScreenState extends State<PinUnlockScreen> {
  @override
  void initState() {
    super.initState();
  }
  String pinValueOne = '';
  String pinValueTwo = '';
  String pinValueThree = '';
  String pinValueFour = '';

  final _formKey = GlobalKey<FormState>();


  FocusNode _focusInputOne = FocusNode();
  FocusNode _focusInputTwo = FocusNode();
  FocusNode _focusInputThree = FocusNode();
  FocusNode _focusInputFour = FocusNode();

  _inputFieldChange({String value, String field}) {
    final Map<String, FocusNode> mining = {
      'one': _focusInputOne,
      'two': _focusInputTwo,
      'three': _focusInputThree,
      'four': _focusInputFour,
    };

    switch(field) {
      case 'one':
        setState(() {
          pinValueOne = value;
        });
        break;
      case 'two':
        setState(() {
          pinValueTwo = value;
        });
        break;
      case 'three':
        setState(() {
          pinValueThree = value;
        });
        break;
      case 'four':
        setState(() {
          pinValueFour = value;
        });
        break;
    }

    var item = mining.entries.firstWhere((element) => element.key == field);
    if(value.isNotEmpty && field != 'four') {
      item.value.nextFocus();
    }else {
      if (value.isEmpty && field != 'one') {
        item.value.previousFocus();
      }
    }

    if(field == 'four' && value.isNotEmpty) {
      item.value.unfocus();
      _verifyPin();
    }
  }

  _signOut() async{

    BlocProvider.of<UserBloc>(context).add(LogOutUserEvent(user: AppConstants.userData));
  }

  _validateFields() {
    var formErrors = {};

    if(this.pinValueOne.isEmpty || this.pinValueOne.length != 1) {
      formErrors['one'] = true;
    }
    if (this.pinValueTwo.isEmpty || this.pinValueTwo.length != 1) {
      formErrors['two'] = true;
    }
    if (this.pinValueThree.isEmpty || this.pinValueThree.length != 1) {
      formErrors['three'] = true;
    }
    if(this.pinValueFour.isEmpty || this.pinValueFour.length != 1) {
      formErrors['four'] = true;
    }

    if (formErrors.entries.length > 0) {
      return true;
    }
    return false;
  }

  _verifyPin() async{
    bool hasFormErrors = _validateFields();
    SharedPreferences pref = await SharedPreferences.getInstance();
    if (hasFormErrors == false) {
      var pinCode = '${this.pinValueOne + this.pinValueTwo + this.pinValueThree + this.pinValueFour}';
      if(pinCode == pref.getString('__anelloh__user__pin__')){
        if (await getFromPref('current_route') != null) {
          Navigator.of(context).popAndPushNamed('/dashboard');
        } else {
          Navigator.of(context).popAndPushNamed('/dashboard');
        }
      } else {
        showToast(
          toastMsg: 'Incorrect Pin',
          toastGravity: ToastGravity.CENTER,
          txtColor: ColorPalette().textBlack,
          bgColor: Colors.grey.withOpacity(0.4)
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<PinUnlockScreenBloc, PinUnlockScreenState>(
      listener: (context, state) {},
      child: BlocListener<UserBloc, UserState>(
        listener: (context, userListenerState) {
          if (userListenerState is LogOutSuccessful) {
            Navigator.popAndPushNamed(context, '/login');
          }
        },
        child:  BlocBuilder<PinUnlockScreenBloc, PinUnlockScreenState>(
          builder: (context, blocState) {
            return  Scaffold(
              resizeToAvoidBottomInset: true,
              body: SafeArea(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            BlocBuilder<UserBloc, UserState>(
                                builder: (context, state) {
                                  return  Container(
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      children: [
                                        Container(
                                          child: ClipRRect(
                                            child: Image.asset(
                                              ImageConstants.anellohLogo,
                                              fit: BoxFit.contain,
                                              width: 250,
                                              height: 200,
                                            ),

                                          ),
                                          width: MediaQuery.of(context).size.width,
                                        ),
                                        Container(
                                          child: CustomText(
                                            title: state is SaveCurrentUser ? '${state.user?.firstName} ${state.user?.lastName}' : '',
                                            isBold: true,
                                            isCenter: true,
                                            textSize: mainSizer(context: context, size: 13),
                                          ),
                                          margin: EdgeInsets.only(bottom: 20),
                                        ),
                                        Container(
                                          child: CustomText(
                                            title: widget.sessionExpired == true ? 'You have been logged out' : 'Welcome Back',
                                            isBold: true,
                                            isCenter: true,
                                            textSize: mainSizer(context: context, size: 23),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 3,
                                        ),
                                        CustomText(
                                          isCenter: true,
                                          title: 'Enter your pin code to Log in',
                                        ),
                                      ],
                                    ),
                                  );
                                }
                            ),
                          ],
                        ),
                        width: MediaQuery.of(context).size.width,
                      ),
                      Container(
                          constraints: BoxConstraints(
                              minHeight: 100
                          ),
                          margin: EdgeInsets.only(top: 17),
                          alignment: Alignment.center,
                          child: Form(
                            key: _formKey,
                            autovalidateMode: AutovalidateMode.always,
                            child: Flex(
                              direction: Axis.horizontal,
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                      maxWidth: 55,
                                      minWidth: 55,
                                      maxHeight: 55,
                                      minHeight: 55
                                  ), child: TextFormField(
                                  autofocus: true,
                                  obscureText: true,
                                  maxLength: 1,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value,
                                        field: 'one'
                                    );
                                  },
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.phone,
                                  focusNode: _focusInputOne,
                                  initialValue: pinValueOne,
                                  decoration: InputDecoration(
                                      counterText: "",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(16)
                                      )
                                  ),
                                ),),
                                Padding(padding:EdgeInsets.all(10)),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                      maxWidth: 55,
                                      minWidth: 55,
                                      maxHeight: 55,
                                      minHeight: 55
                                  ), child: TextFormField(
                                  autofocus: true,
                                  obscureText: true,
                                  maxLength: 1,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value,
                                        field: 'two'
                                    );
                                  },
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.phone,
                                  focusNode: _focusInputTwo,
                                  initialValue: pinValueTwo,
                                  decoration: InputDecoration(
                                      counterText: "",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(16)
                                      )
                                  ),
                                ),),
                                Padding(padding:EdgeInsets.all(10)),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                      maxWidth: 55,
                                      minWidth: 55,
                                      maxHeight: 55,
                                      minHeight: 55
                                  ), child: TextFormField(
                                  autofocus: true,
                                  obscureText: true,
                                  maxLength: 1,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value,
                                        field: 'three'
                                    );
                                  },
                                  keyboardType: TextInputType.phone,
                                  textAlign: TextAlign.center,
                                  focusNode: _focusInputThree,
                                  initialValue: pinValueThree,
                                  decoration: InputDecoration(
                                      counterText: "",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(16)
                                      )
                                  ),
                                ),),
                                Padding(padding:EdgeInsets.all(10)),
                                ConstrainedBox(
                                  constraints: BoxConstraints(
                                      maxWidth: 55,
                                      minWidth: 55,
                                      maxHeight: 55,
                                      minHeight: 55
                                  ), child: TextFormField(
                                  autofocus: true,
                                  maxLength: 1,
                                  obscureText: true,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value,
                                        field: 'four'
                                    );
                                  },
                                  keyboardType: TextInputType.phone,
                                  textAlign: TextAlign.center,
                                  focusNode: _focusInputFour,
                                  initialValue: pinValueFour,
                                  decoration: InputDecoration(
                                      counterText: "",
                                      border: OutlineInputBorder(
                                          borderRadius: BorderRadius.circular(16)
                                      )
                                  ),
                                ),),
                              ],
                            ),
                          )
                      ),
                      Padding(
                        padding: EdgeInsets.all(15),
                        child: AnellohRoundButtonLarge(
                            onPressed: _verifyPin,
                            childText: 'Log In'),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.of(context).pushNamed('/face_id_unlock');
                        },
                        child: CustomText(
                          title: 'Use Face ID instead?',
                        ),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                        child: Row(
                          children: [
                            CustomText(
                              isCenter: true,
                              title: 'Not you?',
                            ),
                            AnellohFlatButton(
                              onPressed: _signOut,
                              childText: 'Sign out',
                            )
                          ],
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            );
          },),
      ),
     );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
