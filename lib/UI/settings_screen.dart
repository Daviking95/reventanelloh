import 'package:anelloh_mobile/bloc/profile/profile_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_state.dart';
import 'package:anelloh_mobile/bloc/router/router_bloc.dart';
import 'package:anelloh_mobile/bloc/router/router_event.dart';
import 'package:anelloh_mobile/bloc/router/router_state.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/custom_profile_nav_btn.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../constants.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends State<SettingsScreen> {
  @override
  Widget build(BuildContext context) {
    return BlocListener<RouterBloc, RouterState>(listener: (context, state) {
      if (state is RouterMoveToProfileScreen) {
        Navigator.of(context).pushNamed('/profile');
      }
    }, child: BlocBuilder<ProfileScreenBloc, ProfileScreenState>(
      builder: (context, state) {
        return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
          return LoadingOverlay(
              isLoading: state is RequestingLogout ? true : false,
              child: StarterWidget(
                  isBgAllowed: false,
                  hasNavDrawer: false,
                  hasTopWidget: false,
                  isExitApp: true,
                  hasBackButton: false,
                  hasRefreshOption: false,
                  onRefreshFunction: () {},
                  hasBottomNav: true,
                  bottomNavIndex: 3,
                  bottomPageContent: _bottomPageContent(context: context)));
        });
      },
    ));
  }

  _moveToProfileScreen({BuildContext context}) {
    BlocProvider.of<RouterBloc>(context).add(RouterMoveToProfileScreenEvent());
  }

  _bottomPageContent({@required BuildContext context}) {
    return BlocListener<UserBloc, UserState>(
      listener: (context, state) {
        if (state is LogOutSuccessful) {
          Navigator.of(context).popAndPushNamed('/login');
        }
      },
      child: SafeArea(
        maintainBottomViewPadding: false,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(bottom: 10, top: 10, left: 20, right: 20),
          child: ConstrainedBox(
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: [
                subPageTitle('Settings', 'Your settings information', context: context),
                SizedBox(
                  height: 20,
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: 100,
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        CustomText(
                          title: 'General',
                          isBold: true,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        customProfileNavButton(
                            onPressed: () {
                              _moveToProfileScreen(context: context);
                            },
                            title: 'Profile',
                            icon: Icon(
                              Icons.account_circle_outlined,
                              size: 20.0,
                              color: ColorPalette().main,
                            ),
                            context: context),
                        customProfileNavButton(
                            onPressed: () =>
                                Navigator.pushNamed(context, '/notifications'),
                            title: 'Notification',
                            icon: Icon(MdiIcons.bellOutline,
                                size: 20.0, color: ColorPalette().main),
                            context: context),
                      ]),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: 100,
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        CustomText(
                          title: 'Security',
                          isBold: true,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        customProfileNavButton(
                            onPressed: () {
                              Navigator.of(context)
                                  .pushNamed('/update_password');
                            },
                            title: 'Change Password',
                            icon: Icon(MdiIcons.lockOutline,
                                size: 20.0, color: ColorPalette().main),
                            context: context),
                        // customProfileNavButton(
                        //     onPressed: () {
                        //       Navigator.of(context).pushNamed('/pin_face_id');
                        //     },
                        //     title: 'Pin and Face ID',
                        //     icon: Icon(Icons.vpn_key_outlined,
                        //         size: 20.0, color: ColorPalette().main),
                        //     context: context),
                      ]),
                ),
                ConstrainedBox(
                  constraints: BoxConstraints(
                    minHeight: 100,
                  ),
                  child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        CustomText(
                          title: 'Identity Details',
                          isBold: true,
                        ),
                        SizedBox(
                          height: 15,
                        ),
                        customProfileNavButton(
                          onPressed: () {
                            Navigator.of(context).pushNamed('/identity');
                          },
                          title: 'Identity Details',
                          icon: Icon(MdiIcons.passport,
                              size: 20.0, color: ColorPalette().main),
                          context: context,
                        ),
                        // customProfileNavButton(
                        //   onPressed: () {},
                        //   title: 'Delete Account',
                        //   icon: Icon(MdiIcons.trashCan, size: 20.0, color: ColorPalette().main),
                        //   context: context,
                        // ),
                      ]),
                ),
                customProfileNavButton(
                    onPressed: _logout,
                    title: 'Log out',
                    icon: Icon(Icons.vpn_key_outlined,
                        size: 20.0, color: ColorPalette().main),
                    context: context),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _logout() {
    // clearPref();

    BlocProvider.of<UserBloc>(context)
        .add(LogOutUserEvent(user: AppConstants.userData));
  }
}
