import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_state.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/listings/listings_bloc.dart';
import 'package:anelloh_mobile/bloc/listings/listings_event.dart';
import 'package:anelloh_mobile/bloc/listings/listings_state.dart';
import 'package:anelloh_mobile/bloc/router/router_bloc.dart';
import 'package:anelloh_mobile/bloc/router/router_state.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/listing_activity_card.dart';
import 'package:anelloh_mobile/widgets/notify_prof_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../constants.dart';

enum AllListingPublishState {
  NoMatchFound,
  HasPendingOrder,
  HasNoKnownState,
}

class AllListingsScreen extends StatefulWidget {
  _AllListingScreen createState() => _AllListingScreen();
}

class _AllListingScreen extends State<AllListingsScreen> {
  @override
  void initState() {
    super.initState();
    _fetchListingData();
  }

  _fetchListingData() {
    // print("This is CID ${AppConstants?.userData?.customerId}");
    BlocProvider.of<ListingsActionBloc>(context).add(FetchAllOrderListings(
        customerId: AppConstants?.userData?.customerId ?? "0"));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RouterBloc, RouterState>(
      listener: (context, state) {
        if (state is RouterMoveToCreateOrderScreen) {
          Navigator.of(context).pushNamed('/create_order');
        }
      },
      child: BlocBuilder<ListingsActionBloc, ListingsActionState>(
        builder: (context, state) {
          return LoadingOverlay(
            isLoading: state is FetchingListings ? true : false,
            progressIndicator: ProgressWidget(),
            color: ColorPalette().textBlack,
            opacity: 0.4,
            child: StarterWidget(
              hasRefreshOption: true,
              hasBackButton: false,
              hasTopWidget: false,
              hasBottomNav: true,
              bottomNavIndex: 1,
              hasNavDrawer: false,
              hasPagePadding: true,
              onRefreshFunction: () {
                return _fetchListingData();
              },
              topPageContent: _topPageContent(context: context),
              bottomPageContent: _bottomPageContent(context: context),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

Widget _topPageContent({BuildContext context}) {
  return NotificationProfileWidget();
}

Widget _bottomPageContent({BuildContext context}) {
  return BlocBuilder<AllListingScreenBloc, AllListingScreenState>(
      builder: (context, state) {
    return SafeArea(
        maintainBottomViewPadding: false,
        child: SingleChildScrollView(
          padding: EdgeInsets.symmetric(vertical: 3.0),
          child: ConstrainedBox(
            constraints:
                BoxConstraints(minHeight: MediaQuery.of(context).size.height),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocBuilder<ListingsActionBloc, ListingsActionState>(
                  builder: (context, state) {
                    if (state is SuccessfullyFetchedAllOrderListings) {
                      return ConstrainedBox(
                        constraints: BoxConstraints(
                          maxHeight: MediaQuery.of(context).size.height / 1.3,
                        ),
                        child: ListView.builder(
                            padding: EdgeInsets.symmetric(vertical: 3.0),
                            itemCount: state.listings.length + 1,
                            itemBuilder: (context, idx) {

                              if(state.listings.length <= 0){
                                return Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    subPageTitle('Listings', 'View all listings on Anelloh here',
                                        context: context),
                                    SizedBox(height: 10,),
                                    Center(
                                      child: Container(
                                        height: 150,
                                        child: Column(
                                          children: [
                                            SvgPicture.asset('assets/images/error.svg', width: 100,),
                                            SizedBox(height: 10,),
                                            CustomText(
                                              title: 'No Listings available',
                                              textSize: 15,
                                            )
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                );
                              }

                              if (idx == 0) {
                                return Padding(
                                  child: subPageTitle('Listings', 'View all listings on Anelloh here',
                                      context: context),
                                  padding: EdgeInsets.only(bottom: 20),
                                );
                              } else {
                                return Container(
                                  constraints: BoxConstraints(
                                    maxHeight: 130,
                                    minHeight: 130,
                                  ),
                                  child: listScreenBuilder(
                                      context: context,
                                      state: state,
                                      idx: idx - 1),
                                );
                              }
                            }),
                      );
                    } else {
                      return SizedBox();
                    }
                  },
                )
              ],
            ),
          ),
        ));
  });
}

Widget listScreenBuilder({
  @required ListingsActionState state,
  @required int idx,
  @required BuildContext context,
}) {
  if (state is SuccessfullyFetchedAllOrderListings) {

    return ListingActivityCard(
        matchFoundObj: state.listings[idx].matchFoundObj,
        onPressed: () {
          MatchFoundObj matchFoundObj = state.listings[idx].matchFoundObj;
          Navigator.of(context).pushNamed('/instant_match', arguments: {
            'orderId': int.parse(matchFoundObj.entityId),
            'orderOwnerId': matchFoundObj.customerId,
            'currencyNeededCountryCode': matchFoundObj.myCurrency,
          });
        });
  }
}
