import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/all_banks/all_banks_bloc.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_event.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_state.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_state.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_event.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_state.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_bloc.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_event.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_state.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/bottom_sheet_widgets.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/listing_activity_card.dart';
import 'package:anelloh_mobile/widgets/notify_prof_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'foreign_destination_account.dart';

class MatchNotFoundScreen extends StatefulWidget {
  final CreateOrderObj createOrderObj;
  final bool isEdit;
  final EditOrderObj editOrderObj;

  MatchNotFoundScreen(
      {@required this.createOrderObj, this.isEdit = false, this.editOrderObj})
      : super();

  _MatchNotFoundScreenState createState() => _MatchNotFoundScreenState();
}

class _MatchNotFoundScreenState extends State<MatchNotFoundScreen> {
  String selectedBank = '';
  bool isLoading = false;

  @override
  void initState() {
    super.initState();
    _fetchBanks();
    _fetchOrderByRate();
  }

  GlobalKey<FormState> _accountDetailsKey = GlobalKey<FormState>();

  bool _determineLoadingState({@required dynamic state}) {
    if (state is CreatingOrder) return true;
    return false;
  }

  _fetchOrderByRate() {
    BlocProvider.of<MatchNotFoundBloc>(context).add(FetchOrderByRateEvent(
        rate: widget.createOrderObj.rate,
        currency: widget.createOrderObj.myCurrency,
        customerId: widget.createOrderObj.customerId));
  }

  _updateSelectedBank(value) {
    setState(() {
      selectedBank = value;
    });
  }

  _fetchBanks() {
    if (widget.createOrderObj.convertedCurrency == 'NGN') {
      _fetchNigerianBanks();
    } else {
      _fetchOtherBanks();
    }
  }

  _fetchNigerianBanks() {
    BlocProvider.of<AllBanksBloc>(context).add(GetAllNigerianBanksEvent());
  }

  _fetchOtherBanks() {
    BlocProvider.of<AllBanksBloc>(context).add(GetOtherBanksEvent());
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<CreateOrderScreenBloc, CreateOrderScreenState>(
          listener: (context, state) {
            if (state is CreatingOrder)
              setState(() {
                isLoading = true;
              });
            if (state is CreatingOrderSuccessful) {
              showToast(
                  toastMsg: 'Order created successfully',
                  toastGravity: ToastGravity.CENTER,
                  bgColor: ColorPalette().textWhite,
                  txtColor: ColorPalette().main);
              Navigator.of(context)
                  .pushReplacementNamed(RoutesConstants.dashboardUrl);
            }
            if (state is CreatingOrderFailed) {
              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body: state.message,
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
          },
        ),
        BlocListener<EditOrderScreenBloc, EditOrderScreenState>(
            listener: (context, st) {
          if (st is EditingOrder)
            setState(() {
              isLoading = true;
            });
          if (st is EditOrderSuccessfully) {
            showToast(
                toastMsg: widget.editOrderObj != null
                    ? 'Order updated successfully'
                    : 'Order edited successfully',
                toastLength: Toast.LENGTH_LONG,
                toastGravity: ToastGravity.BOTTOM,
                bgColor: ColorPalette().textWhite,
                txtColor: ColorPalette().main);
            // Future.delayed(Duration(seconds: 1)).then((res) {
            Navigator.of(context).pushReplacementNamed('/dashboard');
            // });
          }
          if (st is EditOrderFailed) {
            showToast(
                toastMsg: 'Failed to edit order',
                toastLength: Toast.LENGTH_LONG,
                toastGravity: ToastGravity.BOTTOM,
                bgColor: ColorPalette().textWhite,
                txtColor: ColorPalette().main);
          }
        }),
      ],
      child: BlocBuilder<CreateOrderScreenBloc, CreateOrderScreenState>(
        builder: (context, createOrderState) {
          return LoadingOverlay(
              isLoading: isLoading,
              progressIndicator: ProgressWidget(),
              color: ColorPalette().textBlack,
              // opacity: 0.4,
              child: StarterWidget(
                hasBackButton: true,
                hasTopWidget: false,
                topPageContent: _topPageContent(context: context),
                bottomPageContent: _bottomPageContent(
                  context: context,
                  accountDetailsKey: _accountDetailsKey,
                  createOrderObj: widget.createOrderObj,
                ),
              ));
        },
      ),
    );
  }

  _topPageContent({@required BuildContext context}) {
    return NotificationProfileWidget();
  }

  Widget _bottomPageContent({
    @required BuildContext context,
    @required GlobalKey<FormState> accountDetailsKey,
    @required CreateOrderObj createOrderObj,
  }) {
    TextEditingController _accountNumberController = TextEditingController();
    TextEditingController _routingNumberController = TextEditingController();

    _mainOrderPublish() {
      if (accountDetailsKey.currentState.validate()) {
        CreateOrderObj updatedCreateOrderObj = createOrderObj.copyWith(
          myBankName: this.selectedBank,
          myAccountNumber: _accountNumberController.text,
          bankRouteNo: _routingNumberController.text,
        );
        Navigator.of(context).pop();
        BlocProvider.of<CreateOrderScreenBloc>(context)
            .add(CreateOrderActionEvent(createOrderObj: updatedCreateOrderObj));
      }
    }

    _editOrder() {

      EditOrderObj editOrderObj = EditOrderObj(
          customerId: widget.createOrderObj.customerId,
          orderId: int.parse(widget.editOrderObj.orderId),
          myAmount: widget.createOrderObj.myAmount,
          myCurrency: widget.createOrderObj.myCurrency,
          rate: widget.createOrderObj.rate,
          convertedAmount: widget.createOrderObj.convertedAmount,
          convertedCurrency: widget.createOrderObj.convertedCurrency,
          myPaymentChannelId: widget.createOrderObj.myPaymentChannelId,
          myBankName: this.selectedBank,
          myAccountNumber: _accountNumberController.text,
          bankRouteNo: _routingNumberController.text ?? 'nil',
          transactionFee: double.parse(widget.editOrderObj.transactionFee),
          paymentStatus:
              double.parse(widget.editOrderObj.paymentStatus).toInt(),
          loggedInDevice: widget.editOrderObj.loggedInDevice,
          paymentReference: widget.editOrderObj.paymentReference);
      BlocProvider.of<EditOrderScreenBloc>(context)
          .add(EditOrderEvent(editOrderObj: editOrderObj));
      Navigator.pop(context);
    }

    List<String> _buildDropdownSearch({@required List<Bank> banks}) {
      return banks.map((e) => e.name).toList();
    }

    _publishOrder({
      @required List<Bank> banks,
    }) {
      TextEditingController rateController = new TextEditingController();
      rateController.text = widget.createOrderObj.rate.toString();

      // print('double.parse(widget.editOrderObj.transactionFee) ${double.parse(widget.editOrderObj.transactionFee)}');
      // print('double.parse(widget.editOrderObj.paymentStatus).toInt() ${double.parse(widget.editOrderObj.paymentStatus).toInt()}');

      // return;

      EditOrderObj editOrderObj = widget.isEdit
          ? EditOrderObj(
              customerId: widget.createOrderObj.customerId,
              orderId: widget.editOrderObj.orderId,
              orderNo: widget.editOrderObj.orderNo,
              myAmount: widget.createOrderObj.myAmount,
              myCurrency: widget.createOrderObj.myCurrency,
              transactionFee: double.parse(widget.editOrderObj.transactionFee) ?? 0,
              paymentStatus: double.parse(widget.editOrderObj.paymentStatus).toInt() ?? 0,
              convertedCurrency: widget.createOrderObj.convertedCurrency,
              rate: widget.createOrderObj.rate,
              convertedAmount: widget.createOrderObj.convertedAmount)
          : EditOrderObj(
              myAmount: widget.createOrderObj.myAmount,
              myCurrency: widget.createOrderObj.myCurrency,
              convertedCurrency: widget.createOrderObj.convertedCurrency,
              rate: widget.createOrderObj.rate,
              convertedAmount: widget.createOrderObj.convertedAmount);

      String accountName = '';
      int hasVerifiedAccountNumber = 0;
      try {
        showModalBottomSheet(
            context: context,
            isScrollControlled: true,
            useRootNavigator: true,
            backgroundColor: ColorPalette().textWhite,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15))),
            builder: (context) {
              return StatefulBuilder(builder: (context, StateSetter setState) {
                return SingleChildScrollView(
                  child: Container(
                    padding: EdgeInsets.only(
                        bottom: MediaQuery.of(context).viewInsets.bottom),
                    child: Form(
                      key: accountDetailsKey,
                      autovalidateMode: AutovalidateMode.always,
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                        child: widget.createOrderObj.convertedCurrency != 'NGN'
                            ? MultiBlocProvider(
                                providers: [
                                    BlocProvider<EditOrderScreenBloc>(
                                        create: (context) =>
                                            EditOrderScreenBloc()),
                                    BlocProvider<CreateOrderScreenBloc>(
                                        create: (context) =>
                                            CreateOrderScreenBloc()),
                                    BlocProvider<CurrencyConversionBloc>(
                                        create: (context) =>
                                            CurrencyConversionBloc()),
                                    BlocProvider<InstantMatchScreenBloc>(
                                        create: (context) =>
                                            InstantMatchScreenBloc()),
                                    BlocProvider<PaymentSummaryStateBloc>(
                                      create: (context) =>
                                          PaymentSummaryStateBloc(),
                                    ),
                                    BlocProvider<MatchFoundScreenBloc>(
                                      create: (context) =>
                                          MatchFoundScreenBloc(),
                                    ),
                                  ],
                                child: Container(
                                  height: Get.height / 2,
                                  child: ForeignDestinationAccount(
                                      context: context,
                                      banks: banks,
                                      editOrderObj: editOrderObj,
                                      rateController: rateController,
                                      isEdit: widget.isEdit),
                                ))
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.min,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  SizedBox(
                                    height: 5,
                                  ),
                                  bottomSheetHeaderWidget(context, 'Please provide destination account details', true, "This is where Anelloh pays your money to"),
                                  DropdownSearch<String>(
                                    mode: Mode.BOTTOM_SHEET,
                                    label: 'Bank Name',
                                    showSelectedItem: true,
                                    items: _buildDropdownSearch(banks: banks),
                                    onChanged: (value) {
                                      _updateSelectedBank(value);
                                    },
                                    showSearchBox: true,
                                    searchBoxDecoration: InputDecoration(
                                        labelText: 'Search for bank'),
                                    selectedItem: this.selectedBank.isNotEmpty
                                        ? this.selectedBank
                                        : banks[0].name,
                                  ),
                                  AppTextInputs(
                                    textInputTitle: 'Account Number',
                                    controller: _accountNumberController,
                                    textInputType: TextInputType.number,
                                    isOnlyDigits: true,
                                    validateInput: createOrderObj
                                                .convertedCurrency !=
                                            'NGN'
                                        ? AppConstants.validators.validateNumber
                                        : AppConstants.validators
                                            .validateNigerianAccountNumber,
                                    onChange: createOrderObj
                                                .convertedCurrency !=
                                            'NGN'
                                        ? (value) async {
                                            setState(() {
                                              hasVerifiedAccountNumber = 2;
                                            });
                                          }
                                        : (value) async {
                                            try {
                                              setState(() {
                                                hasVerifiedAccountNumber = 1;
                                              });
                                              if (value.length == 10) {
                                                String sb =
                                                    this.selectedBank.isNotEmpty
                                                        ? this.selectedBank
                                                        : banks[0].name;
                                                Bank bank = banks.firstWhere(
                                                    (Bank bank) =>
                                                        bank.name == sb);
                                                Dio.Response response =
                                                    await dio.get(
                                                        'https://api.paystack.co/bank/resolve?account_number=${_accountNumberController.text}&bank_code=${bank.code}',
                                                        options: Dio.Options(
                                                            headers: {
                                                              'Authorization':
                                                                  'Bearer ${env['PAYSTACK_LIVE_SECRET_KEY']}'
                                                            }));
                                                if (response.statusCode ==
                                                    200) {
                                                  if (response.data['status'] ==
                                                      true) {
                                                    setState(() {
                                                      accountName =
                                                          response.data['data']
                                                              ['account_name'];
                                                      hasVerifiedAccountNumber =
                                                          2;
                                                    });
                                                  }
                                                } else {
                                                  setState(() {
                                                    accountName =
                                                        'Invalid account number';
                                                    hasVerifiedAccountNumber =
                                                        3;
                                                  });
                                                }
                                              }
                                            } catch (err) {
                                              if (err is Dio.DioError) {
                                                print(err.response);
                                                print(err.response.data);
                                                setState(() {
                                                  accountName = err
                                                      .response.data['message'];
                                                  hasVerifiedAccountNumber = 3;
                                                });
                                              }
                                            }
                                          },
                                  ),
                                  createOrderObj.convertedCurrency != 'NGN'
                                      ? AppTextInputs(
                                          textInputTitle: 'Routing Number',
                                          controller: _routingNumberController,
                                          textInputType: TextInputType.number,
                                          isOnlyDigits: true,
                                        )
                                      : SizedBox(),
                                  SizedBox(
                                    height: 1,
                                  ),
                                  Padding(
                                    padding:
                                        EdgeInsets.only(left: 10, right: 10),
                                    child: Text(
                                      '$accountName',
                                      style: TextStyle(
                                          color: hasVerifiedAccountNumber == 2
                                              ? Colors.green
                                              : Colors.red),
                                    ),
                                  ),
                                  SizedBox(
                                    height: 15,
                                  ),
                                  AnellohRoundNetworkButtonLarge(
                                    onPressed: () {
                                      if (hasVerifiedAccountNumber != 2) {
                                        return () {};
                                      }
                                      if (selectedBank.isEmpty) {
                                        selectedBank = banks[0].name;
                                      }
                                      widget.isEdit
                                          ? _editOrder()
                                          : _mainOrderPublish();
                                    },
                                    childText: hasVerifiedAccountNumber == 2
                                        ? 'Proceed'
                                        : hasVerifiedAccountNumber == 1
                                            ? 'Verifying account number...'
                                            : 'Awaiting account number verification...',
                                    isColorInverted: false,
                                    isLoading: false,
                                  ),
                                ],
                              ).paddingSymmetric(horizontal: 10),
                      ),
                    ),
                  ),
                );
              });
            });
      } catch (err) {
        print(err);
      }
    }

    return SafeArea(
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 17.0, horizontal: 17.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              padding: EdgeInsets.all(12),
              decoration: BoxDecoration(
                  color: Color(0xFFF9968B),
                  borderRadius: BorderRadius.circular(17)),
              constraints: BoxConstraints(),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: CustomText(
                      title: 'Sorry, we couldn\’t find a match.',
                      hasFontWeight: true,
                      fontWeight: FontWeight.w800,
                      textColor: ColorPalette().mainMedium,
                      textSize: mainSizer(context: context, size: 21),
                    ),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: CustomText(
                      title:
                          'View similar listings below, you can also edit your order to find a new match or post order to get notified when we find you a match.',
                      hasFontWeight: true,
                      fontWeight: FontWeight.w400,
                      textColor: ColorPalette().mainMedium,
                      textSize: mainSizer(context: context, size: 29),
                    ),
                  ),
                  SizedBox(
                    height: 7,
                  ),
                  Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      AnellohRoundButtonSmall(
                          childText: 'Edit Order',
                          onPressed: () {
                            Navigator.pop(context);
                          }),
                      SizedBox(
                        width: 7,
                      ),
                      BlocBuilder<AllBanksBloc, AllBanksState>(
                        builder: (context, state) {
                          if (state is SuccessfullyFetchedAllBanks) {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AnellohRoundButtonSmall(
                                    childText: 'Post Order',
                                    onPressed: () {
                                      _publishOrder(banks: state.banks);
                                    }),
                              ],
                            );
                          } else {
                            return Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                AnellohRoundButtonSmall(
                                  childText: 'Please wait...',
                                  onPressed: _fetchBanks,
                                ),
                              ],
                            );
                          }
                        },
                      )
                    ],
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              alignment: Alignment.centerLeft,
              child: CustomText(
                title: 'Listings similar to your rate',
                textSize: 20,
                fontWeight: FontWeight.w900,
                hasFontWeight: true,
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height / 1.777),
              child: BlocBuilder<MatchNotFoundBloc, MatchNotFoundState>(
                  builder: (context, state) {
                if (state is SuccessfullyFetchOrderByRate) {
                  if (state.matchFoundObjs.length > 0) {
                    return ListView.builder(
                      itemCount: state.matchFoundObjs.length,
                      itemBuilder: (context, index) {
                        return Container(
                          constraints: BoxConstraints(maxHeight: 130),
                          child: ListingActivityCard(
                            matchFoundObj: state.matchFoundObjs[index],
                            onPressed: () {
                              MatchFoundObj matchFoundObj =
                                  state.matchFoundObjs[index];
                              Navigator.of(context)
                                  .pushNamed('/instant_match', arguments: {
                                'orderId': int.parse(matchFoundObj.entityId),
                                'orderOwnerId': matchFoundObj.customerId,
                                'currencyNeededCountryCode':
                                    matchFoundObj.myCurrency,
                              });
                            },
                          ),
                        );
                      },
                      // physics: AlwaysScrollableScrollPhysics(),
                    );
                  } else {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      child: CustomText(
                        title:
                            'No Listing rate similar to ${widget.createOrderObj.rate}',
                      ),
                    );
                  }
                } else if (state is FetchingOrdersByRate) {
                  return Platform.isIOS
                      ? CupertinoActivityIndicator()
                      : CircularProgressIndicator(
                          backgroundColor: ColorPalette().main,
                        );
                } else {
                  return SizedBox();
                }
              }),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
