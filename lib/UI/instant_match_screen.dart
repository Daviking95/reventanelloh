import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/UI/payment_options_screen.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_bloc.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_event.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_state.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_event.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_state.dart';
import 'package:anelloh_mobile/bloc/listings/listings_bloc.dart';
import 'package:anelloh_mobile/bloc/listings/listings_event.dart';
import 'package:anelloh_mobile/bloc/listings/listings_state.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_event.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:anelloh_mobile/helpers/classes/order_listing.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/bottom_sheet_widgets.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_placeholder_textlines/placeholder_lines.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'foreign_destination_account.dart';

class InstantMatchScreen extends StatefulWidget {
  final String orderOwnerId;
  final int orderId;
  final OrderListing orderListing;
  final String currencyNeededCountryCode;

  InstantMatchScreen({
    @required this.orderOwnerId,
    @required this.orderId,
    @required this.orderListing,
    @required this.currencyNeededCountryCode,
  }) : super();

  _InstantMatchScreenState createState() => _InstantMatchScreenState();
}

class _InstantMatchScreenState extends State<InstantMatchScreen> {
  TextEditingController _accountNumberController = TextEditingController();
  TextEditingController _routingNumberController =
      TextEditingController(text: '');

  GlobalKey<FormState> _accountDetailsKey = GlobalKey<FormState>();

  String selectedBank = '';

  String currentCustomerPendingOrderId;

  MatchFoundObj tempMatchFoundObj;

  List tempBanks;

  var _loadingOverlay = false;

  var _configurationFee = 0;

  double _processingFee;

  @override
  void initState() {
    super.initState();
    _fetchListingById();
    _fetchSingleUserOfListing();
    _fetchConfigurationFee();
    _fetchBanks();
    _fetchCurrentCustomerPendingOrder();
  }

  _updateSelectedBank(value) {
    setState(() {
      selectedBank = value;
    });
  }

  _fetchBanks() {
    if (widget.currencyNeededCountryCode == 'NGN') {
      _fetchNigerianBanks();
    } else {
      _fetchOtherBanks();
    }
  }

  _fetchNigerianBanks() {
    BlocProvider.of<AllBanksBloc>(context).add(GetAllNigerianBanksEvent());
  }

  _fetchOtherBanks() {
    BlocProvider.of<AllBanksBloc>(context).add(GetOtherBanksEvent());
  }

  _fetchListingById() {
    BlocProvider.of<ListingsActionBloc>(context)
        .add(FetchListingByIdEvent(listingId: widget.orderId));
  }

  _fetchSingleUserOfListing() {
    BlocProvider.of<UserBloc>(context)
        .add(GetCustomerByIdEvent(customerId: widget.orderOwnerId));
  }

  _createAndMatchExistingOrder({@required MatchFoundObj matchFoundObj}) async {
    try {
      if (_accountDetailsKey.currentState.validate()) {
        Navigator.of(context).pop();

        Map<String, dynamic> userObj;
        if (await readFromPref('__anelloh_current_user__')) {
          userObj = jsonDecode(await getFromPref('__anelloh_current_user__'));
        }
        BlocProvider.of<InstantMatchScreenBloc>(context)
            .add(CreateAndMatchExistingOrderEvent(
          existingOrderNo: matchFoundObj.orderNo,
          myCustomerId: userObj['customerId'],
          myAccountNumber: _accountNumberController.text,
          myBankName: selectedBank,
          myCurrency: matchFoundObj.myCurrency,
          bankRouteNo: _routingNumberController.text != null
              ? _routingNumberController.text
              : 'n/a',
        ));
      }
    } catch (err) {
      print(err);
    }
  }

  List<String> _buildDropdownSearch({@required List<Bank> banks}) {
    return banks.map((e) => e.name).toList();
  }

  _acceptMatch(
      {@required MatchFoundObj matchFoundObj,
      @required BuildContext context,
      @required List<Bank> banks}) {
    try {
      print('matchFoundObj ${matchFoundObj.orderNo}');
      print('matchFoundObj Bank ${banks}');
      if (AppConstants.userLatestOrderStatus != null &&
              AppConstants.userLatestOrderStatus.toString() == '1' ||
          AppConstants.userLatestOrderStatus.toString() == '5' ||
          AppConstants.userLatestOrderStatus.toString() == '7') {
        return anellohOverlay(
            overlayObject: OverlayObject(
              clearToNavigate: true,
              title: 'Error',
              body:
                  // 'You cannot continue with this swap because you have an open order. Click yes to modify order with this card or no to cancel',
                  'You cannot swap now as an opened or unfulfilled or cancelled order already exists for you. Please modify the opened order to continue',
              hasBtn: false,
              hasCancelBtn: false,
              btnText: 'Yes',
              btnFunction: () => _cancelPendingOrder(),
              cancelBtnText: 'No',
              icon: Icon(
                Icons.warning,
                color: Colors.red,
              ),
            ),
            context: context);
      }

      // setState(() {
      //   selectedBank = banks[0].name;
      // });

      String accountName = '';
      int hasVerifiedAccountNumber = 0;
      return showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          backgroundColor: ColorPalette().textWhite,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15), topRight: Radius.circular(15))),
          builder: (context) {
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Form(
                    key: _accountDetailsKey,
                    autovalidateMode: AutovalidateMode.always,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      child: matchFoundObj.myCurrency != 'NGN'
                          ? MultiBlocProvider(
                              providers: [
                                  BlocProvider<CreateOrderScreenBloc>(
                                      create: (context) =>
                                          CreateOrderScreenBloc()),
                                  BlocProvider<EditOrderScreenBloc>(
                                      create: (context) =>
                                          EditOrderScreenBloc()),
                                  BlocProvider<CurrencyConversionBloc>(
                                      create: (context) =>
                                          CurrencyConversionBloc()),
                                  BlocProvider<InstantMatchScreenBloc>(
                                      create: (context) =>
                                          InstantMatchScreenBloc()),
                                  BlocProvider<PaymentSummaryStateBloc>(
                                    create: (context) =>
                                        PaymentSummaryStateBloc(),
                                  ),
                                  BlocProvider<MatchFoundScreenBloc>(
                                    create: (context) => MatchFoundScreenBloc(),
                                  ),
                                ],
                              child: Container(
                                height: Get.height / 2,
                                child: ForeignDestinationAccount(
                                    context: context,
                                    banks: banks,
                                    // editOrderObj: ,
                                    matchFoundObj: matchFoundObj,
                                    isInstantMatch: true),
                              ))
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                bottomSheetHeaderWidget(
                                    context,
                                    'Destination Account Details',
                                    true,
                                    "This is where Anelloh pays your money to"),
                                DropdownSearch<String>(
                                  mode: Mode.BOTTOM_SHEET,
                                  label: 'Bank Name',
                                  showSelectedItem: true,
                                  items: _buildDropdownSearch(banks: banks),
                                  onChanged: (value) {
                                    _updateSelectedBank(value);
                                  },
                                  maxHeight: 460,
                                  showSearchBox: true,
                                  searchBoxDecoration: InputDecoration(
                                      labelText: 'Search for bank'),
                                  selectedItem: this.selectedBank.isNotEmpty
                                      ? this.selectedBank
                                      : banks[0].name,
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                AppTextInputs(
                                  textInputTitle: 'Account Number',
                                  controller: _accountNumberController,
                                  textInputType: TextInputType.number,
                                  isOnlyDigits: true,
                                  validateInput: matchFoundObj.myCurrency !=
                                          'NGN'
                                      ? AppConstants.validators.validateNumber
                                      : AppConstants.validators
                                          .validateNigerianAccountNumber,
                                  onChange: matchFoundObj.myCurrency != 'NGN'
                                      ? (value) async {
                                          setState(() {
                                            hasVerifiedAccountNumber = 2;
                                          });
                                        }
                                      : (value) async {
                                          try {
                                            setState(() {
                                              hasVerifiedAccountNumber = 1;
                                            });
                                            if (value.length == 10) {
                                              String sb =
                                                  this.selectedBank.isNotEmpty
                                                      ? this.selectedBank
                                                      : banks[0].name;
                                              Bank bank = banks.firstWhere(
                                                  (Bank bank) =>
                                                      bank.name == sb);
                                              Dio.Response response = await dio.get(
                                                  'https://api.paystack.co/bank/resolve?account_number=${_accountNumberController.text}&bank_code=${bank.code}',
                                                  options: Dio.Options(
                                                      headers: {
                                                        'Authorization':
                                                            'Bearer ${env['PAYSTACK_LIVE_SECRET_KEY']}'
                                                      }));
                                              if (response.statusCode == 200) {
                                                if (response.data['status'] ==
                                                    true) {
                                                  setState(() {
                                                    accountName =
                                                        response.data['data']
                                                            ['account_name'];
                                                    hasVerifiedAccountNumber =
                                                        2;
                                                  });
                                                }
                                              } else {
                                                setState(() {
                                                  accountName =
                                                      'Invalid account number';
                                                  hasVerifiedAccountNumber = 3;
                                                });
                                              }
                                            }
                                          } catch (err) {
                                            if (err is Dio.DioError) {
                                              print(err.response);
                                              print(err.response.data);
                                              setState(() {
                                                accountName = err
                                                    .response.data['message'];
                                                hasVerifiedAccountNumber = 3;
                                              });
                                            }
                                          }
                                        },
                                ),
                                matchFoundObj.myCurrency != 'NGN'
                                    ? AppTextInputs(
                                        textInputTitle: 'Routing Number',
                                        controller: _routingNumberController,
                                        textInputType: TextInputType.number,
                                        isOnlyDigits: true,
                                      )
                                    : SizedBox(),
                                SizedBox(
                                  height: 1,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  child: Text(
                                    '$accountName',
                                    style: TextStyle(
                                        color: hasVerifiedAccountNumber == 2
                                            ? Colors.green
                                            : Colors.red),
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                AnellohRoundNetworkButtonLarge(
                                  onPressed: () {
                                    if (hasVerifiedAccountNumber != 2) {
                                      return () {};
                                    }
                                    _createAndMatchExistingOrder(
                                        matchFoundObj: matchFoundObj);
                                  },
                                  childText: hasVerifiedAccountNumber == 2
                                      ? 'Proceed'
                                      : hasVerifiedAccountNumber == 1
                                          ? 'Verifying account number...'
                                          : 'Awaiting account number verification...',
                                  isColorInverted: false,
                                  isLoading: false,
                                )
                              ],
                            ).paddingSymmetric(horizontal: 10),
                    ),
                  ),
                ),
              );
            });
          });
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<MainUserOrderBloc, MainUserOrderState>(
            listener: (context, state) {
          if (state is CancellingUserPendingStateChanged) {
            log('_loadingOverlay 1');

            _loadingOverlay = true;
          }

          if (state is GettingCheckConfigurationFee) {

            _loadingOverlay = true;
          }

          if (state is GettingCheckConfigurationFeeSuccessful) {

            _configurationFee = state.configurationFee;
          }

          if (state is CancellingUserPendingOrderSuccessful ||
              state is CancellingUserPendingOrderFailed) {
            _loadingOverlay = false;
          }
          if (state is CheckingUserPendingOrderSuccessful) {
            if (state.pendingOrders.length > 0) {
              log('state?.pendingOrders[0]?.order?.id ${state?.pendingOrders[0]?.order?.id}');
              currentCustomerPendingOrderId =
                  state?.pendingOrders[0]?.order?.id.toString();
            }
          }
          if (state is CancellingUserPendingOrderSuccessful) {
            AppConstants.userLatestOrderStatus = '4';
            _fetchCurrentCustomerUpdatedOrderAndAcceptMatch(
                state.tempMatchFoundObj, state.tempBanks);
          }
        }),
        BlocListener<ListingsActionBloc, ListingsActionState>(
            listener: (context, state) {
          if (state is FetchingListingById) {
            log('_loadingOverlay 2');

            _loadingOverlay = true;
          }

          if (state is SuccessFullyFetchedListingById ||
              state is ErrorFetchingListingById) {
            _loadingOverlay = false;
          }
        }),
        BlocListener<UserBloc, UserState>(listener: (context, state) {
          if (state is FetchingCustomerById) {
            log('_loadingOverlay 3');

            _loadingOverlay = true;
          }

          if (state is SuccessfullyFetchedCustomerById ||
              state is ErrorFetchingCustomerById) {
            _loadingOverlay = false;
          }
        }),
        BlocListener<InstantMatchScreenBloc, InstantMatchScreenState>(
          listener: (context, state) {
            if (state is MatchingExistingOrder) {
              log('_loadingOverlay 4');

              _loadingOverlay = true;
            }
            if (state is SuccessfullyMatchedExistingOrder) {
              Navigator.of(context).pushReplacementNamed(
                '/payment_summary',
                arguments: {
                  'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                      customerId: state.paymentSummaryOrderCard.customerId,
                      orderNo: state.paymentSummaryOrderCard.orderNo,
                      myAmount: state.paymentSummaryOrderCard.myAmount,
                      totalAmount: state.paymentSummaryOrderCard.totalAmount,
                      myCurrency: state.paymentSummaryOrderCard.myCurrency,
                      rate: state.paymentSummaryOrderCard.rate,
                      convertedAmount:
                          state.paymentSummaryOrderCard.convertedAmount,
                      convertedCurrency:
                          state.paymentSummaryOrderCard.convertedCurrency,
                      transactionFee:
                          state.paymentSummaryOrderCard.transactionFee),
                },
              );
            }
            if (state is ErrorMatchingExistingOrder) {
              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body: state.errorMessage,
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
            if (state is MatchExistingOrderSocketException) {
              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body:
                          'Oops, please check your internet connection,then retry',
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
            if (state is MatchExistingOrderHttpException) {
              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body: 'Service currently unavailable',
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
          },
        )
      ],
      child: BlocBuilder<InstantMatchScreenBloc, InstantMatchScreenState>(
          builder: (context, state) {
        return BlocBuilder<UserBloc, UserState>(
          builder: (context, state) {
            return BlocBuilder<ListingsActionBloc, ListingsActionState>(
              builder: (context, state) {
                return BlocBuilder<MainUserOrderBloc, MainUserOrderState>(
                  builder: (context, state) {
                    return LoadingOverlay(
                      isLoading: _loadingOverlay ? true : false,
                      progressIndicator: ProgressWidget(),
                      color: ColorPalette().textBlack,
                      opacity: 0.4,
                      child: StarterWidget(
                        hasBackButton: true,
                        hasBgimage: false,
                        hasBottomNav: false,
                        hasNavDrawer: false,
                        hasPagePadding: true,
                        hasRefreshOption: false,
                        hasTopWidget: false,
                        isBgAllowed: false,
                        isFabAllowed: false,
                        isExitApp: true,
                        bottomPageContent: _bottomPageContent(context: context),
                      ),
                    );
                  },
                );
              },
            );
          },
        );
      }),
    );
  }

  _bottomPageContent({@required BuildContext context}) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              subPageTitle(
                'Order Details',
                'View more information about your preferred order',
                context: context,
              ),
              SizedBox(
                height: 20,
              ),
              ConstrainedBox(
                constraints: BoxConstraints(),
                child: BlocBuilder<UserBloc, UserState>(
                  builder: (context, state) {
                    log('UserState $state');
                    if (state is FetchingCustomerById) {
                      return Container(
                          constraints: BoxConstraints(maxHeight: 40),
                          margin: EdgeInsets.only(top: 5, bottom: 5),
                          child: PlaceholderLines(
                            count: 1,
                            animate: true,
                            color: Colors.grey.withOpacity(0.003),
                            lineHeight: 10,
                          )
                          // Platform.isIOS
                          //     ? CupertinoActivityIndicator()
                          //     : CircularProgressIndicator(
                          //         backgroundColor: ColorPalette().main,
                          //       ),
                          );
                    } else if (state is SuccessfullyFetchedCustomerById) {
                      return Container(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            CustomText(
                              title: 'By: @${state.orderCustomer.userName}',
                              textSize: 18,
                              isCenter: true,
                              textColor: ColorPalette().dangerRed,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      );
                    } else if (state is SaveCurrentUser) {
                      _fetchSingleUserOfListing();
                      return SizedBox();
                    } else {
                      return Platform.isIOS
                          ? CupertinoActivityIndicator()
                          : CircularProgressIndicator(
                              backgroundColor: ColorPalette().main,
                            );
                    }
                  },
                ),
              ),
              SizedBox(
                height: 10,
              ),
              BlocBuilder<ListingsActionBloc, ListingsActionState>(
                builder: (context, state) {
                  if (state is FetchingListingById) {
                    return PlaceholderLines(
                      count: 5,
                      animate: true,
                      color: Colors.grey.withOpacity(0.003),
                      lineHeight: 10,
                    );
                    // return Platform.isIOS
                    //     ? CupertinoActivityIndicator()
                    //     : CircularProgressIndicator(
                    //         backgroundColor: ColorPalette().main,
                    //       );
                  } else if (state is SuccessFullyFetchedListingById) {
                    _processingFee = ((_configurationFee/100) * double.parse(state.listingObj.matchFoundObj.convertedAmount));
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                PaymentItemWidget(
                                    context,
                                    'The currency you want',
                                    '${state.listingObj.matchFoundObj.myCurrency}',
                                    false)
                                // Expanded(
                                //   child: CustomText(
                                //     title: 'Currency',
                                //     textSize: 14,
                                //   ),
                                // ),
                                // Expanded(
                                //   child: CustomText(
                                //     title:
                                //         '${state.listingObj.matchFoundObj.myCurrency}',
                                //   ),
                                // ),
                              ],
                            ),
                          ),
                          // SizedBox(
                          //   height: 10,
                          // ),
                          // Divider(),
                          // SizedBox(
                          //   height: 10,
                          // ),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              PaymentItemWidget(
                                  context,
                                  'You will receive',
                                  '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.myCurrency, amount: formatToCurrency(state.listingObj.matchFoundObj.myAmount))}',
                                  false),
                              // Expanded(
                              //   child: CustomText(
                              //     title: 'Amount',
                              //     textSize: 14,
                              //   ),
                              // ),
                              // Expanded(
                              //   child: CustomText(
                              //     title:
                              //         '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.myCurrency, amount: formatToCurrency(state.listingObj.matchFoundObj.myAmount))}',
                              //     textSize: 16,
                              //     isBold: true,
                              //     fontWeight: FontWeight.w800,
                              //   ),
                              // ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              PaymentItemWidget(
                                  context,
                                  'You will pay',
                                  '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.convertedCurrency, amount: formatToCurrency(state.listingObj.matchFoundObj.convertedAmount))}',
                                  false),
                              // Expanded(
                              //   child: CustomText(
                              //     title: 'Amount',
                              //     textSize: 14,
                              //   ),
                              // ),
                              // Expanded(
                              //   child: CustomText(
                              //     title:
                              //         '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.myCurrency, amount: formatToCurrency(state.listingObj.matchFoundObj.myAmount))}',
                              //     textSize: 16,
                              //     isBold: true,
                              //     fontWeight: FontWeight.w800,
                              //   ),
                              // ),
                            ],
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              PaymentItemWidget(
                                  context,
                                  '@ the Rate of',
                                  '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.myCurrency == 'NGN' ? state.listingObj.matchFoundObj.myCurrency : state.listingObj.matchFoundObj.convertedCurrency, amount: state.listingObj.matchFoundObj.exchangeRate)}/${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.convertedCurrency == 'NGN' ? state.listingObj.matchFoundObj.myCurrency : state.listingObj.matchFoundObj.convertedCurrency, amount: '')}',
                                  false),
                              // Expanded(
                              //   child: CustomText(
                              //     title: 'Rate',
                              //     textSize: 14,
                              //   ),
                              // ),
                              // Expanded(
                              //   child: CustomText(
                              //     title:
                              //         '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.myCurrency == 'NGN' ? state.listingObj.matchFoundObj.myCurrency : state.listingObj.matchFoundObj.convertedCurrency, amount: state.listingObj.matchFoundObj.exchangeRate)}/${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.convertedCurrency == 'NGN' ? state.listingObj.matchFoundObj.myCurrency : state.listingObj.matchFoundObj.convertedCurrency, amount: '')}',
                              //     textSize: 16,
                              //     isBold: true,
                              //     fontWeight: FontWeight.w800,
                              //   ),
                              // ),
                            ],
                          ),
                          // Column(
                          //   crossAxisAlignment: CrossAxisAlignment.start,
                          //   mainAxisAlignment: MainAxisAlignment.start,
                          //   mainAxisSize: MainAxisSize.max,
                          //   children: [
                          //     PaymentItemWidget(
                          //         context,
                          //         'Subtotal',
                          //     '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.myCurrency, amount: formatToCurrency(state.listingObj.matchFoundObj.subTotal))}',
                          //         false),
                          //
                          //   ],
                          // ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              PaymentItemWidget(
                                  context,
                                  'Processing Fee',
                              '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.convertedCurrency, amount: formatToCurrency(_processingFee.toString()))}',
                                  false),

                            ],
                          ),

                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              PaymentItemWidget(
                                  context,
                                  'Total',
                                  '${CurrencyFormatter.format(currencyCode: state.listingObj.matchFoundObj.convertedCurrency, amount: formatToCurrency((_processingFee + double.parse(state.listingObj.matchFoundObj.convertedAmount)).toString()))}',
                                  false),

                            ],
                          ),

                          SizedBox(
                            height: 30,
                          ),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              Expanded(
                                child: InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Container(
                                    padding: EdgeInsets.all(12),
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: Colors.white,
                                        border: Border.all(
                                          color: ColorPalette().main,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(17))),
                                    child: CustomText(
                                      title: 'Reject',
                                      isBold: true,
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              BlocBuilder<AllBanksBloc, AllBanksState>(
                                  builder: (context, allBanksState) {
                                if (allBanksState
                                    is SuccessfullyFetchedAllBanks) {
                                  return Expanded(
                                    child: AnellohRoundNetworkButtonLarge(
                                      isLoading: false,
                                      onPressed: () {
                                        tempMatchFoundObj =
                                            state.listingObj.matchFoundObj;
                                        tempBanks = allBanksState
                                                is SuccessfullyFetchedAllBanks
                                            ? allBanksState.banks
                                            : [];

                                        _acceptMatch(
                                            matchFoundObj:
                                                state.listingObj.matchFoundObj,
                                            banks: allBanksState
                                                    is SuccessfullyFetchedAllBanks
                                                ? allBanksState.banks
                                                : [],
                                            context: context);
                                      },
                                      childText: 'Swap',
                                    ),
                                  );
                                } else {
                                  return Expanded(
                                      child: AnellohRoundNetworkButtonLarge(
                                    isLoading: false,
                                    onPressed: _fetchBanks,
                                    childText: allBanksState is FetchingBanks
                                        ? 'Please wait...'
                                        : 'Retry',
                                  ));
                                }
                              }),
                            ],
                          )
                        ],
                      ),
                    );
                  } else {
                    return InkWell(
                      onTap: () {
                        _fetchListingById();
                      },
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                children: [
                                  SizedBox(
                                    height: 50,
                                  ),
                                  CustomText(
                                    title: 'Failed to fetch',
                                    textSize: 12,
                                  ),
                                  CustomText(
                                    title: 'RETRY',
                                    textSize: 15,
                                    isBold: true,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  _cancelPendingOrder() async {
    var resultEntity;
    if (await readFromPref('__anelloh_current_user__')) {
      resultEntity = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    log("Hello Order Cancel $currentCustomerPendingOrderId");

    BlocProvider.of<MainUserOrderBloc>(context).add(CancelPendingOrderEvent(
        customerId: resultEntity['customerId'].toString(),
        orderNo: currentCustomerPendingOrderId,
        tempMatchFoundObj: tempMatchFoundObj,
        tempBanks: tempBanks));
  }

  _fetchCurrentCustomerPendingOrder() async {
    var user;
    if (await readFromPref('__anelloh_current_user__')) {
      user = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    BlocProvider.of<MainUserOrderBloc>(context)
        .add(CheckUserPendingOrderEvent(customerId: user['customerId']));
  }

  _fetchCurrentCustomerUpdatedOrderAndAcceptMatch(
      MatchFoundObj tempMatchFoundObj, List<dynamic> tempBanks) async {
    await _fetchCurrentCustomerPendingOrder();

    _acceptMatch(
        matchFoundObj: tempMatchFoundObj, banks: tempBanks, context: context);
  }

  void _fetchConfigurationFee() {
    BlocProvider.of<MainUserOrderBloc>(context)
        .add(CheckConfigurationFeeEvent());
  }
}
