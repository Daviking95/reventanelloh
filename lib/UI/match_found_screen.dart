import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/UI/payment_options_screen.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_bloc.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_event.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_state.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_event.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_state.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_event.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/bottom_sheet_widgets.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

import 'foreign_destination_account.dart';

class MatchFoundScreen extends StatefulWidget {
  final MatchFoundObj matchFoundObj;

  MatchFoundScreen({@required this.matchFoundObj});

  _MatchFoundScreenState createState() => _MatchFoundScreenState();

  static _MatchFoundScreenState of(BuildContext context) =>
      context.findAncestorStateOfType<_MatchFoundScreenState>();
}

class _MatchFoundScreenState extends State<MatchFoundScreen> {
  String currentCustomerPendingOrderId;

  List tempBanks;

  @override
  void initState() {
    super.initState();
    _fetchBanks();

    _fetchCurrentCustomerPendingOrder();

    // log('AppConstants.userLatestOrderStatus match ${AppConstants.userLatestOrderStatus}');
  }

  GlobalKey<FormState> _accountDetailsKey = GlobalKey<FormState>();
  var customState;

  _fetchNigerianBanks() {
    BlocProvider.of<AllBanksBloc>(context).add(GetAllNigerianBanksEvent());
  }

  _fetchOtherBanks() {
    BlocProvider.of<AllBanksBloc>(context).add(GetOtherBanksEvent());
  }

  _fetchBanks() {
    if (widget.matchFoundObj.convertedCurrency == 'NGN') {
      _fetchNigerianBanks();
    } else {
      _fetchOtherBanks();
    }
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<MainUserOrderBloc, MainUserOrderState>(
            listener: (context, state) {
              if (state is CheckingUserPendingOrderSuccessful) {
                if (state.pendingOrders.length > 0) {
                  log('state?.pendingOrders[0]?.order?.id ${state?.pendingOrders[0]?.order?.id}');
                  currentCustomerPendingOrderId =
                      state?.pendingOrders[0]?.order?.id.toString();
                }
              }
              if (state is CancellingUserPendingOrderSuccessful) {

                AppConstants.userLatestOrderStatus = '4';
                _fetchCurrentCustomerUpdatedOrderAndAcceptMatch();

              }
            }),
        BlocListener<MatchFoundScreenBloc, MatchFoundScreenState>(
          listener: (context, state) {
            log('Printing parent state $state');

            if (state is AcceptMatchFound) {
              Navigator.of(context).pushNamed('/payment_info',
                  arguments: {'matchFoundObj': widget.matchFoundObj});
            }
            if (state is MoveToDashboardScreen) {
              Navigator.of(context).popAndPushNamed('/dashboard');
            }
            // if (state is RejectMatchFound) {
            //   print('Move to payment info page');
            //   Navigator.of(context).pushNamed('/listings');
            // }
            if (state is MoveToCreateOrderScreen) {
              Navigator.of(context).popAndPushNamed('/create_order');
            }
            if (state is CreateAndMatchOrderSuccessful) {
              return anellohOverlay(
                  overlayObject: OverlayObject(
                    title: 'Notice',
                    body:
                        'You have 12 hours to make payment or your match will be cancelled.',
                    hasBtn: true,
                    hasCancelBtn: true,
                    clearToNavigate: true,
                    ctnFunction: () {
                      // move to dashboard
                    },
                    btnFunction: () {
                      // go to payment summary page...
                      Navigator.of(context)
                          .pushReplacementNamed('/payment_summary', arguments: {
                        'matchFoundObj': state.matchFoundObj,
                        'orderNo': state.customerOrderNo,
                        'customerId': state.customerId,
                        'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                            customerId: state.matchFoundObj.customerId,
                            orderNo: state.matchFoundObj.orderNo,
                            myAmount: state.matchFoundObj.myAmount,
                            totalAmount: state.matchFoundObj.totalAmount,
                            myCurrency: state.matchFoundObj.myCurrency,
                            rate: state.matchFoundObj.exchangeRate,
                            convertedCurrency:
                                state.matchFoundObj.convertedCurrency,
                            convertedAmount:
                                state.matchFoundObj.convertedAmount,
                            transactionFee: state.matchFoundObj.processingFee),
                      });
                    },
                    btnText: 'Pay Now',
                    cancelBtnText: 'Pay Later',
                    cancelBtnFunction: () {
                      Navigator.of(context)
                          .pushReplacementNamed(RoutesConstants.dashboardUrl);
                    },
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    ),
                  ),
                  context: context);
            }
            if (state is CreateAndMatchOrderFailed) {
              return anellohOverlay(
                  overlayObject: OverlayObject(
                    title: 'Error',
                    body: '${state.errorMessage}',
                    hasBtn: true,
                    hasCancelBtn: false,
                    clearToNavigate: true,
                    ctnFunction: () {
                      // move to dashboard
                      _moveToDashboard(context: context);
                    },
                    btnFunction: () {
                      _moveToDashboard(context: context);
                    },
                    btnText: 'Ok',
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    ),
                  ),
                  context: context);
            }
          },
        )
      ],
      child: BlocBuilder<MatchFoundScreenBloc, MatchFoundScreenState>(
        builder: (context, state) {
          return BlocBuilder<MainUserOrderBloc, MainUserOrderState>(
  builder: (context, state) {
    return LoadingOverlay(
            child: StarterWidget(
              hasBackButton: true,
              hasBottomNav: false,
              hasTopWidget: false,
              bottomPageContent: _bottomPageContent(
                matchFoundObj: widget.matchFoundObj,
                context: context,
                accountDetailsKey: _accountDetailsKey,
              ),
            ),
            isLoading: state is CreatingAndMatchingOrder ||
                    customState is CreatingAndMatchingOrder || state is CancellingUserPendingStateChanged
                ? true
                : false,
            progressIndicator: ProgressWidget(),
            color: ColorPalette().textBlack,
            opacity: 0.4,
          );
  },
);
        },
      ),
    );
  }

  _moveToDashboard({@required BuildContext context}) {
    BlocProvider.of<MatchFoundScreenBloc>(context)
        .add(MoveToDashboardScreenEvent());
  }

  _acceptMatchFound(
      {@required BuildContext context, @required List<Bank> banks}) {
    try {
      // log('AppConstants.userLatestOrderStatus ${AppConstants.userLatestOrderStatus}');

      if (AppConstants.userLatestOrderStatus != null &&
          AppConstants.userLatestOrderStatus.toString() == '1' ||
          AppConstants.userLatestOrderStatus.toString() == '5' ||
          AppConstants.userLatestOrderStatus.toString() == '7') {
        return anellohOverlay(
            overlayObject: OverlayObject(
              clearToNavigate: true,
              title: 'Error',
              body:
              // 'You cannot continue with this swap because you have an open order. Click yes to modify order with this card or no to cancel',
              'You cannot swap now as an opened or unfulfilled or cancelled order already exists for you. Please modify the opened order to continue',
              hasBtn: false,
              hasCancelBtn: false,
              btnText: 'Yes',
              btnFunction: () => _cancelPendingOrder(),
              cancelBtnText: 'No',
              icon: Icon(
                Icons.warning,
                color: Colors.red,
              ),
            ),
            context: context);
      }

      return showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          isDismissible:
          customState is CreatingAndMatchingOrder ? false : true,
          backgroundColor: ColorPalette().textWhite,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15),
                  topRight: Radius.circular(15))),
          builder: (context) {
            return MultiBlocProvider(
              providers: [
                BlocProvider<MatchFoundScreenBloc>(
                    create: (context) => MatchFoundScreenBloc()),
                BlocProvider<AllBanksBloc>(
                    create: (context) => AllBanksBloc()),
              ],
              child: DestinationAccountWidget(
                widget.matchFoundObj,
                banks,
                _runCallBackParentFunc,
              ),
            );
          });
    } catch (err) {
      print(err);
    }
  }

  _rejectMatchFound() {
    return anellohOverlay(
        overlayObject: OverlayObject(
            title: 'Notice',
            body: 'Are you sure you want to reject this match?',
            hasBtn: true,
            btnText: 'Yes',
            hasCancelBtn: true,
            btnFunction: () {
              BlocProvider.of<MatchFoundScreenBloc>(context)
                  .add(MoveToCreateOrderScreenEvent());
            },
            icon: Icon(
              Icons.warning,
              color: Colors.red,
            )),
        context: context);
  }

  Widget _bottomPageContent({
    @required MatchFoundObj matchFoundObj,
    @required BuildContext context,
    @required GlobalKey<FormState> accountDetailsKey,
  }) {

    return SafeArea(
      maintainBottomViewPadding: true,
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 40.0, horizontal: 17.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Center(
                  child: subPageTitle('Order matched', 'Your order has been matched',
                      context: context),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomText(
                  title: 'You may now complete your transaction',
                  isCenter: false,
                ),
                SizedBox(
                  height: 10.0,
                ),
              ],
            ),
            SizedBox(
              height: 40.0,
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  subPageTitle('Summary', 'View information about your match', context: context),
                  SizedBox(
                    height: 10.0,
                  ),
                  PaymentItemWidget(
                      context,
                      'You will receive',
                      '${CurrencyFormatter.format(currencyCode: matchFoundObj.convertedCurrency, amount: formatToCurrency(matchFoundObj.convertedAmount))}',
                      false),
                  PaymentItemWidget(
                      context,
                      '@ Rate',
                      '${CurrencyFormatter.format(currencyCode: matchFoundObj.myCurrency == 'NGN' ? matchFoundObj.myCurrency : matchFoundObj.convertedCurrency, amount: matchFoundObj.exchangeRate)}/${CurrencyFormatter.format(currencyCode: matchFoundObj.convertedCurrency == 'NGN' ? matchFoundObj.myCurrency : matchFoundObj.convertedCurrency, amount: '')}',
                      false),
                PaymentItemWidget(
                    context,
                    'Processing Fee',
                    '${CurrencyFormatter.format(currencyCode: matchFoundObj.myCurrency, amount: formatToCurrency(matchFoundObj.processingFee))}',
                    false),
                  PaymentItemWidget(
                      context,
                      'You will pay',
                      '${CurrencyFormatter.format(currencyCode: matchFoundObj.myCurrency, amount: formatToCurrency(matchFoundObj.totalAmount))}',
                      false),
                  SizedBox(
                    height: 30.0,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: InkWell(
                          onTap: _rejectMatchFound,
                          child: Container(
                            padding: EdgeInsets.all(12),
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                border: Border.all(
                                  color: ColorPalette().main,
                                ),
                                borderRadius:
                                    BorderRadius.all(Radius.circular(17))),
                            child: CustomText(
                              title: 'Reject',
                              isBold: true,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        width: 10.0,
                      ),
                      BlocBuilder<MatchFoundScreenBloc, MatchFoundScreenState>(
                        builder: (context, state) {
                          return BlocBuilder<AllBanksBloc, AllBanksState>(
                            builder: (context, allBanksState) {
                              if (allBanksState
                                  is SuccessfullyFetchedAllBanks) {
                                return Expanded(
                                    child: AnellohRoundNetworkButtonLarge(
                                        onPressed: () {
                                          
                                          tempBanks = allBanksState
                                          is SuccessfullyFetchedAllBanks
                                              ? allBanksState.banks
                                              : [];
                                          
                                          _acceptMatchFound(
                                              banks: allBanksState
                                                      is SuccessfullyFetchedAllBanks
                                                  ? allBanksState.banks
                                                  : [],
                                              context: context);
                                        },
                                        childText: 'Accept',
                                        isLoading:
                                            state is CreatingAndMatchingOrder
                                                ? true
                                                : false));
                              } else {
                                return Expanded(
                                    child: AnellohRoundNetworkButtonLarge(
                                  isLoading: false,
                                  onPressed: _fetchBanks,
                                  childText: allBanksState is FetchingBanks
                                      ? 'Please wait...'
                                      : 'Retry',
                                ));
                              }
                            },
                          );
                        },
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  _runCallBackParentFunc(val) {
    customState = val;
  }

  _cancelPendingOrder() async {
    var resultEntity;
    if (await readFromPref('__anelloh_current_user__')) {
      resultEntity = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    log("Hello Order Cancel $currentCustomerPendingOrderId");

    BlocProvider.of<MainUserOrderBloc>(context).add(CancelPendingOrderEvent(
        customerId: resultEntity['customerId'].toString(),
        orderNo: currentCustomerPendingOrderId));
  }

  _fetchCurrentCustomerPendingOrder() async {
    var user;
    if (await readFromPref('__anelloh_current_user__')) {
      user = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    BlocProvider.of<MainUserOrderBloc>(context)
        .add(CheckUserPendingOrderEvent(customerId: user['customerId']));
  }

  void _fetchCurrentCustomerUpdatedOrderAndAcceptMatch() async{
    await _fetchCurrentCustomerPendingOrder();

    _acceptMatchFound(
        banks: tempBanks,
        context: context);
    
  }
}

class DestinationAccountWidget extends StatefulWidget {
  final MatchFoundObj matchFoundObj;
  final List<Bank> banks;
  final Function callback;

  DestinationAccountWidget(this.matchFoundObj, this.banks, this.callback);

  @override
  _DestinationAccountWidgetState createState() =>
      _DestinationAccountWidgetState();

  static _DestinationAccountWidgetState of(BuildContext context) =>
      context.findAncestorStateOfType<_DestinationAccountWidgetState>();
}

class _DestinationAccountWidgetState extends State<DestinationAccountWidget> {
  int hasVerifiedAccountNumber;
  String accountName;
  GlobalKey<FormState> accountDetailsKey = GlobalKey<FormState>();
  String selectedBank = '';
  TextEditingController _accountNumberController = TextEditingController();
  TextEditingController _routingNumberController = TextEditingController();
  var customState;

  _updateSelectedBank(value) {
    setState(() {
      selectedBank = value;
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // customState = InitializeMatchFoundState();
    setState(() {
      selectedBank = widget.banks[0].name;
      accountName = '';
    });
  }

  List<String> _buildDropdownSearch({@required List<Bank> banks}) {
    return banks.map((e) => e.name).toList();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<MatchFoundScreenBloc, MatchFoundScreenState>(
      listener: (context, state) {
        if (state is CreatingAndMatchingOrder) {
          customState = state;
        }
        if (state is AcceptMatchFound) {
          Navigator.of(context).pushNamed('/payment_info',
              arguments: {'matchFoundObj': widget.matchFoundObj});
        }
        if (state is MoveToDashboardScreen) {
          Navigator.of(context).popAndPushNamed('/dashboard');
        }
        // if (state is RejectMatchFound) {
        //   print('Move to payment info page');
        //   Navigator.of(context).pushNamed('/listings');
        // }
        if (state is MoveToCreateOrderScreen) {
          Navigator.of(context).popAndPushNamed('/create_order');
        }
        if (state is CreateAndMatchOrderSuccessful) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Notice',
                body:
                    'You have 12 hours to make payment or your match will be cancelled.',
                hasBtn: true,
                hasCancelBtn: true,
                clearToNavigate: true,
                ctnFunction: () {
                  // move to dashboard
                },
                btnFunction: () {
                  // go to payment summary page...
                  log('result entity orderNo 2 ${state.customerOrderNo}');
                  log('result entity customerId 2 ${state.customerId}');
                  Navigator.of(context)
                      .pushReplacementNamed('/payment_summary', arguments: {
                    'matchFoundObj': state.matchFoundObj,
                    'orderNo': state.customerOrderNo,
                    'customerId': state.customerId,
                    'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                        customerId: state.matchFoundObj.customerId,
                        orderNo: state.matchFoundObj.orderNo,
                        myAmount: state.matchFoundObj.myAmount,
                        totalAmount: state.matchFoundObj.totalAmount,
                        myCurrency: state.matchFoundObj.myCurrency,
                        rate: state.matchFoundObj.exchangeRate,
                        convertedCurrency:
                            state.matchFoundObj.convertedCurrency,
                        convertedAmount: state.matchFoundObj.convertedAmount,
                        transactionFee: state.matchFoundObj.processingFee),
                  });
                },
                btnText: 'Pay Now',
                cancelBtnText: 'Pay Later',
                cancelBtnFunction: () {
                  Navigator.of(context)
                      .pushReplacementNamed(RoutesConstants.dashboardUrl);
                },
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
        if (state is CreateAndMatchOrderFailed) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Error',
                body: '${state.errorMessage}',
                hasBtn: true,
                hasCancelBtn: false,
                clearToNavigate: true,
                ctnFunction: () {
                  // move to dashboard
                  BlocProvider.of<MatchFoundScreenBloc>(context)
                      .add(MoveToDashboardScreenEvent());
                },
                btnFunction: () {
                  BlocProvider.of<MatchFoundScreenBloc>(context)
                      .add(MoveToDashboardScreenEvent());
                },
                btnText: 'Ok',
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
      },
      child: BlocBuilder<MatchFoundScreenBloc, MatchFoundScreenState>(
        builder: (context, state) {
          log('What is state builder $customState');

          state = customState;
          widget.callback(customState);

          return LoadingOverlay(
            isLoading: state is CreatingAndMatchingOrder ? true : false,
            progressIndicator: ProgressWidget(),
            color: ColorPalette().textBlack,
            opacity: 0.4,
            child: SingleChildScrollView(
              child: Container(
                padding: EdgeInsets.only(
                    bottom: MediaQuery.of(context).viewInsets.bottom),
                child: Form(
                  key: accountDetailsKey,
                  autovalidateMode: AutovalidateMode.always,
                  child: Container(
                    width: Get.width,
                    padding: EdgeInsets.all(10),
                    child: widget.matchFoundObj.convertedCurrency != 'NGN'
                        ? MultiBlocProvider(
                            providers: [
                                BlocProvider<CreateOrderScreenBloc>(
                                    create: (context) => CreateOrderScreenBloc()),
                                BlocProvider<EditOrderScreenBloc>(
                                    create: (context) => EditOrderScreenBloc()),
                                BlocProvider<CurrencyConversionBloc>(
                                    create: (context) => CurrencyConversionBloc()),
                                BlocProvider<InstantMatchScreenBloc>(
                                    create: (context) => InstantMatchScreenBloc()),
                                BlocProvider<PaymentSummaryStateBloc>(
                                  create: (context) => PaymentSummaryStateBloc(),
                                ),
                                BlocProvider<MatchFoundScreenBloc>(
                                  create: (context) => MatchFoundScreenBloc(),
                                ),
                              ],
                            child: Container(
                              height: Get.height / 2,
                              child: ForeignDestinationAccount(
                                  context: context,
                                  banks: widget.banks,
                                  matchFoundObj: widget.matchFoundObj,
                                  callback: _runCallBackFunc,
                                  isMatchFound: true),
                            ))
                        : Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              bottomSheetHeaderWidget(context, 'Destination Account Details', true, "This is where Anelloh pays your money to"),
                              DropdownSearch<String>(
                                mode: Mode.BOTTOM_SHEET,
                                label: 'Bank Name',
                                showSelectedItem: true,
                                items: _buildDropdownSearch(banks: widget.banks),
                                onChanged: (value) {
                                  _updateSelectedBank(value);
                                },
                                showSearchBox: true,
                                searchBoxDecoration:
                                    InputDecoration(labelText: 'Search for bank'),
                                selectedItem: this.selectedBank.isNotEmpty
                                    ? this.selectedBank
                                    : widget.banks[0].name,
                              ),
                              SizedBox(
                                height: 6,
                              ),
                              AppTextInputs(
                                textInputTitle: 'Account Number',
                                controller: _accountNumberController,
                                textInputType: TextInputType.number,
                                autoFocus: true,
                                validateInput: (value) {
                                  if (widget.matchFoundObj.convertedCurrency ==
                                      'NGN') {
                                    return AppConstants.validators
                                        .validateNigerianAccountNumber(value);
                                  } else {
                                    return AppConstants.validators
                                        .validateNumber(value);
                                  }
                                },
                                isOnlyDigits: true,
                                onChange: (value) async {
                                  print('_accountNumberController $value');
                                  try {
                                    setState(() {
                                      hasVerifiedAccountNumber = 1;
                                    });
                                    if (value.length == 10) {
                                      String sb = this.selectedBank.isNotEmpty
                                          ? this.selectedBank
                                          : widget.banks[0].name;
                                      Bank bank = widget.banks.firstWhere(
                                          (Bank bank) => bank.name == sb);

                                      print(
                                          '_accountNumberController ${bank.code}');

                                      Dio.Response response = await dio.get(
                                          'https://api.paystack.co/bank/resolve?account_number=${_accountNumberController.text}&bank_code=${bank.code}',
                                          options: Dio.Options(headers: {
                                            'Authorization':
                                                'Bearer ${env['PAYSTACK_LIVE_SECRET_KEY']}'
                                          }));

                                      if (response.statusCode == 200) {
                                        if (response.data['status'] == true) {
                                          setState(() {
                                            accountName = response.data['data']
                                                ['account_name'];
                                            hasVerifiedAccountNumber = 2;
                                          });

                                          print(
                                              '_accountNumberController REsponse $accountName');
                                        }
                                      } else {
                                        setState(() {
                                          accountName = 'Invalid account number';
                                          hasVerifiedAccountNumber = 3;
                                        });
                                      }
                                    }
                                  } catch (err) {
                                    if (err is Dio.DioError) {
                                      print(err.response);
                                      print(err.response.data);
                                      setState(() {
                                        accountName = err.response.data['message'];
                                        hasVerifiedAccountNumber = 3;
                                      });
                                    }
                                  }
                                },
                              ),
                              SizedBox(
                                height: 1,
                              ),
                              Padding(
                                padding: EdgeInsets.only(left: 10, right: 10),
                                child: Text(
                                  '$accountName',
                                  style: TextStyle(
                                      color: hasVerifiedAccountNumber == 2
                                          ? Colors.green
                                          : Colors.red),
                                ),
                              ),
                              SizedBox(
                                height: 15,
                              ),
                              AnellohRoundNetworkButtonLarge(
                                  onPressed: () {
                                    // Navigator.pop(context);
                                    if (hasVerifiedAccountNumber != 2) {
                                      return () {};
                                    } else {
                                      _createAndMatchOrder();
                                    }
                                  },
                                  childText: hasVerifiedAccountNumber == 2
                                      ? 'Accept Order'
                                      : hasVerifiedAccountNumber == 1
                                          ? 'Verifying account number...'
                                          : 'Awaiting account number verification...',
                                  isColorInverted: false,
                                  isLoading: state is CreatingAndMatchingOrder
                                      ? true
                                      : false)
                            ],
                          ).paddingSymmetric(horizontal: 10),
                  ),
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  _createAndMatchOrder() {
    try {
      if (accountDetailsKey.currentState.validate()) {
        MatchFoundObj updatedMatchFoundObj = widget.matchFoundObj.copyWith(
          myAccountNumber: _accountNumberController.text,
          myBankName: selectedBank,
          bankRouteNo: _routingNumberController.text,
        );
        // Navigator.pop(context);
        BlocProvider.of<MatchFoundScreenBloc>(context)
            .add(CreateAndMatchOrderEvent(
          matchFoundObj: updatedMatchFoundObj,
          user: AppConstants.userData,
          myPaymentChannelId: 8,
          address: "",
          city: "",
          state: "",
          zipCode: "",
          country: "",
        ));
      }
    } catch (err) {
      print(err);
    }
  }

  _runCallBackFunc(val) {
    setState(() => customState = val);
  }
}
