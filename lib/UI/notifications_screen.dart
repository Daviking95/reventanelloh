import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/notification/notificaition_event.dart';
import 'package:anelloh_mobile/bloc/notification/notification_bloc.dart';
import 'package:anelloh_mobile/bloc/notification/notification_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/date_formatter.dart';
import 'package:anelloh_mobile/styles.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

class NotificationsScreen extends StatefulWidget {
  NotificationsScreen({Key key}) : super(key: key);

  @override
  _NotificationsScreenState createState() => _NotificationsScreenState();
}

class _NotificationsScreenState extends State<NotificationsScreen> {
  List<NotificationsData> notificationList = [];

  @override
  void initState() {
    super.initState();
    notificationList = [];
    _fetchNotificationsData();
  }

  @override
  Widget build(BuildContext context) {
    log('AppConstants.userData ${AppConstants.userData}');

    notificationList = [];
    _fetchNotificationsData();

    return BlocListener<NotificationBloc, NotificationState>(
      listener: (context, state) {
        if (state is SuccessfullyFetchedAllNotifications) {
          notificationList = [];
          notificationList = state.notificationsList.reversed.toList();
        }
        if (state is SuccessfullyUpdatedNotification) {
          Navigator.pushReplacementNamed(context, '/notifications');
          showToast(
              toastMsg: state.message.toString(),
              toastGravity: ToastGravity.CENTER,
              txtColor: ColorPalette().textBlack,
              bgColor: Colors.grey.withOpacity(0.4));
        }
      },
      child: BlocBuilder<NotificationBloc, NotificationState>(
        builder: (context, state) {
          return LoadingOverlay(
            isLoading:
                state is FetchingNotifications || state is UpdatingNotifications
                    ? true
                    : false,
            progressIndicator: ProgressWidget(),
            color: ColorPalette().textBlack,
            opacity: 0.4,
            child: StarterWidget(
              hasRefreshOption: true,
              hasBackButton: true,
              hasTopWidget: false,
              hasBottomNav: false,
              bottomNavIndex: 1,
              hasNavDrawer: false,
              hasPagePadding: true,
              onRefreshFunction: _fetchNotificationsData,
              bottomPageContent: _bottomPageContent(context, notificationList),
            ),
          );
        },
      ),
    );
  }

  Widget _bottomPageContent(
    BuildContext context,
    List<NotificationsData> notificationList,
  ) {
    return BlocBuilder<NotificationBloc, NotificationState>(
        builder: (context, state) {
      return SafeArea(
          maintainBottomViewPadding: false,
          child: SingleChildScrollView(
            padding: EdgeInsets.symmetric(vertical: 3.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    child: subPageTitle('Notifications', 'View all your notifications here', context: context),
                    padding: EdgeInsets.only(bottom: 20)),
                if (state is FetchingNotifications) ...[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                      child: CustomText(
                        title: 'Loading Notification',
                        textColor: ColorPalette().main,
                      ),
                    ),
                  )
                ],
                if (state is SuccessfullyFetchedAllNotifications) ...[
                  if (notificationList.length == 0) ...[
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Center(
                        child: CustomText(
                          title: 'No Notification',
                          textColor: ColorPalette().main,
                        ),
                      ),
                    )
                  ] else ...[
                    Container(
                      height: Get.height,
                      child: ListView.builder(
                          padding: EdgeInsets.symmetric(vertical: 3.0),
                          itemCount: notificationList.length,
                          itemBuilder: (context, idx) {
                            final item = notificationList[idx];
                            return Dismissible(
                              key: Key(item.id.toString()),
                              onDismissed: (direction) {
                                ScaffoldMessenger.of(context)
                                    .showSnackBar(SnackBar(
                                        content: Text(
                                  '${notificationList[idx].message} dismissed',
                                  style: textStyleBlackColorNormal,
                                )));

                                _updateNotificationRead(notificationList[idx]);
                              },
                              background: Container(color: Colors.red),
                              child: listScreenBuilder(
                                context: context,
                                notificationList: notificationList[idx],
                              ),
                            );
                          }),
                    )
                  ],
                ]
              ],
            ),
          ));
    });
  }

  Widget listScreenBuilder({
    @required NotificationsData notificationList,
    @required BuildContext context,
  }) {
    return InkWell(
      onTap: () {
        // _notificationBottomSheet(context, notificationList);
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 5, bottom: 5),
        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 10),
        decoration: BoxDecoration(
            color: ColorPalette().textWhite,
            borderRadius: BorderRadius.circular(5),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                offset: Offset(1, 3),
                spreadRadius: 1.1,
                blurRadius: 3.5,
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
                Image.asset(
                  'assets/images/anelloh_full.png',
                  width: 100,
                ),
                CustomText(
                  title: formatDate(notificationList.createdDate),
                  textSize: 11,
                ),
              ],
            ).paddingSymmetric(vertical: 5),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                CustomText(
                  title: '${notificationList?.name ?? "Anelloh Notification"}',
                  textColor: ColorPalette().main,
                  fontWeight: FontWeight.w900,
                  isBold: true,
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  '${notificationList.message ?? ""}',
                  maxLines: 4,
                  softWrap: false,
                  style: TextStyle(color: Color(0xff262C31)),
                  overflow: TextOverflow.ellipsis,
                ),
                SizedBox(
                  height: 5,
                ),
                Align(
                  alignment: Alignment.centerRight,
                  child: Text(
                    'Swipe left to clear notification',
                    maxLines: 4,
                    softWrap: false,
                    style: TextStyle(color: Colors.grey),
                    overflow: TextOverflow.ellipsis,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
              ],
            )
          ],
        ),
      ).marginSymmetric(horizontal: 5),
    );
  }

  void _notificationBottomSheet(
      BuildContext context, NotificationsData element) {
    showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: ColorPalette().textWhite,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        builder: (context) {
          return Container(
            height: Get.height / 1.3,
            child: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Align(
                        alignment: Alignment.centerRight,
                        child: GestureDetector(
                            onTap: () => Navigator.pop(context),
                            child:
                                SvgPicture.asset('assets/images/close.svg'))),
                    SizedBox(
                      height: 10,
                    ),
                    CustomText(
                      title: element.name ?? "Anelloh Notification",
                      textSize: 18,
                      isBold: true,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    CustomText(
                      title: element.message ?? "",
                      textSize: 13,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    AnellohRoundNetworkButtonLarge(
                      onPressed: () {
                        Navigator.pop(context);

                        _updateNotificationRead(element);
                      },
                      childText: 'Acknowledge Notification',
                      isColorInverted: false,
                      isLoading: false,
                    ),
                  ],
                ).paddingSymmetric(horizontal: 15, vertical: 10),
              ),
            ),
          );
        });
  }

  void _fetchNotificationsData() {
    BlocProvider.of<NotificationBloc>(context).add(
        FetchNotificationsEvent(customerId: AppConstants.userData.customerId));
  }

  void _updateNotificationRead(NotificationsData element) {
    BlocProvider.of<NotificationBloc>(context)
        .add(UpdateNotificationsEvent(notificationData: element));
  }
}
