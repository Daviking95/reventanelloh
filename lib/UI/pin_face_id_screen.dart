
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/custom_profile_nav_btn.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PinAndFaceIdScreen extends StatefulWidget {
  _PinAndFaceIdState createState() => _PinAndFaceIdState();
}

class _PinAndFaceIdState extends State<PinAndFaceIdScreen> {
  bool _hasPinAuthentication = false;
  bool _hasFaceIdAuthentication = false;
  @override
  void initState() {
    super.initState();
    _handleSetupDataState();
  }
  @override
  Widget build(BuildContext context) {
    return StarterWidget(
      hasBackButton: true,
      hasBottomNav: false,
      hasTopWidget: false,
      bottomPageContent: _bottomPageContent(context: context),
    );
  }

  _handleSetupDataState() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    var hasPinAuth = pref.getBool('__has__pin__auth') ?? false;
    var hasFaceId =pref.getBool('__has__face_id__auth') ?? false;
    setState(() {
      _hasPinAuthentication = hasPinAuth;
      _hasFaceIdAuthentication = hasFaceId;
    });
  }

  _handleSetPinAuthentication(value) async{
    setState(() {
      _hasPinAuthentication = value;
    });
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool('__has__pin__auth', value);
  }

  _handleSetFaceAuthentication(value) async{
    setState(() {
      _hasFaceIdAuthentication = value;
    });
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool('__has__face_id__auth', value);
  }

  Widget _bottomPageContent({@required BuildContext context}) {
    return SafeArea(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(15),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child:  pageTitle('Pin and Face ID', 'Your Pin or Face ID'),
              margin: EdgeInsets.only(bottom: 30),
            ),
            ConstrainedBox(
              constraints: BoxConstraints(
                minHeight: 100,
                maxHeight: 100
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child:   Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      CustomText(
                        title: 'Pin code Authentication',
                        textSize: 17,
                        isBold: true,
                      ),
                      CustomText(
                        title: 'Verify transactions by adding an extra layer of security',
                      ),
                    ],
                  ),
                    flex: 5,
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  Switch(
                    value: _hasPinAuthentication,
                    activeColor: ColorPalette().main,
                    onChanged: _handleSetPinAuthentication,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 20,
            ),
            ConstrainedBox(
              constraints: BoxConstraints(
                  minHeight: 100,
                  maxHeight: 100
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child:   Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(
                          title: 'Face Identification',
                          textSize: 17,
                          isBold: true,
                        ),
                        CustomText(
                          title: 'Unlock account using face identification',
                        ),
                      ],
                    ),
                    flex: 5,
                  ),
                  SizedBox(
                    width: 40,
                  ),
                  Switch(
                    value: _hasFaceIdAuthentication,
                    activeColor: ColorPalette().main,
                    onChanged: _handleSetFaceAuthentication,
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 15,
            ),
            customProfileNavButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/change_pin');
                },
                title: 'Change Pin',
                icon: Icon(Icons.vpn_key_outlined, size: 20.0),
                context: context),
          ],
        ),
      ),
    );
  }
}
