import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_event.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_state.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_event.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_state.dart';
import 'package:anelloh_mobile/helpers/classes/active_gateways.dart';
import 'package:anelloh_mobile/helpers/classes/bank_config.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/image_size.dart';
import 'package:anelloh_mobile/helpers/payments/paystack_payment.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/helpers/showDialog.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../constants.dart';
import '../styles.dart';

CustomPaystackPayment customPaystackPayment = new CustomPaystackPayment();

class PaymentOptionsScreen extends StatefulWidget {
  final PaymentSummaryOrderCard paymentSummaryOrderCard;
  final EditOrderObj editOrderObj;
  final String orderNo;
  final String customerId;
  final String sessionId;
  final String paymentType;
  final int retryCount;

  PaymentOptionsScreen({Key key,
    @required this.paymentSummaryOrderCard,
    @required this.editOrderObj,
    this.retryCount,
    this.customerId,
    this.orderNo,
    this.sessionId,
    this.paymentType})
      : super(key: key);

  @override
  _PaymentOptionsScreenState createState() => _PaymentOptionsScreenState();
}

class _PaymentOptionsScreenState extends State<PaymentOptionsScreen> {
  List<ActivePaymentGateways> activePaymentGatewayList = [];

  BankConfigEntity bankConfigEntity;
  EditOrderObj _editOrderObj;

  String myCurrency;

  @override
  void initState() {
    super.initState();

    _editOrderObj = widget.editOrderObj;

    if (AppConstants.instantMatchCurrency == null)
      myCurrency = widget.paymentSummaryOrderCard.myCurrency;
    else
      myCurrency = AppConstants.instantMatchCurrency;

    log('result entity orderNo 4 ${widget.orderNo}');
    log('result entity customerId 4 ${widget.customerId}');
    log('widget?.paymentSummaryOrderCard?.myCurrency ${widget
        ?.paymentSummaryOrderCard?.myCurrency}');
    log(
        'AppConstants.instantMatchCurrency Payment Options Screen 1 ${AppConstants
            .instantMatchCurrency}');
    log('AppConstants.instantMatchCurrency Payment Options Screen 2 ${widget
        ?.paymentSummaryOrderCard?.myCurrency ??
        AppConstants.instantMatchCurrency}');
    log(
        'AppConstants.instantMatchCurrency Payment Options Screen 3 ${myCurrency}');

    // if(widget.sessionId == null || myCurrency == null){
    //   Navigator.of(context).pushReplacementNamed('/dashboard');
    // }

    _getAllPaymentOptions(myCurrency);
    _getBankConfigs(myCurrency);

    if (widget.sessionId != null && widget.paymentType == 'paystack') {
      _verifyPaystackPayment();
    }
  }

  _verifyPaystackPayment() async {
    var resultEntity;
    if (await readFromPref('__anelloh_current_user__')) {
      resultEntity = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    String customerId = resultEntity['customerId'].toString();

    log('widget?.customerId ${widget.customerId}');
    log('widget.editOrderObj.customerId ${widget?.editOrderObj?.customerId ??
        ""}');
    log('AppConstants.userData.customerId ${AppConstants.userData.customerId}');
    BlocProvider.of<PaymentSummaryStateBloc>(context).add(
        VerifyPaystackPaymentEvent(
            orderNo: widget.orderNo,
            customerId: customerId,
            sessionId: widget.sessionId,
            retryCount: 3));
  }

  _makePaymentsWithPayStackWebView({
    @required String accessCode,
    @required BuildContext context,
    @required String customerId,
    @required String orderNo,
  }) async {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: true,
        builder: (context) {
          return SafeArea(
            child: WebView(
              initialUrl: 'https://checkout.paystack.com/$accessCode',
              javascriptMode: JavascriptMode.unrestricted,
              userAgent: 'Flutter;Webview',
              navigationDelegate: (navigation) {
                print('navigation.url ${navigation.url}');

                if (navigation.url
                    .split('?')[0]
                    .startsWith('https://app.anelloh.com/payment_summary')) {
                  Navigator.of(context)
                      .popAndPushNamed('/payment_option', arguments: {
                    'orderNo': orderNo,
                    'paymentSummaryOrderCard': widget.paymentSummaryOrderCard,
                    'customerId': customerId,
                    'sessionId': accessCode,
                    'payment_type': 'paystack',
                    'retryCount': 3,
                  });
                }
                return NavigationDecision.navigate;
              },
            ),
          );
        });
  }

  _checkLoadingState({@required dynamic state}) {
    if (state is MakingPayments) return true;
    if (state is CheckingPaymentIntentStatus) return true;
    if (state is VerifyingPayment) return true;
    if (state is LoadingActivePaymentGateway) return true;
    if (state is LoadingBankPaymentConfig) return true;
    if (state is EditingOrder) return true;
    return false;
  }

  @override
  Widget build(BuildContext context) {
    log('widget.paymentSummaryOrderCard.myCurrency ${widget
        .paymentSummaryOrderCard.myCurrency}');

    return BlocListener<EditOrderScreenBloc, EditOrderScreenState>(
      listener: (context, st) {
        if (st is EditOrderSuccessfully) {
          showToast(
              toastMsg: 'Order payment is in process',
              toastLength: Toast.LENGTH_LONG,
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          // Future.delayed(Duration(seconds: 1)).then((res) {
          Navigator.of(context).pushReplacementNamed('/dashboard');
          // });
        }
        if (st is EditOrderFailed) {
          showToast(
              toastMsg: 'Failed to process order',
              toastLength: Toast.LENGTH_LONG,
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              isError: true,
              txtColor: ColorPalette().main);
        }
      },
      child: BlocBuilder<EditOrderScreenBloc, EditOrderScreenState>(
          builder: (context, state) {
            return BlocListener<PaymentSummaryStateBloc, PaymentSummaryState>(
              listener: (context, state) {
                if (state is ActivePaymentGatewaysState &&
                    state.activePaymentGatewayList.length != 0) {
                  activePaymentGatewayList = state.activePaymentGatewayList;
                }

                if (state is ActiveBankPaymentConfigState) {
                  bankConfigEntity = state.bankConfigEntity;
                }

                if (state is SuccessfullyMadePayStackPayments) {
                  _makePaymentsWithPayStackWebView(
                    accessCode: state.accessCode,
                    context: context,
                    customerId: AppConstants.userData.customerId,
                    orderNo:
                    widget?.orderNo ?? widget.paymentSummaryOrderCard.orderNo,
                  );
                }
                if (state is PaymentVerifiedSuccessfully) {
                  showToast(
                      toastMsg: 'Payment verified',
                      toastGravity: ToastGravity.CENTER,
                      bgColor: ColorPalette().textWhite,
                      toastLength: Toast.LENGTH_LONG,
                      txtColor: ColorPalette().main);
                  Navigator.pushNamed(context, '/dashboard');
                }
                if (state is PaymentVerificationFailed) {
                  if (state.retryCount > 0) {
                    BlocProvider.of<PaymentSummaryStateBloc>(context).add(
                        VerifyPaystackPaymentEvent(
                            orderNo: state.orderNo,
                            customerId: state.customerId,
                            sessionId: state.sessionId,
                            retryCount: state.retryCount));
                  } else if (state.retryCount == 0) {
                    // send email to anelloh
                    showToast(
                        toastMsg: state.message,
                        toastGravity: ToastGravity.CENTER,
                        bgColor: ColorPalette().dangerRed,
                        toastLength: Toast.LENGTH_LONG,
                        txtColor: ColorPalette().main);
                  }
                }
                if (state is SuccessfullyConfirmedPayment) {
                  showToast(
                      toastMsg: 'Payment confirmed',
                      toastGravity: ToastGravity.CENTER,
                      bgColor: ColorPalette().textWhite,
                      toastLength: Toast.LENGTH_LONG,
                      txtColor: ColorPalette().main);

                  Navigator.of(context).pushReplacementNamed('/dashboard');
                }
                if (state is ErrorAfterMakingInitialPayments) {
                  if (state.retryCount > 0) {
                    BlocProvider.of<PaymentSummaryStateBloc>(context).add(
                        VerifyPaystackPaymentEvent(
                            orderNo: state.orderNo,
                            customerId: state.customerId,
                            sessionId: state.sessionId,
                            retryCount: state.retryCount));
                  } else if (state.retryCount == 0) {
                    // send email to anelloh
                    showToast(
                        toastMsg: "Request Failed",
                        toastGravity: ToastGravity.CENTER,
                        bgColor: ColorPalette().dangerRed,
                        toastLength: Toast.LENGTH_LONG,
                        txtColor: ColorPalette().main);
                  }
                }
                if (state is ErrorRequestingInitialPayments) {
                  showToast(
                      toastMsg: '${state.errorMessage}',
                      toastGravity: ToastGravity.CENTER,
                      toastLength: Toast.LENGTH_LONG,
                      bgColor: ColorPalette().textWhite,
                      isError: true,
                      txtColor: ColorPalette().dangerRed);
                }
                if (state is ErrorListingActivePayments) {
                  showToast(
                      toastMsg: '${state.errorMessage}',
                      toastGravity: ToastGravity.CENTER,
                      toastLength: Toast.LENGTH_LONG,
                      bgColor: ColorPalette().textWhite,
                      isError: true,
                      txtColor: ColorPalette().dangerRed);
                }
              },
              child: BlocBuilder<PaymentSummaryStateBloc, PaymentSummaryState>(
                builder: (context, state) {
                  return LoadingOverlay(
                    child: StarterWidget(
                      hasTopWidget: true,
                      hasBackButton: true,
                      isBgAllowed: true,
                      bgColor: ColorPalette().main,
                      hasRefreshOption: false,
                      bottomPageContent: _bottomPageContext(
                          context: context, widget: widget, state: state),
                    ),
                    isLoading: _checkLoadingState(state: state),
                    progressIndicator: ProgressWidget(),
                    color: ColorPalette().textBlack,
                    opacity: 0.4,
                  );
                },
              ),
            );
          }),
    );
  }

  void _getAllPaymentOptions(String myCurrency) {
    BlocProvider.of<PaymentSummaryStateBloc>(context)
        .add(RetrieveActivePaymentGateway(myCurrency: myCurrency));
  }

  void _getBankConfigs(String myCurrency) {
    BlocProvider.of<PaymentSummaryStateBloc>(context)
        .add(RetrieveBankPaymentConfig(myCurrency: myCurrency));
  }

  Widget _bottomPageContext({@required BuildContext context,
    @required PaymentOptionsScreen widget,
    PaymentSummaryState state}) {
    return SafeArea(
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            subPageTitle('Choose a payment option', 'Select a payment option',
                context: context, color: Colors.white),
            SizedBox(
              height: 40,
            ),
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // state is LoadingActivePaymentGateway ||
                  //         state is LoadingBankPaymentConfig
                  //     ? Container()
                  //     : widget.paymentSummaryOrderCard.myCurrency == "NGN"
                  //         ? AnellohRoundButtonLarge(
                  //             onPressed: () {
                  //               // Navigator.pop(context);
                  //               _showBottomSheetForBankPayment(
                  //                   widget.editOrderObj,
                  //                   bankConfigEntity,
                  //                   widget.orderNo,
                  //                   widget.customerId);
                  //             },
                  //             isInverted: true,
                  //             childText: 'Pay with Bank',
                  //             hasImage: true,
                  //             assetImage: Icon(
                  //               Icons.account_balance,
                  //               color: ColorPalette().main,
                  //               size: 19.0,
                  //             )).paddingSymmetric(vertical: 10)
                  //         : Container(),
                  activePaymentGatewayList.length != 0
                      ? _returnGatewayList(activePaymentGatewayList, context,
                      bankConfigEntity, widget.orderNo, widget.customerId)
                      : Container()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _returnGatewayList(
      List<ActivePaymentGateways> activePaymentGatewayList,
      BuildContext context,
      BankConfigEntity bankConfigEntity,
      String orderNo,
      String customerId) {
    for (var payment in activePaymentGatewayList) {
      log('${payment.name}');
    }
    return Column(
      children: [
        for (var activePaymentGatewayList in activePaymentGatewayList) ...[
          // if(activePaymentGatewayList.name.contains("Bank of America")) ...[Container()]
          // else
          AnellohRoundButtonLarge(
            onPressed: () =>
            activePaymentGatewayList.name.contains("Bank Transfer")
                ? _showBottomSheetForBankPayment(
                widget.editOrderObj, bankConfigEntity, orderNo, customerId)
                : _showBottomSheetForPaymentGateway(
                activePaymentGatewayList, context, orderNo, customerId),
            isInverted: true,
            childText: 'Pay with ${activePaymentGatewayList.name}',
            hasImage: true,
            assetImage: ClipRRect(
              child: activePaymentGatewayList.paymentChannelLogo == null ||
                  activePaymentGatewayList.paymentChannelLogo == 'string'
                  ? Icon(Icons.account_balance_outlined)
                  : activePaymentGatewayList.paymentChannelLogo != null &&
                  activePaymentGatewayList.paymentChannelLogo.length > 0
                  ? Image.network(
                  '${activePaymentGatewayList.paymentChannelLogo}',
                  fit: BoxFit.contain,
                  width: 20,
                  height: 20, errorBuilder: (context, _, __) {
                return Container(
                    width: 20,
                    height: 20,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(100)),
                    child: Center(
                      child: Platform.isIOS
                          ? CupertinoActivityIndicator()
                          : CircularProgressIndicator(),
                    ));
              })
                  : Icon(
                Icons.account_balance,
                size: 20,
              ),
              borderRadius: BorderRadius.circular(100),
            ),
          ).paddingSymmetric(vertical: 10)
        ]
      ],
    );
  }

  _showBottomSheetForBankPayment(EditOrderObj editOrderObj,
      BankConfigEntity bankConfigEntity, String orderNo, String customerId) {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: ColorPalette().textWhite,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<EditOrderScreenBloc>(
                create: (context) => EditOrderScreenBloc(),
              ),
            ],
            child: BankPaymentScreen(
                paymentSummaryOrderCard: widget.paymentSummaryOrderCard,
                editOrderObj: widget.editOrderObj,
                orderNo: orderNo,
                customerId: customerId,
                bankConfigEntity: bankConfigEntity),
          );
        });
  }

  _showBottomSheetForPaymentGateway(
      ActivePaymentGateways activePaymentGatewayList,
      BuildContext context,
      String orderNo,
      String customerId) {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        backgroundColor: ColorPalette().textWhite,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(30), topRight: Radius.circular(30))),
        builder: (context) {
          return MultiBlocProvider(
            providers: [
              BlocProvider<PaymentSummaryStateBloc>(
                create: (context) => PaymentSummaryStateBloc(),
              ),
            ],
            child: activePaymentGatewayList.name.trim().toLowerCase() ==
                'paystack'
                ? _paystackPaymentWidget(
                context, activePaymentGatewayList, orderNo, customerId)
                : MultiBlocProvider(
              providers: [
                BlocProvider<EditOrderScreenBloc>(
                  create: (context) => EditOrderScreenBloc(),
                ),
              ],
              child: PaymentGatewayScreen(
                  paymentSummaryOrderCard: widget.paymentSummaryOrderCard,
                  editOrderObj: widget.editOrderObj,
                  customerId: widget.customerId,
                  orderNo: widget.orderNo,
                  activePaymentGateways: activePaymentGatewayList),
            ),
          );
        });
  }

  Container _paystackPaymentWidget(BuildContext context,
      ActivePaymentGateways activePaymentGateways,
      String orderNo,
      String customerID) {
    log('_paystackPaymentWidgetorderNo $orderNo');

    return Container(
      height: Get.height / 1.5,
      child: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Align(
                alignment: Alignment.centerRight,
                child: GestureDetector(
                    onTap: () => Navigator.pop(context),
                    child: SvgPicture.asset('assets/images/close.svg'))),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                ClipRRect(
                  child: activePaymentGateways.paymentChannelLogo != null &&
                      activePaymentGateways.paymentChannelLogo.length > 0
                      ? Image.network(
                      '${activePaymentGateways.paymentChannelLogo}',
                      fit: BoxFit.contain,
                      width: 30,
                      height: 30, errorBuilder: (context, _, __) {
                    return Container(
                        width: 30,
                        height: 30,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100)),
                        child: Center(
                          child: Platform.isIOS
                              ? CupertinoActivityIndicator()
                              : CircularProgressIndicator(),
                        ));
                  })
                      : Icon(
                    Icons.account_balance,
                    size: 20,
                  ),
                  borderRadius: BorderRadius.circular(100),
                ),
                CustomText(
                  title:
                  '${CurrencyFormatter.format(
                      currencyCode: widget.paymentSummaryOrderCard.myCurrency,
                      amount: formatToCurrency(
                          widget.paymentSummaryOrderCard.totalAmount))}',
                  textSize: 18,
                  textColor: ColorPalette().dangerRed,
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            RichText(
                text: TextSpan(children: [
                  TextSpan(
                      text: "Please click on the button below to proceed",
                      style: TextStyle(height: 2, color: appPrimaryColor)),
                ])),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                  title: 'Processing Fee',
                  textSize: 15,
                ),
                CustomText(
                  title:
                  '${CurrencyFormatter.format(
                      currencyCode: widget.paymentSummaryOrderCard.myCurrency,
                      amount: formatToCurrency(
                          widget.paymentSummaryOrderCard.transactionFee))}',
                  textSize: 18,
                  textColor: ColorPalette().dangerRed,
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                CustomText(
                  title: 'Rate',
                  textSize: 15,
                ),
                CustomText(
                  title:
                  '${CurrencyFormatter.format(
                      currencyCode: widget.paymentSummaryOrderCard
                          .convertedCurrency,
                      amount: formatToCurrency("1"))} / ${CurrencyFormatter
                      .format(
                      currencyCode: widget.paymentSummaryOrderCard.myCurrency,
                      amount: formatToCurrency(
                          widget.paymentSummaryOrderCard.rate))}',
                  textSize: 18,
                  textColor: ColorPalette().dangerRed,
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            SizedBox(
              height: 30,
            ),
            AnellohRoundNetworkButtonLarge(
              onPressed: () {
                Navigator.pop(context);
                _processPaystackPaymentAsDirectCharge(orderNo, customerID);
              },
              childText: 'Pay',
              isColorInverted: false,
              isLoading: false,
            )
          ],
        ).paddingSymmetric(horizontal: 20, vertical: 20),
      ),
    );
  }

  Future<void> _processPaystackPaymentAsDirectCharge(String orderNo,
      String cId) async {
    try {
      var resultEntity;
      if (await readFromPref('__anelloh_current_user__')) {
        resultEntity =
            jsonDecode(await getFromPref('__anelloh_current_user__'));
      }

      String customerId = resultEntity['customerId'].toString();

      print('customerId $customerId');
      BlocProvider.of<PaymentSummaryStateBloc>(context).add(
        PayWithPaystackEvent(
          orderAmount: widget.paymentSummaryOrderCard.totalAmount,
          orderCurrency: widget.paymentSummaryOrderCard.myCurrency,
          customerId: cId ?? customerId,
          orderNo: orderNo ?? widget.paymentSummaryOrderCard.orderNo,
          orderTransactionFee: widget.paymentSummaryOrderCard.transactionFee,
        ),
      );
    } catch (err) {
      print(err);
    }
  }
}

class BankPaymentScreen extends StatefulWidget {
  final PaymentSummaryOrderCard paymentSummaryOrderCard;
  final EditOrderObj editOrderObj;
  final BankConfigEntity bankConfigEntity;
  final String customerId;
  final String orderNo;

  BankPaymentScreen({this.paymentSummaryOrderCard,
    this.editOrderObj,
    this.bankConfigEntity,
    this.customerId,
    this.orderNo});

  @override
  _BankPaymentScreenState createState() => _BankPaymentScreenState();
}

class _BankPaymentScreenState extends State<BankPaymentScreen> {
  File preferredID;
  bool hasDocumentError = true;
  bool hasDocumentSizeError = true;

  var imageSize;

  _removeDocumentImage() {
    setState(() {
      preferredID = null;
      hasDocumentError = true;
      hasDocumentSizeError = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EditOrderScreenBloc, EditOrderScreenState>(
      listener: (context, st) {
        // if (st is EditingOrder) {
        //   Navigator.pop(context);
        // }

        if (st is EditOrderSuccessfully) {
          // Navigator.pop(context);
          showToast(
              toastMsg: 'Order payment is in process',
              toastLength: Toast.LENGTH_LONG,
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          // Future.delayed(Duration(seconds: 4)).then((res) {
          Navigator.of(context).pushReplacementNamed('/dashboard');
          // });
        }
        if (st is EditOrderFailed) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                  title: 'Error',
                  body: st?.error ?? 'Failed to process order',
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  )),
              context: context);
        }
      },
      child: BlocBuilder<EditOrderScreenBloc, EditOrderScreenState>(
        builder: (context, state) =>
            LoadingOverlay(
              isLoading: state is EditingOrder ? true : false,
              progressIndicator: ProgressWidget(),
              color: ColorPalette().textBlack,
              opacity: 0.4,
              child: Container(
                height: Get.height / 1.3,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Align(
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                              onTap: () => Navigator.pop(context),
                              child: SvgPicture.asset(
                                  'assets/images/close.svg'))),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Icon(Icons.account_balance),
                          CustomText(
                            title:
                            '${CurrencyFormatter.format(
                                currencyCode: widget.paymentSummaryOrderCard
                                    .myCurrency,
                                amount: formatToCurrency(
                                    widget.paymentSummaryOrderCard
                                        .totalAmount))}',
                            textSize: 21,
                            hasFontWeight: true,
                            fontWeight: FontWeight.w800,
                            textColor: ColorPalette().dangerRed,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      RichText(
                          text: TextSpan(children: [
                            TextSpan(
                                text:
                                'You can make payment to the bank details below and we will process your transaction once payment has been confirmed',
                                style: TextStyle(
                                  color: Color(0xffb6b6b6), fontSize: 15,)),
                          ])),
                      SizedBox(
                        height: 15,
                      ),
                      PaymentItemWidget(context, 'Processing Fee',
                          '${CurrencyFormatter.format(
                              currencyCode: widget.paymentSummaryOrderCard
                                  .myCurrency, amount: formatToCurrency(widget
                              .paymentSummaryOrderCard.transactionFee))}',
                          false),
                      PaymentItemWidget(context, 'Rate', '${CurrencyFormatter.format(
                          currencyCode: widget.paymentSummaryOrderCard
                              .convertedCurrency,
                          amount: formatToCurrency("1"))} / ${CurrencyFormatter
                          .format(currencyCode: widget.paymentSummaryOrderCard
                          .myCurrency, amount: formatToCurrency(widget
                          .paymentSummaryOrderCard.rate))}', false),
                      PaymentItemWidget(context,
                          'Bank Name', widget?.bankConfigEntity?.bankName ?? "",
                          false),
                      PaymentItemWidget(context, 'Account Number',
                          widget?.bankConfigEntity?.accountNumber ?? "", true),
                      PaymentItemWidget(context, 'Routing Number',
                          widget?.bankConfigEntity?.bankRouteNo ?? "", true),

                      this.preferredID != null
                          ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [
                              SvgPicture.asset(
                                'assets/images/document_text_outline.svg',
                                width: 50,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Container(
                                child: Image.file(
                                  this.preferredID,
                                  width: 100.0,
                                  height: 100.0,
                                  fit: BoxFit.cover,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              CustomText(
                                title: imageSize.toString(),
                                textColor: Colors.black,
                                textSize: 18,
                              )
                            ],
                          ),
                          InkWell(
                            child: Icon(Icons.clear),
                            onTap: _removeDocumentImage,
                          ),
                        ],
                      )
                          : InkWell(
                        onTap: () =>
                            displayImagePickerDialog(context, () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              Navigator.pop(context);
                              PickedFile image = await ImagePicker().getImage(
                                source: ImageSource.camera,
                                imageQuality: 5,
                              );
                              if (image != null) {
                                setState(() {
                                  preferredID = File(image.path);
                                  hasDocumentError = false;
                                  hasDocumentSizeError = false;
                                });

                                imageSize = await getFileSize(image.path, 1);

                                print(
                                    'Image Size ${imageSize.toString().split(
                                        ' ')[1] == 'KB' && double.parse(
                                        imageSize.toString().split(' ')[0])
                                        .toInt() > 1024}');

                                if (imageSize.toString().split(' ')[1] !=
                                    'KB' ||
                                    imageSize.toString().split(' ')[1] != 'B' &&
                                        double.parse(imageSize
                                            .toString()
                                            .split(' ')[0])
                                            .toInt() >
                                            1024) {
                                  setState(() {
                                    hasDocumentSizeError = true;
                                  });
                                  return;
                                }
                              } else {
                                print('No image selected.');
                              }
                            }, () async {
                              FocusScope.of(context).requestFocus(FocusNode());
                              Navigator.pop(context);
                              PickedFile image = await ImagePicker().getImage(
                                source: ImageSource.gallery,
                                imageQuality: 5,
                              );
                              if (image != null) {
                                setState(() {
                                  preferredID = File(image.path);
                                  hasDocumentError = false;
                                  hasDocumentSizeError = false;
                                });

                                imageSize = await getFileSize(image.path, 1);

                                print(
                                    'Image Size ${imageSize.toString().split(
                                        ' ')[1] == 'KB' && double.parse(
                                        imageSize.toString().split(' ')[0])
                                        .toInt() > 1024}');

                                if (imageSize.toString().split(' ')[1] !=
                                    'KB' ||
                                    imageSize.toString().split(' ')[1] != 'B' &&
                                        double.parse(imageSize
                                            .toString()
                                            .split(' ')[0])
                                            .toInt() >
                                            1024) {
                                  setState(() {
                                    hasDocumentSizeError = true;
                                  });
                                  return;
                                }
                              } else {
                                print('No image selected.');
                              }
                            }),
                        child: Center(
                          child: DottedBorder(
                            color: ColorPalette().main,
                            dashPattern: [10, 5],
                            borderType: BorderType.RRect,
                            radius: Radius.circular(3),
                            strokeWidth: 1,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              width: 150,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.file_upload,
                                      color: ColorPalette().main,
                                    ),
                                    CustomText(
                                      isCenter: true,
                                      title: 'Upload Payment Receipt',
                                      textColor: ColorPalette().dangerRed,
                                    ),
                                    this.preferredID != null
                                        ? Center(
                                      child: Column(
                                        children: [
                                          Container(
                                            child: Image.file(
                                              this.preferredID,
                                              width: 150.0,
                                              height: 150.0,
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                          SizedBox(
                                            height: 20,
                                          ),
                                          CustomText(
                                            title: imageSize.toString(),
                                            textColor: Colors.black,
                                            textSize: 18,
                                          )
                                        ],
                                      ),
                                    )
                                        : SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      this.hasDocumentError == true ||
                          hasDocumentSizeError == true
                          ? Padding(
                        padding: EdgeInsets.only(left: 15),
                        child: CustomText(
                          title:
                          'Document Image is required and must be less than 1 MB',
                          textSize: mainSizer(context: context, size: 36),
                          textColor: Colors.red,
                        ),
                      )
                          : SizedBox(),
                      SizedBox(
                        height: 30,
                      ),
                      AnellohRoundNetworkButtonLarge(
                        onPressed: () {
                          if (hasDocumentError || hasDocumentSizeError) return;

                          _editOrder();
                        },
                        childText: 'Payment Made',
                        isColorInverted: false,
                        isLoading: false,
                      )
                    ],
                  ).paddingSymmetric(horizontal: 20, vertical: 20),
                ),
              ),
            ),
      ),
    );
  }

  _editOrder() async {
    String base64Image = base64Encode(preferredID.readAsBytesSync());

    print(
        'widget?.editOrderObj?.customerId Bank ${widget?.editOrderObj
            ?.customerId}');
    print(
        'widget?.paymentSummaryOrderCard?.customerId Bank ${widget
            ?.paymentSummaryOrderCard?.customerId}');
    print('widget.customerId Bank ${widget.customerId}');
    print(
        'widget?.paymentSummaryOrderCard?.orderNo Bank ${widget
            .paymentSummaryOrderCard.orderNo}');
    print(' widget.orderNo Bank ${widget.orderNo}');
    // print('widget?.paymentSummaryOrderCard?.orderNo ?? widget.orderNo ${widget?.paymentSummaryOrderCard?.orderNo ?? widget.orderNo}');

    EditOrderObj editOrderObj = EditOrderObj(
        customerId: widget?.customerId ??
            widget?.editOrderObj?.customerId ??
            widget?.paymentSummaryOrderCard?.customerId,
        orderNo: widget?.orderNo ?? widget?.paymentSummaryOrderCard?.orderNo,
        loggedInDevice: "1",
        receipt: base64Image);

    // print("Edit Object ${jsonEncode(editOrderObj)}");

    BlocProvider.of<EditOrderScreenBloc>(context)
        .add(EditOrderEvent(editOrderObj: editOrderObj, isUploadReceipt: true));
  }

}

class PaymentGatewayScreen extends StatefulWidget {
  final PaymentSummaryOrderCard paymentSummaryOrderCard;
  final ActivePaymentGateways activePaymentGateways;
  final EditOrderObj editOrderObj;
  final String customerId;
  final String orderNo;

  PaymentGatewayScreen({this.paymentSummaryOrderCard,
    this.activePaymentGateways,
    this.editOrderObj,
    this.customerId,
    this.orderNo});

  @override
  _PaymentGatewayScreenState createState() => _PaymentGatewayScreenState();
}

class _PaymentGatewayScreenState extends State<PaymentGatewayScreen> {
  File preferredID;
  String fileName;
  bool hasDocumentError = true;
  bool hasDocumentSizeError = true;

  var imageSize;

  _removeDocumentImage() {
    setState(() {
      preferredID = null;
      fileName = null;
      hasDocumentError = true;
      hasDocumentSizeError = true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<EditOrderScreenBloc, EditOrderScreenState>(
      listener: (context, st) {
        // if (st is EditingOrder) {
        //   Navigator.pop(context);
        // }

        if (st is EditOrderSuccessfully) {
          // Navigator.pop(context);
          showToast(
              toastMsg: 'Order payment is in process',
              toastLength: Toast.LENGTH_LONG,
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          // Future.delayed(Duration(seconds: 4)).then((res) {
          Navigator.of(context).pushReplacementNamed('/dashboard');
          // });
        }
        if (st is EditOrderFailed) {
          showToast(
              toastMsg: 'Failed to process order',
              toastLength: Toast.LENGTH_LONG,
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              isError: true,
              txtColor: ColorPalette().main);
        }
      },
      child: BlocBuilder<EditOrderScreenBloc, EditOrderScreenState>(
        builder: (context, state) =>
            LoadingOverlay(
              isLoading: state is EditingOrder ? true : false,
              progressIndicator: ProgressWidget(),
              color: ColorPalette().textBlack,
              opacity: 0.4,
              child: Container(
                height: Get.height / 1.5,
                child: SingleChildScrollView(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Align(
                          alignment: Alignment.centerRight,
                          child: GestureDetector(
                              onTap: () => Navigator.pop(context),
                              child: SvgPicture.asset(
                                  'assets/images/close.svg'))),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          ClipRRect(
                            child: widget.activePaymentGateways
                                .paymentChannelLogo !=
                                null &&
                                widget.activePaymentGateways.paymentChannelLogo
                                    .length >
                                    0
                                ? Image.network(
                                '${widget.activePaymentGateways
                                    .paymentChannelLogo}',
                                fit: BoxFit.contain,
                                width: 30,
                                height: 30, errorBuilder: (context, _, __) {
                              return Container(
                                  width: 30,
                                  height: 30,
                                  decoration: BoxDecoration(
                                      borderRadius:
                                      BorderRadius.circular(100)),
                                  child: Center(
                                    child: Platform.isIOS
                                        ? CupertinoActivityIndicator()
                                        : CircularProgressIndicator(),
                                  ));
                            })
                                : Icon(
                              Icons.account_balance,
                              size: 20,
                            ),
                            borderRadius: BorderRadius.circular(100),
                          ),
                          CustomText(
                            title:
                            '${CurrencyFormatter.format(
                                currencyCode: widget.paymentSummaryOrderCard
                                    .myCurrency,
                                amount: formatToCurrency(
                                    widget.paymentSummaryOrderCard
                                        .totalAmount))}',
                            textSize: 21,
                            hasFontWeight: true,
                            fontWeight: FontWeight.w800,
                            textColor: ColorPalette().dangerRed,
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      GestureDetector(
                        onTap: () =>
                            _launchURL(
                                widget.activePaymentGateways.paymentChannelUrl),
                        child: RichText(
                            text: TextSpan(children: [
                              TextSpan(
                                  text:
                                  'Use the highlighted link to get to Anelloh payment portal for ${widget
                                      .activePaymentGateways.name} ',
                                  style: TextStyle(
                                    color: Color(0xffb6b6b6), fontSize: 15,)),
                              TextSpan(
                                  text:
                                  '${widget.activePaymentGateways
                                      .paymentChannelUrl ?? ""}'.toUpperCase(),
                                  style: TextStyle(
                                    color: ColorPalette().dangerRed,
                                    fontSize: 15,
                                    decoration: TextDecoration.underline,)),
                            ])),
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      PaymentItemWidget(context, 'Processing Fee',
                          '${CurrencyFormatter.format(
                              currencyCode: widget.paymentSummaryOrderCard
                                  .myCurrency, amount: formatToCurrency(widget
                              .paymentSummaryOrderCard.transactionFee))}'),
                      PaymentItemWidget(context, 'Rate', '${CurrencyFormatter.format(
                          currencyCode: widget.paymentSummaryOrderCard
                              .convertedCurrency,
                          amount: formatToCurrency("1"))} / ${CurrencyFormatter
                          .format(currencyCode: widget.paymentSummaryOrderCard
                          .myCurrency, amount: formatToCurrency(widget
                          .paymentSummaryOrderCard.rate))}'),

                      this.preferredID != null
                          ? Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Row(
                            mainAxisSize: MainAxisSize.min,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              SvgPicture.asset(
                                'assets/images/document_text_outline.svg',
                                width: 50,
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Center(
                                child: Container(
                                  child: Image.file(
                                    this.preferredID,
                                    width: 100.0,
                                    height: 100.0,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              CustomText(
                                title: imageSize.toString(),
                                textColor: Colors.black,
                                textSize: 18,
                              )
                            ],
                          ),
                          InkWell(
                            child: Icon(Icons.clear),
                            onTap: _removeDocumentImage,
                          ),
                        ],
                      )
                          : InkWell(
                        onTap: () =>
                            displayImagePickerDialog(context, () async {
                              Navigator.pop(context);
                              PickedFile image = await ImagePicker().getImage(
                                  source: ImageSource.camera, imageQuality: 5);
                              if (image != null) {
                                setState(() {
                                  preferredID = File(image.path);
                                  hasDocumentSizeError = false;
                                  hasDocumentError = false;
                                });

                                imageSize = await getFileSize(image.path, 1);
                                print(
                                    'Image Size ${imageSize.toString().split(
                                        ' ')[1] == 'KB' && double.parse(
                                        imageSize.toString().split(' ')[0])
                                        .toInt() > 1024}');

                                if (imageSize.toString().split(' ')[1] !=
                                    'KB' ||
                                    imageSize.toString().split(' ')[1] != 'B' &&
                                        double.parse(imageSize
                                            .toString()
                                            .split(' ')[0])
                                            .toInt() >
                                            1024) {
                                  setState(() {
                                    hasDocumentSizeError = true;
                                  });
                                  return;
                                }
                              } else {
                                print('No image selected.');
                              }
                            }, () async {
                              Navigator.pop(context);
                              PickedFile image = await ImagePicker().getImage(
                                  source: ImageSource.gallery, imageQuality: 5);
                              if (image != null) {
                                setState(() {
                                  preferredID = File(image.path);
                                  hasDocumentSizeError = false;
                                  hasDocumentError = false;
                                });

                                imageSize = await getFileSize(image.path, 1);
                                print(
                                    'Image Size ${imageSize.toString().split(
                                        ' ')[1] == 'KB' && double.parse(
                                        imageSize.toString().split(' ')[0])
                                        .toInt() > 1024}');

                                if (imageSize.toString().split(' ')[1] !=
                                    'KB' ||
                                    imageSize.toString().split(' ')[1] != 'B' &&
                                        double.parse(imageSize
                                            .toString()
                                            .split(' ')[0])
                                            .toInt() >
                                            1024) {
                                  setState(() {
                                    hasDocumentSizeError = true;
                                  });
                                  return;
                                }
                              } else {
                                print('No image selected.');
                              }
                            }),
                        child: Center(
                          child: this.preferredID != null
                              ? Container(
                              child: Image.file(
                                this.preferredID,
                                width: 100.0,
                                height: 100.0,
                                fit: BoxFit.cover,
                              ))
                              : DottedBorder(
                            color: ColorPalette().main,
                            dashPattern: [10, 5],
                            borderType: BorderType.RRect,
                            radius: Radius.circular(3),
                            strokeWidth: 1,
                            child: Container(
                              margin: EdgeInsets.only(top: 10),
                              padding: EdgeInsets.symmetric(
                                  vertical: 5, horizontal: 5),
                              width: 150,
                              height: 100,
                              child: Center(
                                child: Column(
                                  mainAxisSize: MainAxisSize.max,
                                  mainAxisAlignment:
                                  MainAxisAlignment.center,
                                  crossAxisAlignment:
                                  CrossAxisAlignment.center,
                                  children: [
                                    Icon(
                                      Icons.file_upload,
                                      color: ColorPalette().main,
                                    ),
                                    CustomText(
                                      isCenter: true,
                                      title: 'Upload Payment Receipt',
                                      textColor:
                                      ColorPalette().dangerRed,
                                    ),
                                    this.preferredID != null
                                        ? Column(
                                      children: [
                                        Container(
                                          child: Image.file(
                                            this.preferredID,
                                            width: 100.0,
                                            height: 100.0,
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                        SizedBox(
                                          height: 20,
                                        ),
                                        CustomText(
                                          title: imageSize
                                              .toString(),
                                          textColor: Colors.black,
                                          textSize: 18,
                                        )
                                      ],
                                    )
                                        : SizedBox(),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                      this.hasDocumentError == true ||
                          hasDocumentSizeError == true
                          ? Padding(
                        padding: EdgeInsets.only(left: 15),
                        child: CustomText(
                          title:
                          'Document Image is required and must be less than 1 MB',
                          textSize: mainSizer(context: context, size: 36),
                          textColor: Colors.red,
                        ),
                      )
                          : SizedBox(),
                      SizedBox(
                        height: 30,
                      ),
                      AnellohRoundNetworkButtonLarge(
                        onPressed: () {
                          if (hasDocumentError || hasDocumentSizeError)
                            return;
                          else {
                            // Navigator.pop(context);

                            _editOrder();
                          }
                        },
                        childText: 'Payment Made',
                        isColorInverted: false,
                        isLoading: false,
                      )
                    ],
                  ).paddingSymmetric(horizontal: 20, vertical: 20),
                ),
              ),
            ),
      ),
    );
  }

  _editOrder() async {
    String base64Image = base64Encode(preferredID.readAsBytesSync());

    print(
        'widget?.editOrderObj?.customerId ${widget?.editOrderObj?.customerId}');
    print(
        'widget?.paymentSummaryOrderCard?.customerId ${widget
            ?.paymentSummaryOrderCard?.customerId}');
    print('widget.customerId ${widget.customerId}');
    print(
        'widget?.paymentSummaryOrderCard?.orderNo ${widget
            .paymentSummaryOrderCard.orderNo}');
    print(' widget.orderNo ${widget.orderNo}');

    EditOrderObj editOrderObj = EditOrderObj(
        customerId: widget?.customerId ??
            widget?.editOrderObj?.customerId ??
            widget?.paymentSummaryOrderCard?.customerId,
        orderNo: widget?.orderNo ?? widget?.paymentSummaryOrderCard?.orderNo,
        loggedInDevice: "1",
        receipt: base64Image);

    BlocProvider.of<EditOrderScreenBloc>(context)
        .add(EditOrderEvent(editOrderObj: editOrderObj, isUploadReceipt: true));
  }

  void _launchURL(String authUrl) async =>
      await canLaunch(authUrl)
          ? await launch(
        authUrl,
        forceWebView: true,
        enableJavaScript: true,
      )
          : showToast(
          toastMsg: 'Could not launch $authUrl',
          isError: true,
          toastGravity: ToastGravity.BOTTOM,
          toastLength: Toast.LENGTH_LONG,
          bgColor: ColorPalette().textWhite,
          txtColor: ColorPalette().dangerRed);
// : throw 'Could not launch $authUrl';
}

PaymentItemWidget(BuildContext context, String title, String value, [bool hasCopyFeature = false]) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: title.toUpperCase(),
                  textSize: 12,
                  textColor: Color(0xffb6b6b6),
                ),
                CustomText(
                  title: value,
                  textSize: 16,
                  hasFontWeight: true,
                  fontWeight: FontWeight.w600,
                  // textColor: ColorPalette().dangerRed,
                ),
              ],
            ),
          ),
          hasCopyFeature ? Spacer() : Container(),
          hasCopyFeature
              ? InkWell(onTap: () {
            Clipboard
                .setData(ClipboardData(text: value))
                .then((_) {
              showToast(
                  toastMsg:
                  "$title copied to clipboard",
                  toastLength: Toast.LENGTH_LONG,
                  toastGravity: ToastGravity.BOTTOM,
                  bgColor: appPrimaryColor,
                  txtColor: appWhiteColor);
              // ScaffoldMessenger.of(context).showSnackBar(
              //     SnackBar(content: Text("$title copied to clipboard")));
            });
          },
              child: SvgPicture.asset(
                'assets/images/copy_icon.svg', color: Color(0xffb6b6b6), width: 30,))
              : Container(),
        ],
      ),
      Divider(thickness: 1, color: Color(0xffc4c4c4),),
      SizedBox(
        height: 15,
      ),
    ],
  );
}

