import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_event.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_state.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/activity_card.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';

class ActivityScreen extends StatefulWidget {
  _ActivityScreenState createState() => _ActivityScreenState();
}

class _ActivityScreenState extends State<ActivityScreen> {
  @override
  void initState() {
    super.initState();
    _fetchUserOrders();
  }

  _fetchUserOrders() async {
    try {
      var resultEntity;
      if (await readFromPref('__anelloh_current_user__')) {
        resultEntity =
            jsonDecode(await getFromPref('__anelloh_current_user__'));
      }

      var image;
      if (resultEntity['customerImageFileLocation'] != null) {
        image = resultEntity['customerImageFileLocation'];
      }
      User user = User(
        fullNames: '${resultEntity['firstName']} ${resultEntity['lastName']}',
        firstName: resultEntity['firstName'],
        lastName: resultEntity['lastName'],
        homeAddress: resultEntity['address'],
        gender: resultEntity['gender'].toString(),
        customerId: resultEntity['customerId'].toString(),
        verificationStatus: resultEntity['status'].toString(),
        userName: resultEntity['userName'],
        emailAddress: resultEntity['email'],
        phoneNumber: resultEntity['phoneNumber'].toString(),
        profileImage: image,
        countryCode: resultEntity['countryCode'],
      );
      BlocProvider.of<MainUserOrderBloc>(context)
          .add(FetchUserOrdersEvent(user: user));
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      return BlocBuilder<MainUserOrderBloc, MainUserOrderState>(
          builder: (context, state) {
        return LoadingOverlay(
          isLoading: state is FetchingAllUserOrders ? true : false,
          progressIndicator: ProgressWidget(),
          color: ColorPalette().textBlack,
          opacity: 0.4,
          child: StarterWidget(
            hasTopWidget: false,
            hasBackButton: false,
            hasPagePadding: false,
            hasNavDrawer: false,
            hasBottomNav: true,
            bottomNavIndex: 2,
            hasRefreshOption: true,
            onRefreshFunction: () {
              return _fetchUserOrders();
            },
            hasBgimage: false,
            bottomPageContent:
                _bottomPageContent(context: context, state: state),
          ),
        );
      });
    });
  }

  _bottomPageContent(
      {@required BuildContext context, @required MainUserOrderState state}) {
    return SafeArea(
        maintainBottomViewPadding: true,
        child: Container(
          height: isPortrait(context)
              ? MediaQuery.of(context).size.height
              : MediaQuery.of(context).size.height * 2,
          width: MediaQuery.of(context).size.width,
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 4),
          child: SingleChildScrollView(
            padding: EdgeInsets.only(bottom: 200, top: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: _listBody(context: context, state: state),
            ).paddingSymmetric(horizontal: 5),
          ),
        ));
  }

  List<Widget> _listBody(
      {@required BuildContext context, @required MainUserOrderState state}) {
    List<Widget> body = [];
    body.add(subPageTitle('Activity', 'View all your transactions and activities on Anelloh here', context: context));
    body.add(SizedBox(height: 40));
    if (state is SuccessfullyFetchedAllUserOrders) {
      if (state.activities.length <= 0) {
        Widget noActivities = Container(
          width: MediaQuery.of(context).size.width,
          child: Center(
            child: Container(
              height: 150,
              child: Column(
                children: [
                  SvgPicture.asset('assets/images/error.svg', width: 100,),
                  SizedBox(height: 10,),
                  CustomText(
                    title: 'No Activity',
                    textSize: 15,
                  )
                ],
              ),
            ),
          ),
        );
        body.add(noActivities);
      } else {
        state.activities.reversed.forEach((activity) {
          ActivityCardWidget activityCardWidget =
              ActivityCardWidget(activityCardItem: activity);
          body.add(activityCardWidget);
        });
      }
      return body;
    } else {
      return body;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
