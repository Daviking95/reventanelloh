import 'package:anelloh_mobile/bloc/update_password/update_password_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/update_password/update_password_screen_event.dart';
import 'package:anelloh_mobile/bloc/update_password/update_password_screen_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';

class UpdatePasswordScreen extends StatefulWidget {
  _UpdatePasswordScreen createState() => _UpdatePasswordScreen();
}

class _UpdatePasswordScreen extends State<UpdatePasswordScreen> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final TextEditingController _currentPasswordInput = TextEditingController();
  final TextEditingController _newPasswordInput = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UpdatePasswordScreenBloc, UpdatePasswordScreenState>(
      listener: (context, state) {
        if (state is FailedSavingPassword) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Error',
                body: state.error,
                hasBtn: false,
                btnText: 'Ok',
                btnFunction: null,
                hasCancelBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
        if (state is SuccessfullySavedPassword) {
          showToast(
              toastMsg: 'Successfully changed password',
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          // Navigator.of(context).pop();
          Navigator.of(context).pushReplacementNamed('/login');
        }
      },
      child: BlocBuilder<UpdatePasswordScreenBloc, UpdatePasswordScreenState>(
          builder: (context, profileBuildState) {
        return StarterWidget(
          bgColor: ColorPalette().textWhite,
          hasBackButton: true,
          hasBottomNav: false,
          hasRefreshOption: false,
          bottomNavIndex: 1,
          isExitApp: true,
          isBgAllowed: false,
          backBtnColor: ColorPalette().main,
          bottomPageContent: _bottomPageContent(
            context: context,
            formKey: _formKey,
            profileBuildState: profileBuildState,
          ),
        );
      }),
    );
  }

  @override
  void deactivate() {
    super.deactivate();
  }

  @override
  void dispose() {
    super.dispose();
  }

  _savePassword() {
    if (_formKey.currentState.validate()) {
      UpdatePasswordObj updatePasswordObj = UpdatePasswordObj(
          currentPassword: _currentPasswordInput.text,
          newPassword: _newPasswordInput.text,
          userEmail: AppConstants.userData.emailAddress);
      BlocProvider.of<UpdatePasswordScreenBloc>(context)
          .add(SavePasswordEvent(updatePasswordObj: updatePasswordObj));
    }
  }

  Widget _bottomPageContent({
    @required BuildContext context,
    @required GlobalKey<FormState> formKey,
    @required UpdatePasswordScreenState profileBuildState,
  }) {
    return SingleChildScrollView(
      padding: EdgeInsets.all(15),
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: subPageTitle('Change Password', 'Change your password for security reasons', context: context),
              margin: EdgeInsets.only(bottom: 30),
            ),
            Container(
              child: Form(
                  key: formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 15, bottom: 10),
                          child: AppPasswordTextInput(
                            controller: _currentPasswordInput,
                            textInputTitle: 'Current Password',
                            validateInput:
                                AppConstants.validators.validatePassword,
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.only(top: 10, bottom: 15),
                          child: AppPasswordTextInput(
                            controller: _newPasswordInput,
                            textInputTitle: 'New Password',
                            validateInput:
                                AppConstants.validators.validatePassword,
                          )),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 15, bottom: 15),
                        child: AnellohRoundNetworkButtonLarge(
                          onPressed: _savePassword,
                          childText: 'Save Password',
                          isLoading: profileBuildState is SavingPassword
                              ? true
                              : false,
                        ),
                      ),
                    ],
                  )),
            )
          ],
        ),
      ),
    );
  }
}
