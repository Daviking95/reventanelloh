import 'package:anelloh_mobile/bloc/change_pin/change_pin_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/change_pin/change_pin_screen_event.dart';
import 'package:anelloh_mobile/bloc/change_pin/change_pin_screen_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:local_auth/local_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ChangePinScreen extends StatefulWidget {
  _ChangePinScreenState createState() => _ChangePinScreenState();
}

class _ChangePinScreenState extends State<ChangePinScreen> {
  GlobalKey _formKey = GlobalKey<FormState>();
  String _userPin = '';
  LocalAuthentication auth = new LocalAuthentication();
  bool isSavingPin = false;
  @override
  void initState() {
    super.initState();
    // _currentPin();
  }

  // _currentPin() async {
  //   try {
  //     SharedPreferences pref = await SharedPreferences.getInstance();
  //     if (pref.getString('__anelloh__user__pin__') != null) {
  //       setState(() {
  //         _userPin = pref.getString('__anelloh__user__pin__');
  //       });
  //     }
  //   }catch(err) {
  //     print(err);
  //   }
  // }
  @override
  Widget build(BuildContext context) {
    return StarterWidget(
      bgColor: ColorPalette().textWhite,
      hasBackButton: true,
      hasBottomNav: false,
      hasRefreshOption: false,
      bottomNavIndex: 1,
      isExitApp: true,
      isBgAllowed: false,
      backBtnColor: ColorPalette().main,
      bottomPageContent: _bottomPageContent(
          context: context,
          formKey: _formKey,
          currentPin: _userPin,
          auth: auth),
    );
  }

  Widget _bottomPageContent({
    @required BuildContext context,
    @required GlobalKey<FormState> formKey,
    @required String currentPin,
    @required LocalAuthentication auth,
  }) {
    final TextEditingController _currentPinInput = TextEditingController();
    final TextEditingController _newPinInput = TextEditingController();

    Future<void> _authenticate({@required ChangePinObj changePinObj}) async {
      try {
        setState(() {
          isSavingPin = false;
        });
        bool authenticated = await auth.authenticateWithBiometrics(
            localizedReason: 'Scan to log in',
            useErrorDialogs: true,
            stickyAuth: true);

        if (authenticated) {
          // BlocProvider.of<ChangePinScreenBloc>(context).add(SavingPinEvent(changePinObj: changePinObj));
          SharedPreferences pref = await SharedPreferences.getInstance();
          if (pref.getString('__anelloh__user__pin__') !=
              _currentPinInput.text) {
            showToast(
              toastMsg: 'Incorrect pin',
            );
          } else {
            var newPin = _newPinInput.text;
            await pref.setString('__anelloh__user__pin__', newPin);
            showToast(
              toastMsg: 'Pin saved successfully',
            );
          }
        }
      } on PlatformException catch (e) {
        print(e);
      } catch (err) {
        print(err);
      }
    }

    _savePin() async {
      try {
        if (formKey.currentState.validate()) {
          setState(() {
            isSavingPin = true;
          });
          ChangePinObj changePinObj = ChangePinObj(
              currentPin: _currentPinInput.text, newPin: _newPinInput.text);
          if (await auth.canCheckBiometrics) {
            List<BiometricType> availableBiometrics =
                await auth.getAvailableBiometrics();
            if (availableBiometrics.contains(BiometricType.fingerprint)) {
              _authenticate(changePinObj: changePinObj);
            }
          } else {
            showToast(
              toastMsg: 'Biometrics not supported on this device',
            );
            setState(() {
              isSavingPin = false;
            });
          }
        }
      } catch (err) {
        print(err);
      }
    }

    return BlocListener<ChangePinScreenBloc, ChangePinScreenState>(
      listener: (context, state) {},
      child: BlocBuilder<ChangePinScreenBloc, ChangePinScreenState>(
        builder: (context, state) {
          return SingleChildScrollView(
            padding: EdgeInsets.all(15),
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    child: subPageTitle('Set Pin', 'Set up your pin', context: context),
                    margin: EdgeInsets.only(bottom: 30),
                  ),
                  Container(
                    child: Form(
                        key: formKey,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                                width: MediaQuery.of(context).size.width,
                                child: AppPasswordTextInput(
                                    controller: _currentPinInput,
                                    textInputTitle: 'Current Pin',
                                    maxLength: 4,
                                    isOnlyDigits: true,
                                    validateInput: AppConstants
                                        .validators.validateNumber)),
                            SizedBox(
                              height: 10,
                            ),
                            Container(
                                width: MediaQuery.of(context).size.width,
                                child: AppPasswordTextInput(
                                  controller: _newPinInput,
                                  textInputTitle: 'New Pin',
                                  isOnlyDigits: true,
                                  maxLength: 4,
                                  validateInput:
                                      AppConstants.validators.validateNumber,
                                )),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              margin: EdgeInsets.only(top: 15, bottom: 15),
                              child: AnellohRoundNetworkButtonLarge(
                                onPressed: _savePin,
                                childText: 'Set Pin',
                                isLoading: _determineLoadingState(state),
                              ),
                            ),
                          ],
                        )),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }

  bool _determineLoadingState(ChangePinScreenState state) {
    if (state is SavingPin) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void deactivate() {
    super.deactivate();
  }
}
