import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_event.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_state.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/payments/paystack_payment.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:webview_flutter/webview_flutter.dart';

import '../constants.dart';
import 'payment_options_screen.dart';

// CustomStripePayment customStripePayment = new CustomStripePayment();
CustomPaystackPayment customPaystackPayment = new CustomPaystackPayment();

class PaymentSummary extends StatefulWidget {
  final PaymentSummaryOrderCard paymentSummaryOrderCard;
  final MatchFoundObj matchFoundObj;
  final EditOrderObj editOrderObj;
  final String orderNo;
  final String customerId;
  final String sessionId;
  final String paymentType;
  final int retryCount;

  PaymentSummary(
      {Key key,
      @required this.paymentSummaryOrderCard,
      @required this.matchFoundObj,
      @required this.editOrderObj,
      this.retryCount,
      this.customerId,
      this.orderNo,
      this.sessionId,
      this.paymentType})
      : super(key: key);

  @override
  _PaymentSummaryState createState() => _PaymentSummaryState();
}

class _PaymentSummaryState extends State<PaymentSummary> {
  _makePaymentsWithPayStackWebView({
    @required String accessCode,
    @required BuildContext context,
    @required String customerId,
    @required String orderNo,
  }) async {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        isDismissible: true,
        builder: (context) {
          return SafeArea(
            child: WebView(
              initialUrl: 'https://checkout.paystack.com/$accessCode',
              javascriptMode: JavascriptMode.unrestricted,
              userAgent: 'Flutter;Webview',
              navigationDelegate: (navigation) {
                if (navigation.url
                    .split('?')[0]
                    .startsWith('https://app.anelloh.com/payment_summary')) {
                  Navigator.of(context)
                      .popAndPushNamed('/payment_summary', arguments: {
                    'orderNo': orderNo,
                    'paymentSummaryOrderCard': widget.paymentSummaryOrderCard,
                    'customerId': customerId,
                    'sessionId': accessCode,
                    'payment_type': 'paystack',
                    'retryCount': 3,
                  });
                }
                return NavigationDecision.navigate;
              },
            ),
          );
        });
  }

  @override
  void initState() {
    super.initState();

    log('result entity orderNo 3 ${widget.orderNo}');
    log('result entity customerId 3 ${widget.customerId}');

    if (widget.sessionId != null && widget.paymentType == 'paystack') {
      _verifyPaystackPayment();
    }
  }

  _verifyPaystackPayment() {
    BlocProvider.of<PaymentSummaryStateBloc>(context).add(
        VerifyPaystackPaymentEvent(
            orderNo: widget.orderNo,
            customerId: widget.customerId,
            sessionId: widget.sessionId,
            retryCount: 3));
  }

  @override
  Widget build(BuildContext context) {

    return BlocListener<PaymentSummaryStateBloc, PaymentSummaryState>(
      listener: (context, state) {
        if (state is SuccessfullyMadePayStackPayments) {
          _makePaymentsWithPayStackWebView(
            accessCode: state.accessCode,
            context: context,
            customerId: AppConstants.userData.customerId,
            orderNo: widget.paymentSummaryOrderCard.orderNo,
          );
        }
        if (state is PaymentVerifiedSuccessfully) {
          showToast(
              toastMsg: 'Payment verified',
              toastGravity: ToastGravity.CENTER,
              bgColor: ColorPalette().textWhite,
              toastLength: Toast.LENGTH_LONG,
              txtColor: ColorPalette().main);
          Navigator.of(context).popAndPushNamed('/dashboard');
        }
        if (state is PaymentVerificationFailed) {
          if (state.retryCount > 0) {
            BlocProvider.of<PaymentSummaryStateBloc>(context).add(
                VerifyPaystackPaymentEvent(
                    orderNo: state.orderNo,
                    customerId: state.customerId,
                    sessionId: state.sessionId,
                    retryCount: state.retryCount));
          } else if (state.retryCount == 0) {
            // send email to anelloh

          }
        }
        if (state is SuccessfullyConfirmedPayment) {
          showToast(
              toastMsg: 'Payment confirmed',
              toastGravity: ToastGravity.CENTER,
              bgColor: ColorPalette().textWhite,
              toastLength: Toast.LENGTH_LONG,
              txtColor: ColorPalette().main);
          Navigator.of(context).popAndPushNamed('/dashboard');
        }
        if (state is ErrorAfterMakingInitialPayments) {
          if (state.retryCount > 0) {
            BlocProvider.of<PaymentSummaryStateBloc>(context).add(
                VerifyPaystackPaymentEvent(
                    orderNo: state.orderNo,
                    customerId: state.customerId,
                    sessionId: state.sessionId,
                    retryCount: state.retryCount));
          } else if (state.retryCount == 0) {
            // send email to anelloh
          }
        }
        if (state is ErrorRequestingInitialPayments) {
          showToast(
              toastMsg: '${state.errorMessage}',
              toastGravity: ToastGravity.CENTER,
              toastLength: Toast.LENGTH_LONG,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
        }
      },
      child: BlocBuilder<PaymentSummaryStateBloc, PaymentSummaryState>(
        builder: (context, state) {
          return LoadingOverlay(
            child: StarterWidget(
              hasTopWidget: true,
              hasBackButton: true,
              hasRefreshOption: false,
              bottomPageContent:
                  _bottomPageContext(context: context, widget: widget),
            ),
            isLoading: _checkLoadingState(state: state),
            progressIndicator: ProgressWidget(),
            color: ColorPalette().textBlack,
            opacity: 0.4,
          );
        },
      ),
    );
  }

  _checkLoadingState({@required PaymentSummaryState state}) {
    if (state is MakingPayments) return true;
    if (state is CheckingPaymentIntentStatus) return true;
    if (state is VerifyingPayment) return true;
    return false;
  }
}

Widget _bottomPageContext(
    {@required BuildContext context, @required PaymentSummary widget}) {

  log(('widget.paymentSummaryOrderCard Payment Summary ${widget.paymentSummaryOrderCard.convertedAmount == "null"}'));

  if (widget.paymentSummaryOrderCard == null || widget.paymentSummaryOrderCard.convertedAmount.isEmpty || widget.paymentSummaryOrderCard.convertedAmount == "null" || widget.paymentSummaryOrderCard.convertedAmount == null) {
    Navigator.of(context).popAndPushNamed('/dashboard');
  }

  return SafeArea(
      child: SingleChildScrollView(
    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 25),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        subPageTitle('Payment Summary', 'View your payment summary information here', context: context),
        SizedBox(
          height: 40,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
                PaymentItemWidget(
                    context,
                    'You will Receive',
                    '${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.convertedCurrency, amount: formatToCurrency(widget?.paymentSummaryOrderCard?.convertedAmount ?? '0'))}',
                    false),
                // CustomText(
                //   title: 'You will Receive',
                //   textSize: 15,
                // ),
                // CustomText(
                //   title:
                //       '${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.convertedCurrency, amount: formatToCurrency(widget?.paymentSummaryOrderCard?.convertedAmount ?? '0'))}',
                //   textSize: 18,
                //   textColor: ColorPalette().dangerRed,
                // ),
            PaymentItemWidget(
                context,
                '@ Rate',
                '${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.myCurrency == 'NGN' ? widget.paymentSummaryOrderCard.myCurrency : widget.paymentSummaryOrderCard.convertedCurrency, amount: widget.paymentSummaryOrderCard.rate)}/${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.convertedCurrency == 'NGN' ? widget.paymentSummaryOrderCard.myCurrency : widget.paymentSummaryOrderCard.convertedCurrency, amount: '')}',
                false),
          ],
        ),
        PaymentItemWidget(
            context,
            'Subtotal',
            '${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.myCurrency, amount: formatToCurrency(widget.paymentSummaryOrderCard.myAmount))}',
            false),
        PaymentItemWidget(
            context,
            'Processing Fee',
            '${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.myCurrency, amount: formatToCurrency(widget.paymentSummaryOrderCard.transactionFee))}',
            false),
        PaymentItemWidget(
            context,
            'You will pay',
            '${CurrencyFormatter.format(currencyCode: widget.paymentSummaryOrderCard.myCurrency, amount: formatToCurrency(widget.paymentSummaryOrderCard.totalAmount))}',
            false),

        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 40,
            ),
            AnellohRoundButtonLarge(
                onPressed: () {
                  AppConstants.instantMatchCurrency = null;
                  Navigator.of(context)
                      .pushNamed('/payment_option', arguments: {
                    'matchFoundObj': widget.matchFoundObj,
                    "editOrderObj": widget.editOrderObj,
                    'orderNo': widget.orderNo,
                    'customerId': widget.customerId,
                    'paymentSummaryOrderCard': widget.paymentSummaryOrderCard
                  });
                },
                childText: 'Pay',
                hasImage: true,
                assetImage: Container()),
          ],
        ),
      ],
    ),
  ));
}
