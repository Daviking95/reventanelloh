import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_state.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_event.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_state.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_event.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_state.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_event.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_state.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_event.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_state.dart';
import 'package:anelloh_mobile/helpers/classes/active_gateways.dart';
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/bottom_sheet_widgets.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:csc_picker/csc_picker.dart';
import 'package:dropdown_search/dropdown_search.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';

import '../constants.dart';
import '../styles.dart';

class ForeignDestinationAccount extends StatefulWidget {
  final List<Bank> banks;
  final EditOrderObj editOrderObj;
  final TextEditingController rateController;
  final bool isEdit;
  final bool isInstantMatch;
  final MatchFoundObj matchFoundObj;
  final bool isMatchFound;
  final BuildContext context;
  final Function callback;

  ForeignDestinationAccount(
      {this.banks,
      this.editOrderObj,
      this.context,
      this.rateController,
      this.isEdit = false,
      this.matchFoundObj,
      this.callback,
      this.isInstantMatch = false,
      this.isMatchFound = false});

  @override
  _ForeignDestinationAccountState createState() =>
      _ForeignDestinationAccountState();
}

class _ForeignDestinationAccountState extends State<ForeignDestinationAccount> {
  GlobalKey<FormState> _accountDetailsKey = GlobalKey<FormState>();

  bool isBank = false;
  String selectedOption = '';
  TextEditingController _accountNumberController =
      TextEditingController(text: '');
  TextEditingController _accountNameController =
      TextEditingController(text: '');
  TextEditingController _bankNameController = TextEditingController(text: '');
  TextEditingController _routingNumberController =
      TextEditingController(text: '');
  TextEditingController _zipCodeController = TextEditingController(text: '');
  TextEditingController _addressController = TextEditingController(text: '');
  TextEditingController _stateController = TextEditingController(text: '');
  TextEditingController _emailController = TextEditingController(text: '');
  TextEditingController _cityController = TextEditingController(text: '');
  TextEditingController _countryController = TextEditingController(text: '');
  TextEditingController _otherOptionDetailController =
      TextEditingController(text: '');

  bool _isLoading = false;

  List<ActivePaymentGateways> activePaymentGatewayList = [];

  List<String> _destinationOptions = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _addUserEmailToController();
    _routingNumberController.text = '';
    // log('Foreign Dest ${widget.editOrderObj.convertedCurrency}');

    _getAllPaymentOptions(widget.isInstantMatch
        ? widget?.matchFoundObj?.myCurrency ?? "USD"
        : widget.isMatchFound
            ? widget?.matchFoundObj?.convertedCurrency ?? "USD"
            : widget?.editOrderObj?.convertedCurrency ?? "USD");
  }

  void _getAllPaymentOptions(String myCurrency) {
    BlocProvider.of<PaymentSummaryStateBloc>(context).add(
        RetrieveActivePaymentGateway(
            myCurrency: myCurrency, isFromForeign: true));
  }

  @override
  Widget build(BuildContext context) {
    return MultiBlocListener(
      listeners: [
        BlocListener<MatchFoundScreenBloc, MatchFoundScreenState>(
          listener: (context, state) {
            log('What is foreign state $state');

            if (state is CreatingAndMatchingOrder) {
              widget.callback(state);

              setState(() {
                _isLoading = true;
              });
            }
            if (state is AcceptMatchFound) {
              Navigator.of(context).pushNamed('/payment_info',
                  arguments: {'matchFoundObj': widget.matchFoundObj});
            }
            if (state is MoveToDashboardScreen) {
              Navigator.of(context).popAndPushNamed('/dashboard');
            }
            // if (state is RejectMatchFound) {
            //   print('Move to payment info page');
            //   Navigator.of(context).pushNamed('/listings');
            // }
            if (state is MoveToCreateOrderScreen) {
              Navigator.of(context).popAndPushNamed('/create_order');
            }
            if (state is CreateAndMatchOrderSuccessful) {
              widget.callback(state);

              return anellohOverlay(
                  overlayObject: OverlayObject(
                    title: 'Notice',
                    body:
                        'You have 12 hours to make payment or your match will be cancelled.',
                    hasBtn: true,
                    hasCancelBtn: true,
                    clearToNavigate: true,
                    ctnFunction: () {
                      // move to dashboard
                    },
                    btnFunction: () {
                      // go to payment summary page...
                      Navigator.of(context)
                          .pushReplacementNamed('/payment_summary', arguments: {
                        'matchFoundObj': state.matchFoundObj,
                        'orderNo': state.customerOrderNo,
                        'customerId': state.customerId,
                        'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                            customerId: state.matchFoundObj.customerId,
                            orderNo: state.matchFoundObj.orderNo,
                            myAmount: state.matchFoundObj.myAmount,
                            totalAmount: state.matchFoundObj.totalAmount,
                            myCurrency: state.matchFoundObj.myCurrency,
                            rate: state.matchFoundObj.exchangeRate,
                            convertedCurrency:
                                state.matchFoundObj.convertedCurrency,
                            convertedAmount:
                                state.matchFoundObj.convertedAmount,
                            transactionFee: state.matchFoundObj.processingFee),
                      });
                    },
                    btnText: 'Pay Now',
                    cancelBtnText: 'Pay Later',
                    cancelBtnFunction: () {
                      Navigator.of(context)
                          .pushReplacementNamed(RoutesConstants.dashboardUrl);
                    },
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    ),
                  ),
                  context: context);
            }
            if (state is CreateAndMatchOrderFailed) {
              widget.callback(state);

              return anellohOverlay(
                  overlayObject: OverlayObject(
                    title: 'Error',
                    body: '${state.errorMessage}',
                    hasBtn: true,
                    hasCancelBtn: false,
                    clearToNavigate: true,
                    ctnFunction: () {
                      BlocProvider.of<MatchFoundScreenBloc>(context)
                          .add(MoveToDashboardScreenEvent());
                    },
                    btnFunction: () {
                      BlocProvider.of<MatchFoundScreenBloc>(context)
                          .add(MoveToDashboardScreenEvent());
                    },
                    btnText: 'Ok',
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    ),
                  ),
                  context: context);
            }
          },
        ),
        BlocListener<CreateOrderScreenBloc, CreateOrderScreenState>(
            listener: (context, state) {
          if (state is CreatingOrder)
            setState(() {
              _isLoading = true;
            });
          if (state is CreatingOrderSuccessful) {
            showToast(
                toastMsg: 'Order created successfully',
                toastLength: Toast.LENGTH_LONG,
                toastGravity: ToastGravity.BOTTOM,
                bgColor: ColorPalette().textWhite,
                txtColor: ColorPalette().main);
            // Future.delayed(Duration(seconds: 1)).then((res) {
            Navigator.of(context).pushReplacementNamed('/dashboard');
            // });
          }
          if (state is CreatingOrderFailed) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                    title: 'Error',
                    body: state.message,
                    hasBtn: false,
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    )),
                context: context);
            Navigator.pop(context);
          }
        }),
        BlocListener<EditOrderScreenBloc, EditOrderScreenState>(
            listener: (context, state) {
          if (state is EditingOrder)
            setState(() {
              _isLoading = true;
            });
          if (state is EditOrderSuccessfully) {
            showToast(
                toastMsg: widget.editOrderObj != null
                    ? 'Order updated successfully'
                    : 'Order edited successfully',
                toastLength: Toast.LENGTH_LONG,
                toastGravity: ToastGravity.BOTTOM,
                bgColor: ColorPalette().textWhite,
                txtColor: ColorPalette().main);
            // Future.delayed(Duration(seconds: 1)).then((res) {
            Navigator.of(context).pushReplacementNamed('/dashboard');
            // });
          }
          if (state is EditOrderFailed) {
            return anellohOverlay(
                overlayObject: OverlayObject(
                    title: 'Error',
                    body: state?.error ?? 'Failed to edit order',
                    hasBtn: false,
                    icon: Icon(
                      Icons.warning,
                      color: Colors.red,
                    )),
                context: context);
          }
        }),
        BlocListener<InstantMatchScreenBloc, InstantMatchScreenState>(
          listener: (context, state) {
            if (state is MatchingExistingOrder)
              setState(() {
                _isLoading = true;
              });
            if (state is SuccessfullyMatchedExistingOrder) {
              showToast(
                  toastMsg: 'Order matched successfully',
                  toastGravity: ToastGravity.BOTTOM,
                  toastLength: Toast.LENGTH_LONG,
                  bgColor: ColorPalette().textWhite,
                  txtColor: ColorPalette().main);

              log('state.paymentSummaryOrderCard.myCurrency Foreign Dest ${state?.paymentSummaryOrderCard?.myCurrency ?? AppConstants.instantMatchCurrency}');

              Navigator.of(context).pushReplacementNamed(
                '/payment_option',
                arguments: {
                  'orderNo': state.paymentSummaryOrderCard.orderNo,
                  // 'customerId': widget.customerId,
                  'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                      customerId: state.paymentSummaryOrderCard.customerId,
                      orderNo: state.paymentSummaryOrderCard.orderNo,
                      myAmount: state.paymentSummaryOrderCard.myAmount,
                      totalAmount: state.paymentSummaryOrderCard.totalAmount,
                      myCurrency: state?.paymentSummaryOrderCard?.myCurrency ??
                          AppConstants.instantMatchCurrency,
                      rate: state.paymentSummaryOrderCard.rate,
                      convertedAmount:
                          state.paymentSummaryOrderCard.convertedAmount,
                      convertedCurrency:
                          state.paymentSummaryOrderCard.convertedCurrency,
                      transactionFee:
                          state.paymentSummaryOrderCard.transactionFee),
                },
              );
            }
            if (state is ErrorMatchingExistingOrder) {
              Navigator.pop(context);
              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body: state.errorMessage,
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
            if (state is MatchExistingOrderSocketException) {
              Navigator.pop(context);

              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body:
                          'Oops, please check your internet connection,then retry',
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
            if (state is MatchExistingOrderHttpException) {
              Navigator.pop(context);

              return anellohOverlay(
                  overlayObject: OverlayObject(
                      title: 'Error',
                      body: 'Service currently unavailable',
                      hasBtn: false,
                      icon: Icon(
                        Icons.warning,
                        color: Colors.red,
                      )),
                  context: context);
            }
          },
        ),
        BlocListener<PaymentSummaryStateBloc, PaymentSummaryState>(
            listener: (context, state) {
          if (state is LoadingActivePaymentGateway) {
            setState(() {
              _isLoading = true;
            });
          }
          if (state is ActivePaymentGatewaysState &&
              state.activePaymentGatewayList.length != 0) {
            activePaymentGatewayList = state.activePaymentGatewayList;

            state.activePaymentGatewayList.forEach((element) {
              _destinationOptions.add(element.name);
            });

            setState(() {
              _isLoading = false;
            });
          }
        }),
      ],
      child: BlocBuilder<MatchFoundScreenBloc, MatchFoundScreenState>(
        builder: (context, state) =>
            BlocBuilder<InstantMatchScreenBloc, InstantMatchScreenState>(
          builder: (context, state) =>
              BlocBuilder<EditOrderScreenBloc, EditOrderScreenState>(
            builder: (context, state) =>
                BlocBuilder<CreateOrderScreenBloc, CreateOrderScreenState>(
              builder: (context, state) => _isLoading
                  ? Center(
                      child: Platform.isIOS
                          ? CupertinoActivityIndicator()
                          : CircularProgressIndicator(),
                    )
                  : SafeArea(
                      child: SingleChildScrollView(
                        child: Container(
                          height: _isLoading ? Get.height : Get.height + 220,
                          child: Form(
                            key: _accountDetailsKey,
                            autovalidateMode: AutovalidateMode.always,
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                bottomSheetHeaderWidget(context, 'Select Destination Payment Option', true, "This is where Anelloh pays your money to"),
                                DropdownSearch<String>(
                                  mode: Mode.BOTTOM_SHEET,
                                  label: 'Destination Option',
                                  showSelectedItem: true,
                                  items: _destinationOptions,
                                  onChanged: (value) {
                                    setState(() {
                                      isBank = value.contains('Bank Transfer')
                                          ? true
                                          : false;
                                      selectedOption = value;
                                      // print("Destination Option ${value}");
                                    });
                                  },
                                  showSearchBox: true,
                                  searchBoxDecoration: InputDecoration(
                                      labelText: 'Search destination option'),
                                  selectedItem: "",
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                selectedOption.isEmpty
                                    ? Container()
                                    : isBank
                                        ? _bankOptions()
                                        : AppTextInputs(
                                            textInputTitle:
                                                'Enter your $selectedOption ID',
                                            controller:
                                                _otherOptionDetailController,
                                            autoFocus: true,
                                            validateInput: (value) {
                                              return AppConstants.validators
                                                  .validateString(value);
                                            },
                                            isOnlyDigits: false,
                                          ),
                                SizedBox(
                                  height: 10,
                                ),
                                selectedOption.isEmpty
                                    ? Container()
                                    : AnellohRoundNetworkButtonLarge(
                                        onPressed: () {
                                          // Navigator.pop(context);

                                          print("Tapping Functions");
                                          _mainPublishOrder();
                                        },
                                        childText: 'Post Order',
                                        isColorInverted: false,
                                        isLoading: _isLoading)
                              ],
                            ),
                          ),
                        ).paddingSymmetric(horizontal: 10),
                      ),
                    ),
            ),
          ),
        ),
      ),
    );
  }

  List<String> _buildDropdownSearch({@required List<Bank> banks}) {
    return banks.map((e) => e.name).toList();
  }

  _bankOptions() {
    return Column(
      children: [
        DropdownSearch<String>(
          mode: Mode.BOTTOM_SHEET,
          label: 'Bank Name',
          showSelectedItem: true,
          items: _buildDropdownSearch(banks: widget.banks),
          validator: (String value) {
            if (value.isEmpty) return 'Please select bank.';
            return null;
          },
          onChanged: (value) {
            setState(() {
              _bankNameController.text = value;
              // print("Bank Name ${_bankNameController.text}");
            });
          },
          showSearchBox: true,
          searchBoxDecoration: InputDecoration(labelText: 'Search for bank'),
          selectedItem: "",
        ),
        SizedBox(
          height: 6,
        ),
        AppTextInputs(
          textInputTitle: 'Account Number',
          controller: _accountNumberController,
          textInputType: TextInputType.number,
          autoFocus: true,
          validateInput: (value) {
            return AppConstants.validators.validateNumber(value);
          },
          isOnlyDigits: true,
        ),
        AppTextInputs(
          textInputTitle: 'Account Name',
          controller: _accountNameController,
          validateInput: (value) {
            return AppConstants.validators.validateString(value);
          },
        ),
        AppTextInputs(
          textInputTitle: 'Routing Number',
          controller: _routingNumberController,
          textInputType: TextInputType.number,
          maxLength: 9,
          validateInput: (value) {
            return AppConstants.validators.validateDigitsOnlyLength(value, 9);
          },
          isOnlyDigits: true,
        ),
        AppTextInputs(
          textInputTitle: 'Zip Code',
          controller: _zipCodeController,
          validateInput: (value) {
            return AppConstants.validators.validateString(value);
          },
        ),
        AppTextInputs(
          textInputTitle: 'Address',
          controller: _addressController,
          validateInput: (value) {
            return AppConstants.validators.validateString(value);
          },
        ),
        CSCPicker(
          showStates: true,
          showCities: true,
          defaultCountry: DefaultCountry.United_States,
          flagState: CountryFlag.SHOW_IN_DROP_DOWN_ONLY,
          layout: Layout.vertical,
          dropdownDecoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.white,
              border: Border.all(color: appPrimaryColor, width: 1)),
          disabledDropdownDecoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              color: Colors.grey.shade300,
              border: Border.all(color: Colors.grey.shade300, width: 1)),
          selectedItemStyle: TextStyle(
            color: Colors.black,
            fontSize: 14,
          ),
          dropdownHeadingStyle: TextStyle(
              color: Colors.black, fontSize: 17, fontWeight: FontWeight.bold),
          dropdownItemStyle: TextStyle(
            color: Colors.black,
            fontSize: 14,
          ),
          searchBarRadius: 10,
          onCountryChanged: (value) {
            setState(() {
              _countryController.text = value;
            });
          },
          onStateChanged: (value) {
            setState(() {
              _stateController.text = value;
            });
          },
          onCityChanged: (value) {
            setState(() {
              _cityController.text = value;
            });
          },
        ),
        // AppTextInputs(
        //   textInputTitle: 'City of residence',
        //   controller: _cityController,
        //   validateInput: (value) {
        //     return AppConstants.validators.validateString(value);
        //   },
        // ),
        // AppTextInputs(
        //   textInputTitle: 'State of residence',
        //   controller: _stateController,
        //   validateInput: (value) {
        //     return AppConstants.validators.validateString(value);
        //   },
        // ),
        // AppTextInputs(
        //   textInputTitle: 'Country of residence',
        //   controller: _countryController,
        //   validateInput: (value) {
        //     return AppConstants.validators.validateString(value);
        //   },
        // ),
        SizedBox(
          height: 5,
        ),
        AppTextInputs(
          textInputTitle: 'Email associated with your bank account',
          textInputType: TextInputType.emailAddress,
          controller: _emailController,
          isReadOnly: true,
          validateInput: (value) {
            return AppConstants.validators.validateEmail(value);
          },
        ),
      ],
    );
  }

  _mainPublishOrder() {
    if (_accountDetailsKey.currentState.validate()) {
      // Navigator.of(widget.context).pop();

      widget.isMatchFound
          ? _createAndMatchOrder(matchFoundObj: widget.matchFoundObj)
          : widget.isInstantMatch
              ? _createAndMatchExistingOrder(
                  matchFoundObj: widget.matchFoundObj)
              : widget.isEdit
                  ? _editOrder()
                  : _createOrderBloc();

      // Navigator.pop(context);
    }
  }

  _createAndMatchExistingOrder({@required MatchFoundObj matchFoundObj}) async {
    try {
      if (_accountDetailsKey.currentState.validate()) {
        log('matchFoundObj.orderNo ${matchFoundObj.orderNo}');

        Map<String, dynamic> userObj;
        if (await readFromPref('__anelloh_current_user__')) {
          userObj = jsonDecode(await getFromPref('__anelloh_current_user__'));
        }

        log('userObj $userObj');
        log('AppConstants.userData.customerId ${AppConstants.userData.customerId}');
        log('matchFoundObj.customerId ${matchFoundObj.customerId}');

        BlocProvider.of<InstantMatchScreenBloc>(context)
            .add(CreateAndMatchExistingOrderEvent(
          existingOrderNo: matchFoundObj.orderNo,
          myCustomerId: userObj['customerId'],
          myAccountNumber: !isBank
              ? _otherOptionDetailController.text
              : _accountNumberController.text,
          myBankName:
              !isBank ? selectedOption : _bankNameController.text ?? "nil",
          address: _addressController.text,
          city: _cityController.text,
          state: _stateController.text,
          zipCode: _zipCodeController.text,
          myPaymentChannelId: activePaymentGatewayList
              .elementAt(activePaymentGatewayList.indexWhere(
                  (element) => element.name.toString() == selectedOption))
              .id,
          country: _countryController.text,
          myCurrency: widget.matchFoundObj.myCurrency,
          bankRouteNo: _routingNumberController.text != null
              ? _routingNumberController.text
              : 'n/a',
        ));
      }
    } catch (err) {
      print(err);
    }
  }

  void _createOrderBloc() {
    CreateOrderObj createOrderObj = new CreateOrderObj(
        customerId: AppConstants.userData.customerId,
        myAmount: widget.editOrderObj.myAmount is String
            ? int.parse(widget.editOrderObj.myAmount)
            : widget.editOrderObj.myAmount,
        myCurrency: widget.editOrderObj.myCurrency,
        rate: int.parse(widget.rateController.text),
        convertedCurrency: widget.editOrderObj.convertedCurrency,
        convertedAmount: widget.editOrderObj.convertedAmount,
        myAccountNumber: !isBank
            ? _otherOptionDetailController.text
            : _accountNumberController.text,
        myPaymentChannelId: activePaymentGatewayList
            .elementAt(activePaymentGatewayList.indexWhere(
                (element) => element.name.toString() == selectedOption))
            .id,
        myBankName:
            !isBank ? selectedOption : _bankNameController.text ?? "nil",
        address: _addressController.text,
        city: _cityController.text,
        state: _stateController.text,
        zipCode: _zipCodeController.text,
        country: _countryController.text,
        bankRouteNo: _routingNumberController.text ?? 'nil');
    BlocProvider.of<CreateOrderScreenBloc>(context)
        .add(CreateOrderActionEvent(createOrderObj: createOrderObj));
  }

  _editOrder() {
    EditOrderObj editOrderObj = EditOrderObj(
        customerId: widget.editOrderObj.customerId,
        orderId: widget.editOrderObj.orderId,
        myAmount: widget.editOrderObj.myAmount,
        myCurrency: widget.editOrderObj.myCurrency,
        rate: int.parse(widget.rateController.text),
        convertedAmount: widget.editOrderObj.convertedAmount,
        convertedCurrency: widget.editOrderObj.convertedCurrency,
        // myPaymentChannelId: double.parse(widget.editOrderObj.myPaymentChannelId).toInt(),
        myPaymentChannelId: activePaymentGatewayList
            .elementAt(activePaymentGatewayList.indexWhere(
                (element) => element.name.toString() == selectedOption))
            .id,
        myBankName:
            !isBank ? selectedOption : _bankNameController.text ?? "nil",
        myAccountNumber: !isBank
            ? _otherOptionDetailController.text
            : _accountNumberController.text,
        // widget.editOrderObj.myAccountNumber,
        bankRouteNo: _routingNumberController.text ?? 'nil',
        // widget.editOrderObj.bankRouteNo,
        address: _addressController.text,
        city: _cityController.text,
        state: _stateController.text,
        zipCode: _zipCodeController.text,
        country: _countryController.text,
        transactionFee: widget.editOrderObj.transactionFee,
        paymentStatus: widget.editOrderObj.paymentStatus,
        loggedInDevice: widget.editOrderObj.loggedInDevice,
        paymentReference: widget.editOrderObj.paymentReference);
    BlocProvider.of<EditOrderScreenBloc>(context)
        .add(EditOrderEvent(editOrderObj: editOrderObj));
  }

  void _addUserEmailToController() async {
    if (await readFromPref('__anelloh_current_email__')) {
      _emailController.text = await getFromPref('__anelloh_current_email__');
    }
  }

  _createAndMatchOrder({MatchFoundObj matchFoundObj}) {
    matchFoundObj.copyWith(
      myAccountNumber: !isBank
          ? _otherOptionDetailController.text
          : _accountNumberController.text,
      myBankName: !isBank ? selectedOption : _bankNameController.text ?? "nil",
    );
    BlocProvider.of<MatchFoundScreenBloc>(context).add(CreateAndMatchOrderEvent(
        matchFoundObj: matchFoundObj,
        address: _addressController.text,
        city: _cityController.text,
        state: _stateController.text,
        zipCode: _zipCodeController.text,
        country: _countryController.text,
        myBankName:
            !isBank ? selectedOption : _bankNameController.text ?? "nil",
        myAccountNumber: !isBank
            ? _otherOptionDetailController.text
            : _accountNumberController.text,
        myPaymentChannelId: activePaymentGatewayList
            .elementAt(activePaymentGatewayList.indexWhere(
                (element) => element.name.toString() == selectedOption))
            .id,
        user: AppConstants.userData));

    // Navigator.pop(context);
  }
}
