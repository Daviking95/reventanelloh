import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/UI/payment_options_screen.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/router/router_bloc.dart';
import 'package:anelloh_mobile/bloc/router/router_event.dart';
import 'package:anelloh_mobile/bloc/router/router_state.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_bloc.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_event.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_state.dart';
import 'package:anelloh_mobile/helpers/classes/activity_card.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/date_formatter.dart';
import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/activity_card.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../constants.dart';

class TransactionSummaryScreen extends StatefulWidget {
  final ActivityCardItem activityCardItem;

  TransactionSummaryScreen({@required this.activityCardItem}) : super();

  @override
  _TransactionSummaryScreen createState() => _TransactionSummaryScreen();
}

class _TransactionSummaryScreen extends State<TransactionSummaryScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return _bottomPageContent();
    // return StarterWidget(
    //   hasTopWidget: false,
    //   hasBackButton: true,
    //   hasPagePadding: false,
    //   hasNavDrawer: false,
    //   onRefreshFunction: () {
    //     return () {};
    //   },
    //   hasBgimage: false,
    //   bottomPageContent: _bottomPageContent(),
    // );
  }

  Widget _bottomPageContent() {
    // print("What is Match Order ${widget.activityCardItem.matchedOrderStatus}");
    // print("What is Order ${widget.activityCardItem.orderStatus}");
    // print("Stattus ${widget.activityCardItem.orderStatus == '1' && (widget.activityCardItem.matchedOrderStatus == "null" || widget.activityCardItem.matchedOrderStatus == null)}");

    return MultiBlocListener(
        listeners: [
          BlocListener<MainUserOrderBloc, MainUserOrderState>(
              listener: (context, state) {

                if(state is CancellingUserPendingOrderSuccessful) {

                  showToast(
                      toastMsg: state.message.toString(),
                      toastGravity: ToastGravity.CENTER,
                      bgColor: ColorPalette().textWhite,
                      txtColor: ColorPalette().main);

                  Navigator.of(context).pushNamed('/dashboard');
                }
              }),
          BlocListener<RouterBloc, RouterState>(listener: (context, state) {
            if (state is RouterMoveToPaymentSummaryScreen) {
              Navigator.of(context).pushNamed('/payment_summary', arguments: {
                'matchFoundObj': state.matchFoundObj,
                'editOrderObj': state.editOrderObj,
                'paymentSummaryOrderCard': PaymentSummaryOrderCard(
                  customerId: state.matchFoundObj.customerId,
                  orderNo: state.matchFoundObj.orderNo,
                  myAmount: state.matchFoundObj.myAmount,
                  totalAmount: state.matchFoundObj.totalAmount,
                  myCurrency: state.matchFoundObj.myCurrency,
                  rate: state.matchFoundObj.exchangeRate,
                  convertedCurrency: state.matchFoundObj.convertedCurrency,
                  convertedAmount: state.matchFoundObj.convertedAmount,
                  transactionFee: state.matchFoundObj.processingFee,
                )
              });
            }
          }),
        ],
        child: BlocBuilder<RouterBloc, RouterState>(builder: (context, state) {
          return BlocBuilder<MainUserOrderBloc, MainUserOrderState>(
            builder: (context, state) {
              return LoadingOverlay(
                isLoading: state is CancellingUserPendingStateChanged,
                progressIndicator: ProgressWidget(),
                color: ColorPalette().textBlack,
                opacity: 0.4,
                child: Scaffold(
                  resizeToAvoidBottomInset: true,
                  body: SafeArea(
                    child: SingleChildScrollView(
                      padding: EdgeInsets.only(
                          left: 20, right: 20, bottom: 10, top: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 20,),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: IconButton(
                                icon: Icon(
                                  Icons.arrow_back_ios_rounded,
                                  color: ColorPalette().main,
                                ),
                                onPressed: () {
                                  Navigator.pop(context);
                                }),
                          ),
                          SizedBox(height: 20,),
                          Container(
                            padding: EdgeInsets.only(bottom: 20),
                            width: MediaQuery.of(context).size.width,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                InkWell(
                                  child: Container(
                                    padding: EdgeInsets.all(7),
                                    decoration: BoxDecoration(
                                      border: Border(
                                          bottom: BorderSide(
                                              color: ColorPalette().dangerRed,
                                              width: 2.0)),
                                    ),
                                    child: CustomText(
                                      title: 'Order details',
                                      isBold: true,
                                      textSize:
                                          mainSizer(context: context, size: 22),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: mainSizer(context: context, size: 9),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            // padding: EdgeInsets.only(top: 20),
                            width: MediaQuery.of(context).size.width,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Container(
                                  padding: EdgeInsets.only(bottom: 25, top: 10),

                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            PaymentItemWidget(
                                                context,
                                                'Swap',
                                                '${CurrencyFormatter.format(currencyCode: '${widget.activityCardItem.myCurrency}', amount: formatToCurrency(widget.activityCardItem.myAmount))}',
                                                false),

                                          ],
                                        ),
                                        flex: 1,
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            PaymentItemWidget(
                                                context,
                                                'Currency',
                                                widget
                                                    .activityCardItem.myCurrency,
                                                false),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(bottom: 25, top: 10),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            PaymentItemWidget(
                                                context,
                                                'Rate',
                                                '${CurrencyFormatter.format(currencyCode: widget.activityCardItem.myCurrency == 'NGN' ? widget.activityCardItem.myCurrency : widget.activityCardItem.convertedCurrency, amount: widget.activityCardItem.rate)}/${CurrencyFormatter.format(currencyCode: widget.activityCardItem.convertedCurrency == 'NGN' ? widget.activityCardItem.myCurrency : widget.activityCardItem.convertedCurrency, amount: '')}',
                                                false),

                                          ],
                                        ),
                                        flex: 1,
                                      ),
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            PaymentItemWidget(
                                                context,
                                                'For',
                                                '${CurrencyFormatter.format(currencyCode: widget.activityCardItem.convertedCurrency, amount: formatToCurrency(widget.activityCardItem.convertedAmount))}',
                                                false),

                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Container(
                                  padding: EdgeInsets.only(bottom: 25, top: 10),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: [
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            CustomText(
                                              title: 'Status',
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            orderStatus(
                                                context: context,
                                                status: widget
                                                    .activityCardItem.orderStatus,
                                                matchedOrderStatus: widget
                                                    .activityCardItem
                                                    .matchedOrderStatus,
                                                activityCardItem:
                                                    widget.activityCardItem),
                                          ],
                                        ),
                                        flex: 1,
                                      ),
                                      // SizedBox(
                                      //   width: mainSizer(context: context, size: 5),
                                      // ),
                                      Expanded(
                                        child: Column(
                                          mainAxisSize: MainAxisSize.min,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          children: [
                                            PaymentItemWidget(
                                                context,
                                                'Date/Time',
                                                '${formatDate(widget.activityCardItem.createdTime)}',
                                                false),

                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.max,
                                  children: [
                                    // _getCancelOrderBtn(),
                                    SizedBox(),
                                    _getCTAButton()
                                  ],
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              );
            },
          );
        }));
  }

  _getCTAButton() {
    if (widget.activityCardItem.orderStatus == '7') return Container();
    if ((widget.activityCardItem.matchedOrderStatus.toString() == '1' ||
            widget.activityCardItem.matchedOrderStatus.toString() ==
                'Matched') &&
        widget.activityCardItem.orderStatus.toString() == '1') {
      return AnellohRoundButtonSmall(
          childText: 'Pay Now',
          onPressed: () async {
            var resultEntity;
            if (await readFromPref('__anelloh_current_user__')) {
              resultEntity =
                  jsonDecode(await getFromPref('__anelloh_current_user__'));
            }
            var deviceId;
            if (await readFromPref('__device__token')) {
              deviceId = await getFromPref('__device__token') ??
                  await getDeviceToken();
            } else {
              deviceId = await getDeviceToken();
            }

            MatchFoundObj matchFoundObj = MatchFoundObj(
                orderNo: widget.activityCardItem.orderNo,
                entityId: widget.activityCardItem.orderId,
                customerId: AppConstants.userData.customerId,
                rates: [],
                userEmail: '',
                // add to api
                orderOwnerId: widget.activityCardItem.customerId,
                myCurrency: widget.activityCardItem.myCurrency,
                myAmount: widget.activityCardItem.myAmount,
                orderStatus: int.parse(widget.activityCardItem.orderStatus),
                userName: widget.activityCardItem.userName,
                totalAmount: widget.activityCardItem.totalAmount.toString(),
                isRateAscending: true,
                processingFee:
                    widget.activityCardItem.transactionFee.toString(),
                ascDecValue:
                    double.parse(widget.activityCardItem.convertedAmount),
                subTotal: widget.activityCardItem.totalAmount.toString(),
                exchangeRate: widget.activityCardItem.rate,
                convertedAmount: widget.activityCardItem.convertedAmount,
                convertedCurrency: widget.activityCardItem.convertedCurrency);

            EditOrderObj editOrderObj = EditOrderObj(
              orderId: widget.activityCardItem.orderId,
              myAmount: widget.activityCardItem.myAmount,
              myCurrency: widget.activityCardItem.myCurrency,
              rate: widget.activityCardItem.rate,
              convertedAmount: widget.activityCardItem.convertedAmount,
              convertedCurrency: widget.activityCardItem.convertedCurrency,
              customerId: resultEntity['customerId'].toString(),
              myAccountNumber:
                  widget.activityCardItem.myAccountNumber.toString(),
              myBankName: widget.activityCardItem.myBankName.toString(),
              bankRouteNo: widget.activityCardItem.bankRouteNo.toString(),
              myPaymentChannelId: widget.activityCardItem.myPaymentChannelId,
              transactionFee: widget.activityCardItem.transactionFee,
              paymentStatus: widget.activityCardItem.paymentStatus,
              loggedInDevice: deviceId,
            );

            BlocProvider.of<RouterBloc>(context).add(
                RouterMoveToPaymentSummaryScreenEvent(
                    matchFoundObj: matchFoundObj, editOrderObj: editOrderObj));
          });
    } else if ((widget.activityCardItem.orderStatus.toString() == '1' &&
            (widget.activityCardItem.matchedOrderStatus == "null" ||
                widget.activityCardItem.matchedOrderStatus == null)) ||
        widget.activityCardItem.orderStatus.toString() == '5') {
      return AnellohRoundButtonSmall(
          childText: 'Edit Order',
          onPressed: () async {
            var resultEntity;
            if (await readFromPref('__anelloh_current_user__')) {
              resultEntity =
                  jsonDecode(await getFromPref('__anelloh_current_user__'));
            }

            var deviceId;
            if (await readFromPref('__device__token')) {
              deviceId = await getFromPref('__device__token') ??
                  await getDeviceToken();
            } else {
              deviceId = await getDeviceToken();
            }
            EditOrderObj editOrderObj = EditOrderObj(
              orderId: widget.activityCardItem.orderId.toString(),
              myAmount: widget.activityCardItem.myAmount.toString(),
              myCurrency: widget.activityCardItem.myCurrency.toString(),
              rate: widget.activityCardItem.rate.toString(),
              convertedAmount:
                  widget.activityCardItem.convertedAmount.toString(),
              convertedCurrency:
                  widget.activityCardItem.convertedCurrency.toString(),
              customerId: resultEntity['customerId'].toString(),
              myAccountNumber:
                  widget.activityCardItem.myAccountNumber.toString(),
              myBankName: widget.activityCardItem.myBankName.toString(),
              bankRouteNo: widget.activityCardItem.bankRouteNo.toString(),
              myPaymentChannelId:
                  widget.activityCardItem.myPaymentChannelId.toString(),
              transactionFee: widget.activityCardItem.transactionFee.toString(),
              paymentStatus: widget.activityCardItem.paymentStatus.toString(),
              loggedInDevice: deviceId,
            );
            Navigator.of(context).pushNamed('/edit_order',
                arguments: {'editOrderObj': editOrderObj});
          });
    } else
      return Container();
  }

  _getCancelOrderBtn() {
    if ((widget.activityCardItem.orderStatus.toString() == '1' &&
            (widget.activityCardItem.matchedOrderStatus == "null" ||
                widget.activityCardItem.matchedOrderStatus == null)) ||
        widget.activityCardItem.orderStatus.toString() == '5') {
      return AnellohFlatButton(
          childText: 'Cancel Order',
          color: Colors.red,
          onPressed: () async {

            var resultEntity;
            if (await readFromPref('__anelloh_current_user__')) {
              resultEntity =
                  jsonDecode(await getFromPref('__anelloh_current_user__'));
            }

            log("Hello Order Cancel");

            BlocProvider.of<MainUserOrderBloc>(context).add(
                CancelPendingOrderEvent(
                    customerId: resultEntity['customerId'].toString(),
                    orderNo: widget.activityCardItem.orderId.toString()));
          });
    } else
      return Container();
  }
}
