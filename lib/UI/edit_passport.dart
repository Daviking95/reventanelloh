
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:date_field/date_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:local_auth/local_auth.dart';

class EditPassportScreen extends StatefulWidget {
  _EditPassportScreenState createState() => _EditPassportScreenState();
}

class _EditPassportScreenState extends State<EditPassportScreen> {
  GlobalKey _formKey = GlobalKey<FormState>();
  bool isSaving = false;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
      bgColor: ColorPalette().textWhite,
      hasBackButton: true,
      hasBottomNav: false,
      hasRefreshOption: false,
      bottomNavIndex: 1,
      isExitApp: true,
      isBgAllowed: false,
      backBtnColor: ColorPalette().main,
      bottomPageContent: _bottomPageContent(
          context: context,
          formKey: _formKey,
      ),
    );
  }

  Widget _bottomPageContent({
    @required BuildContext context,
    @required GlobalKey<FormState> formKey,
  }) {

    final TextEditingController _passportNumber = TextEditingController();

    DateTime expiryDate;

    return BlocBuilder<UserBloc, UserState>(
      builder: (context, state) {
        return SingleChildScrollView(
          padding: EdgeInsets.all(15),
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child:  subPageTitle('Change Passport ID', 'You can change your Passport information here', context: context),
                  margin: EdgeInsets.only(bottom: 30),
                ),
                Container(
                  child: Form(
                      key: formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                              width: MediaQuery.of(context).size.width,
                              child: AppPasswordTextInput(
                                  controller: _passportNumber,
                                  textInputTitle: 'Passport Number',
                                  maxLength: 4,
                                  isOnlyDigits: true,
                                  validateInput: AppConstants.validators.validateNumber
                              )
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          DateTimeFormField(
                            decoration: InputDecoration(
                              labelText: 'Expiry Date',
                              border: OutlineInputBorder(
                                borderRadius:
                                BorderRadius.circular(13),
                                borderSide: BorderSide(
                                  color: ColorPalette().main
                                )
                              ),
                            ),
                            validator: (value) {
                              return null;
                            } ,
                            mode: DateTimeFieldPickerMode.date,
                            autovalidateMode: AutovalidateMode.always,
                            onDateSelected: (DateTime value) {
                              setState(() {
                                expiryDate = value;
                              });
                            },
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Container(
                            width:  MediaQuery.of(context).size.width,
                            margin: EdgeInsets.only(top: 15, bottom: 15),
                            child: AnellohRoundNetworkButtonLarge(
                              onPressed: () {},
                              childText: 'Save',
                              isLoading: false,
                            ),
                          ),
                        ],
                      )
                  ),
                )
              ],
            ),
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void deactivate() {
    super.deactivate();
  }
}
