import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/profile/profile_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_event.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_state.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_state.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/selet_profile_image_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/helpers/showDialog.dart';
import 'package:anelloh_mobile/styles.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:photo_manager/photo_manager.dart';

class EditProfileScreen extends StatefulWidget {
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  GlobalKey<FormState> _editProfileFormKey = GlobalKey<FormState>();
  String phoneNumber = '';
  String countryCode = '';

  TextEditingController _firstNameController = TextEditingController();
  TextEditingController _lastNameController = TextEditingController();
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();

  bool _isLoading = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _activateUser();
  }

  _activateUser() async {
    try {
      var resultEntity;
      if (await readFromPref('__anelloh_current_user__')) {
        resultEntity =
            jsonDecode(await getFromPref('__anelloh_current_user__'));
      }

      String customerId = resultEntity['customerId'].toString();
      BlocProvider.of<UserBloc>(context)
          .add(GetCurrentCustomerByIdEvent(customerId: customerId));
    } catch (err) {
      print(err);
    }
  }

  _handleSetFormField({
    @required String field,
    @required String value,
  }) {
    switch (field) {
      case 'countryCode':
        setState(() {
          countryCode = value;
        });
        break;
      default:
        setState(() {
          phoneNumber = value;
        });
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<SelectProfileImageScreenBloc,
        SelectProfileImageScreenState>(
      listener: (context, state) {
        if (state is UploadingUserImage) {
          setState(() {
            _isLoading = true;
          });
        }
        if (state is ImageUploadFailed) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                  title: 'Error',
                  body: 'Image upload failed',
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  )),
              context: context);
        }
        if (state is ImageUploadSuccess) {
          showToast(
              toastMsg: 'Image upload successful',
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          Navigator.of(context).popAndPushNamed('/profile');
        }
      },
      child: BlocBuilder<SelectProfileImageScreenBloc,
          SelectProfileImageScreenState>(
        builder: (context, buildState) =>
            BlocListener<ProfileScreenBloc, ProfileScreenState>(
          listener: (context, state) {
            if (state is MoveToSelectProfileImageScreen) {
              Navigator.of(context).pushNamed('/upload_profile_image',
                  arguments: {'imageList': state.imageList});
            }
          },
          child: BlocBuilder<ProfileScreenBloc, ProfileScreenState>(
            builder: (context, buildState) {
              return BlocBuilder<UserBloc, UserState>(
                  builder: (context, userBuildState) {
                return LoadingOverlay(
                  isLoading: buildState is EditingUserData ? true : false,
                  progressIndicator: ProgressWidget(),
                  color: ColorPalette().textBlack,
                  opacity: 0.4,
                  child: LoadingOverlay(
                    isLoading: _isLoading
                        ? true
                        : userBuildState is SaveCurrentUser
                            ? false
                            : true,
                    progressIndicator: ProgressWidget(),
                    opacity: 0.4,
                    child: StarterWidget(
                      hasBottomNav: false,
                      hasBackButton: true,
                      hasRefreshOption: false,
                      onRefreshFunction: () {},
                      isExitApp: true,
                      isBgAllowed: false,
                      bgColor: ColorPalette().main,
                      backBtnColor: ColorPalette().textWhite,
                      bottomPageContent: _bottomPageContent(
                        context: context,
                        buildState: buildState,
                        userBuildState: userBuildState,
                        handleSetFormField: _handleSetFormField,
                        countryCode: this.countryCode,
                        phoneNumber: this.phoneNumber,
                      ),
                    ),
                  ),
                );
              });
            },
          ),
        ),
      ),
    );
  }

  Widget _bottomPageContent({
    @required BuildContext context,
    @required ProfileScreenState buildState,
    @required UserState userBuildState,
    @required Function handleSetFormField,
    @required String phoneNumber,
    @required String countryCode,
  }) {
    User user = userBuildState.user;
    _firstNameController.value = TextEditingValue(text: user?.firstName ?? '');
    _lastNameController.value = TextEditingValue(text: user?.lastName ?? '');
    _userNameController.value = TextEditingValue(text: user?.userName ?? '');
    _emailController.value = TextEditingValue(text: user?.emailAddress ?? '');

    _handleEditUser() {
      if (_editProfileFormKey.currentState.validate()) {
        EditUserData editUserData = EditUserData(
            firstName: _firstNameController.text,
            lastName: _lastNameController.text,
            userName: _userNameController.text,
            emailAddress: _emailController.text,
            phoneNumber: phoneNumber,
            countryCode: countryCode);
        BlocProvider.of<ProfileScreenBloc>(context)
            .add(EditUserEvent(editUserData: editUserData, user: user));
      }
    }

    return SingleChildScrollView(
      padding: EdgeInsets.only(bottom: isPortrait(context) ? 0 : 250),
      child: Container(
          width: Get.width,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              _profileHeader(context: context),
              Container(
                margin: EdgeInsets.only(top: 25),
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                child: Form(
                  key: _editProfileFormKey,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                          width: MediaQuery.of(context).size.width,
                          child: AppTextInputs(
                            controller: _firstNameController,
                            hintText: user?.firstName ?? '',
                            textInputTitle: 'First name',
                            // autoFocus: true,
                            validateInput: AppConstants.validators.validateName,
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          child: AppTextInputs(
                            controller: _lastNameController,
                            hintText: user?.lastName ?? '',
                            textInputTitle: 'Last name',
                            validateInput: AppConstants.validators.validateName,
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          child: AppTextInputs(
                            controller: _userNameController,
                            hintText: user?.userName ?? '',
                            textInputTitle: 'User name',
                            validateInput:
                                AppConstants.validators.validateString,
                          )),
                      Container(
                          width: MediaQuery.of(context).size.width,
                          child: AppTextInputs(
                            controller: _emailController,
                            hintText: user?.emailAddress ?? '',
                            textInputTitle: 'Email Address',
                            validateInput:
                                AppConstants.validators.validateEmail,
                            textInputType: TextInputType.emailAddress,
                          )),
                      SizedBox(
                        height: 10,
                      ),
                      BlocBuilder<UserBloc, UserState>(builder: (context, bs) {
                        if (bs is SaveCurrentUser) {
                          return IntlPhoneField(
                            keyboardType: TextInputType.phone,
                            style: textStyleWhiteColorNormal.copyWith(
                                color: Colors.black),
                            countryCodeTextColor: ColorPalette().textBlack,
                            decoration: InputDecoration(
                              labelText: 'Phone Number',
                              fillColor: ColorPalette().textBlack,
                              border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(15)),
                            ),
                            dropdownDecoration: BoxDecoration(),
                            initialValue: bs?.user?.phoneNumber ?? '',
                            initialCountryCode: bs?.user?.countryCode ?? '',
                            onChanged: (phone) {
                              handleSetFormField(
                                  value: phone.countryISOCode,
                                  field: 'countryCode');
                              handleSetFormField(
                                  value: phone?.number ?? '',
                                  field: 'phoneNumber');
                            },
                          );
                        } else {
                          return SizedBox();
                        }
                      }),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 15, bottom: 15),
                        child: AnellohRoundNetworkButtonLarge(
                          onPressed: buildState is EditingUserData
                              ? null
                              : _handleEditUser,
                          childText: 'Submit',
                          isLoading:
                              buildState is EditingUserData ? true : false,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )),
    );
  }

  Widget _profileHeader({@required BuildContext context}) {
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
      decoration: BoxDecoration(
          color: ColorPalette().main,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(35),
              bottomRight: Radius.circular(35))),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            child: subPageTitle('Edit Profile', 'Quickly edit your profile information here',
                color: ColorPalette().textWhite, context: context),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(bottom: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  width: MediaQuery.of(context).size.width,
                  margin: EdgeInsets.only(top: 25),
                  alignment: Alignment.center,
                  child: BlocBuilder<UserBloc, UserState>(
                    builder: (context, state) {
                      return _buildUserProfileImage(
                          state: state, context: context);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildUserProfileImage(
      {@required UserState state, @required BuildContext context}) {
    String selectedAssetName = Platform.isAndroid ? 'Recent' : 'Recents';

    File _fileImage2;
    final TextEditingController _profileImageController =
        new TextEditingController();

    _handleImageUpload() async {
      var result = await PhotoManager.requestPermission();
      if (result) {
        List<AssetPathEntity> assetPath =
            await PhotoManager.getAssetPathList(type: RequestType.image);
        if (assetPath.length > 0) {
          AssetPathEntity assetPathEntity = assetPath
              .where((AssetPathEntity element) =>
                  element.name == selectedAssetName)
              .single;
          if (assetPathEntity != null) {
            List<AssetEntity> imageList =
                await assetPathEntity.getAssetListRange(start: 0, end: 500);
            BlocProvider.of<ProfileScreenBloc>(context)
                .add(MoveToSelectProfileImageScreenEvent(imageList: imageList));
          }
        }
      }
    }

    if (state is InitialUserState) {
      User _user = state.user;
      return Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: [
          ClipRRect(
            child: _user.profileImage != null && _user.profileImage.length > 0
                ? Image.network('${_user.profileImage}',
                    fit: BoxFit.cover,
                    width: 110,
                    height: 110, errorBuilder: (context, _, __) {
                    return Container(
                        width: 110,
                        height: 110,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100)),
                        child: Image(
                          width: 110,
                          height: 110,
                          image: AssetImage(ImageConstants.profilePicture),
                          fit: BoxFit.cover,
                        ));
                  })
                : Image(
                    width: 110,
                    height: 110,
                    image: AssetImage(ImageConstants.profilePicture),
                    fit: BoxFit.cover,
                  ),
            borderRadius: BorderRadius.circular(100),
          ),
          Positioned(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(.9),
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.6),
                        blurRadius: 1.4,
                        offset: Offset(1, 1),
                        spreadRadius: 1.2)
                  ]),
              child: InkWell(
                child: Icon(
                  MdiIcons.pencilOutline,
                  color: ColorPalette().main,
                  size: 20,
                ),
                onTap: () => displayImagePickerDialog(context, () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.pop(context);
                  String _fileName;
                  PickedFile image = await ImagePicker().getImage(
                    source: ImageSource.camera,
                    imageQuality: 10,
                  );
                  setState(() {
                    if (image != null) {
                      _fileImage2 = File(image.path);

                      String base64Image =
                          base64Encode(_fileImage2.readAsBytesSync());
                      // _filePath = fileImage.path;
                      _fileName = _fileImage2.path.split("/").last;

                      _profileImageController.text = base64Image;

                      User _user =
                          BlocProvider.of<UserBloc>(context).state.user;
                      BlocProvider.of<SelectProfileImageScreenBloc>(context)
                          .add(UploadUserImageEvent(
                              imageFile: _fileImage2,
                              customerId: _user.customerId));
                    } else {
                      print('No image selected.');
                    }
                  });
                }, () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.pop(context);
                  String _fileName;
                  PickedFile image = await ImagePicker().getImage(
                    source: ImageSource.gallery,
                    imageQuality: 10,
                  );
                  setState(() {
                    if (image != null) {
                      _fileImage2 = File(image.path);

                      String base64Image =
                          base64Encode(_fileImage2.readAsBytesSync());
                      // _filePath = fileImage.path;
                      _fileName = _fileImage2.path.split("/").last;

                      _profileImageController.text = base64Image;

                      User _user =
                          BlocProvider.of<UserBloc>(context).state.user;
                      BlocProvider.of<SelectProfileImageScreenBloc>(context)
                          .add(UploadUserImageEvent(
                              imageFile: _fileImage2,
                              customerId: _user.customerId));
                    } else {
                      print('No image selected.');
                    }
                  });
                }),
              ),
            ),
          ),
        ],
      );
    } else if (state is SaveCurrentUser) {
      User _user = state.user;
      return Stack(
        alignment: AlignmentDirectional.bottomEnd,
        children: [
          ClipRRect(
            child: _user.profileImage != null
                ? Image.network('${_user.profileImage}',
                    fit: BoxFit.cover,
                    width: 110,
                    height: 110, errorBuilder: (context, _, __) {
                    return Container(
                        width: 110,
                        height: 110,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(100)),
                        child: Image(
                          width: 110,
                          height: 110,
                          image: AssetImage(ImageConstants.profilePicture),
                          fit: BoxFit.cover,
                        ));
                  })
                : Image(
                    width: 110,
                    height: 110,
                    image: AssetImage(ImageConstants.profilePicture),
                    fit: BoxFit.cover,
                  ),
            borderRadius: BorderRadius.circular(100),
          ),
          Positioned(
            child: Container(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              decoration: BoxDecoration(
                  color: Colors.white.withOpacity(.9),
                  borderRadius: BorderRadius.circular(100),
                  boxShadow: [
                    BoxShadow(
                        color: Colors.grey.withOpacity(0.6),
                        blurRadius: 1.4,
                        offset: Offset(1, 1),
                        spreadRadius: 1.2)
                  ]),
              child: InkWell(
                child: Icon(
                  MdiIcons.pencilOutline,
                  color: ColorPalette().main,
                  size: 20,
                ),
                onTap: () => displayImagePickerDialog(context, () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.pop(context);
                  String _fileName;
                  PickedFile image = await ImagePicker().getImage(
                    source: ImageSource.camera,
                    imageQuality: 10,
                  );
                  setState(() {
                    if (image != null) {
                      _fileImage2 = File(image.path);

                      String base64Image =
                          base64Encode(_fileImage2.readAsBytesSync());
                      // _filePath = fileImage.path;
                      _fileName = _fileImage2.path.split("/").last;

                      _profileImageController.text = base64Image;

                      User _user =
                          BlocProvider.of<UserBloc>(context).state.user;
                      BlocProvider.of<SelectProfileImageScreenBloc>(context)
                          .add(UploadUserImageEvent(
                              imageFile: _fileImage2,
                              customerId: _user.customerId));
                    } else {
                      print('No image selected.');
                    }
                  });
                }, () async {
                  FocusScope.of(context).requestFocus(FocusNode());
                  Navigator.pop(context);
                  String _fileName;
                  PickedFile image = await ImagePicker().getImage(
                    source: ImageSource.gallery,
                    imageQuality: 10,
                  );
                  setState(() {
                    if (image != null) {
                      _fileImage2 = File(image.path);

                      String base64Image =
                          base64Encode(_fileImage2.readAsBytesSync());
                      // _filePath = fileImage.path;
                      _fileName = _fileImage2.path.split("/").last;

                      _profileImageController.text = base64Image;

                      User _user =
                          BlocProvider.of<UserBloc>(context).state.user;
                      BlocProvider.of<SelectProfileImageScreenBloc>(context)
                          .add(UploadUserImageEvent(
                              imageFile: _fileImage2,
                              customerId: _user.customerId));
                    } else {
                      print('No image selected.');
                    }
                  });
                }),
              ),
            ),
          ),
        ],
      );
    } else {
      return Container(
        child: Image(
          width: 110,
          height: 110,
          image: AssetImage(ImageConstants.profilePicture),
          fit: BoxFit.cover,
        ),
      );
    }
  }

  @override
  void dispose() {
    super.dispose();
  }
}
