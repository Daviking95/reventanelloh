import 'dart:convert';

import 'package:anelloh_mobile/bloc/launch/launch_bloc.dart';
import 'package:anelloh_mobile/bloc/launch/launch_event.dart';
import 'package:anelloh_mobile/bloc/launch/launch_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key key}) : super(key: key);

  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  @override
  void initState() {
    super.initState();
    _checkHasOnboarded();
  }

  void _checkHasOnboarded() {
    Future.delayed(Duration(seconds: 4), () {
      BlocProvider.of<LaunchScreenBloc>(context).add(CheckHasOnboardedEvent());
    });
  }

  Widget build(BuildContext context) {
    return BlocListener<LaunchScreenBloc, LaunchScreenState>(
      listener: (context, state) async {
        if (state is UserHasNotOnboarded) {
          Navigator.of(context)
              .pushNamed(RoutesConstants.onboardingUrl); // onboardingUrl
        }

        if (state is UserHasOnboarded) {
          Map<String, dynamic> user;
          if (await readFromPref('__anelloh_current_user__')) {
            user = jsonDecode(await getFromPref('__anelloh_current_user__'));
          }
          if (user != null) {
            if (user['email'] != null) {
              Navigator.of(context).pushNamed(RoutesConstants.loginUrl,
                  arguments: {'email': user['email']});
            } else {
              Navigator.of(context).pushNamed(RoutesConstants.loginUrl);
            }
          } else {
            Navigator.of(context).pushNamed(RoutesConstants.loginUrl);
          }
        }
      },
      child: Scaffold(
        resizeToAvoidBottomInset: true,
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(color: Colors.white),
          child: Center(
              child: SvgPicture.asset(
            ImageConstants.appIconTwo,
            width: 150,
            height: 150,
            fit: BoxFit.contain,
          )),
        ),
      ),
    );
  }
}
