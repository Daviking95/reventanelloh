import 'package:anelloh_mobile/bloc/otp/otp_bloc.dart';
import 'package:anelloh_mobile/bloc/otp/otp_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class OTPScreen extends StatefulWidget {
  final String token;
  OTPScreen({Key key, this.token}) : super(key: key);
  _OTPScreenState createState() => _OTPScreenState();
}

class _OTPScreenState extends State<OTPScreen> {
  @override
  void initState() {
    super.initState();
  }

  String otpValueOne = '';
  String otpValueTwo = '';
  String otpValueThree = '';
  String otpValueFour = '';

  final _formKey = GlobalKey<FormState>();

  FocusNode _focusInputOne = FocusNode();
  FocusNode _focusInputTwo = FocusNode();
  FocusNode _focusInputThree = FocusNode();
  FocusNode _focusInputFour = FocusNode();

  _inputFieldChange({String value, String field}) {
    final Map<String, FocusNode> mining = {
      'one': _focusInputOne,
      'two': _focusInputTwo,
      'three': _focusInputThree,
      'four': _focusInputFour,
    };

    switch (field) {
      case 'one':
        setState(() {
          otpValueOne = value;
        });
        break;
      case 'two':
        setState(() {
          otpValueTwo = value;
        });
        break;
      case 'three':
        setState(() {
          otpValueThree = value;
        });
        break;
      case 'four':
        setState(() {
          otpValueFour = value;
        });
        break;
    }

    var item = mining.entries.firstWhere((element) => element.key == field);
    if (value.isNotEmpty && field != 'four') {
      item.value.nextFocus();
    } else {
      if (value.isEmpty && field != 'one') {
        item.value.previousFocus();
      }
    }

    if (field == 'four' && value.isNotEmpty) {
      item.value.unfocus();
    }
  }

  _validateFields() {
    var formErrors = {};

    if (this.otpValueOne.isEmpty || this.otpValueOne.length != 1) {
      formErrors['one'] = true;
    }
    if (this.otpValueTwo.isEmpty || this.otpValueTwo.length != 1) {
      formErrors['two'] = true;
    }
    if (this.otpValueThree.isEmpty || this.otpValueThree.length != 1) {
      formErrors['three'] = true;
    }
    if (this.otpValueFour.isEmpty || this.otpValueFour.length != 1) {
      formErrors['four'] = true;
    }

    if (formErrors.entries.length > 0) {
      return true;
    }
    return false;
  }

  _verifyOTP() {
    bool hasFormErrors = _validateFields();
    if (hasFormErrors == false) {
      var otpCode =
          '${this.otpValueOne + this.otpValueTwo + this.otpValueThree + this.otpValueFour}';
      // print(otpCode);
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<OTPScreenBloc, OTPScreenState>(
      listener: (context, state) {},
      child: BlocBuilder<OTPScreenBloc, OTPScreenState>(
        builder: (context, blocState) {
          return Scaffold(
            resizeToAvoidBottomInset: true,
            body: SafeArea(
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ClipRRect(
                            child: SvgPicture.asset(
                              ImageConstants.otp_mail,
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 35),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Container(
                                  child: Text(
                                    'OTP Authentication',
                                    style: GoogleFonts.montserrat(
                                      fontSize: 20,
                                      color: ColorPalette().main,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Text(
                                    'Enter the OTP sent to your mail',
                                    style: GoogleFonts.montserrat(
                                      fontSize: 15,
                                      color: ColorPalette().main,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      width: MediaQuery.of(context).size.width,
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        margin: EdgeInsets.only(top: 40),
                        alignment: Alignment.center,
                        child: Form(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.always,
                          child: Flex(
                            direction: Axis.horizontal,
                            mainAxisSize: MainAxisSize.min,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxWidth: 55,
                                    minWidth: 55,
                                    maxHeight: 55,
                                    minHeight: 55),
                                child: TextFormField(
                                  autofocus: true,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value, field: 'one');
                                  },
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.phone,
                                  focusNode: _focusInputOne,
                                  initialValue: otpValueOne,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(16))),
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(10)),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxWidth: 55,
                                    minWidth: 55,
                                    maxHeight: 55,
                                    minHeight: 55),
                                child: TextFormField(
                                  autofocus: true,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value, field: 'two');
                                  },
                                  textAlign: TextAlign.center,
                                  keyboardType: TextInputType.phone,
                                  focusNode: _focusInputTwo,
                                  initialValue: otpValueTwo,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(16))),
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(10)),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxWidth: 55,
                                    minWidth: 55,
                                    maxHeight: 55,
                                    minHeight: 55),
                                child: TextFormField(
                                  autofocus: true,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value, field: 'three');
                                  },
                                  keyboardType: TextInputType.phone,
                                  textAlign: TextAlign.center,
                                  focusNode: _focusInputThree,
                                  initialValue: otpValueThree,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(16))),
                                ),
                              ),
                              Padding(padding: EdgeInsets.all(10)),
                              ConstrainedBox(
                                constraints: BoxConstraints(
                                    maxWidth: 55,
                                    minWidth: 55,
                                    maxHeight: 55,
                                    minHeight: 55),
                                child: TextFormField(
                                  autofocus: true,
                                  style: TextStyle(
                                    fontSize: 18,
                                  ),
                                  onChanged: (value) {
                                    _inputFieldChange(
                                        value: value, field: 'four');
                                  },
                                  keyboardType: TextInputType.phone,
                                  textAlign: TextAlign.center,
                                  focusNode: _focusInputFour,
                                  initialValue: otpValueFour,
                                  decoration: InputDecoration(
                                      border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(16))),
                                ),
                              ),
                            ],
                          ),
                        )),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 25),
                      width: MediaQuery.of(context).size.width,
                      child: AnellohFlatButton(
                        onPressed: () {},
                        childText: 'Resend OTP?',
                        fontWeight: FontWeight.normal,
                        color: ColorPalette().dangerRed,
                      ),
                    ),
                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 25),
                      width: MediaQuery.of(context).size.width,
                      child: AnellohRoundNetworkButtonLarge(
                        onPressed: _verifyOTP,
                        isLoading: blocState is VerifyingOTP ? true : false,
                        childText: 'Verify',
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
