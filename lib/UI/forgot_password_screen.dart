import 'dart:io';

import 'package:anelloh_mobile/bloc/forgot_password/forgot_password_bloc.dart';
import 'package:anelloh_mobile/bloc/forgot_password/forgot_password_event.dart';
import 'package:anelloh_mobile/bloc/forgot_password/forgot_password_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/font_util.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:loading_overlay/loading_overlay.dart';

import '../styles.dart';

class ForgotPasswordScreen extends StatefulWidget {
  final String email;
  final String token;

  ForgotPasswordScreen({this.email, this.token});

  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}

class _ForgotPasswordScreenState extends State<ForgotPasswordScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController _emailAddressController = TextEditingController();
  final TextEditingController _newPasswordController =
      new TextEditingController();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.email != null) {
      _emailAddressController.value = TextEditingValue(text: widget.email);
      // this._checkBiometrics();
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<ForgotPasswordScreenBloc, ForgotPasswordScreenState>(
      listener: (context, state) {
        if (state is CompletedPasswordRecovery) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Success',
                body: state?.message?.toString() ?? "",
                hasBtn: true,
                btnFunction: () =>
                    Navigator.of(context).pushNamed(RoutesConstants.loginUrl),
                icon: Icon(
                  Icons.check,
                  color: Colors.greenAccent,
                ),
                btnText: "Ok",
              ),
              context: context);
        }

        if (state is FailedPasswordRecoveryException) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Error',
                body: state.error,
                hasBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
        if (state is ForgotPasswordHttpException) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Warning',
                body: 'Slow or poor internet connection',
                hasBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
        if (state is ForgotPasswordNoInternetException) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Warning',
                body: 'Not connected to the internet',
                hasBtn: false,
                hasCancelBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }
      },
      child: BlocBuilder<ForgotPasswordScreenBloc, ForgotPasswordScreenState>(
        builder: (context, blocState) {
          return LoadingOverlay(
            isLoading: blocState is LoadingPasswordRecovery ? true : false,
            progressIndicator: ProgressWidget(),
            color: ColorPalette().textBlack,
            opacity: 0.4,
            child: Scaffold(
              resizeToAvoidBottomInset: true,
              body: SafeArea(
                  child: SingleChildScrollView(
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 15),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            Align(
                              alignment: Alignment.centerLeft,
                              child: IconButton(
                                  icon: Icon(
                                    Icons.arrow_back_ios_rounded,
                                    color: ColorPalette().main,
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  }),
                            ),
                            Container(
                              child: ClipRRect(
                                child: SvgPicture.asset(
                                  ImageConstants.anellohLogo,
                                ),
                              ),
                              width: MediaQuery.of(context).size.width,
                            ),
                            Container(
                              margin: EdgeInsets.only(top: 25),
                              child: Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Container(
                                    child: Text(
                                      widget.email != null
                                          ? 'Reset Password'
                                          : 'Forgot Password',
                                      style: GoogleFonts.montserrat(
                                        fontSize: 20,
                                        color: ColorPalette().main,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(top: 10),
                                    width: MediaQuery.of(context).size.width,
                                    alignment: Alignment.center,
                                    child: Container(
                                      width: 320,
                                      child: Text(
                                        widget.email != null
                                            ? ""
                                            : 'Type in the email address you signed up with.',
                                        style:
                                            googleFontTextStyle(fontSize: 15),
                                        textAlign: TextAlign.center,
                                      ),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(
                            top: 70, left: 20, right: 20, bottom: 20),
                        child: Form(
                          key: _formKey,
                          autovalidateMode: AutovalidateMode.always,
                          child: Column(
                            children: [
                              AppTextInputs(
                                color: ColorPalette().main,
                                controller: _emailAddressController,
                                autoFocus: true,
                                textInputType: TextInputType.emailAddress,
                                validateInput:
                                    AppConstants.validators.validateEmail,
                                textInputTitle: "Email address",
                              ),
                              widget.email != null
                                  ? AppPasswordTextInput(
                                      color: mainColor,
                                      controller: _newPasswordController,
                                      validateInput: AppConstants
                                          .validators.validatePassword,
                                      textInputTitle: "New Password",
                                    )
                                  : Container(),
                              Padding(padding: EdgeInsets.all(10)),
                              AnellohRoundNetworkButtonLarge(
                                  onPressed: () {
                                    BlocProvider.of<ForgotPasswordScreenBloc>(
                                            context)
                                        .add(RecoverPasswordEvent(
                                            emailAddress:
                                                _emailAddressController.text,
                                            token: widget.token,
                                            isChange: widget.email != null
                                                ? true
                                                : false,
                                            newPassword: widget.email != null
                                                ? _newPasswordController.text
                                                : null));
                                  },
                                  childText: widget.email != null
                                      ? 'Change Password'
                                      : 'Recover Password',
                                  isLoading:
                                      blocState is LoadingPasswordRecovery
                                          ? true
                                          : false),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              )),
            ),
          );
        },
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}
