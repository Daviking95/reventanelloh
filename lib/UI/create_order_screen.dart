import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/all_banks/all_banks_bloc.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_event.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_state.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_state.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_bloc.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_event.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_state.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_bloc.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_bloc.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_events.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_state.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_bloc.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_bloc.dart';
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/bottom_sheet_widgets.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/line_chart.dart';
import 'package:anelloh_mobile/widgets/notify_prof_wid.dart';
import 'package:anelloh_mobile/widgets/progress_dialog.dart';
import 'package:anelloh_mobile/widgets/rate_arrow.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:currency_picker/currency_picker.dart';
import 'package:dio/dio.dart' as Dio;
import 'package:dropdown_search/dropdown_search.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_placeholder_textlines/flutter_placeholder_textlines.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:loading_overlay/loading_overlay.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../constants.dart';
import 'foreign_destination_account.dart';

class CreateOrderScreen extends StatefulWidget {
  final EditOrderObj editOrderObj;

  CreateOrderScreen({this.editOrderObj}) : super();

  _CreateOrderScreenState createState() => _CreateOrderScreenState();
}

class _CreateOrderScreenState extends State<CreateOrderScreen> {
  String currencyToSwap = 'USD';
  String currencyNeeded = '';
  String currencyNeededSymbol = '';
  bool canChangeCurrencyNeeded = true;

  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<FormState> _accountDetailsKey = GlobalKey<FormState>();

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  PersistentBottomSheetController controller;

  String selectedBank = '';

  List<String> activeCurrencies = ["NGN"];

  TextEditingController _accountNumberController =
      TextEditingController(text: '');
  TextEditingController _routingNumberController = TextEditingController();
  TextEditingController _valueToBeSwapped = TextEditingController();
  FocusNode _valueToBeSwappedFocus = FocusNode();
  TextEditingController _convertedAmount = TextEditingController();
  FocusNode _valueSwapExpectedFocus = FocusNode();
  TextEditingController _rate = TextEditingController();

  @override
  void initState() {
    super.initState();
    setState(() {
      if (widget.editOrderObj != null) {
        currencyToSwap = widget.editOrderObj.myCurrency;
        currencyNeeded = widget.editOrderObj.convertedCurrency;
        _valueToBeSwapped =
            TextEditingController(text: widget.editOrderObj.myAmount);
        _rate = TextEditingController(text: widget.editOrderObj.rate);
        _convertedAmount.text = widget.editOrderObj.convertedAmount;
      }
    });
    _fetchMarketRates();
    _fetchActiveCurrencies();
    _fetchTransactionLimit();

    _rate.addListener(_calculateAmount);
    _valueToBeSwapped.addListener(_calculateAmount);
  }

  _fetchMarketRates() {
    BlocProvider.of<MarketRateBloc>(context).add(FetchMarketRateEvent());
  }

  _fetchActiveCurrencies() {
    BlocProvider.of<CreateOrderScreenBloc>(context)
        .add(FetchActiveCurrenciesEvent());
  }

  void _fetchTransactionLimit() {
    BlocProvider.of<CreateOrderScreenBloc>(context)
        .add(FetchTransactionLimitEvent());
  }

  _chooseCurrencyToSwap() {
    showCurrencyPicker(
        context: context,
        currencyFilter: activeCurrencies,
        onSelect: (Currency currency) {
          setState(() {
            currencyToSwap = currency.code;
          });
          if (currency.code == 'NGN') {
            currencyNeeded = 'USD';
          }
        });
  }

  _chooseCurrencyNeeded() {
    if (this.currencyToSwap == 'NGN') {
      setState(() {
        showCurrencyPicker(
            context: context,
            currencyFilter: activeCurrencies,
            onSelect: (Currency currency) {
              if (currency.code == this.currencyToSwap) {
                return;
              } else {
                setState(() {
                  currencyNeeded = currency.code;
                  currencyNeededSymbol = currency.symbol;
                });
              }
            });
      });
    }
  }

  _updateSelectedBank(value) {
    setState(() {
      selectedBank = value;
    });
  }

  _autoSetCurrencyNeededIfToSwapNotNGN() {
    if (this.currencyToSwap != 'NGN') {
      setState(() {
        currencyNeeded = 'NGN';
        currencyNeededSymbol = '₦';
        canChangeCurrencyNeeded = false;
      });
    }
    // else {
    //   setState(() {
    //     currencyNeeded = this.currencyNeeded;
    //     currencyNeededSymbol = this.currencyNeededSymbol;
    //     canChangeCurrencyNeeded = true;
    //   });
    // }
  }

  _determinePageLoading({@required CreateOrderScreenState state}) {
    if (state is FindingMatch) return true;
    if (state is CreatingOrder) return true;
    if (state is FindingActiveCurrencies) return true;
    if (state is FindingTransactionLimit) return true;
    return false;
  }

  List<String> _buildDropdownSearch({@required List<Bank> banks}) {
    return banks.map((e) => e.name).toList();
  }

  _publishOrder(
      {@required BuildContext context,
      @required List<Bank> banks,
      @required TextEditingController rateController}) {
    EditOrderObj editOrderObj = EditOrderObj(
        myAmount: _valueToBeSwapped.text,
        myCurrency: currencyToSwap,
        convertedCurrency: currencyNeeded,
        rate: _rate.text,
        convertedAmount: int.parse(_convertedAmount.text));

    String accountName = '';
    int hasVerifiedAccountNumber = 0;
    if (_formKey.currentState.validate()) {
      return showModalBottomSheet(
          context: context,
          isScrollControlled: true,
          useRootNavigator: true,
          backgroundColor: ColorPalette().textWhite,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(15), topRight: Radius.circular(15))),
          builder: (BuildContext context) {
            // 0 not typing, 1 started typing, 2 ended typing && verified, 3 ended typing invalid.
            return StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return SingleChildScrollView(
                child: Container(
                  padding: EdgeInsets.only(
                      bottom: MediaQuery.of(context).viewInsets.bottom),
                  child: Form(
                    key: _accountDetailsKey,
                    autovalidateMode: AutovalidateMode.always,
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 20),
                      child: currencyNeeded != 'NGN'
                          ? MultiBlocProvider(
                              providers: [
                                  BlocProvider<CreateOrderScreenBloc>(
                                      create: (context) =>
                                          CreateOrderScreenBloc()),
                                  BlocProvider<EditOrderScreenBloc>(
                                      create: (context) =>
                                          EditOrderScreenBloc()),
                                  BlocProvider<CurrencyConversionBloc>(
                                      create: (context) =>
                                          CurrencyConversionBloc()),
                                  BlocProvider<InstantMatchScreenBloc>(
                                      create: (context) =>
                                          InstantMatchScreenBloc()),
                                  BlocProvider<PaymentSummaryStateBloc>(
                                    create: (context) =>
                                        PaymentSummaryStateBloc(),
                                  ),
                                  BlocProvider<MatchFoundScreenBloc>(
                                    create: (context) => MatchFoundScreenBloc(),
                                  ),
                                ],
                              child: Container(
                                height: Get.height / 2,
                                child: ForeignDestinationAccount(
                                    context: context,
                                    banks: banks,
                                    editOrderObj: editOrderObj,
                                    rateController: rateController),
                              ))
                          : Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              mainAxisSize: MainAxisSize.min,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                bottomSheetHeaderWidget(
                                    context,
                                    'Destination Account Details',
                                    true,
                                    "This is where Anelloh pays your money to"),
                                DropdownSearch<String>(
                                  mode: Mode.BOTTOM_SHEET,
                                  label: 'Bank Name',
                                  showSelectedItem: true,
                                  items: _buildDropdownSearch(banks: banks),
                                  onChanged: (value) {
                                    _updateSelectedBank(value);
                                  },
                                  showSearchBox: true,
                                  searchBoxDecoration: InputDecoration(
                                      labelText: 'Search for bank'),
                                  selectedItem: this.selectedBank.isNotEmpty
                                      ? this.selectedBank
                                      : banks[0].name,
                                ),
                                SizedBox(
                                  height: 6,
                                ),
                                AppTextInputs(
                                  textInputTitle: 'Account Number',
                                  controller: _accountNumberController,
                                  textInputType: TextInputType.number,
                                  autoFocus: true,
                                  validateInput: (value) {
                                    if (this.currencyNeeded == 'NGN') {
                                      return AppConstants.validators
                                          .validateNigerianAccountNumber(value);
                                    } else {
                                      return AppConstants.validators
                                          .validateNumber(value);
                                    }
                                  },
                                  isOnlyDigits: true,
                                  onChange: (value) async {
                                    try {
                                      setState(() {
                                        hasVerifiedAccountNumber = 1;
                                      });
                                      if (value.length == 10) {
                                        String sb = this.selectedBank.isNotEmpty
                                            ? this.selectedBank
                                            : banks[0].name;
                                        Bank bank = banks.firstWhere(
                                            (Bank bank) => bank.name == sb);
                                        Dio.Response response = await dio.get(
                                            'https://api.paystack.co/bank/resolve?account_number=${_accountNumberController.text}&bank_code=${bank.code}',
                                            options: Dio.Options(headers: {
                                              'Authorization':
                                                  'Bearer ${env['PAYSTACK_LIVE_SECRET_KEY']}'
                                            }));
                                        if (response.statusCode == 200) {
                                          if (response.data['status'] == true) {
                                            setState(() {
                                              accountName = response
                                                  .data['data']['account_name'];
                                              hasVerifiedAccountNumber = 2;
                                            });
                                          }
                                        } else {
                                          setState(() {
                                            accountName =
                                                'Invalid account number';
                                            hasVerifiedAccountNumber = 3;
                                          });
                                        }
                                      }
                                    } catch (err) {
                                      if (err is Dio.DioError) {
                                        print(err.response);
                                        print(err.response.data);
                                        setState(() {
                                          accountName =
                                              err.response.data['message'];
                                          hasVerifiedAccountNumber = 3;
                                        });
                                      }
                                    }
                                  },
                                ),
                                // currencyNeeded != 'NGN'
                                //     ? AppTextInputs(
                                //         textInputTitle: 'Routing Number',
                                //         controller: _routingNumberController,
                                //         textInputType: TextInputType.number,
                                //         isOnlyDigits: true,
                                //       )
                                //     : SizedBox(),
                                SizedBox(
                                  height: 1,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 10, right: 10),
                                  child: Text(
                                    '$accountName',
                                    style: TextStyle(
                                        color: hasVerifiedAccountNumber == 2
                                            ? Colors.green
                                            : Colors.red),
                                  ),
                                ),
                                SizedBox(
                                  height: 15,
                                ),
                                AnellohRoundNetworkButtonLarge(
                                    onPressed: () {
                                      Navigator.pop(context);
                                      if (hasVerifiedAccountNumber != 2) {
                                        return () {};
                                      } else {
                                        _mainPublishOrder();
                                      }
                                    },
                                    childText: hasVerifiedAccountNumber == 2
                                        ? 'Post Order'
                                        : hasVerifiedAccountNumber == 1
                                            ? 'Verifying account number...'
                                            : 'Awaiting account number verification...',
                                    isColorInverted: false,
                                    isLoading: false)
                              ],
                            ),
                    ),
                  ),
                ),
              );
            });
          });
    }
  }

  _mainPublishOrder() {
    if (_accountDetailsKey.currentState.validate()) {
      // int amountToGet = BlocProvider.of<CurrencyConversionBloc>(context).state.rate;

      CreateOrderObj createOrderObj = new CreateOrderObj(
          customerId: AppConstants.userData.customerId,
          myAmount: int.parse(_valueToBeSwapped.text),
          myCurrency: currencyToSwap,
          rate: int.parse(_rate.text),
          convertedCurrency: currencyNeeded,
          convertedAmount: int.parse(_convertedAmount.text),
          myPaymentChannelId: 8,
          myAccountNumber: _accountNumberController.text,
          myBankName: selectedBank,
          bankRouteNo: _routingNumberController.text ?? 'nil');
      // create an order
      BlocProvider.of<CreateOrderScreenBloc>(context)
          .add(CreateOrderActionEvent(createOrderObj: createOrderObj));
    }
  }

  @override
  Widget build(BuildContext context) {
    _fetchActiveCurrencies();

    State self = this;
    _autoSetCurrencyNeededIfToSwapNotNGN();
    return BlocListener<CreateOrderScreenBloc, CreateOrderScreenState>(
      listener: (context, state) {
        if (state is MoveToFindMatchScreen) {
          Navigator.of(context).pushNamed('/match_found',
              arguments: {'matchFoundObj': state.matchFoundObj});
        }
        if (state is MoveToMatchNotFoundScreen) {
          Navigator.of(context).pushNamed('/no_match', arguments: {
            'createOrderObj': state.createOrderObj,
            'isEdit': false
          });
        }
        if (state is ActiveCurrenciesListScreen) {
          activeCurrencies = [];
          state.activeCurrenciesData.forEach((element) {
            activeCurrencies.add(element.currencyCodeString);
          });
        }
        if (state is CreatingOrderSuccessful) {
          showToast(
              toastMsg: 'Order created successfully',
              toastLength: Toast.LENGTH_LONG,
              toastGravity: ToastGravity.BOTTOM,
              bgColor: ColorPalette().textWhite,
              txtColor: ColorPalette().main);
          // Future.delayed(Duration(seconds: 1)).then((res) {
          Navigator.of(context).pushReplacementNamed('/dashboard');
          // });
        }
        if (state is CreatingOrderFailed) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                  title: 'Error',
                  body: state.message,
                  hasBtn: false,
                  icon: Icon(
                    Icons.warning,
                    color: Colors.red,
                  )),
              context: context);
        }
      },
      child: BlocListener<AllBanksBloc, AllBanksState>(
        listener: (context, st) {
          if (st is SuccessfullyFetchedAllBanks) {
            Navigator.of(context).pop();
            _publishOrder(
                context: context, banks: st.banks, rateController: _rate);
          }
          if (st is FetchingBanks) {
            showModalBottomSheet(
                context: context,
                backgroundColor: ColorPalette().textWhite,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(15),
                        topRight: Radius.circular(15))),
                builder: (context) {
                  return Center(
                    child: Platform.isIOS
                        ? CupertinoActivityIndicator()
                        : CircularProgressIndicator(
                            backgroundColor: ColorPalette().main,
                          ),
                  );
                });
          }
          if (st is ErrorFetchingAllBanks) {
            Navigator.of(context).pop();
          }
        },
        child: BlocBuilder<CreateOrderScreenBloc, CreateOrderScreenState>(
          builder: (context, state) {
            return LoadingOverlay(
              child: StarterWidget(
                hasBackButton: true,
                hasBottomNav: false,
                hasTopWidget: false,
                topPageContent: _topPageContent(context: context),
                bottomPageContent: _bottomPageContent(
                  context: context,
                  currencyNeeded: this.currencyNeeded,
                  currencyNeededSymbol: this.currencyNeededSymbol,
                  currencyToSwap: this.currencyToSwap,
                  formKey: _formKey,
                  accountDetailsKey: _accountDetailsKey,
                  chooseCurrencyToSwap: _chooseCurrencyToSwap,
                  chooseCurrencyNeeded: _chooseCurrencyNeeded,
                  canChangeCurrencyNeeded: canChangeCurrencyNeeded,
                ),
              ),
              isLoading: this._determinePageLoading(state: state),
              progressIndicator: ProgressWidget(),
              color: ColorPalette().textBlack,
              opacity: 0.4,
            );
          },
        ),
      ),
    );
  }

  Widget _bottomPageContent({
    @required BuildContext context,
    @required String currencyToSwap,
    @required String currencyNeeded,
    @required String currencyNeededSymbol,
    @required GlobalKey<FormState> formKey,
    @required GlobalKey<FormState> accountDetailsKey,
    @required Function chooseCurrencyToSwap,
    @required Function chooseCurrencyNeeded,
    @required bool canChangeCurrencyNeeded,
  }) {
    _findMatch() {
      if (formKey.currentState.validate()) {
        // int amountToGet = BlocProvider.of<CurrencyConversionBloc>(context).state.rate;
        log('currencyToSwap $currencyToSwap');
        log('currencyNeeded $currencyNeeded');

        int transactionLimit = double.parse(AppConstants.transactionLimitArray
                .elementAt(AppConstants.transactionLimitArray.indexWhere(
                    (element) => element.currency == currencyToSwap))
                .transactionLimit
                .toString())
            .toInt();

        log('transactionLimit $transactionLimit');

        if (int.parse(_valueToBeSwapped.text) >= transactionLimit) {
          return anellohOverlay(
              overlayObject: OverlayObject(
                title: 'Error',
                body:
                    'You cannot swap with this amount. Please input lesser amount',
                hasBtn: false,
                icon: Icon(
                  Icons.warning,
                  color: Colors.red,
                ),
              ),
              context: context);
        }

        CreateOrderObj createOrderObj = new CreateOrderObj(
          customerId: AppConstants.userData.customerId,
          myAmount: int.parse(_valueToBeSwapped.text),
          myCurrency: currencyToSwap,
          rate: int.parse(_rate.text),
          convertedCurrency: currencyNeeded,
          convertedAmount: int.parse(_convertedAmount.text),
        );
        BlocProvider.of<CreateOrderScreenBloc>(context)
            .add(FindMatchEvent(createOrderObj: createOrderObj));
      }
    }

    _fetchNigerianBanks() {
      BlocProvider.of<AllBanksBloc>(context).add(GetAllNigerianBanksEvent());
    }

    _fetchOtherBanks() {
      BlocProvider.of<AllBanksBloc>(context).add(GetOtherBanksEvent());
    }

    _fetchBanks() {
      int transactionLimit = double.parse(AppConstants.transactionLimitArray
              .elementAt(AppConstants.transactionLimitArray
                  .indexWhere((element) => element.currency == currencyToSwap))
              .transactionLimit
              .toString())
          .toInt();

      log('transactionLimit $transactionLimit');

      if (int.parse(_valueToBeSwapped.text) >= transactionLimit) {
        return anellohOverlay(
            overlayObject: OverlayObject(
              title: 'Error',
              body:
                  'You cannot swap with this amount. Please input lesser amount',
              hasBtn: false,
              icon: Icon(
                Icons.warning,
                color: Colors.red,
              ),
            ),
            context: context);
      }

      if (currencyNeeded == 'NGN') {
        _fetchNigerianBanks();
      } else {
        _fetchOtherBanks();
      }
    }

    return SafeArea(
      child: SingleChildScrollView(
        padding: EdgeInsets.symmetric(vertical: 17.0, horizontal: 17.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            pageTitle('Create Order',
                'You can create a new order for swapping/matching'),
            SizedBox(
              height: 15,
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              child: Form(
                key: formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CustomText(
                      title: 'I want to swap',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                            child: InkWell(
                          onTap: () {
                            chooseCurrencyToSwap();
                            BlocProvider.of<CurrencyConversionBloc>(context)
                                .add(ConvertCurrency(convertedAmount: 0));
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 6),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(17),
                              border: Border.all(color: ColorPalette().main),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(
                                  child: Text(
                                    '$currencyToSwap',
                                    style: TextStyle(
                                        color: ColorPalette().textBlack,
                                        fontSize: 16),
                                  ),
                                  flex: 2,
                                ),
                                Expanded(
                                    child: Icon(
                                  MdiIcons.chevronDown,
                                ))
                              ],
                            ),
                          ),
                        )),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 3,
                          child: AppTextInputs(
                            textInputTitle: '',
                            controller: _valueToBeSwapped,
                            // focusNode: _valueToBeSwappedFocus,
                            textInputType: TextInputType.number,
                            // autoFocus: true,
                            validateInput: (String value) =>
                                AppConstants.validators.validateTransLimit(
                                    currencyToSwap, _valueToBeSwapped.text),
                            isOnlyDigits: true,
                            noSpace: true,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    CustomText(
                      title: 'Market Rate',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    BlocBuilder<MarketRateBloc, MarketRateState>(
                        builder: (context, state) {
                      if (state is FetchingMarketRate) {
                        return Center(
                          child: Container(
                            decoration: BoxDecoration(color: Colors.white),
                            height: 100,
                            width: Get.width,
                            margin: EdgeInsets.only(top: 20),
                            child: PlaceholderLines(
                              count: 5,
                              animate: true,
                              color: Colors.grey.withOpacity(0.003),
                              lineHeight: 7,
                            ),
                          ),
                        );
                      } else {
                        if (state is SaveMarketRateData) {
                          if (currencyToSwap != 'NGN') {
                            MarketRateObj _mro = state.marketRates.firstWhere(
                                (element) =>
                                    element.baseCurrency == currencyToSwap);
                            return Container(
                              padding: EdgeInsets.symmetric(
                                  vertical: 8, horizontal: 9),
                              decoration: BoxDecoration(
                                  color: ColorPalette().textWhite,
                                  boxShadow: [
                                    BoxShadow(
                                        color: Colors.grey.withOpacity(0.1),
                                        offset: Offset(4, 1),
                                        blurRadius: 9.3,
                                        spreadRadius: 3.3),
                                  ],
                                  borderRadius: BorderRadius.circular(10)),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.start,
                                mainAxisSize: MainAxisSize.max,
                                children: [
                                  Expanded(
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        SvgPicture.asset(
                                            'assets/images/nigeria_flag.svg'),
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            CustomText(
                                              title: '${_mro.variableCurrency}',
                                            ),
                                            CustomText(
                                              isBold: true,
                                              title:
                                                  '${CurrencyFormatter.format(currencyCode: _mro.variableCurrency, amount: _mro.marketRate)}',
                                            )
                                          ],
                                        ),
                                      ],
                                    ),
                                    flex: 3,
                                  ),
                                  Expanded(
                                      child: ConstrainedBox(
                                    constraints: BoxConstraints(
                                        maxHeight: 100, minHeight: 100),
                                    child: Column(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      children: [
                                        ConstrainedBox(
                                          constraints: BoxConstraints(
                                              maxWidth: 400, maxHeight: 20),
                                          child: CustomFLineChart(
                                            spots: [
                                              FlSpot(5, 15),
                                              FlSpot(10, 10),
                                              FlSpot(15, 33),
                                              FlSpot(20, 20),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 15,
                                        ),
                                        Expanded(
                                          child: Row(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.center,
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              Expanded(
                                                  child: Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.center,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                mainAxisSize: MainAxisSize.min,
                                                children: [
                                                  CustomText(
                                                    title: '+',
                                                  ),
                                                  CustomText(
                                                    title: CurrencyFormatter
                                                        .format(
                                                            currencyCode: _mro
                                                                .variableCurrency,
                                                            amount: _mro
                                                                .ascDecValue
                                                                .toString()),
                                                  ),
                                                  rateArrow(
                                                      isAscending:
                                                          _mro.isRateAscending,
                                                      size: 16.0),
                                                ],
                                              )),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )),
                                ],
                              ),
                            );
                          } else {
                            List<MarketRateObj> _mros = state.marketRates
                                .where(
                                    (element) => element.baseCurrency != 'NGN')
                                .toList();
                            return Container(
                              constraints: BoxConstraints(
                                  maxHeight: 100, minHeight: 100),
                              padding: EdgeInsets.symmetric(
                                  vertical: 10, horizontal: 9),
                              child: ListView.builder(
                                  scrollDirection: Axis.horizontal,
                                  itemCount: _mros.length,
                                  itemBuilder: (BuildContext context, idx) {
                                    return Container(
                                      constraints: BoxConstraints(
                                          maxHeight: 40, minHeight: 40),
                                      margin: EdgeInsets.only(right: 6.0),
                                      padding: EdgeInsets.all(10),
                                      decoration: BoxDecoration(
                                          color: Colors.white,
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(10)),
                                          boxShadow: [
                                            BoxShadow(
                                                color: Colors.grey
                                                    .withOpacity(0.1),
                                                offset: Offset(4, 1),
                                                blurRadius: 9.3,
                                                spreadRadius: 3.3)
                                          ]),
                                      child: Column(
                                        mainAxisSize: MainAxisSize.min,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                            MainAxisAlignment.start,
                                        children: [
                                          Expanded(
                                            child: Row(
                                              mainAxisSize: MainAxisSize.max,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          100),
                                                  child: Image.asset(
                                                    'icons/flags/png/${_mros[idx].baseCurrency.substring(0, 2).toLowerCase()}.png',
                                                    package: 'country_icons',
                                                    width: 30,
                                                    height: 30,
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                CustomText(
                                                  title:
                                                      '${_mros[idx].baseCurrency}',
                                                  textSize: mainSizer(
                                                      context: context,
                                                      size: 35),
                                                  isBold: true,
                                                )
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Expanded(
                                              child: CustomText(
                                            title:
                                                '${CurrencyFormatter.format(currencyCode: _mros[idx].baseCurrency, amount: _mros[idx].marketRate)}',
                                            textSize: mainSizer(
                                                context: context, size: 35),
                                            isBold: true,
                                          ))
                                        ],
                                      ),
                                    );
                                  }),
                            );
                          }
                        } else {
                          return SizedBox();
                        }
                      }
                    }),
                    SizedBox(
                      height: 20,
                    ),
                    CustomText(
                      title: 'My preferred rate',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                            child: InkWell(
                          onTap: () {
                            // chooseCurrencyNeeded();
                            // BlocProvider.of<CurrencyConversionBloc>(context)
                            //     .add(ConvertCurrency(convertedAmount: 0));
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            alignment: Alignment.center,
                            padding: EdgeInsets.symmetric(
                                vertical: 16, horizontal: 6),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15),
                              color: canChangeCurrencyNeeded
                                  ? ColorPalette().textWhite
                                  : Colors.grey.withOpacity(0.1),
                              border: Border.all(
                                color: canChangeCurrencyNeeded
                                    ? ColorPalette().main
                                    : ColorPalette().main,
                              ),
                            ),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                Expanded(
                                  child: Center(
                                    child: Text(
                                      'NGN',
                                      style: TextStyle(
                                          color: ColorPalette().textBlack,
                                          fontSize: 16),
                                    ),
                                  ),
                                  flex: 2,
                                ),
                                canChangeCurrencyNeeded
                                    ? Expanded(
                                        child: Icon(
                                        MdiIcons.chevronDown,
                                      ))
                                    : SizedBox()
                              ],
                            ),
                          ),
                        )),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          flex: 3,
                          child: AppTextInputs(
                            textInputTitle: '',
                            controller: _rate,
                            textInputType: TextInputType.number,
                            // focusNode: _valueSwapExpectedFocus,
                            isOnlyDigits: true,
                            // autoFocus: true,
                            validateInput:
                                AppConstants.validators.validateNumber,
                            noSpace: true,
                          ),
                        )
                      ],
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    CustomText(
                      title: 'You will get',
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: Container(
                            constraints:
                                BoxConstraints(maxHeight: 55, minHeight: 55),
                            decoration: BoxDecoration(
                              border: Border.all(color: ColorPalette().main),
                              borderRadius: BorderRadius.circular(15),
                            ),
                            child: Center(
                              child: Text('$currencyNeeded',
                                  style: TextStyle(
                                      color: ColorPalette().textBlack,
                                      fontSize: 16)),
                            ),
                          ),
                          flex: 2,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        BlocBuilder<CurrencyConversionBloc,
                            CurrencyConversionState>(
                          builder: (context, state) {
                            if (state is CurrencyConversionSuccessful) {
                              _convertedAmount.text =
                                  state?.convertedAmount?.toInt()?.toString() ??
                                      '0';
                              return Expanded(
                                flex: 6,
                                child: Column(
                                  children: [

                                    AppTextInputs(
                                      textInputTitle: '',
                                      controller: _convertedAmount,
                                      // focusNode: _valueToBeSwappedFocus,
                                      textInputType: TextInputType.number,
                                      isReadOnly: true,
                                      // autoFocus: true,
                                      validateInput: (String value) =>
                                          AppConstants.validators.validateTransLimit(
                                              currencyNeeded, state?.convertedAmount?.toInt()?.toString() ??
                                                              _convertedAmount.text),
                                      isOnlyDigits: true,
                                      noSpace: true,
                                    )

                                    // Container(
                                    //   alignment: Alignment.centerLeft,
                                    //   padding:
                                    //       EdgeInsets.symmetric(horizontal: 5),
                                    //   constraints: BoxConstraints(
                                    //       maxHeight: 55, minHeight: 55),
                                    //   decoration: BoxDecoration(
                                    //     border: Border.all(
                                    //         color: ColorPalette().main),
                                    //     borderRadius: BorderRadius.circular(15),
                                    //   ),
                                    //   child: Text(
                                    //       '${state?.convertedAmount ?? _convertedAmount.text}',
                                    //       style: TextStyle(
                                    //           color: ColorPalette().textBlack,
                                    //           fontSize: 16)),
                                    // ),


                                    // if(AppConstants.validators.validateTransLimit(
                                    //     currencyNeeded,
                                    //             state?.convertedAmount ??
                                    //                 _convertedAmount.text) ==
                                    //         null)
                                    //     ...[
                                    //
                                    //       Container()
                                    //     ]
                                    //     else ...[
                                    //       Text(
                                    //         '${state?.convertedAmount ?? _convertedAmount.text}',
                                    //         style: TextStyle(
                                    //             color: ColorPalette().textBlack,
                                    //             fontSize: 16))
                            // ]
                                  ],
                                ),
                              );
                            } else {
                              return Expanded(
                                flex: 6,
                                child: Container(
                                  constraints: BoxConstraints(
                                      maxHeight: 55, minHeight: 55),
                                  decoration: BoxDecoration(
                                    border:
                                        Border.all(color: ColorPalette().main),
                                    borderRadius: BorderRadius.circular(15),
                                  ),
                                ),
                              );
                            }
                          },
                        )
                        // Expanded(),
                      ],
                    ),
                    SizedBox(
                      height: 55,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        Expanded(
                          child: InkWell(
                            onTap: _fetchBanks,
                            child: Container(
                              padding: EdgeInsets.all(12),
                              alignment: Alignment.center,
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  border: Border.all(
                                    color: ColorPalette().main,
                                  ),
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(17))),
                              child: CustomText(
                                title: 'Post Order',
                                isBold: true,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 15,
                        ),
                        BlocBuilder<CreateOrderScreenBloc,
                            CreateOrderScreenState>(builder: (context, state) {
                          return Expanded(
                            child: AnellohRoundNetworkButtonLarge(
                              onPressed: _findMatch,
                              childText: 'Find Match',
                              isLoading: state is FindingMatch ? true : false,
                            ),
                          );
                        }),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Padding(padding: EdgeInsets.all(60))
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _calculateAmount() {
    if (_valueToBeSwapped.text.isNotEmpty) {
      if (_rate.text != '' && _rate.text != null) {
        var valueSE = int.parse(_rate.text.toString());
        var valueTBS = (_valueToBeSwapped.text != null &&
                _valueToBeSwapped.text.isNotEmpty)
            ? double.parse(_valueToBeSwapped.text)
            : 0;
        // calculate rate
        var convertedAmount;
        if (currencyToSwap == 'NGN') {
          convertedAmount = valueTBS / valueSE;
        } else {
          convertedAmount = valueSE * valueTBS;
        }

        BlocProvider.of<CurrencyConversionBloc>(context)
            .add(ConvertCurrency(convertedAmount: convertedAmount.toInt()));
      } else {
        BlocProvider.of<CurrencyConversionBloc>(context)
            .add(ConvertCurrency(convertedAmount: 0));
      }
    } else {
      _convertedAmount.text = '';
      // _valueSwapExpectedFocus.previousFocus();
    }
  }
}

Widget _topPageContent({@required BuildContext context}) {
  return NotificationProfileWidget();
}
