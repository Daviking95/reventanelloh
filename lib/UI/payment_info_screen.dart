import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/inputs_wid.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class PaymentInfoScreen extends StatelessWidget {
  final MatchFoundObj matchFoundObj;
  PaymentInfoScreen({@required this.matchFoundObj});
  @override
  Widget build(BuildContext context) {
    return StarterWidget(
      hasBackButton: true,
      hasBottomNav: false,
      hasTopWidget: false,
      bottomPageContent:
          _bottomPageContent(matchFoundObj: matchFoundObj, context: context),
    );
  }
}

Widget _bottomPageContent(
    {@required MatchFoundObj matchFoundObj, @required BuildContext context}) {
  TextEditingController _accountNameController = TextEditingController();
  TextEditingController _routingNumberController = TextEditingController();

  return SafeArea(
    child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(vertical: 17.0, horizontal: 17.0),
            width: MediaQuery.of(context).size.width,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                CustomText(
                  title: 'Swap',
                ),
                CustomText(
                  title:
                      '${CurrencyFormatter.format(currencyCode: matchFoundObj.myCurrency, amount: matchFoundObj.totalAmount)}',
                  textSize: 16,
                  textColor: ColorPalette().dangerRed,
                  fontWeight: FontWeight.w900,
                  hasFontWeight: true,
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.symmetric(vertical: 17.0, horizontal: 17.0),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: ColorPalette().main,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(18),
                topRight: Radius.circular(18),
              ),
            ),
            child: matchFoundObj.myCurrency != 'NGN'
                ? Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      subPageTitle('Destination Account Details', 'This is where Anelloh pays your money to',
                          color: ColorPalette().textWhite, context: context),
                      SizedBox(
                        height: 15,
                      ),
                      Form(
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppSecondaryTextInput(
                              textInputTitle: 'Account Name',
                              controller: _accountNameController,
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            AppSecondaryTextInput(
                              textInputTitle: 'Routing Number',
                              controller: _routingNumberController,
                              textInputType: TextInputType.number,
                              isOnlyDigits: true,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            AnellohRoundNetworkButtonLarge(
                                onPressed: () {},
                                childText: 'Pay',
                                isColorInverted: true,
                                isLoading: false)
                          ],
                        ),
                      ),
                    ],
                  )
                : Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      subPageTitle('Destination Account Details', 'This is where Anelloh pays your money to',
                          color: ColorPalette().textWhite, context: context),
                      SizedBox(
                        height: 15,
                      ),
                      Form(
                        autovalidateMode: AutovalidateMode.always,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.max,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            AppSecondaryTextInput(
                              textInputTitle: 'Account Name',
                              controller: _accountNameController,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            AnellohRoundNetworkButtonLarge(
                                onPressed: () {},
                                childText: 'Pay',
                                isColorInverted: true,
                                isLoading: false)
                          ],
                        ),
                      ),
                    ],
                  ),
          ),
        ],
      ),
    ),
  );
}
