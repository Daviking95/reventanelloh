import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:anelloh_mobile/widgets/custom_profile_nav_btn.dart';
import 'package:anelloh_mobile/widgets/starter_widget.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:simple_moment/simple_moment.dart';

import '../constants.dart';

class IdentityDetailsScreen extends StatefulWidget {
  @override
  _IdentityDetailsScreenState createState() => _IdentityDetailsScreenState();
}

class _IdentityDetailsScreenState extends State<IdentityDetailsScreen> {
  User resultEntity;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    _activateUser();
  }

  _activateUser() async {
    try {
      var resultEntity;
      if (await readFromPref('__anelloh_current_user__')) {
        resultEntity =
            jsonDecode(await getFromPref('__anelloh_current_user__'));
      }

      String customerId = resultEntity['customerId'].toString();
      BlocProvider.of<UserBloc>(context)
          .add(GetCurrentCustomerByIdEvent(customerId: customerId));
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    return StarterWidget(
      hasBackButton: true,
      hasBottomNav: false,
      bottomPageContent: _bottomPageContent(context: context),
    );
  }

  _determineDocumentType({@required int documentType}) {
    switch (documentType) {
      case 1:
        return 'National Identity';
        break;
      case 2:
        return 'International Passport';
        break;
      default:
        return '';
    }
  }

  Widget _bottomPageContent({@required BuildContext context}) {
    return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
      if (state is SaveCurrentUser) {
        resultEntity = AppConstants.userData;

        // log("Doc ${resultEntity.documentType}");
        // log("Doc ${resultEntity.documentNumber}");
        // log("Doc ${resultEntity.documentImage}");

        return SafeArea(
          child: SingleChildScrollView(
            padding: EdgeInsets.all(15),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: [
                pageTitle('Identity Details', 'This show your information about the Identification details you provided to Anelloh'),
                SizedBox(
                  height: 30,
                ),
                CustomText(
                  title: _determineDocumentType(
                      documentType: resultEntity?.documentType ?? ""),
                  isBold: true,
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        CustomText(
                          title: resultEntity.documentType == 0
                              ? ''
                              : resultEntity.documentType == 1
                                  ? 'NIN'
                                  : 'Passport Number',
                        ),
                        CustomText(
                          title: '${resultEntity?.documentNumber ?? ""}',
                        ),
                      ],
                    ),
                    resultEntity.documentType == 0
                        ? Container()
                        : resultEntity.documentType == 1
                            ? Container()
                            : Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  CustomText(
                                    title: 'Expiry Date',
                                  ),
                                  CustomText(
                                    title:
                                        '${Moment.parse(resultEntity?.documentExpiryDate ?? DateTime.now().toIso8601String()).format('dd/MM/yyyy')}',
                                  ),
                                ],
                              ),
                  ],
                ),
                SizedBox(
                  height: 50,
                ),
                CustomText(
                  title: 'Your Identity Document',
                ),
                SizedBox(
                  height: 20,
                ),
                Container(
                  width: MediaQuery.of(context).size.height,
                  height: 300,
                  alignment: Alignment.topCenter,
                  child: resultEntity.documentImage == null
                      ? Icon(
                          Icons.file_copy_outlined,
                          size: 150,
                        )
                      : Image.network(
                          resultEntity?.documentImage ??
                              'assets/images/appIcon-01.svg',
                          fit: BoxFit.cover,
                          width: 300,
                          height: 300,
                          loadingBuilder: (context, _, __) {
                            if (__ == null)
                              return Image.network(
                                resultEntity?.documentImage ??
                                    'assets/images/appIcon-01.svg',
                                fit: BoxFit.cover,
                                width: 300,
                                height: 300,
                              );
                            return Column(
                              children: [
                                Platform.isIOS
                                    ? CupertinoActivityIndicator()
                                    : CircularProgressIndicator(),
                                SizedBox(
                                  height: 10,
                                ),
                                CustomText(
                                  title: 'Loading Document...',
                                ),
                              ],
                            );
                          },
                          errorBuilder: (context, _, __) {
                            return Icon(
                              Icons.file_copy_outlined,
                              size: 150,
                            );
                          },
                        ),
                ),
                SizedBox(
                  height: 15,
                ),
                resultEntity.documentType == 1
                    ? Container()
                    : customProfileNavButton(
                        context: context,
                        onPressed: () {
                          Navigator.of(context).pushNamed('/edit_passport');
                        },
                        title: 'Change Passport Id',
                        icon: Icon(Icons.more_horiz_outlined),
                      )
              ],
            ),
          ),
        );
      } else {
        return Container(
          padding: EdgeInsets.all(20),
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.topCenter,
          child: Platform.isIOS
              ? CupertinoActivityIndicator()
              : CircularProgressIndicator(),
        );
      }
    });
  }
}
