import 'package:flutter/material.dart';

import 'helpers/color_palette.dart';

final ThemeData base = ThemeData.dark();

ThemeData appTheme = new ThemeData(
  // fontFamily: AppConstants.appFontFamily,
  primaryColor: appPrimaryColor,
  primarySwatch: MaterialColor(0xff26474E, swatch),
  accentColor: appSecondaryColor,
  buttonColor: appPrimaryColor,
  errorColor: appErrorColor,
  highlightColor: appActiveColor,
  buttonTheme: appButtonTheme,
  backgroundColor: appWhiteTwoColor,
  textTheme: _buildTextTheme(base.textTheme),
  primaryTextTheme: _buildTextTheme(base.primaryTextTheme),
  accentTextTheme: _buildTextTheme(base.accentTextTheme),
  iconTheme: new IconThemeData(color: appPrimaryColor),
  floatingActionButtonTheme: FloatingActionButtonThemeData(
    backgroundColor: ColorPalette().main,
    foregroundColor: Colors.white,
  ),
  visualDensity: VisualDensity.adaptivePlatformDensity,
);

const Color appPrimaryColor = const Color(0xff26474E);
// const Color appPrimaryDarkColor = const Color(0XFF004286);
// const Color appPrimaryLightColor = const Color(0XFF0071BC);

const Color appSecondaryColor = const Color(0xff254750);
// const Color appSecondaryLightColor = const Color(0XFFFFFFF);
// const Color appSecondaryDarkColor = const Color(0XFFFFFFF);

const Color appErrorColor = const Color(0XFFef6a6e);
const Color mainColor = const Color(0xff26474E);
const Color appWhiteColor = const Color(0XFFffffff);
const Color appWhiteTwoColor = const Color(0XFFfafafa);
const Color appBlackColor = const Color(0XFF212121);
const Color appBlackTwoColor = const Color(0XFF303030);
const Color appBrownGrayOneColor = const Color(0XFF979797);
const Color appBrownGrayTwoColor = const Color(0XFF777777);
const Color appBrownGrayThreeColor = const Color(0XFFeeeeee);
const Color appWhite70Color = Colors.white70;
const Color appBackgroundColor = const Color(0XFFF0F4F7); // const Color(0XFFeeeeee);
const Color appActiveColor = const Color(0XFFef6a6e);
const Color transparentColor = const Color.fromRGBO(0, 0, 0, 0.2);

Map<int, Color> swatch = {
  50: Color.fromRGBO(38, 71, 77, .1),
  100: Color.fromRGBO(38, 71, 77, .2),
  200: Color.fromRGBO(38, 71, 77, .3),
  300: Color.fromRGBO(38, 71, 77, .4),
  400: Color.fromRGBO(38, 71, 77, .5),
  500: Color.fromRGBO(38, 71, 77,.6),
  600: Color.fromRGBO(38, 71, 77, .7),
  700: Color.fromRGBO(38, 71, 77, .8),
  800: Color.fromRGBO(38, 71, 77, .9),
  900: Color.fromRGBO(38, 71, 77, 1),
};

final ButtonThemeData appButtonTheme = ButtonThemeData(
    buttonColor: appPrimaryColor,
//    highlightColor: appSecondaryColor,
    disabledColor: appBrownGrayOneColor,
    textTheme: ButtonTextTheme.primary,
    focusColor: appSecondaryColor);

TextTheme _buildTextTheme(TextTheme base) {
  return base.copyWith(
    headline6: base.headline6.copyWith(),
  );
}

TextStyle textStylePrimaryColorBold = TextStyle(
    color: appPrimaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStylePrimaryColorNormal = TextStyle(
    color: appPrimaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleSecondaryColorBold = TextStyle(
    color: appSecondaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleSecondaryColorNormal = TextStyle(
    color: appSecondaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStylePrimaryLightColorBold = TextStyle(
    // color: appPrimaryLightColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStylePrimaryLightColorNormal = TextStyle(
    // color: appPrimaryLightColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStylePrimaryDarkColorBold = TextStyle(
    color: appPrimaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStylePrimaryDarkColorNormal = TextStyle(
    color: appPrimaryColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleBlackColorBold = TextStyle(
    color: appBlackColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleBlackColorNormal = TextStyle(
    color: appBlackColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleGrayColorBold = TextStyle(
    color: appBrownGrayOneColor,
    fontSize: 14.0,
    fontWeight: FontWeight.bold,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleGrayColorNormal = TextStyle(
    color: appBrownGrayOneColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);

TextStyle textStyleWhiteColorNormal = TextStyle(
    color: appWhiteColor,
    fontSize: 14.0,
    fontWeight: FontWeight.normal,
    // fontFamily: AppConstants.appFontFamily
);