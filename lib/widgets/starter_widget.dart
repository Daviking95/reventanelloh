import 'dart:convert';

import 'package:anelloh_mobile/bloc/session_timer/session_timer_bloc.dart';
import 'package:anelloh_mobile/bloc/session_timer/session_timer_event.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/dialog_helper.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:get/get.dart';

import '../styles.dart';
import 'custom_bottom_nav_wid.dart';
import 'fab_wid.dart';

class StarterWidget extends StatefulWidget {
  final Widget topPageContent;
  final Widget bottomPageContent;
  final bool isBgAllowed;
  final bool isFabAllowed;
  final bool hasTopWidget;
  final bool hasNavDrawer;
  final bool hasBackButton;
  final IconData fabIcon;
  final Function fabFunc;
  final double extraSpace;
  final String backButtonRoute;
  final bool isExitApp;
  final bool hasRefreshOption;
  final Function onRefreshFunction;
  final bool hasBgimage;
  final bool hasPagePadding;
  final bool hasBottomNav;
  final int bottomNavIndex;
  final Color bgColor;
  final Color backBtnColor;
  final bool hasScrollChild;

  StarterWidget({
    this.topPageContent,
    this.bgColor,
    this.backBtnColor,
    this.bottomPageContent,
    this.isBgAllowed = false,
    this.isFabAllowed = false,
    this.hasTopWidget = false,
    this.hasNavDrawer = false,
    this.hasScrollChild = true,
    this.fabIcon,
    this.fabFunc,
    this.extraSpace = 0.0,
    this.backButtonRoute = "",
    this.isExitApp = false,
    this.hasRefreshOption = false,
    this.onRefreshFunction,
    this.hasBgimage = false,
    this.hasPagePadding = false,
    this.hasBottomNav = false,
    this.hasBackButton = false,
    this.bottomNavIndex = 0,
  });

  @override
  _StarterWidgetState createState() => _StarterWidgetState();
}

class _StarterWidgetState extends State<StarterWidget> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  final GlobalKey<RefreshIndicatorState> _refreshKey =
      new GlobalKey<RefreshIndicatorState>();

  Future<bool> _willPopCallback() async {
    if (widget.isExitApp) {
      return exitApp(context);
    } else if (widget.backButtonRoute == "")
      return Future.value(true);
    else
      Navigator.of(context).pushNamed(widget.backButtonRoute);

    return true;
  }

  _handleLogout({@required User user}) async {
    this._scaffoldKey.currentState.openEndDrawer();
    Navigator.of(context).pushReplacementNamed('/login', arguments: {
      'email': user.emailAddress,
    });
    BlocProvider.of<UserBloc>(context).add(LogOutUserEvent(user: user));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<UserBloc, UserState>(
      listener: (context, state) async {
        if (state is LogOutSuccessful) {
          Map<String, dynamic> user;
          if (await readFromPref('__anelloh_current_user__')) {
            user = jsonDecode(await getFromPref('__anelloh_current_user__'));
          }

          Navigator.of(context).pushReplacementNamed('/login', arguments: {
            'email': user['email'],
          });
        }
      },
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          elevation: 0.0,
          backgroundColor:
              widget.bgColor != null ? widget.bgColor : Colors.white,
          leading: widget.hasNavDrawer
              ? IconButton(
                  icon: Icon(
                    Icons.menu,
                    color:
                        widget.bgColor != null ? Colors.white : appPrimaryColor,
                  ),
                  onPressed: () {
                    // BlocProvider.of<SessionTimerBloc>(context)
                    //     .add(SessionTimerRestartEvent());
                    _scaffoldKey.currentState.openDrawer();
                  },
                )
              : widget.hasBackButton
                  ? IconButton(
                      icon: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: widget.backBtnColor != null
                            ? widget.backBtnColor
                            : widget.bgColor != null
                                ? Colors.white
                                : ColorPalette().main,
                      ),
                      onPressed: () {
                        Navigator.pop(context);
                      })
                  : Container(),
          actions: [
            // widget.hasNavDrawer ? Spacer() : Container(),
            widget.hasTopWidget
                ? Container(
                    child: widget.topPageContent,
                  )
                : Container(),
          ],
        ),
        body: SafeArea(
          child: widget.hasRefreshOption
              ? RefreshIndicator(
                  triggerMode: RefreshIndicatorTriggerMode.anywhere,
                  key: _refreshKey,
                  onRefresh: () async {
                    widget.onRefreshFunction();
                    this._refreshKey.currentState.deactivate();
                  },
                  child: pageContent(),
                )
              : pageContent(),
        ),
        backgroundColor: widget.isBgAllowed ? widget.bgColor : Colors.white,
        floatingActionButton: widget.isFabAllowed
            ? startFabWidget(widget.fabFunc, appPrimaryColor, widget.fabIcon)
            : Container(),
        bottomNavigationBar: widget.hasBottomNav
            ? CustomBottomNavigation(selectedIndex: widget.bottomNavIndex)
            : SizedBox(),
        // drawer: BlocBuilder<UserBloc, UserState>(
        //   builder: (context, state) {
        //     return Drawer(
        //       child: SingleChildScrollView(
        //         padding: EdgeInsets.symmetric(horizontal: 10, vertical: 3),
        //         child: SafeArea(
        //           child: Column(
        //             mainAxisSize: MainAxisSize.min,
        //             mainAxisAlignment: MainAxisAlignment.start,
        //             crossAxisAlignment: CrossAxisAlignment.start,
        //             children: [
        //               ListTile(
        //                 onTap: () {
        //                   Navigator.of(context).popAndPushNamed('/profile');
        //                 },
        //                 leading: Icon(Icons.account_circle_outlined),
        //                 title: Text('Profile'),
        //               ),
        //               ListTile(
        //                 onTap: () {
        //                   _handleLogout(user: state.user);
        //                 },
        //                 leading: Icon(Icons.logout),
        //                 title: Text('Log Out'),
        //               ),
        //             ],
        //           ),
        //         ),
        //       ),
        //     );
        //   },
        // ),
      ),
    );
  }

  Widget pageContent() {
    return widget.hasScrollChild
        ? SingleChildScrollView(
            physics: AlwaysScrollableScrollPhysics(),
            child: Container(
                // height: Get.height + widget.extraSpace,
                width: MediaQuery.of(context).size.width,
                padding: widget.hasPagePadding
                    ? EdgeInsets.symmetric(vertical: 17.0, horizontal: 17.0)
                    : EdgeInsets.all(0.0),
                decoration: widget.hasBgimage
                    ? BoxDecoration(
                        // image: builBackgroundImage(),
                        gradient: new LinearGradient(
                            colors: [
                              widget.isBgAllowed
                                  ? widget.bgColor
                                  : Color(0xffE5E5E5),
                              widget.isBgAllowed
                                  ? widget.bgColor
                                  : Color(0xffE5E5E5)
                            ],
                            begin: const FractionalOffset(0.0, 0.0),
                            end: const FractionalOffset(1.0, 1.0),
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      )
                    : BoxDecoration(
                        gradient: new LinearGradient(
                            colors: [
                              widget.isBgAllowed
                                  ? widget.bgColor
                                  : appWhiteColor,
                              widget.isBgAllowed
                                  ? widget.bgColor
                                  : appWhiteTwoColor
                            ],
                            begin: const FractionalOffset(0.0, 0.0),
                            end: const FractionalOffset(1.0, 1.0),
                            stops: [0.0, 1.0],
                            tileMode: TileMode.clamp),
                      ),
                child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minWidth: Get.width,
                      minHeight: MediaQuery.of(context).size.height,
                    ),
                    // padding: widget.hasPagePadding
                    //     ? EdgeInsets.symmetric(vertical: 15.0)
                    //     : EdgeInsets.all(0.0),
                    child: widget.bottomPageContent)),
          )
        : ConstrainedBox(
            constraints: BoxConstraints(
              minWidth: Get.width,
              minHeight: MediaQuery.of(context).size.height,
            ),
            // padding: widget.hasPagePadding
            //     ? EdgeInsets.symmetric(vertical: 15.0)
            //     : EdgeInsets.all(0.0),
            child: widget.bottomPageContent);
  }
}
