//
// import 'package:flutter/material.dart';
// // import 'package:currency_pickers/country.dart' as currencyCountry;
// // import 'package:currency_pickers/currency_pickers.dart' as currencyPicker;
// import '../styles.dart';
// import 'text_wid.dart';
//
// class AppSpinner extends StatefulWidget {
//
//   final List<String> _spinnerList;
//   final double borderRadius;
//   final String hint;
//   final TextEditingController controller;
//   String result;
//   final bool shouldRunAFunctionOnChange;
//   final Function onChangeFunction;
//
//   AppSpinner(this._spinnerList, this.borderRadius, this.hint, this.controller,{this.result, this.shouldRunAFunctionOnChange = false, this.onChangeFunction});
//
//   @override
//   _AppSpinnerState createState() => _AppSpinnerState();
// }
//
// class _AppSpinnerState extends State<AppSpinner> {
//
//   String dropdownValueForAccountSelected ;
//
//   @override
//   void initState() {
//     super.initState();
//
//   }
//
//   @override
//   Widget build(BuildContext context) {
//
//     widget.result = widget.controller.text.isEmpty ? null : widget.controller.text;
//
//     return Padding(
//       padding: const EdgeInsets.symmetric(vertical: 8.0),
//       child: Container(
//         decoration: BoxDecoration(
//             color: Colors.white,
//             border: Border.all(color: Color(0XFFFFE3E3), width: 1.0),
//             borderRadius: BorderRadius.circular(widget.borderRadius)),
//         child: SizedBox(
//           width: double.infinity,
//           height: 50.0,
//           child: Padding(
//             padding: const EdgeInsets.symmetric(horizontal: 15.0),
//             child: Theme(
//               data: Theme.of(context).copyWith(
//                 canvasColor: Colors.white,
//               ),
//               child: DropdownButton<String>(
//                 underline: SizedBox(),
//                 value: widget.result,
//                 hint: CustomText(
//                   title: widget._spinnerList.length == 0 || widget._spinnerList == null ? '${widget.hint}' : widget.hint,
//                   textColor: Colors.black,
//                   textSize: 15,
//                   isBold: true,
//                 ),
//                 icon: Icon(Icons.arrow_drop_down),
//                 iconSize: 24,
//                 elevation: 16,
//                 style: textStyleSecondaryColorNormal,
//                 onChanged: (String data) {
//                   setState(() {
//
//                     dropdownValueForAccountSelected = data;
//
//                     _returnValueString(data);
//
//                     if(widget.shouldRunAFunctionOnChange) widget.onChangeFunction();
//                   });
//                 },
//                 isExpanded: true,
//                 items: widget._spinnerList
//                     .map<DropdownMenuItem<String>>((String value) {
//                   return DropdownMenuItem<String>(
//                     value: value,
//                     child: Container(
// //                      color: appBrownGrayTwoColor.withOpacity(.1),
//                       padding: EdgeInsets.all(2.0),
//                       child: Text(
//                         value,
//                         style: textStylePrimaryColorNormal.copyWith(fontWeight: FontWeight.bold),
//                       ),
//                     ),
//                   );
//                 }).toList(),
//               ),
//             ),
//           ),
//         ),
//       ),
//     );
//   }
//
//   TextEditingController _returnValueString(String data) {
//     widget.result = data;
//     widget.controller.text = data;
//     widget.result = widget.controller.text;
//     print("Data coming ${widget.result}");
//     return widget.controller;
//   }
//
// }
//
//
// class AppCurrencySpinner extends StatefulWidget {
//
//   final TextEditingController controller;
//   final bool isCurrency;
//   final bool isOnlyFlag;
//
//   AppCurrencySpinner(this.controller, this.isCurrency, {this.isOnlyFlag = false});
//
//   @override
//   _AppCurrencySpinnerState createState() => _AppCurrencySpinnerState();
// }
//
// class _AppCurrencySpinnerState extends State<AppCurrencySpinner> {
//   @override
//   Widget build(BuildContext context) {
//     return _buildCountryDropdown(context: context);
//   }
//
//   Widget _buildCountryDropdown({BuildContext context}) {
//     return Container(
//       width: MediaQuery.of(context).size.width,
//       height: 60,
//       alignment: Alignment.center,
//       padding: EdgeInsets.all(11),
//       margin: EdgeInsets.symmetric(vertical: 10.0),
//       decoration: BoxDecoration(
//         borderRadius: BorderRadius.circular(17.0),
//         color: Colors.transparent,
//         border: Border.all(color: appBrownGrayTwoColor, width: 1.0),
//       ),
//       child: currencyPicker.CurrencyPickerDropdown(
//         initialValue: widget.isCurrency ? 'NG' : "NG",
//         itemBuilder: _buildDropdownItem,
//         onValuePicked: (currencyCountry.Country country) {
//           print("${country.name}");
//           _returnValueString(country);
//         },
//       ),
//     );
//   }
//
//   Widget _buildDropdownItem(currencyCountry.Country country) {
//     String titleToShow = widget.isCurrency ? "+${country.currencyCode}(${country.isoCode})" : "${country.name}";
//     return  widget.isOnlyFlag ?
//       currencyPicker.CurrencyPickerUtils.getDefaultFlagImage(country)
//         : Container(
//       width: MediaQuery
//           .of(context)
//           .size
//           .width,
//       child: Row(
//         children: <Widget>[
//           currencyPicker.CurrencyPickerUtils.getDefaultFlagImage(country),
//           SizedBox(width: 5.0),
//           CustomText(
//             title: titleToShow, textColor: appPrimaryColor, textSize: 13.0,),
//         ],
//       ),
//     );
//   }
//
//   TextEditingController _returnValueString(currencyCountry.Country country) {
//     widget.controller.text =  widget.isCurrency ? country.currencyCode : country.name;
//     print("Data coming ${widget.controller.text}");
//     return widget.controller;
//   }
// }
