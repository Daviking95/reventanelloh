import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:bezier_chart/bezier_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/material.dart';

class CustomLineChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  CustomLineChart(this.seriesList, {this.animate});
  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      defaultInteractions: false,
      primaryMeasureAxis: new charts.NumericAxisSpec(
        tickProviderSpec: new charts.BasicNumericTickProviderSpec(desiredMaxTickCount: 2)
      ),
    );
  }
}

class DateRateRow {
  final DateTime timeStamp;
  final int rate;
  DateRateRow(this.timeStamp, this.rate);
}


class CustomFLineChart extends StatelessWidget{
  final List<FlSpot> spots;
  CustomFLineChart({@required this.spots});
  @override
  Widget build(BuildContext context) {
    return LineChart(
        LineChartData(
          lineBarsData: [
            LineChartBarData(
              spots: this.spots,
              colors: [Colors.greenAccent],
              dotData: FlDotData(show: false),
              isCurved: true
            ),
          ],
          borderData: FlBorderData(show: false),
          titlesData: FlTitlesData(show: false),
          gridData: FlGridData(show: false),

        ),
        swapAnimationDuration: Duration(milliseconds: 150),
        swapAnimationCurve: Curves.linear
    );
  }
}