import 'package:anelloh_mobile/bloc/notification/notificaition_event.dart';
import 'package:anelloh_mobile/bloc/notification/notification_bloc.dart';
import 'package:anelloh_mobile/bloc/notification/notification_state.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class NotificationProfileWidget extends StatefulWidget {
  final bool hasBackNav;

  NotificationProfileWidget({this.hasBackNav});

  @override
  _NotificationProfileWidgetState createState() =>
      _NotificationProfileWidgetState();
}

class _NotificationProfileWidgetState extends State<NotificationProfileWidget> {
  List<NotificationsData> notificationList = [];

  User user;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    // log('AppConstants?.userData?.profileImage ${AppConstants?.userData?.profileImage}');

    _fetchNotificationsData();
  }

  void _fetchNotificationsData() {
    try {
      // BlocProvider.of<UserBloc>(context).add(GetCurrentCustomerByIdEvent(
      //     customerId: AppConstants?.userData?.customerId ?? "0"));

      BlocProvider.of<NotificationBloc>(context).add(FetchNotificationsEvent(
          customerId: AppConstants?.userData?.customerId ?? "0"));
    } catch (err) {
      print(err);
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    _fetchNotificationsData();
    return MultiBlocListener(
      listeners: [
        BlocListener<NotificationBloc, NotificationState>(
          listener: (context, state) {
            if (state is SuccessfullyFetchedAllNotifications) {
              setState(() {
                notificationList = state.notificationsList;
              });
            }
          },
        ),
        BlocListener<UserBloc, UserState>(
          listener: (context, state) {
            if (state is SaveCurrentUser) {
              // log("Do I get hee ?");
              setState(() {
                user = state.user;
              });
            }
          },
        ),
      ],
      child: BlocBuilder<NotificationBloc, NotificationState>(
          builder: (context, state) {
        return BlocBuilder<UserBloc, UserState>(builder: (context, state) {
          return Container(
            margin: EdgeInsets.only(top: 10, bottom: 5),
            width: MediaQuery.of(context).size.width / 3,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      state is FetchingNotifications
                          ? CircularProgressIndicator()
                          : GestureDetector(
                              onTap: () => Navigator.pushNamed(
                                  context, '/notifications'),
                              child: Stack(
                                children: [
                                  SvgPicture.asset(
                                    ImageConstants.notificationIcon,
                                    width: 30,
                                  ),
                                  notificationList.length == 0
                                      ? Container()
                                      : Positioned(
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(100),
                                            child: Container(
                                                color: Colors.red,
                                                height: 20,
                                                width: 20,
                                                child: Center(
                                                  child: CustomText(
                                                    title: notificationList
                                                        .length
                                                        .toString(),
                                                    textColor: Colors.white,
                                                  ),
                                                )),
                                          ),
                                          top: 0,
                                        ),
                                ],
                              ),
                            ),
                      SizedBox(
                        width: 10,
                      ),
                      InkWell(
                        onTap: () {
                          Navigator.pushNamed(
                              context, RoutesConstants.settingsUrl);
                          // Scaffold.of(context).openDrawer();
                        },
                        child: ClipRRect(
                          child: Image.network(
                            '${AppConstants?.userData?.profileImage ?? user?.profileImage}',
                            errorBuilder: (context, _, __) {
                              return Image.asset(
                                ImageConstants.profilePicture,
                                width: 50,
                                height: 50,
                              );
                            },
                            width: 35,
                            height: 35,
                            fit: BoxFit.cover,
                          ),
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        });
      }),
    );
  }
}
