import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';

class HeaderCard extends StatelessWidget {
  final Function onTap;
  final Widget headerWidget;
  final Widget bodyWidget;
  final Widget subChild;
  HeaderCard({
    @required this.onTap,
    @required this.headerWidget,
    @required this.bodyWidget,
    @required this.subChild,
  }) : super();

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: this.onTap,
      child: Container(
        width: MediaQuery.of(context).size.width,
        // height: 100,
        padding: EdgeInsets.symmetric(vertical: 4, horizontal: 10),
        decoration: BoxDecoration(
            color: Color(0xFFF9968B), borderRadius: BorderRadius.circular(17)),
        child: Stack(
          children: [
            Container(
              padding: EdgeInsets.symmetric(vertical: 2, horizontal: 5),
              alignment: Alignment.center,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        this.headerWidget,
                        this.bodyWidget
                      ],
                    ),
                    flex: 1,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  subChild != null
                      ? Container(
                          constraints: BoxConstraints(
                            maxHeight: 100,
                            maxWidth: 110
                          ),
                          child: subChild,
                        )
                      : SizedBox()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
