import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../styles.dart';

class ProgressWidget extends StatelessWidget {
  const ProgressWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Center(
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.all(Radius.circular(10)), color: Colors.white),
          width: Get.width / 1.5,
          height: 80,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Platform.isIOS
                  ? CupertinoActivityIndicator()
                  : CircularProgressIndicator(),
              SizedBox(width: 30,),
              Text("Please wait...", style: TextStyle(color: mainColor, fontSize: 15),)
            ],
          ).paddingSymmetric(horizontal: 15, vertical: 5),
        ),
      ),
    );
  }
}
