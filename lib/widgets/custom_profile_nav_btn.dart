import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/material.dart';

Widget customProfileNavButton({
  @required BuildContext context,
  @required Function onPressed,
  @required String title,
  @required Icon icon,
  Color textColor,
}) {
  return GestureDetector(
    onTap: onPressed,
    child: Container(
      margin: EdgeInsets.only(top: 5, bottom: 5),
      decoration: BoxDecoration(
          color: ColorPalette().textWhite,
          boxShadow: [
            BoxShadow(
                offset: Offset(1, 2),
                blurRadius: 10.4,
                spreadRadius: 6.1,
                color: Colors.grey.withOpacity(0.1))
          ],
          borderRadius: BorderRadius.circular(10)),
      child: Ink(
        width: MediaQuery.of(context).size.width,
        padding: EdgeInsets.all(15),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(100),
                  color: ColorPalette().textWhite,
                  boxShadow: [
                    BoxShadow(
                        offset: Offset(1, 2),
                        blurRadius: 10.4,
                        spreadRadius: 6.1,
                        color: Colors.grey.withOpacity(0.1))
                  ]),
              padding: EdgeInsets.all(10),
              child: icon,
            ),
            Container(
              child: CustomText(
                title: title,
                textSize: 17,
                textColor: textColor ?? ColorPalette().main,
              ),
            ),
            Container(
              child: Icon(Icons.arrow_forward_ios_outlined,
                  color: ColorPalette().main, size: 14.0),
            )
          ],
        ),
      ),
    ),
  );
}
