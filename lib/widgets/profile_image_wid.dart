
import 'package:anelloh_mobile/UI/dashboard_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';

import '../constants.dart';
import '../styles.dart';
import 'text_wid.dart';

Widget profileImage(BuildContext context){
  return GestureDetector(
    onTap: () {
      // logout(context);
    },
    child: SvgPicture.asset("assets/images/user_prof.svg", height: 30.0,),
  );
}

Widget backButtonWithName(BuildContext context, String title, bool hasProfileImage, [Widget backWidget, bool hasBackWidget = false, bool isPop = false]){
  return GestureDetector(
    onTap: () => isPop ? Navigator.pop(context) : Get.to(hasBackWidget ? backWidget : MainDashboardScreen()),
    child: Container(
      width: MediaQuery.of(context).size.width,
      height: 100,
      padding: EdgeInsets.only(left: 20),
      color: Color(0XFFFFF0F1),
      child: Row(
        children: <Widget>[
          GestureDetector(
            onTap: () => Navigator.of(context).pushNamed(RoutesConstants.dashboardUrl),
            child: Icon(
              Icons.arrow_left,
              color: appSecondaryColor,
            ),
          ),
          Expanded(
            flex: 3,
              child: CustomText(title: title, textColor: appSecondaryColor, textSize: 18, isBold: true,)),
          Spacer(),
          hasProfileImage ? profileImage(context) : Container(),
        ],
      ),
    ),
  );
}