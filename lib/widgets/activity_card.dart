import 'package:anelloh_mobile/helpers/classes/activity_card.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/date_formatter.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/rate_arrow.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ActivityCardWidget extends StatelessWidget {
  final ActivityCardItem activityCardItem;
  ActivityCardWidget({@required this.activityCardItem});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Navigator.of(context).pushNamed('/transaction_summary',
            arguments: {'activityCardItem': this.activityCardItem});
      },
      child: Container(
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.only(top: 5, bottom: 5),
        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 10),
        decoration: BoxDecoration(
            color: ColorPalette().textWhite,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                offset: Offset(1, 3),
                spreadRadius: 1.1,
                blurRadius: 3.5,
              )
            ]),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: [
                  Padding(
                    padding: EdgeInsets.only(top: 10),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Image.asset(
                        'icons/flags/png/${this.activityCardItem.myCurrency.substring(0, 2).toLowerCase()}.png',
                        package: 'country_icons',
                        width: 40,
                        height: 40,
                        fit: BoxFit.fill,
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    flex: 3,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        CustomText(
                          title:
                              '${CurrencyFormatter.format(currencyCode: '${this.activityCardItem.myCurrency}', amount: '${formatToCurrency(activityCardItem.myAmount)}')}',
                          textColor: ColorPalette().dangerRed,
                          fontWeight: FontWeight.w900,
                          isBold: true,
                        ),
                        CustomText(
                          title:
                              'Rate ${CurrencyFormatter.format(currencyCode: '${this.activityCardItem.myCurrency}', amount: '${activityCardItem.rate}')}',
                          textSize: 11,
                        ),
                        CustomText(
                          title:
                              'Rate: ${CurrencyFormatter.format(currencyCode: this.activityCardItem.myCurrency == 'NGN' ? this.activityCardItem.myCurrency : this.activityCardItem.convertedCurrency, amount: this.activityCardItem.rate)}/${CurrencyFormatter.format(currencyCode: this.activityCardItem.convertedCurrency == 'NGN' ? this.activityCardItem.convertedCurrency : this.activityCardItem.convertedCurrency, amount: '')}',
                          textColor: Colors.grey.withOpacity(0.8),
                          textSize: mainSizer(context: context, size: 34),
                        ),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            CustomText(
                              title: '+ N4.00',
                              textSize: 11,
                            ),
                            rateArrow(isAscending: false, size: 15.0),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisSize: MainAxisSize.max,
                children: [
                  CustomText(
                    title: formatDate(this.activityCardItem.createdTime),
                    textSize: 11,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  orderStatus(
                      context: context,
                      status: this.activityCardItem.orderStatus,
                      matchedOrderStatus:
                          this.activityCardItem.matchedOrderStatus,
                      activityCardItem: activityCardItem),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget orderStatusCard(
    {@required Color cardColour,
    @required String cardText,
    @required BuildContext context}) {
  return Container(
    constraints: BoxConstraints(
      maxWidth: 150,
      maxHeight: 80,
    ),
    padding: EdgeInsets.only(left: 5, right: 5, top: 2, bottom: 2),
    child: CustomText(
      title: cardText,
      textSize: mainSizer(context: context, size: 30),
      textColor: cardColour,
      isCenter: true,
    ),
    decoration: BoxDecoration(
        color: cardColour.withOpacity(0.2),
        borderRadius: BorderRadius.circular(5)),
  );
}

Widget orderStatus(
    {@required BuildContext context,
    @required String status,
    String matchedOrderStatus = '',
    ActivityCardItem activityCardItem}) {
  if (status == '1' && matchedOrderStatus.toLowerCase() == 'matched')
    return orderStatusCard(
        cardColour: ColorPalette().orderSuccess,
        cardText: 'Matched',
        context: context);

  switch (status) {
    case '1':
      return orderStatusCard(
          cardColour: ColorPalette().orderSuccess,
          cardText: 'Open',
          context: context);
      break;
    case '2':
      return orderStatusCard(
          cardColour: ColorPalette().orderSuccess,
          cardText: 'Fulfilled',
          context: context);
      break;
    case '3':
      return orderStatusCard(
          cardColour: ColorPalette().orderSuccess,
          cardText: 'Completed',
          context: context);
      break;
    case '4':
      return orderStatusCard(
          cardColour: ColorPalette().dangerRed,
          cardText: 'Cancelled',
          context: context);
      break;
    case '5':
      return orderStatusCard(
          cardColour: ColorPalette().dangerRed,
          cardText: 'Unfulfilled',
          context: context);
      break;
    case '6':
      return orderStatusCard(
          cardColour: ColorPalette().dangerRed,
          cardText: 'Refunded',
          context: context);
      break;
    case '7':
      return orderStatusCard(
          cardColour: Colors.blue,
          cardText: "Pending Confirmation",
          context: context);
      break;
    default:
      return Container();
  }
}
