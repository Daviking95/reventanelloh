import 'dart:convert';
import 'dart:developer';

import 'package:anelloh_mobile/bloc/market_rates/market_rates_events.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:charts_flutter/flutter.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class StackedAreaLineChart extends StatelessWidget {
  final List<MarketRateObj> marketRates;
  final bool animate;

  StackedAreaLineChart({this.animate, @required this.marketRates});

  @override
  Widget build(BuildContext context) {

    return charts.TimeSeriesChart(
      _createSampleDataTime(),
      domainAxis: charts.DateTimeAxisSpec(
        showAxisLine: true,
        // tickProviderSpec: charts.StaticDateTimeTickProviderSpec(
        //   // chartsTickSpec,
        //   <charts.TickSpec<DateTime>>[
        //     charts.TickSpec<DateTime>(DateTime(now.year, now.month, now.day - 6)),
        //     charts.TickSpec<DateTime>(DateTime(now.year, now.month, now.day - 5)),
        //     charts.TickSpec<DateTime>(DateTime(now.year, now.month, now.day - 4)),
        //     charts.TickSpec<DateTime>(DateTime(now.year, now.month, now.day - 3)),
        //     charts.TickSpec<DateTime>(DateTime(now.year, now.month, now.day - 2)),
        //     charts.TickSpec<DateTime>(DateTime(now.year, now.month, now.day - 1)),
        //     charts.TickSpec<DateTime>(now),
        //   ],
        // ),
      ),
      animate: animate,
      behaviors: [
        charts.SlidingViewport(),
        charts.PanAndZoomBehavior(),
        charts.SeriesLegend(
            position: charts.BehaviorPosition.bottom,
            showMeasures: true,
            horizontalFirst: true),
        charts.LinePointHighlighter(
            selectionModelType: charts.SelectionModelType.info,
            drawFollowLinesAcrossChart: true)
      ],
      dateTimeFactory: const charts.LocalDateTimeFactory(),
      defaultRenderer: charts.LineRendererConfig(
        includePoints: true,
        stacked: false,
        includeArea: true,
        includeLine: true,
      ),
    );
  }

  List<charts.Series<Data, DateTime>> _createSampleDataTime() {
    List<charts.Series<Data, DateTime>> chartList = [];

    var baseCurrency = 'USD';
    int start = 0;
    List<Data> data = [];

    this.marketRates.forEach((element) {

      baseCurrency = 'USD';
      baseCurrency = baseCurrency == element.baseCurrency
          ? element.baseCurrency
          : baseCurrency;

      if (baseCurrency != element.baseCurrency) data = [];

      element.rates.forEach((rate) {
        // log('$start ${DateTime.parse(element.createdDates[start])} ${element.rates[start]} ${element.baseCurrency}');

        data.add(Data(DateTime.parse(element.createdDates[start]),
            double.parse(element.rates[start].toString()).toInt()));
        start = start + 1;
      });

      start = 0;

      var series = new charts.Series<Data, DateTime>(
        id: element.baseCurrency,
        displayName: element.baseCurrency,
        data: data,
        colorFn: (_, __) => _returnCurrencyColor(baseCurrency, element.baseCurrency),
        domainFn: (Data sales, _) => sales.time,
        measureFn: (Data sales, _) => sales.rates,
      );

      chartList.add(series);
    });

    return chartList;
  }

  _returnCurrencyColor(String baseCurrency, String currency) {
    Color color = charts.MaterialPalette.blue.shadeDefault;
    switch(currency){
      case 'USD' : color = charts.MaterialPalette.teal.shadeDefault;
      break;

      case 'NGN' : color = charts.MaterialPalette.red.shadeDefault;
      break;

      case 'GBR' : color = charts.MaterialPalette.purple.shadeDefault;
      break;

      default : color = charts.MaterialPalette.red.shadeDefault;
      break;

    }

    return color;
  }
}

/// Sample linear data type.
class Data {
  final DateTime time;
  final int rates;

  Data(this.time, this.rates);
}
