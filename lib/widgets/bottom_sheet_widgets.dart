
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/material.dart';

bottomSheetHeaderWidget(BuildContext context, String headerTitle, [bool hastoolTip = false, String toolTip = ''] ){
  return Column(
    children: [
      Row(
        children: [
          Expanded(
            child: CustomText(
              textColor: ColorPalette().main,
              title: headerTitle.toUpperCase(),
              isBold: true,
              textSize: mainSizer(context: context, size: 23),
            ),
          ),
          SizedBox(width: 5,),
          hastoolTip ? MyTooltip(message: toolTip, child: Icon(Icons.info_outline_rounded, color: Color(0xffb6b6b6), size: 17,)) : Container()
        ],
      ),
      Divider(thickness: 1, color: Color(0xffb6b6b6),),
      SizedBox(
        height: 20,
      )
    ],
  );
}

class MyTooltip extends StatelessWidget {
  final Widget child;
  final String message;

  MyTooltip({@required this.message, @required this.child});

  @override
  Widget build(BuildContext context) {
    final key = GlobalKey<State<Tooltip>>();
    bool isTap = false;
    return Tooltip(
      key: key,
      message: message,
      child: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => _onTap(key, !isTap),
        child: child,
      ),
    );
  }

  void _onTap(GlobalKey key, bool isTap) {
    final dynamic tooltip = key.currentState;
    isTap ? tooltip?.ensureTooltipVisible() : tooltip?.dispose();
  }
}