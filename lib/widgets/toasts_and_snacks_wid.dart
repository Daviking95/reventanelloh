import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:anelloh_mobile/styles.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:overlay/overlay.dart';
import 'package:snack/snack.dart';

showToast(
    {String toastMsg,
    Toast toastLength,
    ToastGravity toastGravity,
    Color bgColor,
    Color txtColor,
    bool isError = false}) {
  Fluttertoast.showToast(
      msg: toastMsg,
      toastLength: toastLength,
      gravity: ToastGravity.CENTER,
      timeInSecForIosWeb: 1,
      backgroundColor: isError ? ColorPalette().dangerRed : ColorPalette().main,
      textColor: Colors.white,
      fontSize: 16.0);
}

showSnackBar(BuildContext context, Widget widget) {
  SnackBar(
    content: widget,
    duration: Duration(seconds: 2),
  ).show(context);
}

class OverlayObject {
  final String title;
  final String body;
  final bool hasBtn;
  final bool hasCancelBtn;
  final String cancelBtnText;
  final Function cancelBtnFunction;
  final Icon icon;
  final String btnText;
  final Function btnFunction;
  final bool clearToNavigate;
  final Function ctnFunction;

  OverlayObject({
    @required this.title,
    @required this.body,
    @required this.hasBtn,
    this.hasCancelBtn = false,
    this.cancelBtnText,
    this.cancelBtnFunction,
    this.btnText,
    this.btnFunction,
    this.clearToNavigate,
    this.ctnFunction,
    @required this.icon,
  });
}

CustomOverlay anellohOverlay(
    {@required OverlayObject overlayObject, @required BuildContext context}) {
  return CustomOverlay(
      context: context,
      builder: (removeOverlay) => Container(
            padding: EdgeInsets.all(0.8),
            margin: EdgeInsets.only(left: 14, right: 14),
            width: isPortrait(context)
                ? MediaQuery.of(context).size.width
                : MediaQuery.of(context).size.width / 2,
            child: Card(
              color: Colors.white,
              clipBehavior: Clip.hardEdge,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              child: Container(
                child: Column(
                  children: [
                    Container(
                      width: isPortrait(context)
                          ? MediaQuery.of(context).size.width
                          : MediaQuery.of(context).size.width / 2,
                      decoration: BoxDecoration(
                        color: appPrimaryColor,
                      ),
                      padding: EdgeInsets.all(3),
                      child: Flex(
                        direction: Axis.horizontal,
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 20,
                            height: 20,
                          ),
                          CustomText(
                            title: overlayObject.title,
                            textColor: ColorPalette().textWhite,
                            isBold: true,
                            hasFontWeight: true,
                            fontWeight: FontWeight.w600,
                          ),
                          IconButton(
                            iconSize: 20.0,
                            padding: EdgeInsets.zero,
                            icon: Icon(
                              Icons.clear,
                              color: ColorPalette().textWhite,
                            ),
                            onPressed: overlayObject.clearToNavigate == true
                                ? () {
                                    removeOverlay();
                                    overlayObject.ctnFunction();
                                  }
                                : removeOverlay,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: isPortrait(context)
                          ? MediaQuery.of(context).size.width
                          : MediaQuery.of(context).size.width / 2,
                      decoration: BoxDecoration(
                        color: ColorPalette().textWhite,
                      ),
                      padding: EdgeInsets.all(10),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: overlayObject.icon),
                          Expanded(
                            flex: 6,
                            child: CustomText(
                              isCenter: false,
                              title: overlayObject?.body ?? "",
                              textColor: ColorPalette().textBlack,
                            ),
                          )
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 20,
                      height: 20,
                    ),
                    overlayObject.hasBtn
                        ? Container(
                            margin: EdgeInsets.only(bottom: 20),
                            child: Center(
                              child: overlayObject.hasCancelBtn
                                  ? Row(
                                      mainAxisSize: MainAxisSize.min,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        AnellohRoundButtonSmall(
                                            onPressed: overlayObject
                                                        .cancelBtnFunction !=
                                                    null
                                                ? overlayObject
                                                    .cancelBtnFunction
                                                : removeOverlay,
                                            childText: overlayObject
                                                        .cancelBtnText !=
                                                    null
                                                ? overlayObject.cancelBtnText
                                                : 'Cancel',
                                            bgColor: ColorPalette().mainMedium),
                                        SizedBox(
                                          width: 20,
                                          height: 20,
                                        ),
                                        AnellohRoundButtonSmall(
                                            onPressed: () {
                                              if (overlayObject.btnFunction !=
                                                  null) {
                                                overlayObject.btnFunction();
                                              }
                                              removeOverlay();
                                            },
                                            childText: overlayObject.btnText),
                                      ],
                                    )
                                  : AnellohRoundButtonSmall(
                                      onPressed: () {
                                        if (overlayObject.btnFunction != null) {
                                          overlayObject.btnFunction();
                                        }
                                        removeOverlay();
                                      },
                                      childText: overlayObject.btnText),
                            ),
                          )
                        : SizedBox(
                            width: 0,
                            height: 0,
                          )
                  ],
                ),
              ),
            ),
          ));
}

getXErrorSnackbar(String title, String message, Color color) {
  return Get.snackbar(title, message,
      snackPosition: SnackPosition.BOTTOM,
      backgroundColor: color,
      colorText: Colors.white);
}
