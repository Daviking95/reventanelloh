
import 'package:flutter/material.dart';

import '../styles.dart';

Widget startFabWidget(Function function, Color fabBgColor, [IconData icon = Icons.add]) {
  return FloatingActionButton(
    onPressed: () => function(),
    elevation: 5.0,
    child: Icon(
      icon,
      color: appWhiteColor,
      size: 40.0,
    ),
    backgroundColor: fabBgColor,
  );
}