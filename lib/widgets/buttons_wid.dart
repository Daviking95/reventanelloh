import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/font_util.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';

import '../styles.dart';

Widget AnellohFlatButton({
  Key key,
  @required Function onPressed,
  @required String childText,
  FontWeight fontWeight,
  Color color,
}) {
  return TextButton(
      onPressed: () {
        FocusManager.instance.primaryFocus?.unfocus();
        onPressed();
      } ,
      child: Text(
        '$childText',
        style: GoogleFonts.montserrat(
            color: color != null ? color : ColorPalette().main,
            fontSize: 14,
            fontWeight: fontWeight != null ? fontWeight : FontWeight.w600),
      ));
}

Widget AnellohRoundButtonSmall({
  @required final Function onPressed,
  @required final String childText,
  double textSize,
  final Color bgColor,
  final Color textColor,
}) {
  return RaisedButton(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
      color: bgColor != null ? bgColor : ColorPalette().main,
      child: CustomText(
        title: '$childText',
        textColor: textColor != null ? textColor : ColorPalette().textWhite,
        fontWeight: FontWeight.w900,
        textSize: textSize ?? null,
        hasFontWeight: true,
      ),
      onPressed: () {
        FocusManager.instance.primaryFocus?.unfocus();
        onPressed();
      });
}

Widget AnellohRoundButtonLarge({
  @required final Function onPressed,
  @required final String childText,
  double textSize,
  bool isInverted = false,
  final hasImage = false,
  Widget assetImage,
}) {
  return Container(
    width: Get.width,
    child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(13)),
        padding: EdgeInsets.all(14),
        color: isInverted ? ColorPalette().textWhite : ColorPalette().main,
        child: hasImage == true
            ? Row(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  assetImage,
                  SizedBox(
                    width: 10,
                  ),
                  CustomText(
                    title: '$childText',
                    textColor: isInverted
                        ? ColorPalette().main
                        : ColorPalette().textWhite,
                    fontWeight: FontWeight.w900,
                    textSize: textSize,
                    hasFontWeight: true,
                  ),
                ],
              )
            : CustomText(
                title: '$childText',
                textColor:
                    isInverted ? ColorPalette().main : ColorPalette().textWhite,
                fontWeight: FontWeight.w900,
                hasFontWeight: true,
              ),
        onPressed: () {
          FocusManager.instance.primaryFocus?.unfocus();
          onPressed();
        } ),
  );
}

Widget AnellohRoundNetworkButtonLarge(
    {@required final Function onPressed,
    @required final String childText,
    final bool isColorInverted = false,
    @required final bool isLoading}) {
  return Container(
    width: Get.width,
    child: RaisedButton(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(13)),
        padding: EdgeInsets.all(14),
        color: isColorInverted == true
            ? ColorPalette().textWhite
            : ColorPalette().main,
        child: CustomText(
          title: '${isLoading ? 'Loading...' : childText}',
          textColor:
              isColorInverted ? ColorPalette().main : ColorPalette().textWhite,
          fontWeight: FontWeight.w900,
          hasFontWeight: true,
        ),
        onPressed: isLoading == true
            ? null
            : () {
                FocusManager.instance.primaryFocus?.unfocus();
                onPressed();
              }),
  );
}

class AppPrimaryDarkButtonRound extends StatelessWidget {
  final String textTitle;
  final Widget routeToGo;
  final Color bgColor;
  final TextStyle textStyle;
  final double width;
  final double borderRadius;
  final Object arguments;
  final Function functionToRun;
  final bool isFakeButton;
  final double height;

  const AppPrimaryDarkButtonRound(
      {Key key,
      this.textTitle,
      this.routeToGo,
      this.bgColor,
      this.textStyle,
      this.width = 0,
      this.arguments = true,
      this.functionToRun,
      this.height = 30,
      this.borderRadius = 20.0,
      this.isFakeButton = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 50.0,
      // width: width == 0 ? 0 : width,
      padding: EdgeInsets.all(3),
      child: RaisedButton(
        onPressed: isFakeButton
            ? () {}
            : () {
            FocusManager.instance.primaryFocus?.unfocus();
                routeToGo != null ? Get.to(routeToGo) : functionToRun();
              },
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(16.0)),
        padding: EdgeInsets.all(0.0),
        child: Ink(
          decoration: BoxDecoration(
              color: appPrimaryColor,
              borderRadius: BorderRadius.circular(16.0)),
          child: Container(
            constraints: BoxConstraints(
                maxWidth: width == 0 ? 0 : width, minHeight: 50.0),
            alignment: Alignment.center,
            child: Text(textTitle,
                textAlign: TextAlign.center,
                style: googleFontTextStyle(
                  fontSize: 18,
                  color: ColorPalette().textWhite,
                  fontWeight: FontWeight.w600,
                )),
          ),
        ),
      ),
    );
  }
}

class AppOutlineButton extends StatelessWidget {
  final double borderRadius;
  final BuildContext context;
  final double width;
  final Color borderColor;
  final TextStyle textStyle;
  final String textTitle;
  final String routeToGo;
  final Object arguments;
  final Function functionToRun;

  AppOutlineButton(
      {this.borderRadius = 20.0,
      this.context,
      this.width = 0,
      this.borderColor,
      this.textTitle,
      this.routeToGo,
      this.arguments = true,
      this.functionToRun,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width == 0 ? 0 : width,
      child: OutlineButton(
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.circular(borderRadius),
        ),
        borderSide: BorderSide(
          color: ColorPalette().main, // borderColor,
          style: BorderStyle.solid,
          width: 2.0,
        ),
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {
          FocusManager.instance.primaryFocus?.unfocus();
          routeToGo != null
              ? Navigator.of(context).pushNamed(routeToGo)
              : functionToRun();
        },
        child: CustomText(
          title: '$textTitle',
          textColor: ColorPalette().main,
          fontWeight: FontWeight.w900,
          hasFontWeight: true,
        ),
      ),
    );
  }
}
