import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/widgets/rate_arrow.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';

class ListingsDetails extends StatelessWidget {
  final MatchFoundObj matchFoundObj;
  ListingsDetails({
    @required this.matchFoundObj,
  }) : super();
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: [
        Expanded(
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              // image - country flag
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  CustomText(
                    title:
                        'To Swap ${this.matchFoundObj.myCurrency}',
                    textSize: 13,
                  ),
                  CustomText(
                    title:
                        '${CurrencyFormatter.format(currencyCode: this.matchFoundObj.myCurrency, amount: this.matchFoundObj.totalAmount)}',
                    textSize: 16,
                    textColor: ColorPalette().dangerRed,
                    fontWeight: FontWeight.w900,
                    hasFontWeight: true,
                  ),
                  CustomText(
                    title:
                    'Rate: ${CurrencyFormatter.format(currencyCode: this.matchFoundObj.myCurrency == 'NGN' ? this.matchFoundObj.myCurrency : this.matchFoundObj.convertedCurrency, amount: this.matchFoundObj.exchangeRate)}/${CurrencyFormatter.format(currencyCode: this.matchFoundObj.convertedCurrency == 'NGN' ? this.matchFoundObj.convertedCurrency : this.matchFoundObj.myCurrency, amount: '')}',
                    textSize: 11,
                  ),
                ],
              ),
            ],
          ),
          flex: 2,
        ),
        Expanded(
          child: ConstrainedBox(
            constraints: BoxConstraints(maxHeight: 100, minHeight: 100),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                // line graph
                Expanded(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                        child: CustomText(
                          title:
                              '${this.matchFoundObj.isRateAscending ? '+' : '-'} ${CurrencyFormatter.format(currencyCode: this.matchFoundObj.myCurrency == 'NGN' ? this.matchFoundObj.myCurrency : this.matchFoundObj.convertedCurrency, amount: this.matchFoundObj.ascDecValue.toString())} ',
                          textSize: 13,
                        ),
                        flex: 4,
                      ),
                      Expanded(
                        child: rateArrow(
                            isAscending: this.matchFoundObj.isRateAscending,
                            size: 16.0),
                      )
                    ],
                  ),
                ),
                Expanded(
                    child: CustomText(
                  title: 'By: @${this.matchFoundObj.userName}',
                  textSize: 13,
                )),
              ],
            ),
          ),
        )
      ],
    );
  }
}
