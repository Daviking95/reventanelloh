import 'package:anelloh_mobile/helpers/font_util.dart';
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import '../styles.dart';
import 'bottom_sheet_widgets.dart';

class CustomText extends StatelessWidget {
  final String title;
  final bool isCenter;
  final bool isBold;
  final bool isPrimary;
  final double textSize;
  final Color textColor;
  final FontWeight fontWeight;
  final bool hasFontWeight;
  final TextOverflow textOverflow;

  const CustomText(
      {Key key,
      this.title,
      this.isCenter = false,
      this.textSize = 15,
      this.isBold = false,
      this.isPrimary = false,
      this.textColor = appPrimaryColor,
      this.fontWeight,
        this.textOverflow,
      this.hasFontWeight = false})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _buildTitleText();
  }

  Widget _buildTitleText() {
    FontWeight _fontWeight;
    TextStyle _textStyle;
    Color _txtColor;

    _fontWeight = hasFontWeight
        ? fontWeight
        : isBold
            ? FontWeight.w900
            : FontWeight.normal;
    _textStyle = googleFontTextStyle();
    _txtColor = isPrimary ? appPrimaryColor : textColor;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 3),
      child: AutoSizeText(title,
          textAlign: isCenter ? TextAlign.center : TextAlign.left,
          style: _textStyle.copyWith(
              fontSize: textSize, fontWeight: _fontWeight, color: _txtColor),
          wrapWords: true,
          softWrap: true,
         overflow: this.textOverflow ?? TextOverflow.clip
      ),
    );
  }
}

Widget pageTitle(String title, String toolTip, {Color color}) {
  return Row(
    children: [
      CustomText(
        title: title,
        hasFontWeight: true,
        fontWeight: FontWeight.w600,
        textSize: 23,
        textColor: color == null ? appPrimaryColor : color,
      ),
      SizedBox(width: 10,),
      MyTooltip(message: toolTip, child: Icon(Icons.info_outline_rounded, color: Color(0xffb6b6b6), size: 17,))
    ],
  );
}

Widget subPageTitle(String title, String toolTip, {Color color, BuildContext context}) {
  return Row(
    children: [
      Expanded(
        child: CustomText(
          title: title,
          isBold: true,
          hasFontWeight: true,
          fontWeight: FontWeight.w600,
          textSize: 23,
          textColor: color == null ? appPrimaryColor : color,
          // textSize: isPortrait(context)
          //     ? MediaQuery.of(context).size.width / 24
          //     : MediaQuery.of(context).size.height / 24,
        ),
      ),
      SizedBox(width: 10,),
      MyTooltip(message: toolTip, child: Icon(Icons.info_outline_rounded, color: Color(0xffb6b6b6), size: 17,))
    ],
  );
}
