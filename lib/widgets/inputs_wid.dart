import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/helpers/validators.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import '../styles.dart';

class AppTextInputs extends StatelessWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final int maxLength;
  final TextInputType textInputType;
  final Color color;
  final bool autoFocus;
  final Function validateInput;
  final Function onTapFunction;
  final Function onChange;
  final String hintText;
  final bool isReadOnly;
  final int maxLine;
  final Widget suffixIcon;
  final double borderRadius;
  final bool isOnlyDigits;
  final bool noSpace;
  final FocusNode focusNode;
  final List<TextInputFormatter> textInputFormatter;

  AppTextInputs(
      {this.textInputTitle,
      this.controller,
      this.maxLength = 0,
      this.textInputType,
      this.color = appWhiteColor,
      this.autoFocus = false,
      this.validateInput,
      this.onTapFunction,
      this.isReadOnly = false,
      this.hintText,
      this.onChange,
      this.maxLine = 1,
      this.suffixIcon,
      this.borderRadius = 5.0,
      this.focusNode,
      this.isOnlyDigits = false,
      this.textInputFormatter,
      this.noSpace = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: returnScreenSize(context, this.noSpace ? 0 : 6),
        ),
        maxLength == 0
            ? _buildTextInput(context)
            : _buildTextInputWithMaxLength(context),
        SizedBox(
          height: returnScreenSize(context, this.noSpace ? 0 : 6),
        ),
      ],
    );
  }

  Widget _buildTextInput(BuildContext context) {
    return TextFormField(
        decoration: textInputDecorator(
            textInputTitle, color, suffixIcon, borderRadius, hintText),
        style: textStyleWhiteColorNormal.copyWith(color: Colors.black),
        cursorColor: Colors.black,
        keyboardType: textInputType,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        controller: controller,
        autofocus: autoFocus,
        focusNode: focusNode,
        validator: validateInput,
        onTap: onTapFunction,
        readOnly: isReadOnly,
        onChanged: onChange,
        maxLines: maxLine,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        inputFormatters: this.textInputFormatter != null
            ? <TextInputFormatter>[
                if (isOnlyDigits) ...[FilteringTextInputFormatter.digitsOnly]
              ]
            : this.textInputFormatter);
  }

  Widget _buildTextInputWithMaxLength(BuildContext context) {
    return TextFormField(
        decoration:
            textInputDecorator(textInputTitle, color, suffixIcon, borderRadius),
        style: textStyleWhiteColorNormal.copyWith(color: Colors.black),
        cursorColor: Colors.black,
        keyboardType: textInputType,
        maxLength: maxLength,
        focusNode: focusNode,
        maxLengthEnforced: true,
        controller: controller,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        validator: validateInput,
        onTap: onTapFunction,
        readOnly: isReadOnly,
        onChanged: onChange,
        maxLines: maxLine,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        inputFormatters: this.textInputFormatter == null
            ? <TextInputFormatter>[
                if (isOnlyDigits) ...[FilteringTextInputFormatter.digitsOnly]
              ]
            : this.textInputFormatter);
  }
}

class AppPasswordTextInput extends StatefulWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final Function validateInput;
  final Color color;
  final TextInputType textInputType;
  final bool isOnlyDigits;
  final int maxLength;
  final Widget suffixIcon;

  AppPasswordTextInput(
      {this.textInputTitle,
      this.controller,
      this.validateInput,
      this.color,
      this.maxLength,
      this.textInputType,
      this.isOnlyDigits = false,
      this.suffixIcon});

  @override
  _AppPasswordTextInputState createState() => _AppPasswordTextInputState();
}

class _AppPasswordTextInputState extends State<AppPasswordTextInput> {
  bool _obscureText = true;
  final focus = FocusNode();

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: _obscureText,
      keyboardType: widget.textInputType != null
          ? widget.textInputType
          : TextInputType.visiblePassword,
      style: textStyleWhiteColorNormal.copyWith(color: Colors.black),
      validator: widget.validateInput,
      controller: widget.controller,
      maxLength: widget.maxLength,
      autovalidateMode: AutovalidateMode.onUserInteraction,
      cursorColor: Colors.black,
      textInputAction: TextInputAction.next,
      onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
      inputFormatters: <TextInputFormatter>[
        if (widget.isOnlyDigits) ...[FilteringTextInputFormatter.digitsOnly]
      ],
      decoration:
          textInputDecorator(widget.textInputTitle, widget.color).copyWith(
        suffixIcon: widget.suffixIcon != null
            ? widget.suffixIcon
            : FlatButton(
                onPressed: _toggle,
                child: new Icon(
                  _obscureText ? MdiIcons.eye : MdiIcons.eyeOff,
                  color: appBrownGrayOneColor,
                )),
      ),
    );
  }

  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }
}

InputDecoration textInputDecorator(String text, Color color,
    [Widget suffixIcon, double borderRadius = 15.0, String hintText]) {
  return InputDecoration(
    suffixIcon: suffixIcon == null ? SizedBox() : suffixIcon,
    counterStyle: textStyleWhiteColorNormal.copyWith(color: color),
    labelText: text,
    fillColor: Colors.white,
    hintText: hintText,
    counterText: '',
    labelStyle:
        textStylePrimaryColorNormal.copyWith(color: Colors.black, fontSize: 15),
    filled: true,
    border: OutlineInputBorder(borderRadius: BorderRadius.circular(15)),
    focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: appPrimaryColor, width: 1.0),
        borderRadius: BorderRadius.circular(15)),
    enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: appPrimaryColor, width: 1.0),
        borderRadius: BorderRadius.circular(15)),
  );
}

searchBoxWidget(TextEditingController controller, Function function) {
  Container(
    height: 40,
    child: TextField(
      onChanged: (value) {
        function();
      },
      controller: controller,
      style: textStyleBlackColorNormal,
      decoration: InputDecoration(
          labelText: "Search",
          hintText: "Search",
          prefixIcon: Icon(Icons.search),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(25.0)))),
    ),
  );
}

class AppSecondaryTextInput extends StatelessWidget {
  final String textInputTitle;
  final TextEditingController controller;
  final int maxLength;
  final TextInputType textInputType;
  final Color color;
  final bool autoFocus;
  final Function validateInput;
  final Function onTapFunction;
  final Function onChange;
  final bool isReadOnly;
  final int maxLine;
  final Widget suffixIcon;
  final double borderRadius;
  final bool isOnlyDigits;
  final bool noSpace;
  final FocusNode focusNode;

  AppSecondaryTextInput(
      {this.textInputTitle,
      this.controller,
      this.maxLength = 0,
      this.textInputType,
      this.color = appWhiteColor,
      this.autoFocus = false,
      this.validateInput,
      this.onTapFunction,
      this.isReadOnly = false,
      this.onChange,
      this.maxLine = 1,
      this.suffixIcon,
      this.borderRadius = 5.0,
      this.focusNode,
      this.isOnlyDigits = false,
      this.noSpace = false});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          height: returnScreenSize(context, this.noSpace ? 0 : 6),
        ),
        maxLength == 0
            ? _buildTextInput(context)
            : _buildTextInputWithMaxLength(context),
        SizedBox(
          height: returnScreenSize(context, this.noSpace ? 0 : 6),
        ),
      ],
    );
  }

  Widget _buildTextInput(BuildContext context) {
    return TextFormField(
        decoration: _decoration(labelText: textInputTitle),
        style:
            textStyleWhiteColorNormal.copyWith(color: ColorPalette().textWhite),
        cursorColor: Colors.black,
        keyboardType: textInputType,
        controller: controller,
        autofocus: autoFocus,
        focusNode: focusNode,
        validator: validateInput,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        onTap: onTapFunction,
        readOnly: isReadOnly,
        onChanged: onChange,
        maxLines: maxLine,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        inputFormatters: <TextInputFormatter>[
          if (isOnlyDigits) ...[FilteringTextInputFormatter.digitsOnly]
        ]);
  }

  Widget _buildTextInputWithMaxLength(BuildContext context) {
    return TextFormField(
        decoration: _decoration(labelText: textInputTitle),
        style:
            textStyleWhiteColorNormal.copyWith(color: ColorPalette().textWhite),
        cursorColor: Colors.black,
        keyboardType: textInputType,
        maxLength: maxLength,
        focusNode: focusNode,
        maxLengthEnforced: true,
        controller: controller,
        validator: validateInput,
        autovalidateMode: AutovalidateMode.onUserInteraction,
        onTap: onTapFunction,
        readOnly: isReadOnly,
        onChanged: onChange,
        maxLines: maxLine,
        textInputAction: TextInputAction.next,
        onFieldSubmitted: (_) => FocusScope.of(context).nextFocus(),
        inputFormatters: <TextInputFormatter>[
          if (isOnlyDigits) ...[FilteringTextInputFormatter.digitsOnly]
        ]);
  }

  InputDecoration _decoration({@required String labelText}) {
    return InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(
          color: ColorPalette().textWhite,
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide:
                BorderSide(color: ColorPalette().textWhite, width: 1.5)),
        enabled: true,
        filled: true,
        enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide:
                BorderSide(color: ColorPalette().textWhite, width: 1.5)),
        border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(15),
            borderSide:
                BorderSide(color: ColorPalette().textWhite, width: 1.5)));
  }
}
