import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/color_palette.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/font_util.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/widgets/buttons_wid.dart';
import 'package:anelloh_mobile/widgets/rate_arrow.dart';
import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ListingActivityCard extends StatelessWidget {
  final MatchFoundObj matchFoundObj;
  final Function onPressed;
  ListingActivityCard(
      {Key key, @required this.matchFoundObj, @required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onPressed,
      child: Container(
        decoration: BoxDecoration(
            color: ColorPalette().textWhite,
            borderRadius: BorderRadius.circular(10),
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.4),
                offset: Offset(1, 3),
                spreadRadius: 1.1,
                blurRadius: 3.5,
              )
            ]),
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.only(top: 5, bottom: 5, left: 3, right: 3),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: [
            Expanded(
                flex: 2,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    CustomText(
                      title: 'Swap Amount in ${this.matchFoundObj.myCurrency}',
                    ),
                    SizedBox(
                      height: 2,
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      mainAxisSize: MainAxisSize.max,
                      children: [
                        ClipRRect(
                          borderRadius: BorderRadius.circular(100),
                          child: Image.asset(
                            'icons/flags/png/${this.matchFoundObj.myCurrency.substring(0, 2).toLowerCase()}.png',
                            package: 'country_icons',
                            width: 40,
                            height: 40,
                            fit: BoxFit.fill,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Expanded(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.max,
                            children: [
                              CustomText(
                                title:
                                    '${CurrencyFormatter.format(currencyCode: this.matchFoundObj.myCurrency, amount: formatToCurrency(this.matchFoundObj.myAmount))}',
                                textColor: ColorPalette().dangerRed,
                                textSize: mainSizer(context: context, size: 23),
                                isBold: true,
                              ),
                              CustomText(
                                title:
                                    'Rate: ${CurrencyFormatter.format(currencyCode: this.matchFoundObj.myCurrency == 'NGN' ? this.matchFoundObj.myCurrency : this.matchFoundObj.convertedCurrency, amount: this.matchFoundObj.exchangeRate)}/${CurrencyFormatter.format(currencyCode: this.matchFoundObj.convertedCurrency == 'NGN' ? this.matchFoundObj.myCurrency : this.matchFoundObj.convertedCurrency, amount: '')}',
                                textColor: Colors.grey.withOpacity(0.8),
                                textSize: mainSizer(context: context, size: 34),
                              ),
                              SizedBox(
                                height: 8,
                              ),
                              Container(
                                child: Text(
                                  'By: @${this.matchFoundObj.userName}',
                                  style: googleFontTextStyle(
                                      fontSize:
                                          mainSizer(context: context, size: 38),
                                      fontWeight: FontWeight.w400),
                                  softWrap: false,
                                  overflow: TextOverflow.fade,
                                ),
                                width: mainSizer(context: context, size: 2),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                )),
            Container(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  SizedBox(
                    height: 10,
                  ),
                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              CustomText(
                                title:
                                    '${this.matchFoundObj.isRateAscending ? '+' : '-'} ${CurrencyFormatter.format(currencyCode: 'NGN', amount: this.matchFoundObj.exchangeRate)}',
                              ),
                              rateArrow(
                                  isAscending: true,
                                  size: mainSizer(context: context, size: 30)),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  AnellohRoundButtonSmall(
                      onPressed: onPressed, childText: 'View'),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
