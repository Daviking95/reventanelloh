import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

import '../constants.dart';

class CustomBottomNavigation extends StatefulWidget {
  final int selectedIndex;

  CustomBottomNavigation({this.selectedIndex});

  @override
  State<StatefulWidget> createState() {
    return CustomBottomNavigationState();
  }
}

class CustomBottomNavigationState extends State<CustomBottomNavigation> {
  double iconSize = 18.0;

  void _onItemTapped(int index) {
    setState(() {
      if (index == 0) {
        Navigator.pushNamed(context, RoutesConstants.dashboardUrl);
      } else if (index == 1) {
        Navigator.pushNamed(context, RoutesConstants.listingUrl);
      } else if (index == 2) {
        Navigator.pushNamed(context, RoutesConstants.activityUrl);
      } else if (index == 3) {
        Navigator.pushNamed(context, RoutesConstants.settingsUrl);
      } else {
        Navigator.pushNamed(context, RoutesConstants.supportUrl);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return BottomNavigationBar(
      elevation: 10.0,
      type: BottomNavigationBarType.fixed,
      unselectedItemColor: Color(0XFFFFFFFF),
      backgroundColor: Color(0xff26474E),
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing(SvgPicture.asset(
            "assets/images/dashboardIcon.svg",
            color: Color(0XFFF27348),
          )),
          icon: _bNavSpacing(SvgPicture.asset(
            "assets/images/dashboardIcon.svg",
            color: Color(0XFFFFFFFF),
          )),
          title: _bNavSpacing(Text(
            "Home",
            style: _bNavTextStyle(),
          )),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing(
            SvgPicture.asset(
              "assets/images/listingIcon.svg",
              color: Color(0XFFF27348),
            ),
          ),
          icon: _bNavSpacing(
            SvgPicture.asset(
              "assets/images/listingIcon.svg",
              color: Color(0XFFFFFFFF),
            ),
          ),
          title: _bNavSpacing(Text(
            "Listing",
            style: _bNavTextStyle(),
          )),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing(
              SvgPicture.asset(
                "assets/images/activityIcon.svg",
                color: Color(0XFFF27348),
                width: 15,
                height: 15,
              ),
              9),
          icon: _bNavSpacing(
              SvgPicture.asset(
                "assets/images/activityIcon.svg",
                color: Color(0XFFFFFFFF),
                width: 15,
                height: 15,
              ),
              9),
          title: _bNavSpacing(Text(
            "Activity",
            style: _bNavTextStyle(),
          )),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing(
            SvgPicture.asset(
              "assets/images/settingsIcon.svg",
              color: Color(0XFFF27348),
            ),
          ),
          icon: _bNavSpacing(
            SvgPicture.asset(
              "assets/images/settingsIcon.svg",
              color: Color(0XFFFFFFFF),
            ),
          ),
          title: _bNavSpacing(Text(
            "Settings",
            style: _bNavTextStyle(),
          )),
        ),
        BottomNavigationBarItem(
          activeIcon: _bNavSpacing(
            SvgPicture.asset(
              "assets/images/supportIcon.svg",
              color: Color(0XFFF27348),
            ),
          ),
          icon: _bNavSpacing(
            SvgPicture.asset(
              "assets/images/supportIcon.svg",
              color: Color(0XFFFFFFFF),
            ),
          ),
          title: _bNavSpacing(Text(
            "Support",
            style: _bNavTextStyle(),
          )),
        ),
      ],
      currentIndex: widget.selectedIndex,
      selectedItemColor: Color(0XFFF27348),
      onTap: _onItemTapped,
    );
  }

  Widget _bNavSpacing(Widget widget, [double spacingSize = 0]) {
    return Container(
        margin:
            EdgeInsets.symmetric(vertical: spacingSize == 0 ? 5 : spacingSize),
        child: widget);
  }

  TextStyle _bNavTextStyle() {
    TextStyle textStyle = TextStyle(fontSize: 15, fontWeight: FontWeight.w400);
    return textStyle;
  }
}
