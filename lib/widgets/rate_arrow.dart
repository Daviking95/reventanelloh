import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

Widget rateArrow({isAscending, double size}) {
  if (isAscending) {
    return Icon(
      MdiIcons.arrowUp,
      color: Colors.green,
      size: size,
    );
  } else {
    return Icon(
      MdiIcons.arrowDown,
      color: Colors.red,
      size: size,
    );
  }
}