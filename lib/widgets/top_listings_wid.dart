import 'package:anelloh_mobile/helpers/classes/order_listing.dart';
import 'package:anelloh_mobile/helpers/currency_formatter.dart';
import 'package:anelloh_mobile/helpers/currency_symbol_formatter.dart';
import 'package:anelloh_mobile/helpers/font_util.dart';
import 'package:anelloh_mobile/helpers/screen_size.dart';
import 'package:anelloh_mobile/styles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:line_icons/line_icons.dart';

import '../constants.dart';
import 'buttons_wid.dart';
import 'text_wid.dart';

Widget topListingsWidget(BuildContext context,
    {@required OrderListing orderListing}) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
    margin: EdgeInsets.only(right: 10, top: 10, bottom: 10),
    decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              offset: Offset(2, 3),
              blurRadius: 10.4,
              spreadRadius: 0.2,
              color: Colors.grey.withOpacity(0.2)),
        ],
        borderRadius: BorderRadius.circular(10)),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: [
        Row(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(100),
              child: Image.asset(
                'icons/flags/png/${orderListing.myCurrency.substring(0, 2).toLowerCase()}.png',
                package: 'country_icons',
                width: 40,
                height: 40,
                fit: BoxFit.fill,
              ),
            ),
            SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                CustomText(
                  title: "Swap amount in ${orderListing.myCurrency}",
                  textSize: 15,
                ),
                CustomText(
                  title:
                      '${CurrencyFormatter.format(currencyCode: orderListing.myCurrency, amount: formatToCurrency(orderListing.myAmount))}',
                  hasFontWeight: true,
                  textColor: Color(0xffF27348),
                  fontWeight: FontWeight.w800,
                  textSize: 20,
                ),
                CustomText(
                  title:
                      'Rate ${CurrencyFormatter.format(currencyCode: orderListing.myCurrency == 'NGN' ? orderListing.myCurrency : orderListing.convertedCurrency, amount: orderListing.exchangeRate)}/${CurrencyFormatter.format(currencyCode: orderListing.convertedCurrency == 'NGN' ? orderListing.myCurrency : orderListing.convertedCurrency, amount: '')}',
                  hasFontWeight: true,
                  textColor: Color(0xff848484),
                  fontWeight: FontWeight.w400,
                  textSize: 13,
                ),
              ],
            ),
            SizedBox(
              width: 10,
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SvgPicture.asset(ImageConstants.wiglyChart),
                Row(
                  children: [
                    CustomText(
                      title:
                          '${CurrencyFormatter.format(currencyCode: orderListing.myCurrency == 'NGN' ? orderListing.myCurrency : orderListing.convertedCurrency, amount: orderListing.ascDecValue.toString())} ',
                      hasFontWeight: true,
                      textColor: Color(0xff848484),
                      fontWeight: FontWeight.w400,
                      textSize: 13,
                    ),
                    // SizedBox(
                    //   width: 4,
                    // ),
                    orderListing.isRateAscending
                        ? Icon(
                            LineIcons.alternateLongArrowUp,
                            color: Color(0xff39D979),
                          )
                        : Icon(
                            LineIcons.alternateLongArrowDown,
                            color: Colors.red,
                          )
                  ],
                ),
              ],
            )
          ],
        ),
        SizedBox(
          height: 10,
        ),
        LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
          return Container(
            width: 310,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              children: [
                Expanded(
                  child: Container(
                    child: Text(
                      'By: @${orderListing.userName}',
                      style: googleFontTextStyle(
                          fontSize: mainSizer(context: context, size: 38),
                          fontWeight: FontWeight.w400),
                      softWrap: false,
                      overflow: TextOverflow.fade,
                    ),
                    width: mainSizer(context: context, size: 2),
                  ),
                ),
                Spacer(),
                AnellohRoundButtonSmall(
                  bgColor: appPrimaryColor,
                  childText: "View",
                  onPressed: () {
                    Navigator.of(context)
                        .pushNamed('/instant_match', arguments: {
                      'orderId': int.parse(orderListing.orderId),
                      'orderListing': orderListing,
                      'orderOwnerId': orderListing.customerId,
                      'currencyNeededCountryCode': orderListing.myCurrency,
                    });
                  },
                ),
              ],
            ),
          );
        }),
      ],
    ),
  );
}

// ignore: non_constant_identifier_names
Widget OrderListingsSmallWidget() {
  return InkWell(
    onTap: () {
      print('dsd');
    },
    child: Container(
      padding: EdgeInsets.only(top: 7, left: 7, right: 30, bottom: 0),
      margin: EdgeInsets.all(5),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              offset: Offset(2, 1),
              blurRadius: 9.3,
              spreadRadius: 0.2,
              color: Colors.grey.withOpacity(0.3))
        ],
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: [
          SvgPicture.asset(
            ImageConstants.countryIcon,
            width: 30,
            height: 30,
          ),
          SizedBox(
            height: 5,
          ),
          CustomText(
            title:
                "${AppConstants.moneyFormatter.returnFormattedMoney(4, "\$")}",
            hasFontWeight: true,
            fontWeight: FontWeight.w700,
            textSize: 13,
          ),
        ],
      ),
    ),
  );
}
