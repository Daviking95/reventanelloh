enum Flavor {
  PILOT,
  STAGING,
  LIVE,
}

class F {
  static Flavor appFlavor;

  static String get title {
    switch (appFlavor) {
      case Flavor.PILOT:
        return 'Anelloh Test';
      case Flavor.STAGING:
        return 'Anelloh Staging';
      case Flavor.LIVE:
        return 'Anelloh';
      default:
        return 'Anelloh';
    }
  }

  static String get baseUrl {
    switch (appFlavor) {
      case Flavor.PILOT:
        return 'https://api-poc.anelloh.com/api/';
      case Flavor.STAGING:
        return 'https://api-poc.anelloh.com/api/';
      case Flavor.LIVE:
        return 'https://api-prod.anelloh.com/api/';
      default:
        return 'https://api-prod.anelloh.com/api/';
    }
  }

}
