import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/UI/splash_screen.dart';
import 'package:anelloh_mobile/bloc/launch/launch_bloc.dart';
import 'package:anelloh_mobile/bloc/user/user_bloc.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/deepLinkController.dart';
import 'package:anelloh_mobile/router.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intercom_plugin/intercom_plugin.dart';
import 'package:intl/intl.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:overlay_support/overlay_support.dart';
import 'package:uni_links/uni_links.dart' as UniLinks;

import 'bloc/notification/notification_bloc.dart';
import 'dynamic_link_service.dart';
import 'helpers/device_info.dart';
import 'helpers/fcm_service.dart';
import 'helpers/shared_pref_helper.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  checkDeepLinking();
  DynamicLinkService().handleDynamicLinks();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
          AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await DotEnv.load(fileName: '.env');
  await Intercom.initialize(
      appId: 'pwta5071',
      iOSApiKey: 'ios_sdk-3b929daff10f72d82e3cb2aa89b4d65dc468a2a2',
      androidApiKey: 'android_sdk-6d6bf312895de81dd592b3995404d12be0019a1b');
  Intercom.registerUnidentifiedUser();
  runApp(AnellohApp());
}

class AnellohApp extends StatefulWidget {
  final Uri uri;

  AnellohApp({Key key, this.uri}) : super(key: key);

  @override
  _AnellohAppState createState() => _AnellohAppState();
}

class _AnellohAppState extends State<AnellohApp> with WidgetsBindingObserver {
  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
      FlutterLocalNotificationsPlugin();

  String token;
  List subscribed = [];

  final _navigatorKey = GlobalKey<NavigatorState>();
  Timer _timer;

  Future selectNotification(String payload) async {
    if (payload != null) {
      debugPrint('notification payload: $payload');
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    FocusManager.instance.primaryFocus?.unfocus();

    getDeviceToken();

    _initializeTimer();

    FirebaseMessaging.instance
        .getInitialMessage()
        .then((RemoteMessage message) {
      if (message != null) {
        Navigator.pushNamed(
          context,
          '/notifications',
        );
      }
    });

    // initialise the plugin. app_icon needs to be a added as a drawable resource to the Android head project
    AndroidInitializationSettings initializationSettingsAndroid =
        AndroidInitializationSettings('app_icon');

    IOSInitializationSettings iosInitializationSettings =
        IOSInitializationSettings(requestAlertPermission: true);

    final InitializationSettings initializationSettings =
        InitializationSettings(
            android: initializationSettingsAndroid,
            iOS: iosInitializationSettings,
            macOS: null);

    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    FirebaseMessaging.onMessage.listen((RemoteMessage message) {});

    getToken();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    // dependOnInheritedWidgetOfExactType();
  }

  _initializeTimer() async {

    log("Main Touchtime ${DateTime.now()}");
    if (_timer != null) {
      await _isTokenVerified();
      _timer.cancel();
    }

    _timer = Timer(const Duration(minutes: 5), _initializeLogOut);
  }

  _isTokenVerified() async {
    bool isTokenVerified = false;
    isTokenVerified = await _checkSession();

    // log('_isTokenVerified $isTokenVerified');

    if (!isTokenVerified) {
      _logUserOut();
      return false;
    }
  }

  _checkSession() async {
    String token;
    if (await readFromPref('__Anelloh_MOBL_User_Session_token__')) {
      token = await getFromPref('__Anelloh_MOBL_User_Session_token__');
    }

    // log('_checkSession token $token');

    if (token != null && token != '') {
      Map<String, dynamic> payload = Jwt.parseJwt(token);

      String formattedDate = DateFormat('yyyy-MM-dd – kk:mm')
          .format(DateTime.fromMillisecondsSinceEpoch((payload['exp'] * 1000)));

      String currentDateFormatted =
      DateFormat('yyyy-MM-dd – kk:mm').format(DateTime.now());

      // log("Protected token $formattedDate");
      // log("Protected token $currentDateFormatted");
      // log("Protected token ${(DateTime.now().isAfter(DateTime.fromMillisecondsSinceEpoch(payload['exp'] * 1000)))}");

      if ((DateTime.now().isAfter(
          DateTime.fromMillisecondsSinceEpoch(payload['exp'] * 1000)))) {
        // log("Protected token expiry $token");

        return false;
      } else
        return true;
    } else {
      _logUserOut();
    }
  }

  void _initializeLogOut() {
    log("Main Logout Touchtime ${DateTime.now()}");
    _timer?.cancel();
    _timer = null;

    _logUserOut();
  }

  _logUserOut() async {
    await saveToPref('__Anelloh_MOBL_User_Session_token__', '');
    var resultEntity;
    if (await readFromPref('__anelloh_current_user__')) {
      resultEntity = jsonDecode(await getFromPref('__anelloh_current_user__'));
    }

    _navigatorKey.currentState
        .pushNamedAndRemoveUntil('/login', (_) => false, arguments: {
      'isSessionExpired': true,
      'email': resultEntity['email'] ?? '',
    });
  }

  getToken() async {
    token = await FirebaseMessaging.instance.getToken();

    // String token = await _fcm.getToken();
    // print("FirebaseMessaging token: $token");
    setState(() {
      token = token;
    });
  }

  @override
  didChangeAppLifecycleState(AppLifecycleState state) {
    print('state = $state');
    switch (state) {
      case AppLifecycleState.resumed:
        print("resumed");

        break;
      case AppLifecycleState.inactive:
        print("inactive");

        break;
      case AppLifecycleState.paused:
        print("paused");

        break;
      case AppLifecycleState.detached:
        print("detached");

        break;
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onTap: () {
        log('OnTap');
        FocusManager.instance.primaryFocus?.unfocus();
        _initializeTimer();
      },
      onPanDown: (_) => _initializeTimer(),
      onPanUpdate: (_) => _initializeTimer(),
      onPanStart: (_) => _initializeTimer(),
      onLongPress: _initializeTimer,
      onHorizontalDragStart: (_) => _initializeTimer(),
      onHorizontalDragEnd: (_) => _initializeTimer(),
      child: MultiBlocProvider(
        providers: [
          BlocProvider<UserBloc>(create: (context) => UserBloc()),
          BlocProvider<NotificationBloc>(
              create: (context) => NotificationBloc()),
        ],
        child: OverlaySupport(
          child: MaterialApp(
            title: AppConstants.APP_TITLE,
            debugShowCheckedModeBanner: false,
            navigatorKey: _navigatorKey,
            // showSemanticsDebugger: true,
            onGenerateRoute: RouteGenerator.generateRoute,
            onUnknownRoute: RouteGenerator.generateRoute,
            home: DeepLinkController(
              uri: this.widget.uri,
              child: BlocProvider<LaunchScreenBloc>(
                create: (context) => LaunchScreenBloc(),
                child: WillPopScope(
                  child: MainScreen(),
                  onWillPop: () async {
                    return false;
                  },
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}

checkDeepLinking() async {
  StreamSubscription _sub;
  try {
    await UniLinks.getInitialUri();
    _sub = UniLinks.getUriLinksStream().listen((Uri uri) {
      // print('uri: $uri');
      WidgetsFlutterBinding.ensureInitialized();
      runApp(AnellohApp(
        uri: uri,
      ));
    });
  } on PlatformException {
    print('platform error');
    _sub.cancel();
  } on HttpException {
    print('http exception');
    _sub.cancel();
  } on SocketException {
    print('socket exception');
    _sub.cancel();
  } catch (err) {
    print('error');
    _sub.cancel();
  }
}
