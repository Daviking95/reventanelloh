import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/classes/transaction_limit.dart';

import 'flavors.dart';
import 'helpers/money_formatter.dart';
import 'helpers/validators.dart';

class AppConstants {
  static String APP_TITLE = F.title;

  static String appNo = "No";

  static String appYes = "Yes";

  static const String baseUrl =
      'https://dev-app-ip.eastus.cloudapp.azure.com:44312/api/';

  static Validators validators = new Validators();

  static MoneyFormatter moneyFormatter = new MoneyFormatter();

  static const String howCanWeHelp = "How may I help you today?";

  static User userData = User(
    fullNames: '',
    firstName: '',
    lastName: '',
    userName: '',
    emailAddress: '',
    profileImage: null,
    phoneNumber: '',
    bvn: '',
    homeAddress: '',
    gender: '',
    customerId: '',
    verificationStatus: '',
    countryCode: '',
  );

  static String sharedPrefToken = "token";

  static String userLatestOrderStatus;

  static String profileImage = '';

  static List<TransactionLimitResponseModelEntity> transactionLimitArray = [
    TransactionLimitResponseModelEntity.fromJson({
      "id": 1,
      "transactionLimit": 5000,
      "currency": "USD",
      "createdBy": "string",
      "createdDate": "2021-04-16T19:36:13.0301625",
      "lastModifiedBy": null,
      "lastModifiedDate": null,
      "status": 1
    }),
    TransactionLimitResponseModelEntity.fromJson({
      "id": 2,
      "transactionLimit": 1000000,
      "currency": "NGN",
      "createdBy": "string",
      "createdDate": "2021-04-16T19:36:28.2885718",
      "lastModifiedBy": null,
      "lastModifiedDate": null,
      "status": 1
    })
  ];

  static dynamic Anelloh_MOBL_User_Session_token__;

  static String instantMatchCurrency;
}

class RoutesConstants {
  static const String starterUrl = "/";
  static const String onboardingUrl = "/onboarding";
  static const String signUpUrl = "/sign_up";
  static const String loginUrl = "/login";
  static const String forgotPasswordUrl = "/forgot_password";
  static const String dashboardUrl = "/dashboard";
  static const String createOrderUrl = "/create_order";
  static const String profileUrl = '/profile';
  static const String settingsUrl = '/settings';
  static const String activityUrl = '/activities';
  static const String listingUrl = '/listings';
  static const String supportUrl = '/support';
  static const String pinCreationUrl = '/create_pin';
}

class ImageConstants {
  static const String logoSplash = 'assets/images/favicon.png';

  static const String anellohLogo = 'assets/images/anelloh_main_logo.png';

  static const String bgIllus = 'assets/images/bg_illus.svg';

  static const String otp_mail = 'assets/images/otp_mail.svg';

  static const String screenOneIllus = 'assets/images/screen_one_illus.svg';

  static const String screenTwoIllus = 'assets/images/screen_two_illus.svg';

  static const String screenThreeIllus = 'assets/images/screen_three_illus.svg';

  static const String profilePicture = "assets/images/user_prof.png";

  static const String notificationIcon = 'assets/images/bell.svg';

  static const String createOrderBgIcon = "assets/images/create_order_img.svg";

  static const String countryIcon = "assets/images/usa_logo.svg";

  static const String nigCountryIcon = 'assets/images/nigeria_flag.svg';

  static const String wiglyChart = "assets/images/wigly_chart.svg";

  static const String backNavIcon = 'assets/images/back_arrow.svg';

  static const appIconOne = 'assets/images/appIcon-01.svg';

  static const appIconTwo = 'assets/images/appIcon-02.svg';

  static const String createOrderSwapIcon =
      'assets/images/create_order_swap.svg';

  static const String errorIcon = 'assets/images/error.svg';
}

class ExitAppConstants {
  static const String _exitAppTitle = 'Do you want to exit this application?';

  static const String _exitAppContent = 'We hate to see you leave...';

  static String get exitAppContent => _exitAppContent;

  static String get exitAppTitle => _exitAppTitle;
}

const List<Map<String, dynamic>> pageScreenData = [
  {
    'illustration': ImageConstants.screenOneIllus,
    'page_header': 'Quick and easy',
    'page_description':
        'Get paid the currency you choose in as quickly as 30 minutes. Some transactions may be delayed for up to 24 hours.'
  },
  {
    'illustration': ImageConstants.screenTwoIllus,
    'page_header': 'Low rates',
    'page_description':
        'Anelloh gives you the freedom to set your own rates. Extremely low rates take much longer to swap.'
  },
  {
    'illustration': ImageConstants.screenThreeIllus,
    'page_header': 'Secure transactions',
    'page_description':
        'We make sure that every step of your swap is protected to prevent fraud.'
  },
];

class HttpDataConstants {
  static const String userName = "Uju";
}
