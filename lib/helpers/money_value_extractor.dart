
import 'package:flutter/cupertino.dart';

String returnMoneyValue(TextEditingController controller){
  return controller.text.replaceAll(",", "").trim();
}