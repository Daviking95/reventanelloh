import 'dart:io';

import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();

class DeviceInfo {
  DeviceInfoPlugin deviceInfoPlugin;
  DeviceInfo() {
    deviceInfoPlugin = DeviceInfoPlugin();
  }

  Future<String> iosInfo() async {
    // return await this.deviceInfoPlugin.iosInfo;
    IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
    // DeviceInfo.deviceName = iosInfo.name;
    return iosInfo.identifierForVendor;
  }

  Future<String> androidInfo() async {
    AndroidDeviceInfo androidDeviceInfo = await deviceInfo.androidInfo;
    // DeviceInfo.deviceName = androidDeviceInfo.brand;
    return androidDeviceInfo.androidId;
    // return await this.deviceInfoPlugin.androidInfo;
  }
}

Future<String> getDeviceToken() async {
  await saveToPref(
      '__device__token', await FirebaseMessaging.instance.getToken());
  // Platform.isIOS ? DeviceInfo().iosInfo : DeviceInfo().androidInfo);

  return await FirebaseMessaging.instance.getToken();
}

Future<String> getDeviceId() async {
  // log('GetDevieID ${Platform.isIOS ? await DeviceInfo().iosInfo() : await DeviceInfo().androidInfo() + DateTime.now().microsecondsSinceEpoch.toString()}');

  return (Platform.isIOS
      ? await DeviceInfo().iosInfo()
      : await DeviceInfo().androidInfo());
  // +DateTime.now().microsecondsSinceEpoch.toString();
}
