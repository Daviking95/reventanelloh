import 'package:intl/intl.dart';

formatToCurrency(String value) {
  if(value != null && value.isNotEmpty) {
    NumberFormat numberFormat =  NumberFormat('#,##0.00', 'en_US');
    // print(value);
    if(value != null) return  numberFormat.format((double.parse(value)));
    return '';
  }
}