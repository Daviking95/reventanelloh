import 'package:anelloh_mobile/widgets/toasts_and_snacks_wid.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:form_field_validator/form_field_validator.dart';

import '../constants.dart';
import '../styles.dart';

class Validators {
  bool isFieldEmpty(String fieldValue) => fieldValue?.isEmpty ?? true;

  String validateName(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^["A-za-z. -]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only alphabetical characters.';
    return null;
  }

  String validateEmail(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value.trim()))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validateNigerianAccountNumber(String value) {
    String validateNum = this.validateNumber(value);
    if (validateNum == null) {
      if (value.length != 10) return 'Invalid account number';
      return null;
    } else {
      return validateNum;
    }
  }

  String validateBVN(String value) {
    if (value.trim().isEmpty) return 'Field is required';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';
    if (value.trim().length != 11) return 'Invalid BVN';

    return null;
  }

  String validateSSN(String value) {
    if (value.trim().isEmpty) return 'Field is required';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';
    if (value.trim().length != 9) return 'Invalid SSN';

    return null;
  }

  String validateInternationalPassportNumber(String value) {
    if (value.trim().isEmpty) return 'Field is required';
    return null;
  }

  bool passwordValidationToast(TextEditingController controller) {
    Pattern pattern = r'^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^a-zA-Z\d]).{8,}$';
    RegExp regex = new RegExp(pattern);
    if (controller.text.isEmpty) {
      showToast(
          toastMsg: 'Please enter password.',
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.CENTER,
          bgColor: appPrimaryColor,
          txtColor: appWhiteColor);
      return false;
    } else if (!regex.hasMatch(controller.text.trim())) {
      showToast(
          toastMsg:
              'Password should contain at least one upper case, at least one lower case, at least one digit and at least one Alphanumeric characters and minimum of 8 characters',
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.BOTTOM,
          bgColor: appPrimaryColor,
          txtColor: appWhiteColor);
      return false;
    } else {
      return true;
    }
  }

  final validatePassword = MultiValidator([
    RequiredValidator(errorText: 'password is required'),
    MinLengthValidator(8, errorText: 'must be at least 8 digits long'),
    PatternValidator(r'(?=.*?[A-Z])',
        errorText: 'must have at least one capital letter'),
    PatternValidator(r'(?=.*?[0-9])',
        errorText: 'must have at least one number'),
    PatternValidator(r'(?=.*?[#?!@$%^&*-])',
        errorText: 'must have at least one special character')
  ]);

  String validateNumber(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';
//    if (value.trim().length != 11)
//      return 'Field must be of 11 digits';
    else
      return null;
  }

  String validateNIN(String value) {
    var validateNum = validateNumber(value);
    if (validateNum != null) {
      return validateNum;
    }
    if (value.length != 11 || value.length > 11 || value.length < 11)
      return 'Please enter a valid NIN';
    return null;
  }

  String validatePhoneNumber(String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^(?:[+0])?[0-9]{10}$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only correct phone number.';
    if (value.trim().length != 11)
      return 'Field must be of 11 digits';
    else
      return null;
  }

  String validateString(dynamic value) {
    if (value.toString().trim().isEmpty)
      return 'Field is required.';
    else
      return null;
  }

  bool spinnerVaidation(TextEditingController controller, String text) {
    if (controller.text.isEmpty) {
      showToast(
          toastMsg: text,
          toastLength: Toast.LENGTH_LONG,
          toastGravity: ToastGravity.BOTTOM,
          bgColor: appPrimaryColor,
          txtColor: appWhiteColor);
      return false;
    } else {
      return true;
    }
  }

  String comparePasswords(
      TextEditingController controller, TextEditingController controller2) {
    if (controller2.text != controller.text) {
      return 'Confirm password not the same with password';
      // showToast(
      //     toastMsg: 'Confirm password not the same with password',
      //     toastLength: Toast.LENGTH_LONG,
      //     toastGravity: ToastGravity.BOTTOM,
      //     bgColor: Colors.red,
      //     txtColor: appWhiteColor);
      // return false;
    }

    return '';
  }

  String validateTransLimit(String currencyToSwap, String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';

    int transactionLimit = double.parse(AppConstants.transactionLimitArray
        .elementAt(AppConstants.transactionLimitArray
        .indexWhere((element) => element.currency == currencyToSwap))
        .transactionLimit
        .toString())
        .toInt();

   if (int.parse(value) >= transactionLimit)
     return 'You cannot swap with this amount. \nYour amount must be less than \n${AppConstants.moneyFormatter.returnFormattedMoney(transactionLimit.toDouble(), currencyToSwap)}';
    else
      return null;
  }

  String validateTransLimitDollarOnly(String currencyToSwap, String value) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';

    int transactionLimit = double.parse(AppConstants.transactionLimitArray
        .elementAt(AppConstants.transactionLimitArray
        .indexWhere((element) => element.currency == currencyToSwap))
        .transactionLimit
        .toString())
        .toInt();

   if (currencyToSwap.contains("USD") && int.parse(value) >= transactionLimit)
     return 'You cannot swap with this amount. \nYour amount must be less than \n${AppConstants.moneyFormatter.returnFormattedMoney(transactionLimit.toDouble(), currencyToSwap)}';
    else
      return null;
  }

  String validateDigitsOnlyLength(dynamic value, int length) {
    if (value.trim().isEmpty) return 'Field is required.';
    final RegExp nameExp = new RegExp(r'^[-0-9 ]+$');
    if (!nameExp.hasMatch(value.trim()))
      return 'Please enter only numeric characters.';

    if(value.length != length)
      return 'The length of your input must $length';
    else
      return null;
  }
}
