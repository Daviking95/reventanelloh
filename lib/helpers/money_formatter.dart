
import 'package:flutter_money_formatter/flutter_money_formatter.dart';

class MoneyFormatter {

  String returnFormattedMoney(double value, String currencySymbol, [bool isShowAll = true]){

    MoneyFormatterOutput fmf = new FlutterMoneyFormatter(
        amount: value,
        settings: MoneyFormatterSettings(
            symbol: currencySymbol,
            thousandSeparator: ',',
            decimalSeparator: '.',
            symbolAndNumberSeparator: ' ',
            fractionDigits: 2,
            compactFormatType: CompactFormatType.short
        )
    ).output;

    String val = value.toString();
    if(isShowAll) return fmf.symbolOnLeft;
    return val.length < 9 ? fmf.symbolOnLeft : fmf.compactSymbolOnLeft;
  }
}