import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

//Helper Links : https://www.freecodecamp.org/news/local-notifications-in-flutter/
// https://stackoverflow.com/questions/57891871/flutter-firebase-message-plugin-setup-errors
// https://pub.dev/packages/flutter_local_notifications#example-app

// Future<dynamic> myBackgroundHandler(Map<String, dynamic> message) {
//   return _showNotification(message);
// }

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  await Firebase.initializeApp();
  print('Handling a background message ${message.messageId}');
  print(message.data);
  // showOverlayNotification(
  //   (context) {
  //     return Container(
  //       color: ColorPalette().main,
  //       margin: EdgeInsets.only(top: 20),
  //       padding: EdgeInsets.only(bottom: 10),
  //       child: GestureDetector(
  //         onTap: () => Navigator.pushNamed(context, '/notifications'),
  //         child: Card(
  //           color: ColorPalette().main,
  //           margin: const EdgeInsets.symmetric(horizontal: 4),
  //           child: SafeArea(
  //             child: ListTile(
  //               leading: SizedBox.fromSize(
  //                   size: const Size(40, 40),
  //                   child: ClipOval(
  //                     child: Container(
  //                       child: Image.asset('assets/images/anellohlogo.png',
  //                           fit: BoxFit.cover),
  //                       width: mainSizer(context: context, size: 2),
  //                     ),
  //                   )),
  //               title: Text(
  //                 message.data['title'],
  //                 style: TextStyle(color: Colors.white),
  //               ),
  //               subtitle: Text(
  //                 message.data['body'],
  //                 maxLines: 3,
  //                 softWrap: false,
  //                 overflow: TextOverflow.ellipsis,
  //                 style: TextStyle(color: Colors.white),
  //               ),
  //               trailing: IconButton(
  //                   icon: Icon(
  //                     Icons.close,
  //                     color: Colors.white,
  //                   ),
  //                   onPressed: () {
  //                     OverlaySupportEntry.of(context).dismiss();
  //                   }),
  //             ),
  //           ),
  //         ),
  //       ),
  //     );
  //   },
  //   duration: Duration(seconds: 10),
  // );

  flutterLocalNotificationsPlugin.show(
      message.data.hashCode,
      message.data['title'],
      message.data['body'],
      NotificationDetails(
        android: AndroidNotificationDetails(
          channel.id,
          channel.name,
          channel.description,
        ),
      ));
}

const AndroidNotificationChannel channel = AndroidNotificationChannel(
  'high_importance_channel', // id
  'High Importance Notifications', // title
  'This channel is used for important notifications.', // description
  importance: Importance.high,
);

/// Initialize the [FlutterLocalNotificationsPlugin] package.
final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

// Future _showNotification(Map<String, dynamic> message) async {
//   var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
//     'channel id',
//     'channel name',
//     'channel desc',
//     importance: Importance.high,
//     priority: Priority.high,
//   );
//
//   var platformChannelSpecifics = new NotificationDetails(
//       android: androidPlatformChannelSpecifics, iOS: null);
//   await FlutterLocalNotificationsPlugin().show(
//     0,
//     message['notification']['title'],
//     message['notification']['body'],
//     platformChannelSpecifics,
//     payload: 'Default_Sound',
//   );
// }

// class PushNotificationService {
//   final FirebaseMessaging _fcm;
//
//   PushNotificationService(this._fcm);
//
//   Future initialise() async {
//     try {
//       var deviceToken;
//       if (Platform.isIOS) {
//         bool response = await _fcm
//             .requestNotificationPermissions(IosNotificationSettings());
//         if (response) {
//           deviceToken = await _fcm.getToken();
//           print('from fcm ios');
//           print(deviceToken);
//           await session.set('__device__token', deviceToken ?? null);
//         }
//       } else {
//         deviceToken = await _fcm.getToken();
//         print(deviceToken);
//         await session.set('__device__token', deviceToken ?? null);
//       }
//     } catch (err) {
//       print(err);
//     }
//
//     String token = await _fcm.getToken();
//     log("FirebaseMessaging token: $token");
//
//     _fcm.configure(
//       onBackgroundMessage: myBackgroundHandler,
//       onMessage: (message) async {
//         showOverlayNotification((context) {
//           return Container(
//             color: ColorPalette().main,
//             margin: EdgeInsets.only(top: 20),
//             padding: EdgeInsets.only(bottom: 10),
//             child: GestureDetector(
//               onTap: () => Navigator.pushNamed(context, '/notifications'),
//               child: Card(
//                 color: ColorPalette().main,
//                 margin: const EdgeInsets.symmetric(horizontal: 4),
//                 child: SafeArea(
//                   child: ListTile(
//                     leading: SizedBox.fromSize(
//                         size: const Size(40, 40),
//                         child: ClipOval(
//                           child: Container(
//                             child: Image.asset('assets/images/anellohlogo.png',
//                                 fit: BoxFit.cover),
//                             width: mainSizer(context: context, size: 2),
//                           ),
//                         )),
//                     title: Text(
//                       message['notification']['title'],
//                       style: TextStyle(color: Colors.white),
//                     ),
//                     subtitle: Text(
//                       message['notification']['body'].toString(),
//                       maxLines: 3,
//                       softWrap: false,
//                       overflow: TextOverflow.ellipsis,
//                       style: TextStyle(color: Colors.white),
//                     ),
//                     trailing: IconButton(
//                         icon: Icon(
//                           Icons.close,
//                           color: Colors.white,
//                         ),
//                         onPressed: () {
//                           OverlaySupportEntry.of(context).dismiss();
//                         }),
//                   ),
//                 ),
//               ),
//             ),
//           );
//         },
//             duration: Duration(milliseconds: 10000),
//             position: NotificationPosition.top);
//         print(message['notification']['title']);
//         print('on message cb');
//         print(message);
//       },
//       onLaunch: (message) async {
//         print('on launch message cb');
//         print(message);
//       },
//       onResume: (message) async {
//         print('on background message cb');
//         print(message);
//       },
//     );
//   }
// }
