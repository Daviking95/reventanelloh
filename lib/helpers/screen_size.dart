
import 'package:anelloh_mobile/helpers/orientation.dart';
import 'package:flutter/cupertino.dart';

double returnScreenSize(BuildContext context, double size){
  if(MediaQuery.of(context).size.height <= 568.0)
    return size;
  else if(MediaQuery.of(context).size.height > 568.0 && MediaQuery.of(context).size.height <= 667.0)
    return size * 1.3;
  else if(MediaQuery.of(context).size.height > 667.0 && MediaQuery.of(context).size.height <= 812.0)
    return size * 1.5;
  else if(MediaQuery.of(context).size.height == 896.0)
    return size * 1.9;
  else return size * 1.8;
}

double returnSpecificSize(BuildContext context, double deviceHeight, double sizeToReturn){
  if(MediaQuery.of(context).size.height == deviceHeight)
    return sizeToReturn;
  else
    return 0;
}

mainSizer({@required BuildContext context, int size}) {
  if (isPortrait(context)) {
    return MediaQuery.of(context).size.width / (size ?? 22);
  } else {
    return MediaQuery.of(context).size.height / (size ?? 22);
  }
}