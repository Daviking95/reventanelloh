import 'dart:developer';

import 'package:anelloh_mobile/UI/signup_screen.dart';
import 'package:anelloh_mobile/bloc/signup/signup_bloc.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DeepLinkController extends StatefulWidget {
  final Uri uri;
  final Widget child;

  DeepLinkController({Key key, this.uri, @required this.child})
      : super(key: key);

  @override
  _DeepLinkControllerState createState() => _DeepLinkControllerState();
}

class _DeepLinkControllerState extends State<DeepLinkController> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SchedulerBinding.instance.addPostFrameCallback((_) async {
      if (widget.uri != null) {
        var path = '';
        Map<String, String> query;
        if (widget.uri.hasQuery) {
          query = widget.uri.queryParameters;
          // log('queryParameters $query');
          // log('queryParameters ${query.runtimeType}');
        }
        path = widget.uri.path;
        if (path == '/register') {
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return BlocProvider<SignUpScreenBloc>(
              create: (context) {
                return SignUpScreenBloc();
              },
              child: WillPopScope(
                child: SignUpScreen(),
                onWillPop: () async {
                  return false;
                },
              ),
            );
          }));
        }
        if (path == '/login') {
          var email = query['email'];
          var token = query['token'];

          log('token $email ${email} $token');

          SharedPreferences _pref = await SharedPreferences.getInstance();
          await _pref.setString('_EMAIL_VERIFY_', "true");
          Navigator.of(context)
              .pushNamed('/login', arguments: {'email': email, 'token': token});
        }
        if (path == '/resetlink') {
          var email = query['email'];
          var token = query['token'];

          log('resetlinktoken $email ${email} $token');

          // SharedPreferences _pref = await SharedPreferences.getInstance();
          // await _pref.setString('_EMAIL_VERIFY_', "true");
          Navigator.of(context).pushNamed('/forgot_password',
              arguments: {'email': email, 'token': token});
        }
        if (path == '/forgot_password') {
          var email = query['email'];
          var token = query['token'];

          log('resetlinktoken $email ${email} $token');

          // SharedPreferences _pref = await SharedPreferences.getInstance();
          // await _pref.setString('_EMAIL_VERIFY_', "true");
          Navigator.of(context).pushNamed('/forgot_password',
              arguments: {'email': email, 'token': token});
        }
        if (path == '/') {
          return widget.child;
        }
      } else {
        return widget.child;
      }
    });
    return widget.child;
  }
}
