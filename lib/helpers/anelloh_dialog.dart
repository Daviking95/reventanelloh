import 'package:flutter/material.dart';

import '../styles.dart';

Future anellohDialog(context, {@required Widget child, bool canClick = true}) {
  return showDialog(
      context: context,
      builder: (BuildContext context) {
        return GestureDetector(
          onTap: () {
            if (canClick) {
              Navigator.of(context).pop();
            } else {
              return;
            }
          },
          child: Scaffold(
            resizeToAvoidBottomInset: true,
            backgroundColor: Colors.transparent,
            body: Center(
              child: Container(
                margin: EdgeInsets.all(13),
                child: child,
              ),
            ),
          ),
        );
      });
}

showWidgetDialog(
    {BuildContext context,
    String title,
    Widget widget,
    bool hasNextButton = false,
    Function nextFunction}) async {
  return showDialog<String>(
    context: context,
    barrierDismissible: false,
    builder: (BuildContext context) {
      return AlertDialog(
        scrollable: true,
        title: Text(
          title,
          style: textStyleBlackColorBold.copyWith(fontSize: 20.0),
          textAlign: TextAlign.center,
        ),
        content: widget,
        actions: <Widget>[
          FlatButton(
            child: Text('Close'),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          hasNextButton
              ? FlatButton(
                  child: Text(
                    "Proceed",
                    style: textStylePrimaryColorNormal,
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();

                    nextFunction();
                  },
                )
              : Container()
        ],
      );
    },
  );
}
