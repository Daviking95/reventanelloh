import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';

class CurrencyFormatter {
  static format({@required currencyCode, @required String amount}) {
    try {
      String value = '${NumberFormat().simpleCurrencySymbol(currencyCode.toString().toUpperCase()) +  amount.toString()}';
      return value;
    } catch (err) {
      print(err);
    }
  }
}
