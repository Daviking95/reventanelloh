import 'package:flutter/cupertino.dart';

String determineOrdeerHeaderTextString(
    {@required String matchedOrderStatus, @required String orderStatus}) {
  // print('matchedOrderStatus $matchedOrderStatus');
  // print('orderStatus $orderStatus');

  if (orderStatus == '1') {
    // print(orderStatus);
    switch (matchedOrderStatus) {
      case '1':
        return 'Pending Order';
        break;
      case '2':
        return 'Your order is processing';
        break;
      case '3':
        return 'Your order is processing';
        break;
      default:
        return 'Open Order';
    }
  }
  if (orderStatus == '2') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Your order is processing';
        break;
      case '2':
        return 'Fulfilled Order';
        break;
      default:
        return 'Your order is processing';
    }
  }

  if (orderStatus == '3') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Your order is processing';
        break;
      case '2':
        return 'Fulfilled Order';
        break;
      case '3':
        return 'Your order is processing';
        break;
      default:
        return 'Create Order';
    }
  }

  if (orderStatus == '4') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Create Order';
        break;
      default:
        return 'Create Order';
    }
  }

  if (orderStatus == '5') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Unfulfilled';
        break;
      default:
        return 'Unfulfilled';
    }
  }

  if (orderStatus == '6') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Refunded';
        break;
      default:
        return 'Create Order';
    }
  }

  if (orderStatus == '7') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Pending Confirmation';
        break;
      default:
        return 'Pending Confirmation';
    }
  }
}

String determineOrderBodyTextString(
    {@required String matchedOrderStatus, @required String orderStatus}) {
  if (orderStatus == '1') {
    switch (matchedOrderStatus) {
      case '1':
        return 'You have a pending order. Please, pay now to complete it';
        break;
      case '2':
        return 'You can continue swapping while your order is being completed';
        break;
      case '3':
        return 'You can continue swapping while your order is being completed';
        break;
      default:
        return 'You have an open order, you can edit this order or wait for a match';
    }
  }
  if (orderStatus == '2') {
    switch (matchedOrderStatus) {
      case '1':
        return 'You can continue swapping while your order is being completed';
        break;
      case '2':
        return 'Your last order was fulfilled';
        break;
      default:
        return 'You can continue swapping while your order is being completed';
    }
  }

  if (orderStatus == '3') {
    switch (matchedOrderStatus) {
      case '1':
        return 'You can continue swapping while your order is being completed';
        break;
      case '2':
        return 'Your last order was fulfilled';
        break;
      case '3':
        return 'You can continue swapping while your order is being completed';
        break;
      default:
        return 'Start here to swap currencies';
    }
  }

  if (orderStatus == '4') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Start here to swap currencies';
        break;
      default:
        return 'Start here to swap currencies';
    }
  }

  if (orderStatus == '5') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Your order was cancelled because you ran out of time.';
        break;
      default:
        return 'Your order was cancelled because you ran out of time.';
    }
  }

  if (orderStatus == '6') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Funds has been refunded for your last transaction';
        break;
      default:
        return 'Start here to swap currencies';
    }
  }

  if (orderStatus == '7') {
    switch (matchedOrderStatus) {
      case '1':
        return 'Your payment is pending confirmation';
        break;
      default:
        return 'Your payment is pending confirmation';
    }
  }
}
