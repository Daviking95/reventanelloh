
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

TextStyle googleFontTextStyle ({double fontSize = 12, Color color = const Color(0xff26474E), FontWeight fontWeight = FontWeight.normal}) {
  return GoogleFonts.montserrat(
    fontSize: fontSize,
    color: color,
    fontWeight: fontWeight,
  );
}