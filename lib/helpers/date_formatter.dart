import 'package:simple_moment/simple_moment.dart';

String formatDate(String dt) {
  if(dt != null) {
    Moment data = Moment.parse(dt);
    return Moment.now().from(data.date);
  }else {
    return '';
  }
}