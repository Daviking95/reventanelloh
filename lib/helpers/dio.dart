import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

import '../constants.dart';
import '../flavors.dart';

//https://dev-app-ip.eastus.cloudapp.azure.com:44312/api/
PrettyDioLogger logger = PrettyDioLogger(
    requestHeader: true,
    requestBody: true,
    responseBody: true,
    responseHeader: false,
    error: true,
    compact: true,
    maxWidth: 90);

Dio dio = new Dio();
void interceptor = dio.interceptors.add(logger);

// HTTP CLIENT
class HttpService {
  // final String baseUrl = 'https://api-poc.anelloh.com/api/'; // 'https://api-prod.anelloh.com/api/';  //

  Future<HttpClientResponse> get({
    @required String url,
    bool isTokenHeader = false,
    dynamic requestData,
  }) async {
    HttpClient client = new HttpClient();
    String token;
    if (await readFromPref('__Anelloh_MOBL_User_Session_token__')) {
      token = await getFromPref('__Anelloh_MOBL_User_Session_token__');
    }

    // token = AppConstants.Anelloh_MOBL_User_Session_token__;

    print("BaseUrl ${F.title} ${F.baseUrl}");

    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => false);
    HttpClientRequest request =
        await client.getUrl(Uri.parse('${F.baseUrl + url}'));
    request.headers.set('Content-Type', 'application/json');
    if (token != null) {
      request.headers.set('Authorization', 'Bearer $token');
    }
    if (requestData != null) {
      request.add(utf8.encode(jsonEncode(requestData)));
    }

    HttpClientResponse result = await request.close();
    return result;
  }

  Future<HttpClientResponse> post(
      {@required String url, @required dynamic requestData}) async {
    HttpClient client = new HttpClient();
    String token;
    if (await readFromPref('__Anelloh_MOBL_User_Session_token__')) {
      token = await getFromPref('__Anelloh_MOBL_User_Session_token__');
    }

    // token = AppConstants.Anelloh_MOBL_User_Session_token__;

    print("BaseUrl ${F.title} ${F.baseUrl}");

    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request =
        await client.postUrl(Uri.parse('${F.baseUrl + url}'));
    if (token != null) {
      request.headers.set('Authorization', 'Bearer $token');
    }
    request.headers.set('Content-Type', 'application/json');
    request.add(utf8.encode(jsonEncode(requestData)));
    HttpClientResponse result = await request.close();
    return result;
  }

  Future<HttpClientResponse> put(
      {@required String url, @required dynamic requestData}) async {
    HttpClient client = new HttpClient();
    String token;
    if (await readFromPref('__Anelloh_MOBL_User_Session_token__')) {
      token = await getFromPref('__Anelloh_MOBL_User_Session_token__');
    }

    // token = AppConstants.Anelloh_MOBL_User_Session_token__;

    print("BaseUrl ${F.title} ${F.baseUrl}");

    client.badCertificateCallback =
        ((X509Certificate cert, String host, int port) => true);
    HttpClientRequest request =
        await client.putUrl(Uri.parse('${F.baseUrl + url}'));
    if (token != null) {
      request.headers.set('Authorization', 'Bearer $token');
    }
    request.headers.set('Content-Type', 'application/json');
    request.add(utf8.encode(jsonEncode(requestData)));
    HttpClientResponse result = await request.close();
    return result;
  }
}
