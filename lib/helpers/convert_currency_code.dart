
import 'package:flutter/foundation.dart';

int convertCurrencyCode({@required String currencyCode}) {
  switch(currencyCode) {
    case 'NGN':
      return 1;
      break;
    case 'USD':
      return 2;
      break;
    case 'GBP':
      return 3;
      break;
    case 'EUR':
      return 4;
      break;
    case 'CAD':
      return 5;
    default:
      throw new Error();
      break;
  }
}