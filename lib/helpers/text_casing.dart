import 'package:flutter/material.dart';

class CustomTextCasing {
  static String toSentenceCase({@required String text}) {
    if (text.isNotEmpty) {
      var firstLetter = text[0].toUpperCase();
      var otherLetters = text.substring(1, text.length).toLowerCase();
      return '$firstLetter$otherLetters';
    } else {
      return '';
    }
  }
}
