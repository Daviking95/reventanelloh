import "package:flutter/material.dart";

bool isPortrait(context) {
  bool value = MediaQuery.of(context).size.height > MediaQuery.of(context).size.width;
  return value;
}