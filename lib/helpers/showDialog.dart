import 'dart:io' show Platform;

import 'package:anelloh_mobile/widgets/text_wid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'color_palette.dart';

displayAlert(
    {String title, String content, bool success, BuildContext context}) {
  Widget okButton = TextButton(
    child: Text("OK"),
    onPressed: () {
      Navigator.pop(context);
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: Text(title != null
        ? title
        : success
            ? 'Success'
            : 'Error'),
    content: Text(content),
    actions: [
      okButton,
    ],
  );

  // ios AlertDialog
  CupertinoAlertDialog alertIos = CupertinoAlertDialog(
    title: Text(title != null
        ? title
        : success
            ? 'Success'
            : 'Error'),
    content: Text(content),
    actions: [
      CupertinoDialogAction(
          onPressed: () {
            Navigator.pop(context);
          },
          isDefaultAction: true,
          child: new Text("OK"))
    ],
  );
  return showDialog(
    context: context,
    builder: (BuildContext context) {
      return Platform.isIOS ? alertIos : alert;
    },
  );
}

displayImagePickerDialog(
    BuildContext context, Function openCamera, Function openGallery) async {
  return showModalBottomSheet(
    context: context,
    isScrollControlled: true,
    backgroundColor: ColorPalette().textWhite,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
            topLeft: Radius.circular(30), topRight: Radius.circular(30))),
    builder: (context) => Container(
      height: Get.height / 3,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Expanded(
            child: InkWell(
              onTap: openCamera,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.camera,
                      size: 30,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomText(
                      title: "Open Camera",
                      textSize: 15.0,
                      textColor: Color(0xff263743),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Expanded(
            child: InkWell(
              onTap: openGallery,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.photo,
                      size: 30,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    CustomText(
                      title: "Open Gallery",
                      textSize: 15.0,
                      textColor: Color(0xff263743),
                    ),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    ),
  );
}
