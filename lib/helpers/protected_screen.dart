import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProtectedScreen extends StatefulWidget {
  final Widget child;

  ProtectedScreen({Key key, this.child}) : super(key: key);

  _ProtectedScreenState createState() => _ProtectedScreenState();
}

class _ProtectedScreenState extends State<ProtectedScreen> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: widget.child,
    );
  }
}
