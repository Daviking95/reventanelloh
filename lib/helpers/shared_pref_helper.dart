import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../constants.dart';

Future<bool> readFromPref(String key) async {
  try {
    final prefs = await SharedPreferences.getInstance();
    final value = prefs.get(key) ?? null;

    // print('readFromPref $value');

    return value == null || value == '' ? false : true;
  } catch (e) {
    return false;
  }
}

Future<dynamic> getFromPref(String key) async {
  try {
    final prefs = await SharedPreferences.getInstance();
    final value = json.decode(prefs.getString(key)) ?? null;

    // print('getFromPref $value');

    return value;
  } catch (e) {
    return null;
  }
}

Future<bool> saveToPref(String key, var value) async {
  try {
    if (value == null) return false;

    final prefs = await SharedPreferences.getInstance();
    prefs.setString(key, json.encode(value));

    // print('saveToPref $value');

    return true;
  } catch (e) {
    print('error $e');

    return false;
  }
}

Future<bool> fetchPrefToken() async {
  try {
    return await getFromPref(AppConstants.sharedPrefToken);
  } catch (e) {
    print('error $e');

    return null;
  }
}

Future<bool> clearPref() async {
  try {
    final prefs = await SharedPreferences.getInstance();
    prefs.clear();

    return true;
  } catch (e) {
    return false;
  }
}

Future<bool> removePref(String key) async {
  try {
    final prefs = await SharedPreferences.getInstance();
    prefs.remove(key);
    return true;
  } catch (e) {
    return false;
  }
}
