class ActivePaymentGateways {
  dynamic _id;
  String _name;
  String _description;
  String _currencyCode;
  dynamic _feeType;
  dynamic _feeRate;
  dynamic _minTransactionFee;
  dynamic _maxTransactionFee;
  bool _allowAutoReversal;
  dynamic _reversalFeeRate;
  dynamic _reversalTransactionFee;
  String _createdBy;
  String _createdDate;
  String _paymentChannelLogo;
  String _paymentChannelUrl;
  String _lastModifiedBy;
  String _lastModifiedDate;

  ActivePaymentGateways(
      {dynamic id,
      String name,
      String description,
      String currencyCode,
      dynamic feeType,
      dynamic feeRate,
      dynamic minTransactionFee,
      dynamic maxTransactionFee,
      bool allowAutoReversal,
      dynamic reversalFeeRate,
      dynamic reversalTransactionFee,
      String createdBy,
      String paymentChannelLogo,
      String paymentChannelUrl,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate}) {
    this._id = id;
    this._name = name;
    this._description = description;
    this._currencyCode = currencyCode;
    this._feeType = feeType;
    this._feeRate = feeRate;
    this._minTransactionFee = minTransactionFee;
    this._maxTransactionFee = maxTransactionFee;
    this._allowAutoReversal = allowAutoReversal;
    this._reversalFeeRate = reversalFeeRate;
    this._reversalTransactionFee = reversalTransactionFee;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._paymentChannelLogo = paymentChannelLogo;
    this._paymentChannelUrl = paymentChannelUrl;
  }

  dynamic get id => _id;
  set id(dynamic id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get description => _description;
  set description(String description) => _description = description;
  String get currencyCode => _currencyCode;
  set currencyCode(String currencyCode) => _currencyCode = currencyCode;
  dynamic get feeType => _feeType;
  set feeType(dynamic feeType) => _feeType = feeType;
  dynamic get feeRate => _feeRate;
  set feeRate(dynamic feeRate) => _feeRate = feeRate;
  dynamic get minTransactionFee => _minTransactionFee;
  set minTransactionFee(dynamic minTransactionFee) =>
      _minTransactionFee = minTransactionFee;
  dynamic get maxTransactionFee => _maxTransactionFee;
  set maxTransactionFee(dynamic maxTransactionFee) =>
      _maxTransactionFee = maxTransactionFee;
  bool get allowAutoReversal => _allowAutoReversal;
  set allowAutoReversal(bool allowAutoReversal) =>
      _allowAutoReversal = allowAutoReversal;
  dynamic get reversalFeeRate => _reversalFeeRate;
  set reversalFeeRate(dynamic reversalFeeRate) =>
      _reversalFeeRate = reversalFeeRate;
  dynamic get reversalTransactionFee => _reversalTransactionFee;
  set reversalTransactionFee(dynamic reversalTransactionFee) =>
      _reversalTransactionFee = reversalTransactionFee;
  String get createdBy => _createdBy;
  set createdBy(String createdBy) => _createdBy = createdBy;
  String get createdDate => _createdDate;
  set createdDate(String createdDate) => _createdDate = createdDate;
  String get lastModifiedBy => _lastModifiedBy;
  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;
  String get lastModifiedDate => _lastModifiedDate;
  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;
  String get paymentChannelLogo => _paymentChannelLogo;
  set paymentChannelLogo(String value) {
    _paymentChannelLogo = value;
  }

  String get paymentChannelUrl => _paymentChannelUrl;
  set paymentChannelUrl(String value) {
    _paymentChannelUrl = value;
  }

  ActivePaymentGateways.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _name = json['name'];
    _description = json['description'];
    _currencyCode = json['currencyCode'];
    _feeType = json['feeType'];
    _feeRate = json['feeRate'];
    _minTransactionFee = json['minTransactionFee'];
    _maxTransactionFee = json['maxTransactionFee'];
    _allowAutoReversal = json['allowAutoReversal'];
    _reversalFeeRate = json['reversalFeeRate'];
    _reversalTransactionFee = json['reversalTransactionFee'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _paymentChannelUrl = json['paymentChannelUrl'];
    _paymentChannelLogo = json['paymentChannelLogo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['name'] = this._name;
    data['description'] = this._description;
    data['currencyCode'] = this._currencyCode;
    data['feeType'] = this._feeType;
    data['feeRate'] = this._feeRate;
    data['minTransactionFee'] = this._minTransactionFee;
    data['maxTransactionFee'] = this._maxTransactionFee;
    data['allowAutoReversal'] = this._allowAutoReversal;
    data['reversalFeeRate'] = this._reversalFeeRate;
    data['reversalTransactionFee'] = this._reversalTransactionFee;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['paymentChannelLogo'] = this._paymentChannelLogo;
    data['paymentChannelUrl'] = this._paymentChannelUrl;
    return data;
  }
}
