
import 'package:flutter/cupertino.dart';

class PaymentSummaryOrderCard {
  final String customerId;
  final String orderNo;
  final String myAmount;
  final String totalAmount;
  final String myCurrency;
  final String rate;
  final String convertedCurrency;
  final String convertedAmount;
  final String transactionFee;

  PaymentSummaryOrderCard(
  {
    @required this.customerId,
    @required this.orderNo,
    @required this.myAmount,
    @required this.totalAmount,
    @required this.myCurrency,
    @required this.rate,
    @required this.convertedCurrency,
    @required this.convertedAmount,
    @required this.transactionFee,});
}