class OrderMatched {
  final String entityId;
  final dynamic myOrderId;
  final dynamic pairedOrderId;
  final dynamic matchedOrderStatus;
  final DateTime matchedDate;
  final DateTime fulfilledDate;
  final DateTime processedDate;
  final DateTime completedDate;
  final DateTime cancelledDate;
  final DateTime refundDate;
  final String name;
  final String createdBy;
  final DateTime createdDate;
  final String lastModifiedBy;
  final String status;

  OrderMatched(
      {this.entityId,
      this.myOrderId,
      this.pairedOrderId,
      this.matchedOrderStatus,
      this.matchedDate,
      this.fulfilledDate,
      this.processedDate,
      this.completedDate,
      this.cancelledDate,
      this.refundDate,
      this.name,
      this.createdBy,
      this.createdDate,
      this.lastModifiedBy,
      this.status});

  factory OrderMatched.fromJson(Map<String, dynamic> data) => OrderMatched(
        entityId: data['matchedOrder']['pairedOrder']['id'].toString(),
        status: data['matchedOrder']['pairedOrder']['status'].toString(),
        myOrderId: data['matchedOrder']['pairedOrder']['myOrderId'].toString(),
        matchedOrderStatus:
            data['matchedOrder']['pairedOrder']['orderStatus'].toString(),
        pairedOrderId:
            data['matchedOrder']['pairedOrder']['pairedOrderId'].toString(),
        createdBy: data['matchedOrder']['pairedOrder']['createdBy'],
        createdDate:
            DateTime.parse(data['matchedOrder']['pairedOrder']['createdDate']),
        matchedDate:
            DateTime.parse(data['matchedOrder']['pairedOrder']['createdDate']),
        name: data['matchedOrder']['pairedOrder']['userName'],
      );
}
