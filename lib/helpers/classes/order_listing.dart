import 'package:anelloh_mobile/helpers/classes/order_matched.dart';
import 'package:anelloh_mobile/widgets/line_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class OrderListing {
  final String orderId;
  final String customerId;
  final String orderNo;
  final String myCurrency;
  final String convertedCurrency;
  final dynamic myAmount;
  final dynamic convertedAmount;
  final dynamic exchangeRate;
  final String currencyNeededSymbol;
  final String currencyToSwapSymbol;
  final String userName;
  final String userEmail;
  final List<charts.Series<DateRateRow, DateTime>> rates;
  final bool isRateAscending;
  final dynamic ascDecValue;
  final String subTotal;
  final String processingFee;
  final String processingFeeCurrency;
  final dynamic totalAmount;
  final String myAccountNumber;
  final String myBankName;
  final String bankRouteNo;
  final dynamic myPaymentChannelId;
  final String myPaymentReferenceNo;
  final String myPaymentResponse;
  final String myPaymentError;
  final dynamic convertedPaymentChannelId;
  final String convertedPaymentReferenceNo;
  final String convertedPaymentResponse;
  final String convertedPaymentError;
  final dynamic orderStatus;
  final dynamic paymentStatus;
  final dynamic refundFee;
  final dynamic preferredRate;
  final dynamic marketRate;
  final dynamic rateChange;
  final dynamic percentageChange;
  final DateTime createdAt;
  final bool isMatched;
  final String matchedOrderStatus;
  final OrderMatched matchedOrder;

  OrderListing({
    this.orderId,
    this.customerId,
    this.isMatched,
    this.matchedOrder,
    this.matchedOrderStatus,
    this.orderNo,
    this.myCurrency,
    this.convertedCurrency,
    this.myAmount,
    this.convertedAmount,
    this.exchangeRate,
    this.currencyNeededSymbol,
    this.currencyToSwapSymbol,
    this.userName,
    this.userEmail,
    this.rates,
    this.isRateAscending,
    this.ascDecValue,
    this.subTotal,
    this.processingFee,
    this.totalAmount,
    this.myAccountNumber,
    this.myBankName,
    this.bankRouteNo,
    this.myPaymentChannelId,
    this.myPaymentReferenceNo,
    this.myPaymentResponse,
    this.myPaymentError,
    this.convertedPaymentChannelId,
    this.convertedPaymentReferenceNo,
    this.convertedPaymentResponse,
    this.convertedPaymentError,
    this.processingFeeCurrency,
    this.orderStatus,
    this.paymentStatus,
    this.refundFee,
    this.preferredRate,
    this.marketRate,
    this.rateChange,
    this.percentageChange,
    this.createdAt,
  });

  factory OrderListing.fromJson(Map<String, dynamic> data) {
    if (data['matchedOrder'] == null) {
      return OrderListing(
          orderId: data['order']['id'].toString(),
          isMatched: data['isMatched'] ?? false,
          orderNo: data['order']['orderNo'].toString(),
          customerId: data['order']['customerId'].toString(),
          myCurrency: data['order']['myCurrency'].toString(),
          convertedCurrency: data['order']['convertedCurrency'].toString(),
          convertedAmount: data['order']['convertedAmount'].toString(),
          exchangeRate: data['order']['rate'].toString(),
          ascDecValue: data['order']['rateChange'],
          subTotal: data['order']['myAmount'].toString(),
          myAmount: data['order']['myAmount'].toString(),
          processingFeeCurrency:
              data['order']['transactionFeeCurrency'].toString(),
          orderStatus: data['order']['orderStatus'].toString(),
          processingFee: data['order']['transactionFee'].toString(),
          totalAmount: data['order']['totalAmount'].toString(),
          paymentStatus: data['paymentStatus'].toString(),
          refundFee: data['order']['refundFee'],
          myAccountNumber: data['order']['myAccountNumber'].toString(),
          myBankName: data['order']['myBankName'].toString(),
          bankRouteNo: data['order']['bankRouteNo'].toString(),
          myPaymentChannelId: data['order']['myPaymentChannelId'].toString(),
          createdAt: DateTime.parse(data['order']['createdDate']));
    } else {
      return OrderListing(
          matchedOrder: OrderMatched.fromJson(data),
          orderId: data['order']['id'].toString(),
          isMatched: data['isMatched'] ?? false,
          orderNo: data['order']['orderNo'].toString(),
          customerId: data['order']['customerId'].toString(),
          myCurrency: data['order']['myCurrency'].toString(),
          convertedCurrency: data['order']['convertedCurrency'].toString(),
          convertedAmount: data['order']['convertedAmount'].toString(),
          exchangeRate: data['order']['rate'].toString(),
          ascDecValue: data['order']['rateChange'],
          subTotal: data['order']['myAmount'].toString(),
          myAmount: data['order']['myAmount'].toString(),
          processingFeeCurrency:
              data['order']['transactionFeeCurrency'].toString(),
          orderStatus: data['order']['orderStatus'].toString(),
          processingFee: data['order']['transactionFee'].toString(),
          totalAmount: data['order']['totalAmount'].toString(),
          paymentStatus: data['order']['paymentStatus'].toString(),
          refundFee: data['order']['refundFee'],
          myAccountNumber: data['order']['myAccountNumber'].toString(),
          myBankName: data['order']['myBankName'].toString(),
          bankRouteNo: data['order']['bankRouteNo'].toString(),
          myPaymentChannelId: data['order']['myPaymentChannelId'].toString(),
          createdAt: DateTime.parse(data['order']['createdDate']));
    }
  }
}

class OrderListing2 {
  Order _order;
  bool _isMatched;
  MatchedOrder _matchedOrder;

  OrderListing2({Order order, bool isMatched, MatchedOrder matchedOrder}) {
    this._order = order;
    this._isMatched = isMatched;
    this._matchedOrder = matchedOrder;
  }

  Order get order => _order;
  set order(Order order) => _order = order;
  bool get isMatched => _isMatched;
  set isMatched(bool isMatched) => _isMatched = isMatched;
  MatchedOrder get matchedOrder => _matchedOrder;
  set matchedOrder(MatchedOrder matchedOrder) => _matchedOrder = matchedOrder;

  OrderListing2.fromJson(Map<String, dynamic> json) {
    _order = json['order'] != null ? new Order.fromJson(json['order']) : null;
    _isMatched = json['isMatched'];
    _matchedOrder = json['matchedOrder'] != null
        ? new MatchedOrder.fromJson(json['matchedOrder'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this._order != null) {
      data['order'] = this._order.toJson();
    }
    data['isMatched'] = this._isMatched;
    if (this._matchedOrder != null) {
      data['matchedOrder'] = this._matchedOrder.toJson();
    }
    return data;
  }
}

class Order {
  String _customerId;
  String _userName;
  String _email;
  String _orderNo;
  dynamic _applicationType;
  String _receipt;
  dynamic _myAmount;
  dynamic _totalAmount;
  String _myCurrency;
  dynamic _rate;
  String _myAccountNumber;
  String _myBankName;
  String _bankRouteNo;
  dynamic _myPaymentChannelId;
  String _myPaymentReferenceNo;
  String _myPaymentResponse;
  String _myPaymentError;
  String _convertedCurrency;
  dynamic _convertedAmount;
  dynamic _convertedPaymentChannelId;
  String _convertedPaymentReferenceNo;
  String _convertedPaymentResponse;
  String _convertedPaymentError;
  dynamic _transactionFee;
  String _transactionFeeCurrency;
  dynamic _orderStatus;
  String _orderStatusDesc;
  dynamic _paymentStatus;
  String _paymentStatusDesc;
  bool _isReversed;
  dynamic _refundFee;
  Customer _customer;
  String _myPaymentChannel;
  String _convertedPaymentChannel;
  dynamic _id;
  String _name;
  String _createdBy;
  String _createdDate;
  String _lastModifiedBy;
  String _lastModifiedDate;
  dynamic _status;
  String _statusDesc;

  Order(
      {String customerId,
      String userName,
      String email,
      String orderNo,
      dynamic applicationType,
      String receipt,
      dynamic myAmount,
      dynamic totalAmount,
      String myCurrency,
      dynamic rate,
      String myAccountNumber,
      String myBankName,
      String bankRouteNo,
      dynamic myPaymentChannelId,
      String myPaymentReferenceNo,
      String myPaymentResponse,
      String myPaymentError,
      String convertedCurrency,
      dynamic convertedAmount,
      dynamic convertedPaymentChannelId,
      String convertedPaymentReferenceNo,
      String convertedPaymentResponse,
      String convertedPaymentError,
      dynamic transactionFee,
      String transactionFeeCurrency,
      dynamic orderStatus,
      String orderStatusDesc,
      dynamic paymentStatus,
      String paymentStatusDesc,
      bool isReversed,
      dynamic refundFee,
      Customer customer,
      String myPaymentChannel,
      String convertedPaymentChannel,
      dynamic id,
      String name,
      String createdBy,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate,
      dynamic status,
      String statusDesc}) {
    this._customerId = customerId;
    this._userName = userName;
    this._email = email;
    this._orderNo = orderNo;
    this._applicationType = applicationType;
    this._receipt = receipt;
    this._myAmount = myAmount;
    this._totalAmount = totalAmount;
    this._myCurrency = myCurrency;
    this._rate = rate;
    this._myAccountNumber = myAccountNumber;
    this._myBankName = myBankName;
    this._bankRouteNo = bankRouteNo;
    this._myPaymentChannelId = myPaymentChannelId;
    this._myPaymentReferenceNo = myPaymentReferenceNo;
    this._myPaymentResponse = myPaymentResponse;
    this._myPaymentError = myPaymentError;
    this._convertedCurrency = convertedCurrency;
    this._convertedAmount = convertedAmount;
    this._convertedPaymentChannelId = convertedPaymentChannelId;
    this._convertedPaymentReferenceNo = convertedPaymentReferenceNo;
    this._convertedPaymentResponse = convertedPaymentResponse;
    this._convertedPaymentError = convertedPaymentError;
    this._transactionFee = transactionFee;
    this._transactionFeeCurrency = transactionFeeCurrency;
    this._orderStatus = orderStatus;
    this._orderStatusDesc = orderStatusDesc;
    this._paymentStatus = paymentStatus;
    this._paymentStatusDesc = paymentStatusDesc;
    this._isReversed = isReversed;
    this._refundFee = refundFee;
    this._customer = customer;
    this._myPaymentChannel = myPaymentChannel;
    this._convertedPaymentChannel = convertedPaymentChannel;
    this._id = id;
    this._name = name;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._status = status;
    this._statusDesc = statusDesc;
  }

  String get customerId => _customerId;
  set customerId(String customerId) => _customerId = customerId;
  String get userName => _userName;
  set userName(String userName) => _userName = userName;
  String get email => _email;
  set email(String email) => _email = email;
  String get orderNo => _orderNo;
  set orderNo(String orderNo) => _orderNo = orderNo;
  dynamic get applicationType => _applicationType;
  set applicationType(dynamic applicationType) =>
      _applicationType = applicationType;
  String get receipt => _receipt;
  set receipt(String value) {
    _receipt = value;
  }

  dynamic get myAmount => _myAmount;
  set myAmount(dynamic myAmount) => _myAmount = myAmount;
  dynamic get totalAmount => _totalAmount;
  set totalAmount(dynamic totalAmount) => _totalAmount = totalAmount;
  String get myCurrency => _myCurrency;
  set myCurrency(String myCurrency) => _myCurrency = myCurrency;
  dynamic get rate => _rate;
  set rate(dynamic rate) => _rate = rate;
  String get myAccountNumber => _myAccountNumber;
  set myAccountNumber(String myAccountNumber) =>
      _myAccountNumber = myAccountNumber;
  String get myBankName => _myBankName;
  set myBankName(String myBankName) => _myBankName = myBankName;
  String get bankRouteNo => _bankRouteNo;
  set bankRouteNo(String bankRouteNo) => _bankRouteNo = bankRouteNo;
  dynamic get myPaymentChannelId => _myPaymentChannelId;
  set myPaymentChannelId(dynamic myPaymentChannelId) =>
      _myPaymentChannelId = myPaymentChannelId;
  String get myPaymentReferenceNo => _myPaymentReferenceNo;
  set myPaymentReferenceNo(String myPaymentReferenceNo) =>
      _myPaymentReferenceNo = myPaymentReferenceNo;
  String get myPaymentResponse => _myPaymentResponse;
  set myPaymentResponse(String myPaymentResponse) =>
      _myPaymentResponse = myPaymentResponse;
  String get myPaymentError => _myPaymentError;
  set myPaymentError(String myPaymentError) => _myPaymentError = myPaymentError;
  String get convertedCurrency => _convertedCurrency;
  set convertedCurrency(String convertedCurrency) =>
      _convertedCurrency = convertedCurrency;
  dynamic get convertedAmount => _convertedAmount;
  set convertedAmount(dynamic convertedAmount) =>
      _convertedAmount = convertedAmount;
  dynamic get convertedPaymentChannelId => _convertedPaymentChannelId;
  set convertedPaymentChannelId(dynamic convertedPaymentChannelId) =>
      _convertedPaymentChannelId = convertedPaymentChannelId;
  String get convertedPaymentReferenceNo => _convertedPaymentReferenceNo;
  set convertedPaymentReferenceNo(String convertedPaymentReferenceNo) =>
      _convertedPaymentReferenceNo = convertedPaymentReferenceNo;
  String get convertedPaymentResponse => _convertedPaymentResponse;
  set convertedPaymentResponse(String convertedPaymentResponse) =>
      _convertedPaymentResponse = convertedPaymentResponse;
  String get convertedPaymentError => _convertedPaymentError;
  set convertedPaymentError(String convertedPaymentError) =>
      _convertedPaymentError = convertedPaymentError;
  dynamic get transactionFee => _transactionFee;
  set transactionFee(dynamic transactionFee) =>
      _transactionFee = transactionFee;
  String get transactionFeeCurrency => _transactionFeeCurrency;
  set transactionFeeCurrency(String transactionFeeCurrency) =>
      _transactionFeeCurrency = transactionFeeCurrency;
  dynamic get orderStatus => _orderStatus;
  set orderStatus(dynamic orderStatus) => _orderStatus = orderStatus;
  String get orderStatusDesc => _orderStatusDesc;
  set orderStatusDesc(String orderStatusDesc) =>
      _orderStatusDesc = orderStatusDesc;
  dynamic get paymentStatus => _paymentStatus;
  set paymentStatus(dynamic paymentStatus) => _paymentStatus = paymentStatus;
  String get paymentStatusDesc => _paymentStatusDesc;
  set paymentStatusDesc(String paymentStatusDesc) =>
      _paymentStatusDesc = paymentStatusDesc;
  bool get isReversed => _isReversed;
  set isReversed(bool isReversed) => _isReversed = isReversed;
  dynamic get refundFee => _refundFee;
  set refundFee(dynamic refundFee) => _refundFee = refundFee;
  Customer get customer => _customer;
  set customer(Customer customer) => _customer = customer;
  String get myPaymentChannel => _myPaymentChannel;
  set myPaymentChannel(String myPaymentChannel) =>
      _myPaymentChannel = myPaymentChannel;
  String get convertedPaymentChannel => _convertedPaymentChannel;
  set convertedPaymentChannel(String convertedPaymentChannel) =>
      _convertedPaymentChannel = convertedPaymentChannel;
  dynamic get id => _id;
  set id(dynamic id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get createdBy => _createdBy;
  set createdBy(String createdBy) => _createdBy = createdBy;
  String get createdDate => _createdDate;
  set createdDate(String createdDate) => _createdDate = createdDate;
  String get lastModifiedBy => _lastModifiedBy;
  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;
  String get lastModifiedDate => _lastModifiedDate;
  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;
  dynamic get status => _status;
  set status(dynamic status) => _status = status;
  String get statusDesc => _statusDesc;
  set statusDesc(String statusDesc) => _statusDesc = statusDesc;

  Order.fromJson(Map<String, dynamic> json) {
    _customerId = json['customerId'];
    _userName = json['userName'];
    _email = json['email'];
    _orderNo = json['orderNo'];
    _applicationType = json['applicationType'];
    _receipt = json['receipt'];
    _myAmount = json['myAmount'];
    _totalAmount = json['totalAmount'];
    _myCurrency = json['myCurrency'];
    _rate = json['rate'];
    _myAccountNumber = json['myAccountNumber'];
    _myBankName = json['myBankName'];
    _bankRouteNo = json['bankRouteNo'];
    _myPaymentChannelId = json['myPaymentChannelId'];
    _myPaymentReferenceNo = json['myPaymentReferenceNo'];
    _myPaymentResponse = json['myPaymentResponse'];
    _myPaymentError = json['myPaymentError'];
    _convertedCurrency = json['convertedCurrency'];
    _convertedAmount = json['convertedAmount'];
    _convertedPaymentChannelId = json['convertedPaymentChannelId'];
    _convertedPaymentReferenceNo = json['convertedPaymentReferenceNo'];
    _convertedPaymentResponse = json['convertedPaymentResponse'];
    _convertedPaymentError = json['convertedPaymentError'];
    _transactionFee = json['transactionFee'];
    _transactionFeeCurrency = json['transactionFeeCurrency'];
    _orderStatus = json['orderStatus'];
    _orderStatusDesc = json['orderStatusDesc'];
    _paymentStatus = json['paymentStatus'];
    _paymentStatusDesc = json['paymentStatusDesc'];
    _isReversed = json['isReversed'];
    _refundFee = json['refundFee'];
    _customer =
    json['customer'] != null ? new Customer.fromJson(json['customer']) : null;
    _myPaymentChannel = json['myPaymentChannel'];
    _convertedPaymentChannel = json['convertedPaymentChannel'];
    _id = json['id'];
    _name = json['name'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _status = json['status'];
    _statusDesc = json['statusDesc'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customerId'] = this._customerId;
    data['userName'] = this._userName;
    data['email'] = this._email;
    data['orderNo'] = this._orderNo;
    data['applicationType'] = this._applicationType;
    data['receipt'] = this._receipt;
    data['myAmount'] = this._myAmount;
    data['totalAmount'] = this._totalAmount;
    data['myCurrency'] = this._myCurrency;
    data['rate'] = this._rate;
    data['myAccountNumber'] = this._myAccountNumber;
    data['myBankName'] = this._myBankName;
    data['bankRouteNo'] = this._bankRouteNo;
    data['myPaymentChannelId'] = this._myPaymentChannelId;
    data['myPaymentReferenceNo'] = this._myPaymentReferenceNo;
    data['myPaymentResponse'] = this._myPaymentResponse;
    data['myPaymentError'] = this._myPaymentError;
    data['convertedCurrency'] = this._convertedCurrency;
    data['convertedAmount'] = this._convertedAmount;
    data['convertedPaymentChannelId'] = this._convertedPaymentChannelId;
    data['convertedPaymentReferenceNo'] = this._convertedPaymentReferenceNo;
    data['convertedPaymentResponse'] = this._convertedPaymentResponse;
    data['convertedPaymentError'] = this._convertedPaymentError;
    data['transactionFee'] = this._transactionFee;
    data['transactionFeeCurrency'] = this._transactionFeeCurrency;
    data['orderStatus'] = this._orderStatus;
    data['orderStatusDesc'] = this._orderStatusDesc;
    data['paymentStatus'] = this._paymentStatus;
    data['paymentStatusDesc'] = this._paymentStatusDesc;
    data['isReversed'] = this._isReversed;
    data['refundFee'] = this._refundFee;
    if (this._customer != null) {
      data['customer'] = this._customer.toJson();
    }
    data['myPaymentChannel'] = this._myPaymentChannel;
    data['convertedPaymentChannel'] = this._convertedPaymentChannel;
    data['id'] = this._id;
    data['name'] = this._name;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['status'] = this._status;
    data['statusDesc'] = this._statusDesc;
    return data;
  }
}

class MatchedOrder {
  dynamic _myOrderId;
  Order _myOrder;
  dynamic _pairedOrderId;
  Order _pairedOrder;
  dynamic _matchedOrderStatus;
  String _matchedOrderStatusDesc;
  String _matchedDate;
  String _fulfilledDate;
  String _processedDate;
  String _completedDate;
  String _cancelledDate;
  String _refundDate;
  dynamic _applicationType;
  dynamic _id;
  String _name;
  String _createdBy;
  String _createdDate;
  String _lastModifiedBy;
  String _lastModifiedDate;
  dynamic _status;
  String _statusDesc;

  MatchedOrder(
      {dynamic myOrderId,
      Order myOrder,
      dynamic pairedOrderId,
      Order pairedOrder,
      dynamic matchedOrderStatus,
      String matchedOrderStatusDesc,
      String matchedDate,
      String fulfilledDate,
      String processedDate,
      String completedDate,
      String cancelledDate,
      String refundDate,
      dynamic applicationType,
      dynamic id,
      String name,
      String createdBy,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate,
      dynamic status,
      String statusDesc}) {
    this._myOrderId = myOrderId;
    this._myOrder = myOrder;
    this._pairedOrderId = pairedOrderId;
    this._pairedOrder = pairedOrder;
    this._matchedOrderStatus = matchedOrderStatus;
    this._matchedOrderStatusDesc = matchedOrderStatusDesc;
    this._matchedDate = matchedDate;
    this._fulfilledDate = fulfilledDate;
    this._processedDate = processedDate;
    this._completedDate = completedDate;
    this._cancelledDate = cancelledDate;
    this._refundDate = refundDate;
    this._applicationType = applicationType;
    this._id = id;
    this._name = name;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._status = status;
    this._statusDesc = statusDesc;
  }

  dynamic get myOrderId => _myOrderId;
  set myOrderId(dynamic myOrderId) => _myOrderId = myOrderId;
  Order get myOrder => _myOrder;
  set myOrder(Order myOrder) => _myOrder = myOrder;
  dynamic get pairedOrderId => _pairedOrderId;
  set pairedOrderId(dynamic pairedOrderId) => _pairedOrderId = pairedOrderId;
  Order get pairedOrder => _pairedOrder;
  set pairedOrder(Order pairedOrder) => _pairedOrder = pairedOrder;
  dynamic get matchedOrderStatus => _matchedOrderStatus;
  set matchedOrderStatus(dynamic matchedOrderStatus) =>
      _matchedOrderStatus = matchedOrderStatus;
  String get matchedOrderStatusDesc => _matchedOrderStatusDesc;
  set matchedOrderStatusDesc(String matchedOrderStatusDesc) =>
      _matchedOrderStatusDesc = matchedOrderStatusDesc;
  String get matchedDate => _matchedDate;
  set matchedDate(String matchedDate) => _matchedDate = matchedDate;
  String get fulfilledDate => _fulfilledDate;
  set fulfilledDate(String fulfilledDate) => _fulfilledDate = fulfilledDate;
  String get processedDate => _processedDate;
  set processedDate(String processedDate) => _processedDate = processedDate;
  String get completedDate => _completedDate;
  set completedDate(String completedDate) => _completedDate = completedDate;
  String get cancelledDate => _cancelledDate;
  set cancelledDate(String cancelledDate) => _cancelledDate = cancelledDate;
  String get refundDate => _refundDate;
  set refundDate(String refundDate) => _refundDate = refundDate;
  dynamic get applicationType => _applicationType;
  set applicationType(dynamic applicationType) =>
      _applicationType = applicationType;
  dynamic get id => _id;
  set id(dynamic id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get createdBy => _createdBy;
  set createdBy(String createdBy) => _createdBy = createdBy;
  String get createdDate => _createdDate;
  set createdDate(String createdDate) => _createdDate = createdDate;
  String get lastModifiedBy => _lastModifiedBy;
  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;
  String get lastModifiedDate => _lastModifiedDate;
  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;
  dynamic get status => _status;
  set status(dynamic status) => _status = status;
  String get statusDesc => _statusDesc;
  set statusDesc(String statusDesc) => _statusDesc = statusDesc;

  MatchedOrder.fromJson(Map<String, dynamic> json) {
    _myOrderId = json['myOrderId'];
    _myOrder =
        json['myOrder'] != null ? new Order.fromJson(json['myOrder']) : null;
    _pairedOrderId = json['pairedOrderId'];
    _pairedOrder = json['pairedOrder'] != null
        ? new Order.fromJson(json['pairedOrder'])
        : null;
    _matchedOrderStatus = json['matchedOrderStatus'];
    _matchedOrderStatusDesc = json['matchedOrderStatusDesc'];
    _matchedDate = json['matchedDate'];
    _fulfilledDate = json['fulfilledDate'];
    _processedDate = json['processedDate'];
    _completedDate = json['completedDate'];
    _cancelledDate = json['cancelledDate'];
    _refundDate = json['refundDate'];
    _applicationType = json['applicationType'];
    _id = json['id'];
    _name = json['name'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _status = json['status'];
    _statusDesc = json['statusDesc'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['myOrderId'] = this._myOrderId;
    if (this._myOrder != null) {
      data['myOrder'] = this._myOrder.toJson();
    }
    data['pairedOrderId'] = this._pairedOrderId;
    if (this._pairedOrder != null) {
      data['pairedOrder'] = this._pairedOrder.toJson();
    }
    data['matchedOrderStatus'] = this._matchedOrderStatus;
    data['matchedOrderStatusDesc'] = this._matchedOrderStatusDesc;
    data['matchedDate'] = this._matchedDate;
    data['fulfilledDate'] = this._fulfilledDate;
    data['processedDate'] = this._processedDate;
    data['completedDate'] = this._completedDate;
    data['cancelledDate'] = this._cancelledDate;
    data['refundDate'] = this._refundDate;
    data['applicationType'] = this._applicationType;
    data['id'] = this._id;
    data['name'] = this._name;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['status'] = this._status;
    data['statusDesc'] = this._statusDesc;
    return data;
  }
}

class Customer {
  String _customerId;
  String _firstName;
  String _middleName;
  String _lastName;
  String _name;
  dynamic _bvn;
  dynamic _gender;
  String _address;
  String _customerImageFileLocation;
  String _dateOfBirth;
  String _password;
  String _countryCode;
  dynamic _documentType;
  String _documentNumber;
  String _documentExpiryDate;
  String _documentImage;
  String _createdBy;
  String _createdDate;
  String _lastModifiedBy;
  String _lastModifiedDate;
  dynamic _status;
  String _deviceId;
  String _deviceNotificationId;
  String _ssn;
  bool _isVerified;
  bool _isLockedOut;
  dynamic _noOfPasswordRetries;
  String _lastAccessedDate;
  dynamic _userLoginCount;
  dynamic _applicationType;
  dynamic _pin;
  String _token;
  bool _isSystemCustomer;
  String _ngNBankCode;
  String _ngNBankName;
  String _ngNAccountNo;
  String _usDBankName;
  String _usDAccountNo;
  String _usDRoutingNumber;
  String _id;
  String _userName;
  String _normalizedUserName;
  String _email;
  String _normalizedEmail;
  bool _emailConfirmed;
  String _passwordHash;
  String _securityStamp;
  String _concurrencyStamp;
  String _phoneNumber;
  bool _phoneNumberConfirmed;
  bool _twoFactorEnabled;
  String _lockoutEnd;
  bool _lockoutEnabled;
  dynamic _accessFailedCount;

  Customer(
      {String customerId,
      String firstName,
      String middleName,
      String lastName,
      String name,
      dynamic bvn,
      dynamic gender,
      String address,
      String customerImageFileLocation,
      String dateOfBirth,
      String password,
      String countryCode,
      dynamic documentType,
      String documentNumber,
      String documentExpiryDate,
      String documentImage,
      String createdBy,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate,
      dynamic status,
      String deviceId,
      String deviceNotificationId,
      String ssn,
      bool isVerified,
      bool isLockedOut,
      dynamic noOfPasswordRetries,
      String lastAccessedDate,
      dynamic userLoginCount,
      dynamic applicationType,
      dynamic pin,
      String token,
      bool isSystemCustomer,
      String ngNBankCode,
      String ngNBankName,
      String ngNAccountNo,
      String usDBankName,
      String usDAccountNo,
      String usDRoutingNumber,
      String id,
      String userName,
      String normalizedUserName,
      String email,
      String normalizedEmail,
      bool emailConfirmed,
      String passwordHash,
      String securityStamp,
      String concurrencyStamp,
      String phoneNumber,
      bool phoneNumberConfirmed,
      bool twoFactorEnabled,
      String lockoutEnd,
      bool lockoutEnabled,
      dynamic accessFailedCount}) {
    this._customerId = customerId;
    this._firstName = firstName;
    this._middleName = middleName;
    this._lastName = lastName;
    this._name = name;
    this._bvn = bvn;
    this._gender = gender;
    this._address = address;
    this._customerImageFileLocation = customerImageFileLocation;
    this._dateOfBirth = dateOfBirth;
    this._password = password;
    this._countryCode = countryCode;
    this._documentType = documentType;
    this._documentNumber = documentNumber;
    this._documentExpiryDate = documentExpiryDate;
    this._documentImage = documentImage;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._status = status;
    this._deviceId = deviceId;
    this._deviceNotificationId = deviceNotificationId;
    this._ssn = ssn;
    this._isVerified = isVerified;
    this._isLockedOut = isLockedOut;
    this._noOfPasswordRetries = noOfPasswordRetries;
    this._lastAccessedDate = lastAccessedDate;
    this._userLoginCount = userLoginCount;
    this._applicationType = applicationType;
    this._pin = pin;
    this._token = token;
    this._isSystemCustomer = isSystemCustomer;
    this._ngNBankCode = ngNBankCode;
    this._ngNBankName = ngNBankName;
    this._ngNAccountNo = ngNAccountNo;
    this._usDBankName = usDBankName;
    this._usDAccountNo = usDAccountNo;
    this._usDRoutingNumber = usDRoutingNumber;
    this._id = id;
    this._userName = userName;
    this._normalizedUserName = normalizedUserName;
    this._email = email;
    this._normalizedEmail = normalizedEmail;
    this._emailConfirmed = emailConfirmed;
    this._passwordHash = passwordHash;
    this._securityStamp = securityStamp;
    this._concurrencyStamp = concurrencyStamp;
    this._phoneNumber = phoneNumber;
    this._phoneNumberConfirmed = phoneNumberConfirmed;
    this._twoFactorEnabled = twoFactorEnabled;
    this._lockoutEnd = lockoutEnd;
    this._lockoutEnabled = lockoutEnabled;
    this._accessFailedCount = accessFailedCount;
  }

  String get customerId => _customerId;
  set customerId(String customerId) => _customerId = customerId;
  String get firstName => _firstName;
  set firstName(String firstName) => _firstName = firstName;
  String get middleName => _middleName;
  set middleName(String middleName) => _middleName = middleName;
  String get lastName => _lastName;
  set lastName(String lastName) => _lastName = lastName;
  String get name => _name;
  set name(String name) => _name = name;
  dynamic get bvn => _bvn;
  set bvn(dynamic bvn) => _bvn = bvn;
  dynamic get gender => _gender;
  set gender(dynamic gender) => _gender = gender;
  String get address => _address;
  set address(String address) => _address = address;
  String get customerImageFileLocation => _customerImageFileLocation;
  set customerImageFileLocation(String customerImageFileLocation) =>
      _customerImageFileLocation = customerImageFileLocation;
  String get dateOfBirth => _dateOfBirth;
  set dateOfBirth(String dateOfBirth) => _dateOfBirth = dateOfBirth;
  String get password => _password;
  set password(String password) => _password = password;
  String get countryCode => _countryCode;
  set countryCode(String countryCode) => _countryCode = countryCode;
  dynamic get documentType => _documentType;
  set documentType(dynamic documentType) => _documentType = documentType;
  String get documentNumber => _documentNumber;
  set documentNumber(String documentNumber) => _documentNumber = documentNumber;
  String get documentExpiryDate => _documentExpiryDate;
  set documentExpiryDate(String documentExpiryDate) =>
      _documentExpiryDate = documentExpiryDate;
  String get documentImage => _documentImage;
  set documentImage(String documentImage) => _documentImage = documentImage;
  String get createdBy => _createdBy;
  set createdBy(String createdBy) => _createdBy = createdBy;
  String get createdDate => _createdDate;
  set createdDate(String createdDate) => _createdDate = createdDate;
  String get lastModifiedBy => _lastModifiedBy;
  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;
  String get lastModifiedDate => _lastModifiedDate;
  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;
  dynamic get status => _status;
  set status(dynamic status) => _status = status;
  String get deviceId => _deviceId;
  set deviceId(String deviceId) => _deviceId = deviceId;
  String get deviceNotificationId => _deviceNotificationId;
  set deviceNotificationId(String deviceNotificationId) =>
      _deviceNotificationId = deviceNotificationId;
  String get ssn => _ssn;
  set ssn(String ssn) => _ssn = ssn;
  bool get isVerified => _isVerified;
  set isVerified(bool isVerified) => _isVerified = isVerified;
  bool get isLockedOut => _isLockedOut;
  set isLockedOut(bool isLockedOut) => _isLockedOut = isLockedOut;
  dynamic get noOfPasswordRetries => _noOfPasswordRetries;
  set noOfPasswordRetries(dynamic noOfPasswordRetries) =>
      _noOfPasswordRetries = noOfPasswordRetries;
  String get lastAccessedDate => _lastAccessedDate;
  set lastAccessedDate(String lastAccessedDate) =>
      _lastAccessedDate = lastAccessedDate;
  dynamic get userLoginCount => _userLoginCount;
  set userLoginCount(dynamic userLoginCount) =>
      _userLoginCount = userLoginCount;
  dynamic get applicationType => _applicationType;
  set applicationType(dynamic applicationType) =>
      _applicationType = applicationType;
  dynamic get pin => _pin;
  set pin(int pin) => _pin = pin;
  String get token => _token;
  set token(String token) => _token = token;
  bool get isSystemCustomer => _isSystemCustomer;
  set isSystemCustomer(bool isSystemCustomer) =>
      _isSystemCustomer = isSystemCustomer;
  String get ngNBankCode => _ngNBankCode;
  set ngNBankCode(String ngNBankCode) => _ngNBankCode = ngNBankCode;
  String get ngNBankName => _ngNBankName;
  set ngNBankName(String ngNBankName) => _ngNBankName = ngNBankName;
  String get ngNAccountNo => _ngNAccountNo;
  set ngNAccountNo(String ngNAccountNo) => _ngNAccountNo = ngNAccountNo;
  String get usDBankName => _usDBankName;
  set usDBankName(String usDBankName) => _usDBankName = usDBankName;
  String get usDAccountNo => _usDAccountNo;
  set usDAccountNo(String usDAccountNo) => _usDAccountNo = usDAccountNo;
  String get usDRoutingNumber => _usDRoutingNumber;
  set usDRoutingNumber(String usDRoutingNumber) =>
      _usDRoutingNumber = usDRoutingNumber;
  String get id => _id;
  set id(String id) => _id = id;
  String get userName => _userName;
  set userName(String userName) => _userName = userName;
  String get normalizedUserName => _normalizedUserName;
  set normalizedUserName(String normalizedUserName) =>
      _normalizedUserName = normalizedUserName;
  String get email => _email;
  set email(String email) => _email = email;
  String get normalizedEmail => _normalizedEmail;
  set normalizedEmail(String normalizedEmail) =>
      _normalizedEmail = normalizedEmail;
  bool get emailConfirmed => _emailConfirmed;
  set emailConfirmed(bool emailConfirmed) => _emailConfirmed = emailConfirmed;
  String get passwordHash => _passwordHash;
  set passwordHash(String passwordHash) => _passwordHash = passwordHash;
  String get securityStamp => _securityStamp;
  set securityStamp(String securityStamp) => _securityStamp = securityStamp;
  String get concurrencyStamp => _concurrencyStamp;
  set concurrencyStamp(String concurrencyStamp) =>
      _concurrencyStamp = concurrencyStamp;
  String get phoneNumber => _phoneNumber;
  set phoneNumber(String phoneNumber) => _phoneNumber = phoneNumber;
  bool get phoneNumberConfirmed => _phoneNumberConfirmed;
  set phoneNumberConfirmed(bool phoneNumberConfirmed) =>
      _phoneNumberConfirmed = phoneNumberConfirmed;
  bool get twoFactorEnabled => _twoFactorEnabled;
  set twoFactorEnabled(bool twoFactorEnabled) =>
      _twoFactorEnabled = twoFactorEnabled;
  String get lockoutEnd => _lockoutEnd;
  set lockoutEnd(String lockoutEnd) => _lockoutEnd = lockoutEnd;
  bool get lockoutEnabled => _lockoutEnabled;
  set lockoutEnabled(bool lockoutEnabled) => _lockoutEnabled = lockoutEnabled;
  int get accessFailedCount => _accessFailedCount;
  set accessFailedCount(int accessFailedCount) =>
      _accessFailedCount = accessFailedCount;

  Customer.fromJson(Map<String, dynamic> json) {
    _customerId = json['customerId'];
    _firstName = json['firstName'];
    _middleName = json['middleName'];
    _lastName = json['lastName'];
    _name = json['name'];
    _bvn = json['bvn'];
    _gender = json['gender'];
    _address = json['address'];
    _customerImageFileLocation = json['customerImageFileLocation'];
    _dateOfBirth = json['dateOfBirth'];
    _password = json['password'];
    _countryCode = json['countryCode'];
    _documentType = json['documentType'];
    _documentNumber = json['documentNumber'];
    _documentExpiryDate = json['documentExpiryDate'];
    _documentImage = json['documentImage'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _status = json['status'];
    _deviceId = json['deviceId'];
    _deviceNotificationId = json['deviceNotificationId'];
    _ssn = json['ssn'];
    _isVerified = json['isVerified'];
    _isLockedOut = json['isLockedOut'];
    _noOfPasswordRetries = json['noOfPasswordRetries'];
    _lastAccessedDate = json['lastAccessedDate'];
    _userLoginCount = json['userLoginCount'];
    _applicationType = json['applicationType'];
    _pin = json['pin'];
    _token = json['token'];
    _isSystemCustomer = json['isSystemCustomer'];
    _ngNBankCode = json['ngN_BankCode'];
    _ngNBankName = json['ngN_BankName'];
    _ngNAccountNo = json['ngN_AccountNo'];
    _usDBankName = json['usD_BankName'];
    _usDAccountNo = json['usD_AccountNo'];
    _usDRoutingNumber = json['usD_RoutingNumber'];
    _id = json['id'];
    _userName = json['userName'];
    _normalizedUserName = json['normalizedUserName'];
    _email = json['email'];
    _normalizedEmail = json['normalizedEmail'];
    _emailConfirmed = json['emailConfirmed'];
    _passwordHash = json['passwordHash'];
    _securityStamp = json['securityStamp'];
    _concurrencyStamp = json['concurrencyStamp'];
    _phoneNumber = json['phoneNumber'];
    _phoneNumberConfirmed = json['phoneNumberConfirmed'];
    _twoFactorEnabled = json['twoFactorEnabled'];
    _lockoutEnd = json['lockoutEnd'];
    _lockoutEnabled = json['lockoutEnabled'];
    _accessFailedCount = json['accessFailedCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customerId'] = this._customerId;
    data['firstName'] = this._firstName;
    data['middleName'] = this._middleName;
    data['lastName'] = this._lastName;
    data['name'] = this._name;
    data['bvn'] = this._bvn;
    data['gender'] = this._gender;
    data['address'] = this._address;
    data['customerImageFileLocation'] = this._customerImageFileLocation;
    data['dateOfBirth'] = this._dateOfBirth;
    data['password'] = this._password;
    data['countryCode'] = this._countryCode;
    data['documentType'] = this._documentType;
    data['documentNumber'] = this._documentNumber;
    data['documentExpiryDate'] = this._documentExpiryDate;
    data['documentImage'] = this._documentImage;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['status'] = this._status;
    data['deviceId'] = this._deviceId;
    data['deviceNotificationId'] = this._deviceNotificationId;
    data['ssn'] = this._ssn;
    data['isVerified'] = this._isVerified;
    data['isLockedOut'] = this._isLockedOut;
    data['noOfPasswordRetries'] = this._noOfPasswordRetries;
    data['lastAccessedDate'] = this._lastAccessedDate;
    data['userLoginCount'] = this._userLoginCount;
    data['applicationType'] = this._applicationType;
    data['pin'] = this._pin;
    data['token'] = this._token;
    data['isSystemCustomer'] = this._isSystemCustomer;
    data['ngN_BankCode'] = this._ngNBankCode;
    data['ngN_BankName'] = this._ngNBankName;
    data['ngN_AccountNo'] = this._ngNAccountNo;
    data['usD_BankName'] = this._usDBankName;
    data['usD_AccountNo'] = this._usDAccountNo;
    data['usD_RoutingNumber'] = this._usDRoutingNumber;
    data['id'] = this._id;
    data['userName'] = this._userName;
    data['normalizedUserName'] = this._normalizedUserName;
    data['email'] = this._email;
    data['normalizedEmail'] = this._normalizedEmail;
    data['emailConfirmed'] = this._emailConfirmed;
    data['passwordHash'] = this._passwordHash;
    data['securityStamp'] = this._securityStamp;
    data['concurrencyStamp'] = this._concurrencyStamp;
    data['phoneNumber'] = this._phoneNumber;
    data['phoneNumberConfirmed'] = this._phoneNumberConfirmed;
    data['twoFactorEnabled'] = this._twoFactorEnabled;
    data['lockoutEnd'] = this._lockoutEnd;
    data['lockoutEnabled'] = this._lockoutEnabled;
    data['accessFailedCount'] = this._accessFailedCount;
    return data;
  }
}
