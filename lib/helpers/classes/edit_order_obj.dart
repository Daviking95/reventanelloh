// import 'package:flutter/material.dart';
//
// class EditOrderObj {
//   final String customerId;
//   final dynamic orderId;
//   final dynamic myAmount;
//   final dynamic myCurrency;
//   final dynamic rate;
//   final dynamic convertedAmount;
//   final dynamic convertedCurrency;
//   final String myAccountNumber;
//   final String myBankName;
//   final String bankRouteNo;
//   final dynamic myPaymentChannelId;
//   final dynamic transactionFee;
//   final dynamic paymentStatus;
//   final dynamic loggedInDevice;
//   final String paymentReference;
//   final dynamic receipt;
//
//   EditOrderObj(
//       {@required this.customerId,
//       @required this.orderId,
//       @required this.myAmount,
//       @required this.myCurrency,
//       @required this.rate,
//       @required this.convertedAmount,
//       @required this.convertedCurrency,
//       this.myAccountNumber,
//       this.myBankName,
//       this.bankRouteNo,
//       this.myPaymentChannelId,
//       this.transactionFee,
//       this.paymentStatus,
//       this.receipt,
//       this.loggedInDevice,
//       this.paymentReference});
// }

class EditOrderObj {
  String _customerId;
  dynamic _orderId;
  dynamic _myAmount;
  String _myCurrency;
  dynamic _rate;
  dynamic _convertedAmount;
  String _convertedCurrency;
  String _myAccountNumber;
  String _myBankName;
  String _bankRouteNo;
  dynamic _myPaymentChannelId;
  dynamic _transactionFee;
  dynamic _paymentStatus;
  String _receipt;
  String _loggedInDevice;
  String _paymentReference;
  String _address;
  String _city;
  String _state;
  String _zipCode;
  String _country;
  String _orderNo;

  EditOrderObj(
      {String customerId,
      dynamic orderId,
      dynamic myAmount,
      String myCurrency,
      dynamic rate,
      dynamic convertedAmount,
      String convertedCurrency,
      String myAccountNumber,
      String myBankName,
      String bankRouteNo,
      dynamic myPaymentChannelId,
      dynamic transactionFee,
      dynamic paymentStatus,
      String receipt,
      String loggedInDevice,
      String address,
      String city,
      String state,
      String zipCode,
      String country,
      String paymentReference,
      String orderNo}) {
    this._customerId = customerId;
    this._orderId = orderId;
    this._myAmount = myAmount;
    this._myCurrency = myCurrency;
    this._rate = rate;
    this._convertedAmount = convertedAmount;
    this._convertedCurrency = convertedCurrency;
    this._myAccountNumber = myAccountNumber;
    this._myBankName = myBankName;
    this._bankRouteNo = bankRouteNo;
    this._myPaymentChannelId = myPaymentChannelId;
    this._transactionFee = transactionFee;
    this._paymentStatus = paymentStatus;
    this._receipt = receipt;
    this._loggedInDevice = loggedInDevice;
    this._paymentReference = paymentReference;
    this._address = address;
    this._city = city;
    this._state = state;
    this._zipCode = zipCode;
    this._country = country;
    this._orderNo = orderNo;
  }

  String get customerId => _customerId;
  set customerId(String customerId) => _customerId = customerId;
  dynamic get orderId => _orderId;
  set orderId(dynamic orderId) => _orderId = orderId;
  dynamic get myAmount => _myAmount;
  set myAmount(dynamic myAmount) => _myAmount = myAmount;
  String get myCurrency => _myCurrency;
  set myCurrency(String myCurrency) => _myCurrency = myCurrency;
  dynamic get rate => _rate;
  set rate(dynamic rate) => _rate = rate;
  dynamic get convertedAmount => _convertedAmount;
  set convertedAmount(dynamic convertedAmount) =>
      _convertedAmount = convertedAmount;
  String get convertedCurrency => _convertedCurrency;
  set convertedCurrency(String convertedCurrency) =>
      _convertedCurrency = convertedCurrency;
  String get myAccountNumber => _myAccountNumber;
  set myAccountNumber(String myAccountNumber) =>
      _myAccountNumber = myAccountNumber;
  String get myBankName => _myBankName;
  set myBankName(String myBankName) => _myBankName = myBankName;
  String get bankRouteNo => _bankRouteNo;
  set bankRouteNo(String bankRouteNo) => _bankRouteNo = bankRouteNo;
  dynamic get myPaymentChannelId => _myPaymentChannelId;
  set myPaymentChannelId(dynamic myPaymentChannelId) =>
      _myPaymentChannelId = myPaymentChannelId;
  dynamic get transactionFee => _transactionFee;
  set transactionFee(dynamic transactionFee) =>
      _transactionFee = transactionFee;
  dynamic get paymentStatus => _paymentStatus;
  set paymentStatus(dynamic paymentStatus) => _paymentStatus = paymentStatus;
  String get receipt => _receipt;
  set receipt(String receipt) => _receipt = receipt;
  String get loggedInDevice => _loggedInDevice;
  set loggedInDevice(String loggedInDevice) => _loggedInDevice = loggedInDevice;
  String get paymentReference => _paymentReference;
  set paymentReference(String paymentReference) =>
      _paymentReference = paymentReference;
  String get address => _address;
  set address(String value) {
    _address = value;
  }

  String get city => _city;
  set city(String value) {
    _city = value;
  }

  String get state => _state;
  set state(String value) {
    _state = value;
  }

  String get zipCode => _zipCode;
  set zipCode(String value) {
    _zipCode = value;
  }

  String get country => _country;
  set country(String value) {
    _country = value;
  }

  String get orderNo => _orderNo;

  set orderNo(String value) {
    _orderNo = value;
  }

  EditOrderObj.fromJson(Map<String, dynamic> json) {
    _customerId = json['customerId'];
    _orderId = json['orderId'];
    _myAmount = json['myAmount'];
    _myCurrency = json['myCurrency'];
    _rate = json['rate'];
    _convertedAmount = json['convertedAmount'];
    _convertedCurrency = json['convertedCurrency'];
    _myAccountNumber = json['myAccountNumber'];
    _myBankName = json['myBankName'];
    _bankRouteNo = json['bankRouteNo'];
    _myPaymentChannelId = json['myPaymentChannelId'];
    _transactionFee = json['transactionFee'];
    _paymentStatus = json['paymentStatus'];
    _receipt = json['receipt'];
    _loggedInDevice = json['loggedInDevice'];
    _paymentReference = json['paymentReference'];
    _address = json['address'];
    _city = json['city'];
    _state = json['state'];
    _zipCode = json['zipCode'];
    _country = json['country'];
    _orderNo = json['orderNo'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customerId'] = this._customerId;
    data['orderId'] = this._orderId;
    data['myAmount'] = this._myAmount;
    data['myCurrency'] = this._myCurrency;
    data['rate'] = this._rate;
    data['convertedAmount'] = this._convertedAmount;
    data['convertedCurrency'] = this._convertedCurrency;
    data['myAccountNumber'] = this._myAccountNumber;
    data['myBankName'] = this._myBankName;
    data['bankRouteNo'] = this._bankRouteNo;
    data['myPaymentChannelId'] = this._myPaymentChannelId;
    data['transactionFee'] = this._transactionFee;
    data['paymentStatus'] = this._paymentStatus;
    data['receipt'] = this._receipt;
    data['loggedInDevice'] = this._loggedInDevice;
    data['paymentReference'] = this._paymentReference;
    data['address'] = this._address;
    data['city'] = this._city;
    data['state'] = this._state;
    data['zipCode'] = this._zipCode;
    data['country'] = this._country;
    data['orderNo'] = this._orderNo;
    return data;
  }
}
