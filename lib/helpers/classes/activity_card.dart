import 'package:flutter/cupertino.dart';

class ActivityCardItem {
  final String orderId;
  final String orderNo;
  final String customerId;
  final String rate;
  final String myAmount;
  final String myCurrency;
  final String convertedAmount;
  final String totalAmount;
  final String convertedCurrency;
  final String status;
  final String matchedOrderStatus;
  final String convertedPaymentChannelId;
  final String convertedPaymentReferenceNo;
  final String convertedPaymentResponse;
  final String convertedPaymentError;
  final String transactionFee;
  final String transactionFeeCurrency;
  final String orderStatus;
  final String createdTime;
  final String userName;
  final String myAccountNumber;
  final String myBankName;
  final String bankRouteNo;
  final String myPaymentChannelId;
  final String paymentStatus;

  ActivityCardItem({
    @required this.orderId,
    @required this.orderNo,
    @required this.customerId,
    @required this.rate,
    @required this.myAmount,
    @required this.myCurrency,
    @required this.convertedAmount,
    @required this.totalAmount,
    @required this.matchedOrderStatus,
    @required this.convertedCurrency,
    @required this.status,
    @required this.convertedPaymentChannelId,
    @required this.convertedPaymentReferenceNo,
    @required this.convertedPaymentResponse,
    @required this.convertedPaymentError,
    @required this.transactionFee,
    @required this.transactionFeeCurrency,
    @required this.orderStatus,
    @required this.createdTime,
    @required this.userName,
    @required this.myAccountNumber,
    @required this.myBankName,
    @required this.bankRouteNo,
    @required this.myPaymentChannelId,
    @required this.paymentStatus,
  });
}
