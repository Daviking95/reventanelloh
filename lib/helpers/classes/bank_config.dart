class BankConfig {
  bool _succeeded;
  BankConfigEntity _entity;

  BankConfig({bool succeeded, BankConfigEntity entity}) {
    this._succeeded = succeeded;
    this._entity = entity;
  }

  bool get succeeded => _succeeded;
  set succeeded(bool succeeded) => _succeeded = succeeded;
  BankConfigEntity get entity => _entity;
  set entity(BankConfigEntity entity) => _entity = entity;

  BankConfig.fromJson(Map<String, dynamic> json) {
    _succeeded = json['succeeded'];
    _entity = json['entity'] != null
        ? new BankConfigEntity.fromJson(json['entity'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['succeeded'] = this._succeeded;
    if (this._entity != null) {
      data['entity'] = this._entity.toJson();
    }
    return data;
  }
}

class BankConfigEntity {
  int _id;
  String _accountNumber;
  String _bankCode;
  String _bankName;
  String _bankRouteNo;
  String _email;
  int _currencyCode;
  int _paymentChannelId;
  String _address;
  String _city;
  String _state;
  String _zipCode;
  String _country;
  int _status;
  String _statusDesc;

  BankConfigEntity(
      {int id,
      String accountNumber,
      String bankCode,
      String bankName,
      String bankRouteNo,
      String email,
      int currencyCode,
      int paymentChannelId,
      String address,
      String city,
      String state,
      String zipCode,
      String country,
      int status,
      String statusDesc}) {
    this._id = id;
    this._accountNumber = accountNumber;
    this._bankCode = bankCode;
    this._bankName = bankName;
    this._bankRouteNo = bankRouteNo;
    this._email = email;
    this._currencyCode = currencyCode;
    this._paymentChannelId = paymentChannelId;
    this._address = address;
    this._city = city;
    this._state = state;
    this._zipCode = zipCode;
    this._country = country;
    this._status = status;
    this._statusDesc = statusDesc;
  }

  int get id => _id;
  set id(int id) => _id = id;
  String get accountNumber => _accountNumber;
  set accountNumber(String accountNumber) => _accountNumber = accountNumber;
  String get bankCode => _bankCode;
  set bankCode(String bankCode) => _bankCode = bankCode;
  String get bankName => _bankName;
  set bankName(String bankName) => _bankName = bankName;
  String get bankRouteNo => _bankRouteNo;
  set bankRouteNo(String bankRouteNo) => _bankRouteNo = bankRouteNo;
  String get email => _email;
  set email(String email) => _email = email;
  int get currencyCode => _currencyCode;
  set currencyCode(int currencyCode) => _currencyCode = currencyCode;
  int get paymentChannelId => _paymentChannelId;
  set paymentChannelId(int paymentChannelId) =>
      _paymentChannelId = paymentChannelId;
  String get address => _address;
  set address(String address) => _address = address;
  String get city => _city;
  set city(String city) => _city = city;
  String get state => _state;
  set state(String state) => _state = state;
  String get zipCode => _zipCode;
  set zipCode(String zipCode) => _zipCode = zipCode;
  String get country => _country;
  set country(String country) => _country = country;
  int get status => _status;
  set status(int status) => _status = status;
  String get statusDesc => _statusDesc;
  set statusDesc(String statusDesc) => _statusDesc = statusDesc;

  BankConfigEntity.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _accountNumber = json['accountNumber'];
    _bankCode = json['bankCode'];
    _bankName = json['bankName'];
    _bankRouteNo = json['bankRouteNo'];
    _email = json['email'];
    _currencyCode = json['currencyCode'];
    _paymentChannelId = json['paymentChannelId'];
    _address = json['address'];
    _city = json['city'];
    _state = json['state'];
    _zipCode = json['zipCode'];
    _country = json['country'];
    _status = json['status'];
    _statusDesc = json['statusDesc'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['accountNumber'] = this._accountNumber;
    data['bankCode'] = this._bankCode;
    data['bankName'] = this._bankName;
    data['bankRouteNo'] = this._bankRouteNo;
    data['email'] = this._email;
    data['currencyCode'] = this._currencyCode;
    data['paymentChannelId'] = this._paymentChannelId;
    data['address'] = this._address;
    data['city'] = this._city;
    data['state'] = this._state;
    data['zipCode'] = this._zipCode;
    data['country'] = this._country;
    data['status'] = this._status;
    data['statusDesc'] = this._statusDesc;
    return data;
  }
}
