import 'package:flutter/cupertino.dart';

class Bank {
  final String name;
  final String currency;
  final String code;
  final String slug;

  Bank({@required this.name, @required this.currency, @required this.code, @required this.slug});
}