class TransactionLimitResponseModel {
  bool _succeeded;
  List<TransactionLimitResponseModelEntity> _entity;
  String _message;
  String _messages;

  TransactionLimitResponseModel(
      {bool succeeded,
      List<TransactionLimitResponseModelEntity> entity,
      String message,
      String messages}) {
    this._succeeded = succeeded;
    this._entity = entity;
    this._message = message;
    this._messages = messages;
  }

  bool get succeeded => _succeeded;
  set succeeded(bool succeeded) => _succeeded = succeeded;
  List<TransactionLimitResponseModelEntity> get entity => _entity;
  set entity(List<TransactionLimitResponseModelEntity> entity) =>
      _entity = entity;
  String get message => _message;
  set message(String message) => _message = message;
  String get messages => _messages;
  set messages(String messages) => _messages = messages;

  TransactionLimitResponseModel.fromJson(Map<String, dynamic> json) {
    _succeeded = json['succeeded'];
    if (json['entity'] != null) {
      _entity = new List<TransactionLimitResponseModelEntity>();
      json['entity'].forEach((v) {
        _entity.add(new TransactionLimitResponseModelEntity.fromJson(v));
      });
    }
    _message = json['message'];
    _messages = json['messages'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['succeeded'] = this._succeeded;
    if (this._entity != null) {
      data['entity'] = this._entity.map((v) => v.toJson()).toList();
    }
    data['message'] = this._message;
    data['messages'] = this._messages;
    return data;
  }
}

class TransactionLimitResponseModelEntity {
  dynamic _id;
  dynamic _transactionLimit;
  String _currency;
  String _createdBy;
  String _createdDate;
  String _lastModifiedBy;
  String _lastModifiedDate;
  dynamic _status;

  TransactionLimitResponseModelEntity(
      {dynamic id,
      dynamic transactionLimit,
      String currency,
      String createdBy,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate,
      dynamic status}) {
    this._id = id;
    this._transactionLimit = transactionLimit;
    this._currency = currency;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._status = status;
  }

  dynamic get id => _id;
  set id(dynamic id) => _id = id;
  dynamic get transactionLimit => _transactionLimit;
  set transactionLimit(dynamic transactionLimit) =>
      _transactionLimit = transactionLimit;
  String get currency => _currency;
  set currency(String currency) => _currency = currency;
  String get createdBy => _createdBy;
  set createdBy(String createdBy) => _createdBy = createdBy;
  String get createdDate => _createdDate;
  set createdDate(String createdDate) => _createdDate = createdDate;
  String get lastModifiedBy => _lastModifiedBy;
  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;
  String get lastModifiedDate => _lastModifiedDate;
  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;
  dynamic get status => _status;
  set status(dynamic status) => _status = status;

  TransactionLimitResponseModelEntity.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _transactionLimit = json['transactionLimit'];
    _currency = json['currency'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['transactionLimit'] = this._transactionLimit;
    data['currency'] = this._currency;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['status'] = this._status;
    return data;
  }
}
