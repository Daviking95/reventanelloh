
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColorPalette {
  // initializer
  Color _main = Color(0xff26474E);
  Color _mainMedium = Color(0xFF262C31);
  Color _textWhite = Colors.white;
  Color _dangerRed = Color(0xffF27348);
  Color _textBlack = Colors.black;
  Color _textSecondary = Color(0xffF27348);
  Color _orderSuccess = Color(0xff1FD38A);

  // getters
  Color get main => this._main;
  Color get mainMedium => this._mainMedium;
  Color get textWhite => this._textWhite;
  Color get dangerRed => this._dangerRed;
  Color get textBlack => this._textBlack;
  Color get textSecondary => this._textSecondary;
  Color get orderSuccess => this._orderSuccess;
}