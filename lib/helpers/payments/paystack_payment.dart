
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:flutter_paystack/flutter_paystack.dart';

class CustomPaystackPayment {
  CustomPaystackPayment() {
    print('initializing paystack...');
    PaystackPlugin().initialize(publicKey: env['PAYSTACK_LIVE_PUBLIC_KEY']); // env['PAYSTACK_DEV_PUBLIC_KEY']);
  }

  _getReference({@required String customerId}) {
    try {
      String platform;
      if (Platform.isIOS) {
        platform ='iOS';
      } else {
        platform = 'Android';
      }
      return 'PAYSTACK_ref$customerId:$platform:${DateTime.now().microsecondsSinceEpoch}';
    }catch(err) {
      print(err);
    }
  }

  Future<CheckoutResponse> checkOut({
    @required int amount,
    @required String email,
    @required BuildContext context,
    @required String customerId
}) async{
    try {
      Charge charge = Charge()
          ..amount = amount
          ..accessCode = this._getReference(customerId: customerId)
          ..reference = this._getReference(customerId: customerId)
          ..email = email;

      CheckoutResponse response = await PaystackPlugin().checkout(context, charge: charge);
      return response;
    } catch(err) {
      print(err);
      if (err is ChargeException) {
        print(err.message);
      }
      return err;
    }
  }
}