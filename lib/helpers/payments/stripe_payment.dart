// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:stripe_payment/stripe_payment.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';
//
// class CustomStripePayment {
//   CustomStripePayment() {
//     StripePayment.setOptions(StripeOptions(
//         publishableKey: env['STRIPE_DEV_PUBLIC_KEY'], androidPayMode: 'test', merchantId: 'test'));
//   }
//   Future<bool> checkIfNativePayReady() async {
//     try {
//       print('checking native pay availability...');
//       print(env['STRIPE_DEV_PUBLIC_KEY']);
//       print(env['STRIPE_LIVE_PUBLIC_KEY']);
//       bool deviceSupportsNativePay =
//           await StripePayment.deviceSupportsNativePay();
//       bool isNativePayReady = await StripePayment.canMakeNativePayPayments(
//           ['american_express', 'visa', 'maestro', 'master_card']);
//       if (deviceSupportsNativePay && isNativePayReady) {
//         return true;
//       } else {
//         return false;
//       }
//     } catch (err) {
//       print(err);
//       return false;
//     }
//   }
//
//   Future<PaymentMethod> createPaymentMethodNative({
//     @required String amount,
//     @required String orderId,
//     @required String currencyCode,
//   }) async {
//     try {
//       print('started Stripe Native payment...');
//       StripePayment.setStripeAccount(null);
//       List<ApplePayItem> items = [];
//       items.add(
//         ApplePayItem(
//           label: 'Payment for order ID: $orderId',
//           amount: double.parse(amount).toStringAsFixed(0),
//         ),
//       );
//
//       PaymentMethod paymentMethod = PaymentMethod();
//       print('got here');
//       print(currencyCode);
//       print(amount);
//       Token token = await StripePayment.paymentRequestWithNativePay(
//         androidPayOptions: AndroidPayPaymentRequest(
//           totalPrice: amount,
//           currencyCode: 'USD',
//         ),
//         applePayOptions: ApplePayPaymentOptions(
//             currencyCode: 'USD', items: items, countryCode: 'US'),
//       );
//       print(token.tokenId);
//       paymentMethod = await StripePayment.createPaymentMethod(
//         PaymentMethodRequest(
//           card: CreditCard(token: token.tokenId),
//         ),
//       );
//       return paymentMethod;
//     } catch (err) {
//       print(err);
//       return null;
//     }
//   }
//
//   Future<PaymentMethod> createPaymentMethod({
//     @required String amount,
//     @required String orderId,
//     @required String currencyCode,
//   }) async {
//     try {
//       print('...No ability for Native pay');
//       print('Using custom pay method...');
//       StripePayment.setStripeAccount('test');
//       PaymentMethod paymentMethod = PaymentMethod();
//       paymentMethod = await StripePayment.paymentRequestWithCardForm(
//         CardFormPaymentRequest(),
//       ).then((PaymentMethod paymentMethod) {
//         return paymentMethod;
//       }).catchError((error) {
//         print('Error card: ${error.toString()}');
//       });
//       // paymentMethod != null
//       //     ? this.processPaymentAsDirectCharge(paymentMethod: paymentMethod)
//       //     : anellohOverlay(
//       //         overlayObject: OverlayObject(
//       //             title: 'Card Error',
//       //             body:
//       //                 'Cannot pay with this card, please try again with a different card',
//       //             hasBtn: false,
//       //             hasCancelBtn: true,
//       //             cancelBtnText: 'Close',
//       //             icon: Icon(
//       //               Icons.warning,
//       //               color: Colors.red,
//       //             )),
//       //         context: context);;
//
//       return paymentMethod;
//
//     } catch (err) {
//       print(err);
//       return null;
//     }
//   }
//
//   Future confirmPaymentIntent({@required PaymentMethod paymentMethod, @required String paymentIntentClientSecret}) async {
//     try {
//       // print(paymentIntentClientSecret);
//       // if(paymentIntentClientSecret.isNotEmpty) {
//       //   StripePayment.completeNativePayRequest();
//       // } else {
//       //
//       //   StripePayment.cancelNativePayRequest();
//       // }
//       print('calling confirm payment intent...');
//       PaymentIntentResult paymentIntentResult = await StripePayment.confirmPaymentIntent(
//         PaymentIntent(clientSecret: paymentIntentClientSecret, paymentMethodId: paymentMethod.id),
//       );
//       return paymentIntentResult;
//     }catch(err) {
//       print(err);
//       return err;
//     }
//   }
// }
