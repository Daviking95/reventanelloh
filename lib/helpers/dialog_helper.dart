
import 'package:flutter/material.dart';

import '../constants.dart';
import '../styles.dart';

Future<bool> exitApp(BuildContext context) {
  return showDialog(
    context: context,
    builder: (context) {
      return new AlertDialog(
        title: new Text(
          ExitAppConstants.exitAppTitle,
          style: textStylePrimaryColorNormal.copyWith(
              color: appSecondaryColor),
        ),
        content: new Text(
          ExitAppConstants.exitAppContent,
          style: textStylePrimaryColorNormal,
        ),
        actions: <Widget>[
          new TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: new Text(AppConstants.appNo),
          ),
          new TextButton(
            onPressed: () => Navigator.of(context).pop(true),
            child: new Text(AppConstants.appYes),
          ),
        ],
      );
    },
  ) ??
      false;
}