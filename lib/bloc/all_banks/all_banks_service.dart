
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:dio/dio.dart';

class AllBanksService {
  Future<Response> getNigerianBanks() async{
    try {
      Response response = await dio.get('https://api.paystack.co/bank');

      log('Fetching Banks ${response}');

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    }
    catch(err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> getOtherBanks() async {
    try{
      HttpClientResponse response = await HttpService().post(url: 'Banks/getall', requestData: {});
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    }
    catch(err) {
      throw new Error();
    }
  }
}