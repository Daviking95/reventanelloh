import 'package:equatable/equatable.dart';

class AllBanksEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class GetAllNigerianBanksEvent extends AllBanksEvent {}

class GetOtherBanksEvent extends AllBanksEvent {}