
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class AllBanksState extends Equatable {
 final List<Bank> banks;
 AllBanksState({this.banks = const []}):super();
  @override
  List<Object> get props => [];
 }

 class FetchingBanks extends AllBanksState {}
 class InitializeAllBanks extends AllBanksState {}

 class SuccessfullyFetchedAllBanks extends AllBanksState {
  final List<Bank> banks;
  SuccessfullyFetchedAllBanks({@required this.banks}):super(banks: banks);
 }

 class ErrorFetchingAllBanks extends AllBanksState {}
 class AllBanksStateSocketException extends AllBanksState {}
 class AllBanksStateHttpException extends AllBanksState {}