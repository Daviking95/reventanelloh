
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/all_banks/all_banks_event.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_service.dart';
import 'package:anelloh_mobile/bloc/all_banks/all_banks_state.dart';
import 'package:anelloh_mobile/helpers/classes/banks.dart';
import 'package:dio/dio.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AllBanksBloc extends Bloc<AllBanksEvent, AllBanksState> {
  AllBanksBloc():super(InitializeAllBanks());

  AllBanksService _allBanksService = AllBanksService();

  @override
  Stream<AllBanksState> mapEventToState(AllBanksEvent event) async* {
    if (event is GetAllNigerianBanksEvent) {
      try {
        yield FetchingBanks();
        Response response = await _allBanksService.getNigerianBanks();
        if(response.data['status'] == true) {
          List result = response.data['data'];

          log('Fetching Banks ${result}');

          List<Bank> banks = [];
         result.forEach((element) {
           Bank _bank = new Bank(name: element['name'], currency: element['currency'], code:element['code'], slug: element['slug']);
           banks.add(_bank);
         });
          yield SuccessfullyFetchedAllBanks(banks: banks);
        } else {
          yield ErrorFetchingAllBanks();
        }
      }
      catch(err) {
        print(err);
        if (err is SocketException) {
          yield AllBanksStateSocketException();
        }
        if (err is HttpException) {
          yield AllBanksStateHttpException();
        }
        yield ErrorFetchingAllBanks();
      }
    }

    if (event is GetOtherBanksEvent) {
      try {
        yield FetchingBanks();
        HttpClientResponse response = await _allBanksService.getOtherBanks();
        if(response.statusCode == 200) {
          Map<String, dynamic> result = jsonDecode(await response.transform(utf8.decoder).join());
          List<Bank> banks = [];
          result['entity'].forEach((element) {
            Bank _bank = new Bank(name: element['name'], currency: element['currency'], code:element['code'], slug: element['slug']);
            banks.add(_bank);
          });
          yield SuccessfullyFetchedAllBanks(banks: banks);
        } else {
          yield ErrorFetchingAllBanks();
        }
      }catch(err) {
        print(err);
        if (err is SocketException) {
          yield AllBanksStateSocketException();
        }
        if (err is HttpException) {
          yield AllBanksStateHttpException();
        }
        yield ErrorFetchingAllBanks();
      }
    }

  }
}