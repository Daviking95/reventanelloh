
import 'package:equatable/equatable.dart';

class OTPScreenState extends Equatable {
  OTPScreenState();
  @override
  List<Object> get props => [];
}

class OTPScreenInitialState extends OTPScreenState {}
class OTPStateChanged extends OTPScreenState {}

class ResendingOTP extends OTPScreenState{}
class OTPResendSuccessful extends OTPScreenState {}
class OTPResendFailed extends OTPScreenState {}


class VerifyingOTP extends OTPScreenState {}
class OTPVerificationFailed extends OTPScreenState {}
class OTPVerificationSuccessful extends OTPScreenState {}



class OTPScreenNoInternetException extends OTPScreenState {}

class OTPScreenHttpException extends OTPScreenState {}
