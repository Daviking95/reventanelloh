

import 'package:anelloh_mobile/bloc/otp/otp_event.dart';
import 'package:anelloh_mobile/bloc/otp/otp_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OTPScreenBloc extends Bloc<OTPScreenEvent, OTPScreenState> {
  OTPScreenBloc():super(OTPScreenInitialState());
  @override
  Stream<OTPScreenState> mapEventToState(OTPScreenEvent event) async*{
    if (event is ResendOTPEvent) {
      yield ResendingOTP();
      yield OTPStateChanged();
    }
  }
}