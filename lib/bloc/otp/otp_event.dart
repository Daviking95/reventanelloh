
import 'package:equatable/equatable.dart';

class OTPScreenEvent extends Equatable {
  OTPScreenEvent();
  @override
  List<Object> get props => [];
}

class ResendOTPEvent extends OTPScreenEvent{}

class VerifyOTPEvent extends OTPScreenEvent {}
