
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class ChangePinScreenEvent extends Equatable {
  ChangePinScreenEvent();
  @override
  List<Object> get props => [];
}

class SavingPinEvent extends ChangePinScreenEvent {
  final ChangePinObj changePinObj;
  SavingPinEvent({
    @required this.changePinObj
}):super();
}






// CLASSES ASSOCIATED WITH PIN CHANGE

class ChangePinObj {
  final String currentPin;
  final String newPin;
  ChangePinObj({
    @required this.currentPin,
    @required this.newPin,
});
}