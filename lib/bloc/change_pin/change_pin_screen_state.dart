
import 'package:equatable/equatable.dart';

class ChangePinScreenState extends Equatable {
  ChangePinScreenState();

  @override
  List<Object> get props => [];
}

class InitialChangePinScreenState extends ChangePinScreenState{}

class SavingPin extends ChangePinScreenState {}

class SavingPinSuccess extends ChangePinScreenState {
  final String message;
  SavingPinSuccess({this.message}):super();
}

class SavingPinFailed extends ChangePinScreenState {
  final String error;
  SavingPinFailed({this.error}):super();
}

class ChangePinScreenInternetException extends ChangePinScreenState{}
class ChangePinScreenHttpException extends ChangePinScreenState{}
