

import 'dart:io';

import 'package:anelloh_mobile/bloc/change_pin/change_pin_screen_event.dart';
import 'package:anelloh_mobile/bloc/change_pin/change_pin_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ChangePinScreenBloc extends Bloc<ChangePinScreenEvent, ChangePinScreenState> {
  ChangePinScreenBloc():super(InitialChangePinScreenState());

  @override
  Stream<ChangePinScreenState> mapEventToState(ChangePinScreenEvent event) async*{
    if(event is SavingPinEvent) {
     try{
       yield SavingPin();
       // make an API call to saving pin;
     }on SocketException {
       yield ChangePinScreenInternetException();
     } on HttpException {
       yield ChangePinScreenHttpException();
     }
     catch(error) {
       yield SavingPinFailed(error: 'An error occurred');
     }
    }
  }
}