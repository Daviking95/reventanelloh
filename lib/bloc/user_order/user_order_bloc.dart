import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_event.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_service.dart';
import 'package:anelloh_mobile/bloc/user_order/user_order_state.dart';
import 'package:anelloh_mobile/helpers/classes/activity_card.dart';
import 'package:anelloh_mobile/helpers/classes/order_listing.dart';
import 'package:anelloh_mobile/helpers/print_wrap.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants.dart';

class MainUserOrderBloc extends Bloc<MainUserOrderEvent, MainUserOrderState> {
  MainUserOrderBloc()
      : super(SuccessfullyFetchedUserOfOrder(
            user: User(
                fullNames: 'Dominic olije',
                userName: 'dom',
                lastName: 'olije',
                firstName: 'dom',
                middleName: 'dom',
                bvn: '2342342',
                rating: '5.0',
                noOfSuccessfulTransactions: 100,
                customerId: 'rt43532435t',
                countryCode: 'US',
                emailAddress: 'dom@olije.com',
                phoneNumber: '0903387534',
                gender: 'male',
                homeAddress: '92345trfjdsleortigfvc',
                profileImage: null,
                verificationStatus: '1')));

  MainUserOrderService _mainUserOrderService = MainUserOrderService();
  @override
  Stream<MainUserOrderState> mapEventToState(MainUserOrderEvent event) async* {
    if (event is AutoCheckUserPendingOrderEvent) {
      try {
        HttpClientResponse response = await _mainUserOrderService
            .checkCustomerPendingOrder(customerId: event.customerId);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          // printWrapped('AutoCheckUserPendingOrderEvent $result');

          if (result['succeeded']) {
            List<OrderListing2> listings = [];

            result['entity'].forEach((entity) {
              OrderListing2 orderListing = OrderListing2.fromJson(entity);

              listings.add(orderListing);
            });
            List<OrderListing2> lists = List<OrderListing2>.from(listings);

            AppConstants.userLatestOrderStatus =
                lists[0]?.order?.orderStatus.toString() ?? null;

            // log('AppConstants.userLatestOrderStatus auto ${AppConstants.userLatestOrderStatus}');

            yield CheckingUserPendingStateChanged();
            yield CheckingUserPendingOrderSuccessful(pendingOrders: lists);
          } else {
            yield CheckingUserPendingOrderFailed();
          }
        }
      } catch (err) {
        print(err);
      }
    }
    if (event is CheckUserPendingOrderEvent) {
      try {
        yield CheckingUserPendingStateChanged();
        yield CheckingUserPendingOrderState();
        HttpClientResponse response = await _mainUserOrderService
            .checkCustomerPendingOrder(customerId: event.customerId);

        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          // printWrapped('CheckUserPendingOrderEvent $result');

          if (result['succeeded']) {
            List<OrderListing2> listings = [];
            result['entity'].forEach((entity) {
              OrderListing2 orderListing = OrderListing2.fromJson(entity);
              listings.add(orderListing);
            });
            List<OrderListing2> lists = List<OrderListing2>.from(listings);

            AppConstants.userLatestOrderStatus =
                lists[0]?.order?.orderStatus.toString() ?? null;

            // log('AppConstants.userLatestOrderStatus pending ${AppConstants.userLatestOrderStatus}');

            yield CheckingUserPendingStateChanged();
            yield CheckingUserPendingOrderSuccessful(pendingOrders: lists);
          } else {
            yield CheckingUserPendingOrderFailed();
          }
        } else {
          yield CheckingUserPendingOrderFailed();
        }
      } on SocketException {} on HttpException {} catch (err) {
        print('an error occurred');
        print(err);
        yield CheckingUserPendingOrderFailed();
      }
    }

    if (event is UpdateUserPendingOrderEvent) {
      try {
        yield UpdatingUserPendingStateChanged();
        yield UpdatingUserPendingOrderState();
        HttpClientResponse response =
            await _mainUserOrderService.updateCustomerPendingOrder(
                customerId: event.customerId, orderNo: event.orderNo);

        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          // printWrapped('UpdateUserPendingOrderEvent $result');

          if (result['succeeded']) {
            List<OrderListing2> listings = [];
            result['entity'].forEach((entity) {
              OrderListing2 orderListing = OrderListing2.fromJson(entity);
              listings.add(orderListing);
            });
            List<OrderListing2> lists = List<OrderListing2>.from(listings);
            yield UpdatingUserPendingStateChanged();
            yield UpdatingUserPendingOrderSuccessful(pendingOrders: lists);
          } else {
            yield UpdatingUserPendingOrderFailed();
          }
        } else {
          yield UpdatingUserPendingOrderFailed();
        }
      } on SocketException {} on HttpException {} catch (err) {
        print('an error occurred');
        print(err);
        yield UpdatingUserPendingOrderFailed();
      }
    }
    if (event is FetchUserOrdersEvent) {
      try {
        yield FetchingAllUserOrders();
        HttpClientResponse response =
            await _mainUserOrderService.allCustomerOrders(user: event.user);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          if (result['succeeded'] == true) {
            List orders = result['entity']['orders'];

            // log('Activities Orders ${jsonEncode(orders)}');

            List<ActivityCardItem> activities = [];
            orders.forEach((order) {
              ActivityCardItem activityCardItem = ActivityCardItem(
                orderId: order['id'].toString(),
                orderNo: order['orderNo'].toString(),
                customerId: order['customerId'].toString(),
                rate: order['rate'].toString(),
                myAmount: order['myAmount'].toString(),
                userName: order['userName'].toString(),
                totalAmount: order['totalAmount'].toString(),
                matchedOrderStatus: order['matchedOrderStatus'].toString(),
                myCurrency: order['myCurrency'].toString(),
                myAccountNumber: order['myAccountNumber'].toString(),
                myBankName: order['myBankName'].toString(),
                bankRouteNo: order['bankRouteNo'].toString(),
                myPaymentChannelId: order['myPaymentChannelId'].toString(),
                paymentStatus: order['paymentStatus'].toString(),
                convertedAmount: order['convertedAmount'].toString(),
                convertedCurrency: order['convertedCurrency'].toString(),
                status: order['status'].toString(),
                convertedPaymentChannelId:
                    order['convertedPaymentChannelId'].toString(),
                convertedPaymentReferenceNo:
                    order['convertedPaymentReferenceNo'].toString(),
                convertedPaymentResponse:
                    order['convertedPaymentResponse'].toString(),
                convertedPaymentError:
                    order['convertedPaymentError'].toString(),
                transactionFee: order['transactionFee'].toString(),
                transactionFeeCurrency:
                    order['transactionFeeCurrency'].toString(),
                orderStatus: order['orderStatus'].toString(),
                createdTime: order['createdDate'],
              );
              activities.add(activityCardItem);
            });
            yield SuccessfullyFetchedAllUserOrders(activities: activities);
          } else {
            yield ErrorFetchingAllUserOrders();
          }
        } else {
          yield ErrorFetchingAllUserOrders();
        }
      } on SocketException {
        yield MainUserOrderStateSocketException();
      } on HttpException {
        yield MainUserOrderStateHttpException();
      } catch (err) {
        print(err);
        yield ErrorFetchingAllUserOrders();
      }
    }

    if (event is FetchSingleUserOfOrder) {
      try {} on SocketException {
        yield MainUserOrderStateSocketException();
      } on HttpException {
        yield MainUserOrderStateHttpException();
      } catch (err) {
        print(err);
        yield ErrorFetchingUserOfOrder();
      }
    }

    if (event is CancelPendingOrderEvent) {
      try {
        yield CancellingUserPendingStateChanged();

        HttpClientResponse response =
        await _mainUserOrderService.cancelCustomerPendingOrder(
            customerId: event.customerId, orderNo: event.orderNo);

        Map<String, dynamic> result =
        jsonDecode(await response.transform(utf8.decoder).join());

        // printWrapped('CancelUserPendingOrderEvent $result');

        if (response.statusCode == 200) {

          if (result['succeeded']) {
            yield CancellingUserPendingStateChanged();
            yield CancellingUserPendingOrderSuccessful(message: 'Order Cancelled Successfully', tempBanks: event.tempBanks, tempMatchFoundObj: event.tempMatchFoundObj);
          } else {
            yield CancellingUserPendingOrderFailed(error: result['message'] ??
                result['entity'] ??
                result['messages'][0]);
          }
        } else {
          yield CancellingUserPendingOrderFailed(error: result['message'] ??
              result['entity'] ??
              result['messages'][0]);
        }
      } on SocketException {} on HttpException {} catch (err) {
        print('an error occurred');
        print(err);
        yield CancellingUserPendingOrderFailed(error: err.toString());
      }
    }

    if (event is CheckConfigurationFeeEvent) {
      try {
        yield GettingCheckConfigurationFee();

        HttpClientResponse response =
        await _mainUserOrderService.getConfigurationFee();

        Map<String, dynamic> result =
        jsonDecode(await response.transform(utf8.decoder).join());

        print('GettingCheckConfigurationFeeSuccessful ${result}');

        if (response.statusCode == 200) {

          if (result['succeeded']) {
            yield GettingCheckConfigurationFeeSuccessful(configurationFee: result['entity'][0]['feePercentage']);
          } else {
            yield GettingCheckConfigurationFeeFailed(error: result['message'] ??
                result['entity'] ??
                result['messages'][0]);
          }
        } else {
          yield GettingCheckConfigurationFeeFailed(error: result['message'] ??
              result['entity'] ??
              result['messages'][0]);
        }
      } on SocketException {} on HttpException {} catch (err) {
        print('an error occurred');
        print(err);
        yield GettingCheckConfigurationFeeFailed(error: err.toString());
      }
    }
  }
}
