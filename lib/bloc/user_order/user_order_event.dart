import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class MainUserOrderEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CheckUserPendingOrderEvent extends MainUserOrderEvent {
  final String customerId;
  CheckUserPendingOrderEvent({this.customerId}) : super();
}

class UpdateUserPendingOrderEvent extends MainUserOrderEvent {
  final String customerId;
  final String orderNo;

  UpdateUserPendingOrderEvent({this.customerId, this.orderNo}) : super();
}

class CancelPendingOrderEvent extends MainUserOrderEvent {
  final String customerId;
  final String orderNo;
  final MatchFoundObj tempMatchFoundObj;
  final List<dynamic> tempBanks;

  CancelPendingOrderEvent({this.customerId, this.orderNo, this.tempBanks, this.tempMatchFoundObj}) : super();
}

class AutoCheckUserPendingOrderEvent extends MainUserOrderEvent {
  final String customerId;
  AutoCheckUserPendingOrderEvent({this.customerId}) : super();
}

class FetchUserOrdersEvent extends MainUserOrderEvent {
  final User user;
  FetchUserOrdersEvent({@required this.user});
}

class CheckConfigurationFeeEvent extends MainUserOrderEvent {
}

class FetchSingleUserOfOrder extends MainUserOrderEvent {
  final String customerId;
  FetchSingleUserOfOrder({@required this.customerId}) : super();
}
