import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/classes/activity_card.dart';
import 'package:anelloh_mobile/helpers/classes/order_listing.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class MainUserOrderState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialMainUserPendingOrderState extends MainUserOrderState {}

class CheckingUserPendingOrderState extends MainUserOrderState {}

class UpdatingUserPendingOrderState extends MainUserOrderState {}

class GettingCheckConfigurationFee extends MainUserOrderState {}

class CheckingUserPendingOrderSuccessful extends MainUserOrderState {
  final List<OrderListing2> pendingOrders;
  CheckingUserPendingOrderSuccessful({
    this.pendingOrders = const [],
  }) : super();
}

class GettingCheckConfigurationFeeSuccessful extends MainUserOrderState {
  final dynamic configurationFee;
  GettingCheckConfigurationFeeSuccessful({
    this.configurationFee = const [],
  }) : super();
}

class UpdatingUserPendingOrderSuccessful extends MainUserOrderState {
  final List<OrderListing2> pendingOrders;
  UpdatingUserPendingOrderSuccessful({
    this.pendingOrders = const [],
  }) : super();
}

class CheckingUserPendingStateChanged extends MainUserOrderState {}

class UpdatingUserPendingStateChanged extends MainUserOrderState {}

class CheckingUserPendingOrderFailed extends MainUserOrderState {}

class UpdatingUserPendingOrderFailed extends MainUserOrderState {}

class FetchingAllUserOrders extends MainUserOrderState {}

class FetchingSingleUserOfOrder extends MainUserOrderState {}

class SuccessfullyFetchedUserOfOrder extends MainUserOrderState {
  final User user;
  SuccessfullyFetchedUserOfOrder({@required this.user}) : super();
}

class ErrorFetchingUserOfOrder extends MainUserOrderState {}

class SuccessfullyFetchedAllUserOrders extends MainUserOrderState {
  final List<ActivityCardItem> activities;
  SuccessfullyFetchedAllUserOrders({@required this.activities}) : super();
}

class ErrorFetchingAllUserOrders extends MainUserOrderState {}

class MainUserOrderStateSocketException extends MainUserOrderState {}

class MainUserOrderStateHttpException extends MainUserOrderState {}

class CancellingUserPendingStateChanged extends MainUserOrderState {}

class CancellingUserPendingOrderFailed extends MainUserOrderState {
  final String error;

  CancellingUserPendingOrderFailed({@required this.error}) : super();
}

class GettingCheckConfigurationFeeFailed extends MainUserOrderState {
  final String error;

  GettingCheckConfigurationFeeFailed({@required this.error}) : super();
}

class CancellingUserPendingOrderSuccessful extends MainUserOrderState {
  final String message;
  final MatchFoundObj tempMatchFoundObj;
  final List<dynamic> tempBanks;

  CancellingUserPendingOrderSuccessful({@required this.message, this.tempBanks, this.tempMatchFoundObj}) : super();
}
