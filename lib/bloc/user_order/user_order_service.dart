import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/dio.dart';

class MainUserOrderService {
  Future<HttpClientResponse> checkCustomerPendingOrder(
      {String customerId}) async {
    try {
      HttpClientResponse response = await HttpService()
          .get(url: 'Orders/getcustomerpendingorders/$customerId');

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> updateCustomerPendingOrder(
      {String customerId, String orderNo}) async {
    try {
      Map<String, dynamic> requestData = {
        "customerId": customerId,
        "orderNo": orderNo,
        "applicationType": 1
      };

      log('updateCustomerPendingOrder ${jsonEncode(requestData)}');

      HttpClientResponse response = await HttpService()
          .post(url: 'Orders/unfulfillordercommand', requestData: requestData);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> allCustomerOrders({User user}) async {
    try {
      HttpClientResponse response = await HttpService()
          .get(url: 'Orders/getcustomerorders/${user.customerId}');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  Future<HttpClientResponse> getConfigurationFee() async {
    try {
      HttpClientResponse response = await HttpService()
          .post(url: 'Configurations/getallfees');

      print('Configurations/getallfees');

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  cancelCustomerPendingOrder({String customerId, String orderNo}) async{
    try {
      Map<String, dynamic> requestData = {
        "customerId": customerId,
        "orderId": int.parse(orderNo),
      };

      log('cancelCustomerPendingOrder ${jsonEncode(requestData)}');

      HttpClientResponse response = await HttpService()
          .put(url: 'Orders/cancel/${int.parse(orderNo)}', requestData: requestData);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }
}
