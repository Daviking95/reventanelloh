import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ListingsActionState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialListingsAction extends ListingsActionState {}

class FetchingListings extends ListingsActionState {}

class SuccessfullyFetchedAllOrderListings extends ListingsActionState {
  final List<ListingObj> listings;
  SuccessfullyFetchedAllOrderListings({this.listings}) : super();
}

class ErrorFetchingAllOrderListings extends ListingsActionState {}

class FetchingListingById extends ListingsActionState {}

class SuccessFullyFetchedListingById extends ListingsActionState {
  final ListingObj listingObj;
  SuccessFullyFetchedListingById({@required this.listingObj}) : super();
}

class ErrorFetchingListingById extends ListingsActionState {}

class ListingActionSocketException extends ListingsActionState {}

class ListingActionHttpException extends ListingsActionState {}
