import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_state.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/listings/listings_event.dart';
import 'package:anelloh_mobile/bloc/listings/listings_service.dart';
import 'package:anelloh_mobile/bloc/listings/listings_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListingsActionBloc
    extends Bloc<ListingsActionEvent, ListingsActionState> {
  ListingsActionBloc() : super(InitialListingsAction());

  ListingsActionService _listingsActionService = ListingsActionService();

  @override
  Stream<ListingsActionState> mapEventToState(
      ListingsActionEvent event) async* {
    if (event is FetchAllOrderListings) {
      try {
        yield FetchingListings();
        HttpClientResponse response = await _listingsActionService
            .fetchAllListings(customerId: event.customerId);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            List entity = result['entity'];
            List<ListingObj> listings = [];
            entity.forEach((res) {
              // log('The Listings ${jsonEncode(res['order'])}');
              ListingObj listingObj = new ListingObj(
                matchFoundObj: new MatchFoundObj(
                    entityId: res['order']['id'].toString(),
                    customerId: res['order']['customerId'].toString(),
                    orderOwnerId: res['order']['customerId'].toString(),
                    orderNo: res['order']['orderNo'].toString(),
                    exchangeRate: res['order']['rate'].toString(),
                    totalAmount: res['order']['totalAmount'].toString(),
                    convertedCurrency:
                        res['order']['convertedCurrency'].toString(),
                    myCurrency: res['order']['myCurrency'].toString(),
                    myAmount: res['order']['myAmount'].toString(),
                    processingFee: res['order']['transactionFee'].toString(),
                    status: res['order']['status'].toString(),
                    subTotal: res['order']['convertedAmount'].toString(),
                    convertedAmount: res['order']['convertedAmount'].toString(),
                    userEmail: res['order']['email'], // add to api
                    userName: res['order']['userName'],
                    convertedPaymentChannelId:
                        res['order']['convertedPaymentChannelId'].toString(),
                    convertedPaymentReferenceNo:
                        res['order']['convertedPaymentReferenceNo'].toString(),
                    convertedPaymentResponse:
                        res['order']['convertedPaymentResponse'].toString(),
                    convertedPaymentError:
                        res['order']['convertedPaymentError'].toString(),
                    transactionFeeCurrency:
                        res['order']['transactionFeeCurrency'].toString(),
                    rates: [],
                    ascDecValue: res['rateChange'],
                    isRateAscending: true,
                    orderStatus: res['order']['orderStatus']),
              );

              listings.add(listingObj);
            });
            yield SuccessfullyFetchedAllOrderListings(listings: listings);
          } else {
            yield ErrorFetchingAllOrderListings();
          }
        } else {
          yield ErrorFetchingAllOrderListings();
        }
      } on SocketException {
        yield ListingActionSocketException();
      } on HttpException {
        yield ListingActionHttpException();
      } catch (err) {
        if (err is SocketException) {
          yield ListingActionSocketException();
        }
        if (err is HttpException) {
          yield ListingActionHttpException();
        }
        print(err);
        yield ErrorFetchingAllOrderListings();
      }
    }

    if (event is FetchListingByIdEvent) {
      try {
        yield FetchingListingById();
        HttpClientResponse response = await _listingsActionService
            .fetchListingById(listingId: event.listingId);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        // log('FetchingListingById ${response.statusCode}');
        // log('FetchingListingById Encode ${jsonEncode(result['entity'])}');

        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            var entity = result['entity'];

            // log('FetchingListingById $entity');

            ListingObj listingObj = ListingObj(
              matchFoundObj: MatchFoundObj(
                  entityId: entity['order']['id'].toString(),
                  customerId: entity['order']['customerId'].toString(),
                  orderOwnerId: entity['order']['customerId'].toString(),
                  orderNo: entity['order']['orderNo'].toString(),
                  exchangeRate: entity['order']['rate'].toString(),
                  totalAmount: entity['order']['totalAmount'].toString(),
                  convertedCurrency:
                      entity['order']['convertedCurrency'].toString(),
                  myCurrency: entity['order']['myCurrency'].toString(),
                  myAmount: entity['order']['myAmount'].toString(),
                  processingFee: entity['order']['transactionFee'].toString(),
                  status: entity['order']['status'].toString(),
                  subTotal: entity['order']['myAmount'].toString(),
                  convertedAmount:
                      entity['order']['convertedAmount'].toString(),
                  userEmail: entity['order']['userEmail'], // add to api
                  userName: entity['order']['userName'],
                  convertedPaymentChannelId:
                      entity['order']['convertedPaymentChannelId'].toString(),
                  convertedPaymentReferenceNo:
                      entity['order']['convertedPaymentReferenceNo'].toString(),
                  convertedPaymentResponse:
                      entity['order']['convertedPaymentResponse'].toString(),
                  convertedPaymentError:
                      entity['order']['convertedPaymentError'].toString(),
                  transactionFeeCurrency:
                      entity['order']['transactionFeeCurrency'].toString(),
                  rates: [],
                  ascDecValue: entity['order']['rateChange'],
                  isRateAscending: true,
                  orderStatus: entity['order']['orderStatus']),
            );

            yield SuccessFullyFetchedListingById(listingObj: listingObj);
          } else {
            yield ErrorFetchingListingById();
          }
        } else {
          yield ErrorFetchingListingById();
        }
      } on HttpException {
        yield ListingActionHttpException();
      } on SocketException {
        yield ListingActionSocketException();
      } catch (err) {
        print(err);
        yield ErrorFetchingListingById();
      }
    }
  }
}
