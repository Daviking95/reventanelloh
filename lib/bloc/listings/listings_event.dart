import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ListingsActionEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchAllOrderListings extends ListingsActionEvent {
  final String customerId;
  FetchAllOrderListings({@required this.customerId}):super();
}

class FetchListingByIdEvent extends ListingsActionEvent {
  final int listingId;
  FetchListingByIdEvent({
    @required this.listingId,
  }) : super();
}
