import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/material.dart';

class ListingsActionService {
  // getOrderlistings
  Future<HttpClientResponse> fetchAllListings(
      {@required String customerId}) async {
    try {
      HttpClientResponse response =
          await HttpService().get(url: 'Orders/getorderlistings/$customerId');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  Future<HttpClientResponse> fetchListingById({@required int listingId}) async {
    try {
      // log('FetchingListingById Orders/getbyid/$listingId');

      HttpClientResponse response =
          await HttpService().get(url: 'Orders/getbyid/$listingId');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }
}
