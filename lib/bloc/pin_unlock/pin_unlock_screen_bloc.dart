
import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_event.dart';
import 'package:anelloh_mobile/bloc/pin_unlock/pin_unlock_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PinUnlockScreenBloc extends Bloc<PinUnlockScreenEvent, PinUnlockScreenState> {
  PinUnlockScreenBloc():super(InitialPinUnlockScreenState());

  @override
  Stream<PinUnlockScreenState> mapEventToState(PinUnlockScreenEvent event) async* {}
}