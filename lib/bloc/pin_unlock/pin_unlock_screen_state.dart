
import 'package:equatable/equatable.dart';

class PinUnlockScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialPinUnlockScreenState extends PinUnlockScreenState {}