import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';

class MarketRatesService {
  Future<HttpClientResponse> getAllMarketRates() async {
    try {
      HttpClientResponse response = await HttpService()
          .post(url: 'Configurations/getallmarketrateconfig', requestData: {});
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  getNigeriaMarketRate() async {}
}
