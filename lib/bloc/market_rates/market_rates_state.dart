import 'package:anelloh_mobile/bloc/market_rates/market_rates_events.dart';
import 'package:anelloh_mobile/widgets/line_chart.dart';
import 'package:bezier_chart/bezier_chart.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

final initialLineData = [
  new DateRateRow(new DateTime(2017, 9, 25), 6),
  new DateRateRow(new DateTime(2017, 9, 26), 8),
  new DateRateRow(new DateTime(2017, 9, 27), 6),
  new DateRateRow(new DateTime(2017, 9, 28), 9),
  new DateRateRow(new DateTime(2017, 9, 29), 11),
  new DateRateRow(new DateTime(2017, 9, 30), 15),
  new DateRateRow(new DateTime(2017, 10, 01), 25),
  new DateRateRow(new DateTime(2017, 10, 02), 33),
  new DateRateRow(new DateTime(2017, 10, 03), 27),
  new DateRateRow(new DateTime(2017, 10, 04), 31),
  new DateRateRow(new DateTime(2017, 10, 05), 23),
];

final initialLineForBezier = [
  DataPoint(value: 10),
  DataPoint(value: 130),
  DataPoint(value: 200),
  DataPoint(value: 250)
];

class MarketRateState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialMarketRate extends MarketRateState {}

class SaveMarketRateData extends MarketRateState {
  final List<MarketRateObj> marketRates;
  SaveMarketRateData({
    @required this.marketRates,
}):super();
}

class FetchingMarketRate extends MarketRateState {}
class ErrorFetchingMarketRate extends MarketRateState {}
class FetchingMarketRateSuccessful extends MarketRateState {}
class MarketRateNoInternetException extends MarketRateState {}
class MarketRateHttpException extends MarketRateState {}

