import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/market_rates/market_rates_events.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_service.dart';
import 'package:anelloh_mobile/bloc/market_rates/market_rates_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MarketRateBloc extends Bloc<MarketRateEvent, MarketRateState> {
  MarketRateBloc() : super(InitialMarketRate());
  MarketRatesService _marketRatesService = new MarketRatesService();

  @override
  Stream<MarketRateState> mapEventToState(MarketRateEvent event) async* {
    if (event is FetchMarketRateEvent) {
      try {
        yield FetchingMarketRate();
        HttpClientResponse response =
            await _marketRatesService.getAllMarketRates();
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          List entity = result['entity'];

          log("FetchMarketRateEvent ${jsonEncode(entity)}");

          // entity.add({
          //
          //   "id": 39,
          //   "marketRate": 500.0,
          //   "baseCurrency": "GBR",
          //   "variableCurrency": "NGN",
          //   "createdBy": "febf7012-2d08-4954-97e1-a256bd9691d8",
          //   "createdDate": "2021-04-14T17:14:27.036231",
          //   "lastModifiedBy": null,
          //   "lastModifiedDate": null,
          //   "status": 0
          //
          // });  entity.add({
          //   "id": 36,
          //   "marketRate": 490,
          //   "baseCurrency": "GBR",
          //   "variableCurrency": "NGN",
          //   "createdBy": "febf7012-2d08-4954-97e1-a256bd9691d8",
          //   "createdDate": "2021-04-13T14:05:39.7917874",
          //   "lastModifiedBy": null,
          //   "lastModifiedDate": null,
          //   "status": 1
          // });  entity.add({
          //   "id": 37,
          //   "marketRate": 440,
          //   "baseCurrency": "CAN",
          //   "variableCurrency": "NGN",
          //   "createdBy": "febf7012-2d08-4954-97e1-a256bd9691d8",
          //   "createdDate": "2021-04-12T14:05:39.7917874",
          //   "lastModifiedBy": null,
          //   "lastModifiedDate": null,
          //   "status": 1
          // });  entity.add({
          //       "id": 38,
          //       "marketRate": 420,
          //       "baseCurrency": "USD",
          //       "variableCurrency": "NGN",
          //       "createdBy": "febf7012-2d08-4954-97e1-a256bd9691d8",
          //       "createdDate": "2021-04-11T14:05:39.7917874",
          //       "lastModifiedBy": null,
          //       "lastModifiedDate": null,
          //       "status": 1
          //     });

          List mainEntity = [];
          entity.forEach((value) {
            var idx = mainEntity.indexWhere(
                (element) => element['baseCurrency'] == value['baseCurrency']);
            if (idx != -1) {
              Map<String, dynamic> data = mainEntity.firstWhere((element) =>
                  element['baseCurrency'] == value['baseCurrency']);
              List tempRates = data['rates'];
              tempRates.add(value['marketRate']);
              data['rates'] = tempRates;

              List tempCreatedDate = data['createdDates'];
              tempCreatedDate.add(value['createdDate']);
              data['createdDates'] = tempCreatedDate;

              mainEntity[idx] = data;
            } else {
              Map<String, dynamic> obj = {};
              obj['baseCurrency'] = value['baseCurrency'];
              obj['rates'] = [];
              obj['rates'].add(value['marketRate']);

              obj['createdDates'] = [];
              obj['createdDates'].add(value['createdDate']);

              obj['variableCurrency'] = value['variableCurrency'];
              obj['createdDate'] = value['createdDate'];
              mainEntity.add(obj);
            }
          });

          List<MarketRateObj> marketRates = [];
          mainEntity.forEach((element) async {
            MarketRateObj _marketRateObj = new MarketRateObj(
                baseCurrency: element['baseCurrency'],
                marketRate: element['rates'][0].toString(),
                rates: element['rates'],
                createdDates: element['createdDates'],
                variableCurrency: element['variableCurrency'],
                createdDate: element['createdDate'],
                isRateAscending: true,
                ascDecValue: 4.0);
            marketRates.add(_marketRateObj);
          });
          yield SaveMarketRateData(marketRates: marketRates);
        } else {
          yield ErrorFetchingMarketRate();
        }
      } on HttpException {
        yield MarketRateHttpException();
      } on SocketException {
        yield MarketRateNoInternetException();
      } catch (err) {
        print(err);
        yield ErrorFetchingMarketRate();
      }
    }
  }
}
