import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class MarketRateEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchMarketRateEvent extends MarketRateEvent {}

// CLASSES ASSOCIATED WITH  MARKET RATE

class MarketRateObj {
  String baseCurrency;
  String marketRate;
  String variableCurrency;
  String createdDate;
  bool isRateAscending;
  double ascDecValue;
  List rates;
  List createdDates;
  MarketRateObj({
    @required this.baseCurrency,
    @required this.marketRate,
    @required this.variableCurrency,
    this.isRateAscending,
    this.createdDate,
    this.ascDecValue,
    this.rates, this.createdDates,
});
}