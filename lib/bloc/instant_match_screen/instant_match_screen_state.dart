

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class InstantMatchScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialInstantMatchOrderScreenState extends InstantMatchScreenState {}

class MatchingExistingOrder extends InstantMatchScreenState {}

class SuccessfullyMatchedExistingOrder extends InstantMatchScreenState {
  final PaymentSummaryOrderCard paymentSummaryOrderCard;
 SuccessfullyMatchedExistingOrder(
  {
    @required this.paymentSummaryOrderCard,
}):super();
}

class ErrorMatchingExistingOrder extends InstantMatchScreenState {
  final String errorMessage;
  ErrorMatchingExistingOrder({@required this.errorMessage = ""}):super();
}

class MatchExistingOrderSocketException extends InstantMatchScreenState {}

class MatchExistingOrderHttpException extends InstantMatchScreenState {}
