import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter/material.dart';

class InstantMatchScreenService {
  Future<HttpClientResponse> createAndMatchExistingOrder({
    @required String orderNo,
    @required String myId,
    @required String myAccountNumber,
    @required String myBankName,
    @required String bankRouteNo,
    @required String address,
    @required String city,
    @required String state,
    @required String zipCode,
    @required String country,
    @required String myCurrency,
    @required int myPaymentChannelId,
  }) async {
    try {
      var deviceId;
      if (await readFromPref('__device__token')) {
        deviceId =
            await getFromPref('__device__token') ?? await getDeviceToken();
      } else {
        deviceId = await getDeviceToken();
      }

      Map<String, dynamic> requestData = {
        'existngOrderNo': orderNo,
        'myCustomerId': myId,
        'myAccountNumber': myAccountNumber,
        'myBankName': myBankName,
        'bankRouteNo': bankRouteNo,
        "address": address ?? "nil",
        "city": city ?? "nil",
        "state": state ?? "nil",
        "zipCode": zipCode ?? "nil",
        "country": country ?? "nil",
        "myCurrency": myCurrency == 'USD' ? 2 : 1,
        "myPaymentChannelId": myPaymentChannelId ?? 8,
        "applicationType": 1,
        "deviceId": deviceId
      };

      print('matchexistingorder ${jsonEncode(requestData)}');

      HttpClientResponse response = await HttpService()
          .post(url: 'Orders/matchexistingorder', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }
}
