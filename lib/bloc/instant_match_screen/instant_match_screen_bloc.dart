import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_event.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_service.dart';
import 'package:anelloh_mobile/bloc/instant_match_screen/instant_match_screen_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/payment_summary_order_card.dart';
import 'package:anelloh_mobile/helpers/print_wrap.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class InstantMatchScreenBloc
    extends Bloc<InstantMatchScreenEvent, InstantMatchScreenState> {
  InstantMatchScreenBloc() : super(InitialInstantMatchOrderScreenState());

  InstantMatchScreenService _instantMatchScreenService =
      new InstantMatchScreenService();

  @override
  Stream<InstantMatchScreenState> mapEventToState(
      InstantMatchScreenEvent event) async* {
    if (event is CreateAndMatchExistingOrderEvent) {
      try {
        yield MatchingExistingOrder();
        HttpClientResponse response =
            await _instantMatchScreenService.createAndMatchExistingOrder(
                orderNo: event.existingOrderNo,
                myId: event.myCustomerId,
                myAccountNumber: event.myAccountNumber,
                myBankName: event.myBankName,
                bankRouteNo: event.bankRouteNo,
                address: event.address,
                city: event.city,
                state: event.state,
                zipCode: event.zipCode,
                country: event.country,
                myCurrency: event.myCurrency,
                myPaymentChannelId: event.myPaymentChannelId);

        Map<String, dynamic> result =
            await jsonDecode(await response.transform(utf8.decoder).join());

        if (response.statusCode == 200 && result['succeeded'] == true) {
            var entity = result['entity']['entity'];
            // var entity = result['entity'];

            printWrapped('What is result ${result}');

            printWrapped('What is result entity ${result['entity']['entity']}');
            // printWrapped('What is result entity ${result['entity']}');

            AppConstants.instantMatchCurrency = event.myCurrency == "USD" ? "NGN" : "USD"; // result['entity']['myCurrency']; //

            log('AppConstants.instantMatchCurrency Instant Match Screen ${AppConstants.instantMatchCurrency}');

            PaymentSummaryOrderCard paymentSummaryOrderCard =
                PaymentSummaryOrderCard(
                    customerId:
                        entity['customerId']?.toString() ?? event.myCustomerId,
                    orderNo:
                        entity['orderNo']?.toString() ?? event.existingOrderNo,
                    myAmount: entity['myAmount'].toString(),
                    totalAmount: entity['totalAmount'].toString(),
                    myCurrency: entity['myCurrency'].toString(),
                    rate: entity['rate'].toString(),
                    convertedCurrency: entity['convertedCurrency'].toString(),
                    convertedAmount: entity['convertedAmount'].toString(),
                    transactionFee: entity['transactionFee'].toString());

            yield SuccessfullyMatchedExistingOrder(
              paymentSummaryOrderCard: paymentSummaryOrderCard,
            );
          } else {
            yield ErrorMatchingExistingOrder(
                errorMessage: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
      } on SocketException {
        yield MatchExistingOrderSocketException();
      } on HttpException {
        yield MatchExistingOrderHttpException();
      } catch (err) {
        print(err);
        yield ErrorMatchingExistingOrder(errorMessage: err.toString());
      }
    }
  }
}
