import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class InstantMatchScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class CreateAndMatchExistingOrderEvent extends InstantMatchScreenEvent {
  final String existingOrderNo;
  final String myCustomerId;
  final String myAccountNumber;
  final String myBankName;
  final String bankRouteNo;
  final String address;
  final String city;
  final String state;
  final String zipCode;
  final String country;
  final String myCurrency;
  final int myPaymentChannelId;

  CreateAndMatchExistingOrderEvent({
    @required this.existingOrderNo,
    @required this.myCustomerId,
    @required this.myAccountNumber,
    @required this.myBankName,
    @required this.bankRouteNo,
    @required this.address,
    @required this.city,
    @required this.state,
    @required this.zipCode,
    @required this.country,
    @required this.myCurrency,
    @required this.myPaymentChannelId,
  }) : super();
}
