import 'dart:io';

import 'package:anelloh_mobile/bloc/notification/notification_state.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class NotificationService {
  Future<HttpClientResponse> fetchAllUnreadNotifications(
      {@required String customerId}) async {
    try {
      var notificationId = await FirebaseMessaging.instance.getToken();

      HttpClientResponse response = await HttpService().post(
          url: 'Notifications/unreadnotifications/$customerId/$notificationId',
          requestData: null);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  updateNotification({NotificationsData notificationData}) async {
    try {
      HttpClientResponse response = await HttpService().put(
          url: 'Notifications/update/${notificationData.id}',
          requestData: {
            "notificationId": notificationData.id,
            "message": notificationData.message,
            "customerId": notificationData.customerId,
            "deviceId": notificationData.deviceId,
            "applicationType": 1,
            "notificationStatus": 2
          });
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }
}
