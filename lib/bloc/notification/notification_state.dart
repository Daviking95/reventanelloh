import 'package:equatable/equatable.dart';

class NotificationState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialNotificationState extends NotificationState {}

class FetchingListings extends NotificationState {}

class FetchingNotifications extends NotificationState {}

class ErrorFetchingAllUnreadNotifications extends NotificationState {}

class UnreadNotificationsActionSocketException extends NotificationState {}

class UnreadNotificationsActionHttpException extends NotificationState {}

class SuccessfullyFetchedAllNotifications extends NotificationState {
  final List<NotificationsData> notificationsList;
  SuccessfullyFetchedAllNotifications({this.notificationsList}) : super();
}

class UpdatingNotifications extends NotificationState {}

class ErrorUpdatingAllUnreadNotifications extends NotificationState {}

class UpdatingNotificationsActionSocketException extends NotificationState {}

class UpdatingNotificationsActionHttpException extends NotificationState {}

class SuccessfullyUpdatedNotification extends NotificationState {
  final String message;
  SuccessfullyUpdatedNotification({this.message}) : super();
}

class NotificationsData {
  String _customerId;
  String _message;
  String _deviceId;
  int _notificationStatus;
  int _applicationType;
  int _id;
  String _name;
  String _createdBy;
  String _createdDate;
  String _lastModifiedBy;
  String _lastModifiedDate;
  int _status;
  String _statusDesc;

  NotificationsData(
      {String customerId,
      String message,
      String deviceId,
      int notificationStatus,
      int applicationType,
      int id,
      String name,
      String createdBy,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate,
      int status,
      String statusDesc}) {
    this._customerId = customerId;
    this._message = message;
    this._deviceId = deviceId;
    this._notificationStatus = notificationStatus;
    this._applicationType = applicationType;
    this._id = id;
    this._name = name;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._status = status;
    this._statusDesc = statusDesc;
  }

  String get customerId => _customerId;
  set customerId(String customerId) => _customerId = customerId;
  String get message => _message;
  set message(String message) => _message = message;
  String get deviceId => _deviceId;
  set deviceId(String deviceId) => _deviceId = deviceId;
  int get notificationStatus => _notificationStatus;
  set notificationStatus(int notificationStatus) =>
      _notificationStatus = notificationStatus;
  int get applicationType => _applicationType;
  set applicationType(int applicationType) =>
      _applicationType = applicationType;
  int get id => _id;
  set id(int id) => _id = id;
  String get name => _name;
  set name(String name) => _name = name;
  String get createdBy => _createdBy;
  set createdBy(String createdBy) => _createdBy = createdBy;
  String get createdDate => _createdDate;
  set createdDate(String createdDate) => _createdDate = createdDate;
  String get lastModifiedBy => _lastModifiedBy;
  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;
  String get lastModifiedDate => _lastModifiedDate;
  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;
  int get status => _status;
  set status(int status) => _status = status;
  String get statusDesc => _statusDesc;
  set statusDesc(String statusDesc) => _statusDesc = statusDesc;

  NotificationsData.fromJson(Map<String, dynamic> json) {
    _customerId = json['customerId'];
    _message = json['message'];
    _deviceId = json['deviceId'];
    _notificationStatus = json['notificationStatus'];
    _applicationType = json['applicationType'];
    _id = json['id'];
    _name = json['name'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _status = json['status'];
    _statusDesc = json['statusDesc'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['customerId'] = this._customerId;
    data['message'] = this._message;
    data['deviceId'] = this._deviceId;
    data['notificationStatus'] = this._notificationStatus;
    data['applicationType'] = this._applicationType;
    data['id'] = this._id;
    data['name'] = this._name;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['status'] = this._status;
    data['statusDesc'] = this._statusDesc;
    return data;
  }
}
