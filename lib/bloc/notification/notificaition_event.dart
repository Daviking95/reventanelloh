import 'package:anelloh_mobile/bloc/notification/notification_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class NotificationEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchNotificationsEvent extends NotificationEvent {
  final String customerId;
  FetchNotificationsEvent({@required this.customerId}) : super();
}

class UpdateNotificationsEvent extends NotificationEvent {
  final NotificationsData notificationData;
  UpdateNotificationsEvent({@required this.notificationData}) : super();
}
