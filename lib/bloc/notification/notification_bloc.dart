import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/notification/notificaition_event.dart';
import 'package:anelloh_mobile/bloc/notification/notification_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'notification_service.dart';

class NotificationBloc extends Bloc<NotificationEvent, NotificationState> {
  NotificationBloc() : super(InitialNotificationState());

  NotificationService _notificationService = NotificationService();
  @override
  Stream<NotificationState> mapEventToState(NotificationEvent event) async* {
    if (event is FetchNotificationsEvent) {
      try {
        yield FetchingNotifications();
        HttpClientResponse response = await _notificationService
            .fetchAllUnreadNotifications(customerId: event.customerId);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            List<dynamic> res = result['entity'];
            List<NotificationsData> notificationList = [];

            res.forEach((element) {
              notificationList.add(NotificationsData.fromJson(element));
            });

            yield SuccessfullyFetchedAllNotifications(
                notificationsList: notificationList);
          } else {
            yield ErrorFetchingAllUnreadNotifications();
          }
        } else {
          yield ErrorFetchingAllUnreadNotifications();
        }
      } on SocketException {
        yield UnreadNotificationsActionSocketException();
      } on HttpException {
        yield UnreadNotificationsActionHttpException();
      } catch (err) {
        if (err is SocketException) {
          yield UnreadNotificationsActionSocketException();
        }
        if (err is HttpException) {
          yield UnreadNotificationsActionHttpException();
        }
        print(err);
        yield ErrorFetchingAllUnreadNotifications();
      }
    }

    if (event is UpdateNotificationsEvent) {
      try {
        yield UpdatingNotifications();
        HttpClientResponse response = await _notificationService
            .updateNotification(notificationData: event.notificationData);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          yield SuccessfullyUpdatedNotification(
              message: "Notification updated successfully");
        } else {
          yield ErrorUpdatingAllUnreadNotifications();
        }
      } on SocketException {
        yield UpdatingNotificationsActionSocketException();
      } on HttpException {
        yield UpdatingNotificationsActionHttpException();
      } catch (err) {
        if (err is SocketException) {
          yield UpdatingNotificationsActionSocketException();
        }
        if (err is HttpException) {
          yield UpdatingNotificationsActionHttpException();
        }
        print(err);
        yield ErrorUpdatingAllUnreadNotifications();
      }
    }
  }
}
