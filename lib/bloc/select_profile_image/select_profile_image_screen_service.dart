
import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/cupertino.dart';

class SelectProfileImageScreenService {

  Future<HttpClientResponse> uploadProfileImage({@required customerId, @required File image}) async{
    try{
      var imageAsBytes = await image.readAsBytes();
      var base64Image = base64Encode(imageAsBytes);

      HttpClientResponse result = await HttpService().post(
          url: 'Customers/uploadimage',
          requestData: {
            'customerId': customerId,
            'uploadImage': base64Image
          },
      );
      return result;
    }catch(error) {
      print(error);
      return error;
    }
  }
}