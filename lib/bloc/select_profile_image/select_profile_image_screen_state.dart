
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class SelectProfileImageScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class SelectProfileImageScreenInitialState extends SelectProfileImageScreenState{}

class UploadingUserImage extends SelectProfileImageScreenState {}
class UploadImageStateChanged extends SelectProfileImageScreenState {}

class SaveUserUploadedImage extends SelectProfileImageScreenState {}

class SelectProfileImageNoInternetException extends SelectProfileImageScreenState {}
class SelectProfileImageHttpException extends SelectProfileImageScreenState {}
class ImageUploadSuccess extends SelectProfileImageScreenState {}
class ImageUploadFailed extends SelectProfileImageScreenState {}
