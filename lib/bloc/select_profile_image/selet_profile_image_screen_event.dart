
import 'dart:io';

import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class SelectProfileImageScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}
class UploadUserImageEvent extends SelectProfileImageScreenEvent {
  final File imageFile;
  final String customerId;
  UploadUserImageEvent({@required this.imageFile, @required this.customerId}):super();
}

class SaveUserUploadedImageEvent extends SelectProfileImageScreenEvent {
  final String uploadedImageUrl;
  SaveUserUploadedImageEvent({@required this.uploadedImageUrl}):super();
}