import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_service.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/select_profile_image_screen_state.dart';
import 'package:anelloh_mobile/bloc/select_profile_image/selet_profile_image_screen_event.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../constants.dart';

class SelectProfileImageScreenBloc
    extends Bloc<SelectProfileImageScreenEvent, SelectProfileImageScreenState> {
  SelectProfileImageScreenBloc()
      : super(SelectProfileImageScreenInitialState());

  SelectProfileImageScreenService _selectProfileImageScreenService =
      SelectProfileImageScreenService();

  @override
  Stream<SelectProfileImageScreenState> mapEventToState(
      SelectProfileImageScreenEvent event) async* {
    if (event is UploadUserImageEvent) {
      try {
        yield UploadingUserImage();
        HttpClientResponse response =
            await _selectProfileImageScreenService.uploadProfileImage(
                customerId: event.customerId, image: event.imageFile);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          if (result['succeeded'] == true) {
            log('Profle upload result ${jsonEncode(result)}');

            AppConstants.profileImage = result['customerImageFileLocation'];
            yield ImageUploadSuccess();
          } else {
            yield ImageUploadFailed();
          }
        } else {
          yield ImageUploadFailed();
        }
        yield UploadImageStateChanged();
      } on SocketException {
        yield SelectProfileImageNoInternetException();
      } on HttpException {
        yield SelectProfileImageHttpException();
      } catch (error) {
        yield ImageUploadFailed();
      }
    }
  }
}
