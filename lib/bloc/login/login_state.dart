import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreenState extends Equatable {
  LoginScreenState();

  @override
  List<Object> get props => [];
}

class LoginScreenInitialState extends LoginScreenState {}

class CompletedLoginAction extends LoginScreenState {
  final String message;

  CompletedLoginAction({@required this.message}) : super();
}

class LoginStateChanged extends LoginScreenState {}

class RequestingLogin extends LoginScreenState {}

class FailedLoginException extends LoginScreenState {
  final String error;

  FailedLoginException({@required this.error}) : super();
}

class MoveToSignUpScreen extends LoginScreenState {}

class MoveToForgotPasswordScreen extends LoginScreenState {}

class LoginNoInternetException extends LoginScreenState {
  final String error;

  LoginNoInternetException({this.error = "No Internet error"});
}

class LoginHttpException extends LoginScreenState {
  final String message;

  LoginHttpException({this.message}) : super();
}

class VerifyingEmail extends LoginScreenState {}

class EmailVerificationFailed extends LoginScreenState {
  final String error;

  EmailVerificationFailed({this.error = "Email verification failed"});
}

class EmailVerificationSuccess extends LoginScreenState {}

class LoggingInDeveloper extends LoginScreenState {}

class DeveloperLoginSucceeded extends LoginScreenState {
  final String message;

  DeveloperLoginSucceeded({this.message = "Login Successful"});
}

class DeveloperLoginFailed extends LoginScreenState {
  final String error;

  DeveloperLoginFailed({this.error = "Unknown error"});
}

class DeveloperLoginSocketException extends LoginScreenState {
  final String error;

  DeveloperLoginSocketException({this.error = "Unknown socket error"});
}

class DeveloperLoginHttpException extends LoginScreenState {
  final String error;

  DeveloperLoginHttpException({this.error = "Unknown http error"});
}
