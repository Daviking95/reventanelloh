import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/login/login_event.dart';
import 'package:anelloh_mobile/bloc/login/login_service.dart';
import 'package:anelloh_mobile/bloc/login/login_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:bloc/bloc.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreenBloc extends Bloc<LoginScreenEvent, LoginScreenState> {
  LoginScreenBloc() : super(LoginScreenInitialState());

  LoginService _loginService = LoginService();

  @override
  Stream<LoginScreenState> mapEventToState(LoginScreenEvent event) async* {
    try {
      if (event is MoveToSignUpScreenEvent) {
        yield MoveToSignUpScreen();
        yield LoginStateChanged();
      }

      if (event is LoginActionEvent) {
        try {
          yield RequestingLogin();
          HttpClientResponse response = await _loginService.loginUser(
              loginRequestData: event.loginRequestData);
          // print("Status Code ${response.statusCode}");
          if (response.statusCode == 200) {
            Map<String, dynamic> result =
                jsonDecode(await response.transform(utf8.decoder).join());
            // print("Status REs ${jsonEncode(result)}");

            if (result['isSuccess'] == true) {
              await saveToPref('__Anelloh_MOBL_User_Session_token__',
                  result['token']['accessToken']);

              Map<String, dynamic> payload =
                  Jwt.parseJwt(result['token']['accessToken']);

              log("Payload ${jsonEncode(payload)}");
              if (payload['email'].toString().toLowerCase() !=
                  event.loginRequestData.emailAddress.toLowerCase()) {
                yield FailedLoginException(error: "Invalid email or password");

                yield LoginStateChanged();
                return;
              }

              Map<String, dynamic> resultEntity = result['entity'];

              log('AppConstants.userData resultEntity ${resultEntity}');

              await saveToPref(
                  '__anelloh_current_user__', jsonEncode(resultEntity));
              await saveToPref(
                  '__anelloh_current_email__', resultEntity['email']);

              yield CompletedLoginAction(
                  message: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            } else {
              yield FailedLoginException(
                  error: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            }
            yield LoginStateChanged();
          } else {
            yield LoginStateChanged();
            Map<String, dynamic> result =
                jsonDecode(await response.transform(utf8.decoder).join());
            yield FailedLoginException(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } on SocketException {
          yield LoginNoInternetException();
          yield LoginStateChanged();
        } on HttpException {
          yield LoginHttpException(message: 'Service currently unavailable');
          yield LoginStateChanged();
        } catch (error) {
          if (error is Error) {
            yield FailedLoginException(
                error:
                    'Login failed due to an unknown error. Please try again');
          }
        }
      }

      if (event is LoginWithDeviceIDEvent) {
        try {
          yield RequestingLogin();
          HttpClientResponse response =
              await _loginService.loginWithDeviceId(email: event.email);
          print("Status Code ${response.statusCode}");

          if (response.statusCode == 200) {
            Map<String, dynamic> result =
                jsonDecode(await response.transform(utf8.decoder).join());
            if (result['isSuccess'] == true) {
              await saveToPref('__Anelloh_MOBL_User_Session_token__',
                  result['token']['accessToken']);

              Map<String, dynamic> payload =
                  Jwt.parseJwt(result['token']['accessToken']);

              log("Payload ${jsonEncode(payload)}");
              if (payload['email'].toString().toLowerCase() !=
                  event.email.toLowerCase()) {
                yield FailedLoginException(error: "Invalid email or password");

                yield LoginStateChanged();
                return;
              }

              Map<String, dynamic> resultEntity = result['entity'];
              await saveToPref(
                  '__anelloh_current_user__', jsonEncode(resultEntity));
              yield CompletedLoginAction(
                  message: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            } else {
              yield FailedLoginException(
                  error: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            }
            yield LoginStateChanged();
          } else {
            yield LoginStateChanged();
            Map<String, dynamic> result =
                jsonDecode(await response.transform(utf8.decoder).join());
            yield FailedLoginException(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } catch (err) {
          if (err is SocketException) {
            yield LoginNoInternetException(error: err.message);
            yield LoginStateChanged();
          }
          if (err is HttpException) {
            yield LoginHttpException(message: err.message);
            yield LoginStateChanged();
          }
          yield FailedLoginException(
              error:
                  'Login failed due to an unknown error. Please try again'); // err.toString());
        }
      }

      if (event is MoveToForgotPasswordScreenEvent) {
        yield MoveToForgotPasswordScreen();
        yield LoginStateChanged();
      }

      if (event is LoginDeveloperEvent) {
        try {
          yield LoggingInDeveloper();
          HttpClientResponse response = await _loginService.developerLogin(
              name: event.name, applicationType: 1);

          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          // log("developer login result ${jsonEncode(result)}");

          if (response.statusCode == 200) {
            if (result['isSuccess'] == true) {
              await saveToPref('__Anelloh_MOBL_User_Session_token__',
                  result['token']['accessToken']);
              // AppConstants.Anelloh_MOBL_User_Session_token__ = result['token']['accessToken'];
              yield DeveloperLoginSucceeded(
                  message: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            }
          } else {
            yield DeveloperLoginFailed(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } catch (err) {
          if (err is SocketException) {
            yield DeveloperLoginSocketException(error: err.message);
          }
          if (err is HttpException) {
            yield DeveloperLoginHttpException(error: err.message);
          }
          yield DeveloperLoginFailed();
        }
      }

      if (event is VerifyEmailEvent) {
        try {
          yield VerifyingEmail();
          HttpClientResponse response = await _loginService.verifyEmail(
              emailAddress: event.emailAddress, token: event.token);

          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          log("EmailVerify Result ${jsonEncode(result)}");

          if (response.statusCode == 200) {
            SharedPreferences _pref = await SharedPreferences.getInstance();
            await _pref.setString('_EMAIL_VERIFY_', '');
            if (result['succeeded'] == true) {
              yield EmailVerificationSuccess();
            } else {
              yield EmailVerificationFailed(
                  error: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            }
          } else {
            yield EmailVerificationFailed(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } catch (err) {
          if (err is SocketException) {
            yield LoginNoInternetException(error: err.message);
          }
          if (err is HttpException) {
            yield LoginHttpException(message: err.message);
          }
          yield EmailVerificationFailed(error: "Unknown error");
        }
      }
    } on SocketException {
      yield LoginNoInternetException();
      yield LoginStateChanged();
    } on HttpException {
      yield LoginHttpException(message: 'Service currently unavailable');
      yield LoginStateChanged();
    } catch (error) {
      yield LoginStateChanged();
      yield FailedLoginException(error: 'Server error');
    }
  }
}
