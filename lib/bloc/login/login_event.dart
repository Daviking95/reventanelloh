import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class LoginScreenEvent extends Equatable {
  LoginScreenEvent();

  @override
  List<Object> get props => [];
}

class MoveToSignUpScreenEvent extends LoginScreenEvent {}

class MoveToForgotPasswordScreenEvent extends LoginScreenEvent {}

class LoginActionEvent extends LoginScreenEvent {
  final LoginRequestData loginRequestData;

  LoginActionEvent({@required this.loginRequestData}) : super();
}

class LoginWithDeviceIDEvent extends LoginScreenEvent {
  final String email;
  LoginWithDeviceIDEvent({@required this.email}) : super();
}

class LoginDeveloperEvent extends LoginScreenEvent {
  final String name;
  LoginDeveloperEvent({@required this.name}) : super();
}

class VerifyEmailEvent extends LoginScreenEvent {
  final String emailAddress;
  final String token;
  VerifyEmailEvent({@required this.emailAddress, this.token}) : super();
}

// CLASSES PERTAINING TO LOGIN

class LoginRequestData {
  final String emailAddress;
  final String password;

  LoginRequestData({
    @required this.emailAddress,
    @required this.password,
  });
}
