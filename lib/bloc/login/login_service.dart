import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/login/login_event.dart';
import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';

class LoginService {
  Future<HttpClientResponse> loginUser(
      {LoginRequestData loginRequestData}) async {
    try {
      var deviceId = await getDeviceId();

      Map<String, dynamic> loginData = {
        'email': loginRequestData.emailAddress,
        'password': loginRequestData.password,
        'applicationType': 1,
        'deviceId': deviceId,
        'notificationId': await FirebaseMessaging.instance.getToken(),
      };

      print('loginData $loginData');

      HttpClientResponse result = await HttpService()
          .post(url: 'Authentication/login', requestData: loginData);

      return result;
    } on SocketException {
      throw new SocketException('No Internet Connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (error) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> developerLogin(
      {@required String name, @required int applicationType}) async {
    try {
      Map<String, dynamic> requestData = {
        'name': name,
        'applicationType': applicationType
      };

      // log("developer login Request ${jsonEncode(requestData)}");

      HttpClientResponse response = await HttpService()
          .post(url: 'ApplicationAuth/login', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> verifyEmail(
      {@required String emailAddress, String token}) async {
    try {
      // log('verifyEmail $verifyEmail');
      // log('verifyEmailtoken Customers/emailConfirmation?Email=$emailAddress&Token=$token');

      HttpClientResponse response = await HttpService().post(
          url: 'Customers/emailConfirmation?Email=$emailAddress&Token=$token',
          requestData: {});
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> loginWithDeviceId({@required String email}) async {
    try {
      var deviceId = await getDeviceId();

      Map<String, dynamic> requestData = {
        'email': email,
        'applicationType': 1,
        'deviceId': deviceId,
        'notificationId': await FirebaseMessaging.instance.getToken(),
      };

      // log('loginRequestData ${jsonEncode(requestData)}');

      HttpClientResponse response = await HttpService().post(
          url: 'Authentication/LoginByDeviceId', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }
}
