import 'package:anelloh_mobile/widgets/line_chart.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class CreateOrderScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FindMatchEvent extends CreateOrderScreenEvent {
  final CreateOrderObj createOrderObj;

  FindMatchEvent({@required this.createOrderObj}) : super();
}

class MoveToCreateOrderScreenE extends CreateOrderScreenEvent {}

class FetchActiveCurrenciesEvent extends CreateOrderScreenEvent {}

class FetchTransactionLimitEvent extends CreateOrderScreenEvent {}

class MoveToFindMatchScreenEvent extends CreateOrderScreenEvent {
  final MatchFoundObj matchFoundObj;

  MoveToFindMatchScreenEvent({@required this.matchFoundObj}) : super();
}

class MoveToMatchNotFoundScreenEvent extends CreateOrderScreenEvent {}

class CreateOrderActionEvent extends CreateOrderScreenEvent {
  final CreateOrderObj createOrderObj;

  CreateOrderActionEvent({@required this.createOrderObj}) : super();
}

// CLASSES ASSOCIATED WITH CREATE ORDER SCREEN

class CreateOrderObj {
  final String customerId;
  final int myAmount;
  final String myCurrency;
  final int rate;
  final String myAccountNumber;
  final String myBankName;
  final String bankRouteNo;
  final String convertedCurrency;
  final int convertedAmount;
  final String address;
  final String city;
  final String state;
  final String zipCode;
  final String country;
  final int myPaymentChannelId;

  CreateOrderObj({
    this.customerId,
    this.myAmount,
    this.myCurrency,
    this.rate,
    this.myAccountNumber,
    this.myBankName,
    this.bankRouteNo,
    this.convertedCurrency,
    this.convertedAmount,
    this.address,
    this.city,
    this.state,
    this.zipCode,
    this.country,
    this.myPaymentChannelId,
  });

  CreateOrderObj copyWith(
      {String customerId,
      int myAmount,
      String myCurrency,
      int rate,
      String myAccountNumber,
      String myBankName,
      String bankRouteNo,
      String convertedCurrency,
      String address,
      String city,
      String state,
      String zipCode,
      String country,
      int myPaymentChannelId,
      int convertedAmount}) {
    return CreateOrderObj(
        customerId: customerId ?? this.customerId,
        myAmount: myAmount ?? this.myAmount,
        myCurrency: myCurrency ?? this.myCurrency,
        rate: rate ?? this.rate,
        myAccountNumber: myAccountNumber ?? this.myAccountNumber,
        myBankName: myBankName ?? this.myBankName,
        bankRouteNo: bankRouteNo ?? this.bankRouteNo,
        convertedCurrency: convertedCurrency ?? this.convertedCurrency,
        address: address ?? "",
        city: city ?? "",
        state: state ?? "",
        zipCode: zipCode ?? "",
        country: country ?? "",
        myPaymentChannelId: myPaymentChannelId ?? 0,
        convertedAmount: convertedAmount ?? this.convertedAmount);
  }
}

class MatchFoundObj {
  final String entityId;
  final String customerId;
  final String orderOwnerId;
  final String orderNo;
  final String myCurrency;
  final String convertedCurrency;
  final String myAmount;
  final String convertedAmount;
  final String exchangeRate;
  final String userName;
  final String userEmail;
  final List<charts.Series<DateRateRow, DateTime>> rates;
  final bool isRateAscending;
  final double ascDecValue;
  final String subTotal;
  final String processingFee;
  final String totalAmount;
  final String status;
  final String convertedPaymentChannelId;
  final String convertedPaymentReferenceNo;
  final String convertedPaymentResponse;
  final String convertedPaymentError;
  final String transactionFeeCurrency;
  final int orderStatus;
  final String myAccountNumber;
  final String myBankName;
  final String bankRouteNo;

  MatchFoundObj({
    @required this.entityId,
    @required this.customerId,
    @required this.orderOwnerId,
    @required this.orderNo,
    @required this.myCurrency,
    @required this.convertedCurrency,
    @required this.myAmount,
    @required this.convertedAmount,
    @required this.exchangeRate,
    @required this.userName,
    @required this.userEmail,
    @required this.rates,
    @required this.isRateAscending,
    @required this.ascDecValue,
    @required this.subTotal,
    @required this.processingFee,
    @required this.totalAmount,
    @required this.orderStatus,
    this.status,
    this.convertedPaymentChannelId,
    this.convertedPaymentError,
    this.convertedPaymentReferenceNo,
    this.convertedPaymentResponse,
    this.transactionFeeCurrency,
    this.myAccountNumber,
    this.myBankName,
    this.bankRouteNo,
  });

  MatchFoundObj copyWith({
    String entityId,
    String customerId,
    String orderOwnerId,
    String orderNo,
    String myAmount,
    String convertedAmount,
    String myCurrency,
    String convertedCurrency,
    String exchangeRate,
    String userName,
    String userEmail,
    List<charts.Series<DateRateRow, DateTime>> rates,
    bool isRateAscending,
    double ascDecValue,
    String subTotal,
    String processingFee,
    String totalAmount,
    int orderStatus,
    String myAccountNumber,
    String myBankName,
    String bankRouteNo,
    String status,
  }) {
    return MatchFoundObj(
        entityId: entityId ?? this.entityId,
        customerId: customerId ?? this.customerId,
        orderOwnerId: orderOwnerId ?? this.orderOwnerId,
        orderNo: orderNo ?? this.orderNo,
        myCurrency: myCurrency ?? this.myCurrency,
        convertedCurrency: convertedCurrency ?? this.convertedCurrency,
        convertedAmount: convertedAmount ?? this.convertedAmount,
        myAmount: myAmount ?? this.myAmount,
        exchangeRate: exchangeRate ?? this.exchangeRate,
        userName: userName ?? this.userName,
        userEmail: userEmail ?? this.userEmail,
        rates: rates ?? this.rates,
        isRateAscending: isRateAscending ?? this.isRateAscending,
        ascDecValue: ascDecValue ?? this.ascDecValue,
        subTotal: subTotal ?? this.subTotal,
        processingFee: processingFee ?? this.processingFee,
        totalAmount: totalAmount ?? this.totalAmount,
        orderStatus: orderStatus ?? this.orderStatus,
        myAccountNumber: myAccountNumber ?? this.myAccountNumber,
        myBankName: myBankName ?? this.myBankName,
        bankRouteNo: bankRouteNo ?? this.bankRouteNo,
        status: status ?? this.status);
  }
}

// Active Payments

class ActiveCurrenciesData {
  int _id;
  int _currencyCode;
  String _currencyCodeString;
  String _createdBy;
  String _createdDate;
  String _lastModifiedBy;
  String _lastModifiedDate;
  int _status;

  ActiveCurrenciesData(
      {int id,
      int currencyCode,
      String currencyCodeString,
      String createdBy,
      String createdDate,
      String lastModifiedBy,
      String lastModifiedDate,
      int status}) {
    this._id = id;
    this._currencyCode = currencyCode;
    this._currencyCodeString = currencyCodeString;
    this._createdBy = createdBy;
    this._createdDate = createdDate;
    this._lastModifiedBy = lastModifiedBy;
    this._lastModifiedDate = lastModifiedDate;
    this._status = status;
  }

  int get id => _id;

  set id(int id) => _id = id;

  int get currencyCode => _currencyCode;

  set currencyCode(int currencyCode) => _currencyCode = currencyCode;

  String get currencyCodeString => _currencyCodeString;

  set currencyCodeString(String currencyCodeString) =>
      _currencyCodeString = currencyCodeString;

  String get createdBy => _createdBy;

  set createdBy(String createdBy) => _createdBy = createdBy;

  String get createdDate => _createdDate;

  set createdDate(String createdDate) => _createdDate = createdDate;

  String get lastModifiedBy => _lastModifiedBy;

  set lastModifiedBy(String lastModifiedBy) => _lastModifiedBy = lastModifiedBy;

  String get lastModifiedDate => _lastModifiedDate;

  set lastModifiedDate(String lastModifiedDate) =>
      _lastModifiedDate = lastModifiedDate;

  int get status => _status;

  set status(int status) => _status = status;

  ActiveCurrenciesData.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _currencyCode = json['currencyCode'];
    _currencyCodeString = json['currencyCodeString'];
    _createdBy = json['createdBy'];
    _createdDate = json['createdDate'];
    _lastModifiedBy = json['lastModifiedBy'];
    _lastModifiedDate = json['lastModifiedDate'];
    _status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['currencyCode'] = this._currencyCode;
    data['currencyCodeString'] = this._currencyCodeString;
    data['createdBy'] = this._createdBy;
    data['createdDate'] = this._createdDate;
    data['lastModifiedBy'] = this._lastModifiedBy;
    data['lastModifiedDate'] = this._lastModifiedDate;
    data['status'] = this._status;
    return data;
  }
}
