import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/convert_currency_code.dart';
import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/print_wrap.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter/material.dart';

class CreateOrderService {
  // publish order
  Future<HttpClientResponse> createOrder(
      {@required CreateOrderObj createOrderObj}) async {
    try {
      var deviceId;
      if (await readFromPref('__device__token')) {
        deviceId =
            await getFromPref('__device__token') ?? await getDeviceToken();
      } else {
        deviceId = await getDeviceToken();
      }

      // print('myPaymentId ${createOrderObj?.myPaymentChannelId}');

      Map<String, dynamic> requestData = {
        'customerId': createOrderObj.customerId,
        'myAmount': createOrderObj.myAmount,
        'myCurrency':
            convertCurrencyCode(currencyCode: createOrderObj.myCurrency),
        'rate': createOrderObj.rate,
        "myPaymentChannelId": createOrderObj?.myPaymentChannelId == 0
            ? 8
            : createOrderObj?.myPaymentChannelId ?? 8,
        'convertedCurrency':
            convertCurrencyCode(currencyCode: createOrderObj.convertedCurrency),
        'convertedAmount': createOrderObj.convertedAmount,
        'myAccountNumber': createOrderObj.myAccountNumber,
        'myBankName': createOrderObj?.myBankName ?? "",
        'bankRouteNo': createOrderObj.bankRouteNo.isEmpty ? "0" : createOrderObj.bankRouteNo,
        "applicationType": 1,
        "deviceId": deviceId ?? "",
        "address": createOrderObj?.address ?? "",
        "city": createOrderObj?.city ?? "",
        "state": createOrderObj?.state ?? "",
        "zipCode": createOrderObj?.zipCode ?? "",
        "country": createOrderObj?.country ?? "",
      };

      print('Create Order Request ${jsonEncode(requestData)}');

      // return null;

      HttpClientResponse response = await HttpService()
          .post(url: 'Orders/create', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> findMatch(
      {@required CreateOrderObj createOrderObj}) async {
    try {
      Map<String, dynamic> requestData = {
        "customerId": createOrderObj.customerId,
        'myAmount': createOrderObj.myAmount,
        'myCurrency':
            convertCurrencyCode(currencyCode: createOrderObj.myCurrency),
        'rate': createOrderObj.rate,
        'convertedCurrency':
            convertCurrencyCode(currencyCode: createOrderObj.convertedCurrency),
        'convertedAmount': createOrderObj.convertedAmount
      };
      HttpClientResponse response = await HttpService()
          .post(url: 'Orders/findmatch', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> findActiveCurrencies() async {
    try {
      HttpClientResponse response = await HttpService()
          .post(url: 'Configurations/getactivecurrencies', requestData: null);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> getTransactionLimits() async {
    try {
      HttpClientResponse response = await HttpService()
          .post(url: 'Configurations/getalltransactionlimits', requestData: null);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }
}
