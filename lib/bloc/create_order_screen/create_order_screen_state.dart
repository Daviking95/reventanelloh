import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/classes/transaction_limit.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class CreateOrderScreenState extends Equatable {
  List<Object> get props => [];
}

class InitialCreateOrderScreenState extends CreateOrderScreenState {}

class FindingMatch extends CreateOrderScreenState {}

class FindingActiveCurrencies extends CreateOrderScreenState {}

class FindingTransactionLimit extends CreateOrderScreenState {}

class FindingMatchSuccessful extends CreateOrderScreenState {}

class FindingTransactionLimitSuccessful extends CreateOrderScreenState {
  final List<TransactionLimitResponseModelEntity> transactionLimitArray;
  FindingTransactionLimitSuccessful({@required this.transactionLimitArray}) : super();
}

class FindingActiveCurrenciesSuccessful extends CreateOrderScreenState {}

class FindingMatchFailed extends CreateOrderScreenState {}

class FindingTransactionLimitFailed extends CreateOrderScreenState {}

class FindingActiveCurrenciesFailed extends CreateOrderScreenState {}

class FindingMatchSocketException extends CreateOrderScreenState {}

class FindingTransactionLimitSocketException extends CreateOrderScreenState {}

class FindingActiveCurrenciesSocketException extends CreateOrderScreenState {}

class FindingMatchHttpException extends CreateOrderScreenState {}

class FindingTransactionLimitHttpException extends CreateOrderScreenState {}

class FindingActiveCurrenciesHttpException extends CreateOrderScreenState {}

class MoveToCreateOrderScreenS extends CreateOrderScreenState {}

class MoveToFindMatchScreen extends CreateOrderScreenState {
  final MatchFoundObj matchFoundObj;
  MoveToFindMatchScreen({@required this.matchFoundObj}) : super();
}

class ActiveCurrenciesListScreen extends CreateOrderScreenState {
  final List<ActiveCurrenciesData> activeCurrenciesData;
  ActiveCurrenciesListScreen({@required this.activeCurrenciesData}) : super();
}

class MoveToMatchNotFoundScreen extends CreateOrderScreenState {
  final CreateOrderObj createOrderObj;
  MoveToMatchNotFoundScreen({@required this.createOrderObj}) : super();
}

class CreateOrderStateChanged extends CreateOrderScreenState {}

class CreatingOrder extends CreateOrderScreenState {}

class CreatingOrderSuccessful extends CreateOrderScreenState {}

class CreatingOrderFailed extends CreateOrderScreenState {
  final String message;
  CreatingOrderFailed(
      {this.message = "Create order failed due to unknown error"})
      : super();
}

class CreatingOrderSocketException extends CreateOrderScreenState {}

class CreatingOrderHttpException extends CreateOrderScreenState {}
