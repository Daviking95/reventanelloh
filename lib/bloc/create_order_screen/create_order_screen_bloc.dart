import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_service.dart';
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/transaction_limit.dart';
import 'package:anelloh_mobile/helpers/print_wrap.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CreateOrderScreenBloc
    extends Bloc<CreateOrderScreenEvent, CreateOrderScreenState> {
  CreateOrderScreenBloc() : super(InitialCreateOrderScreenState());

  CreateOrderService _createOrderService = new CreateOrderService();

  @override
  Stream<CreateOrderScreenState> mapEventToState(
      CreateOrderScreenEvent event) async* {
    if (event is MoveToFindMatchScreenEvent) {
      yield MoveToFindMatchScreen(matchFoundObj: event.matchFoundObj);
      yield CreateOrderStateChanged();
    }
    if (event is MoveToCreateOrderScreenE) {
      yield MoveToCreateOrderScreenS();
    }

    if (event is FetchActiveCurrenciesEvent) {
      try {
        yield FindingActiveCurrencies();
        HttpClientResponse response =
            await _createOrderService.findActiveCurrencies();

        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        if (response.statusCode == 200) {
          if (result['entity'] == null) {
            yield FindingActiveCurrenciesSuccessful();
            yield ActiveCurrenciesListScreen(activeCurrenciesData: null);
          } else {
            List<dynamic> res = result['entity'];
            List<ActiveCurrenciesData> activeCurrenciesData = [];
            res.forEach((element) {
              activeCurrenciesData.add(ActiveCurrenciesData.fromJson(element));
            });
            yield ActiveCurrenciesListScreen(
                activeCurrenciesData: activeCurrenciesData);
            yield FindingActiveCurrenciesSuccessful();
          }
        } else {
          yield FindingActiveCurrenciesFailed();
        }
      } on SocketException {
        yield FindingActiveCurrenciesSocketException();
      } on HttpException {
        yield FindingActiveCurrenciesHttpException();
      } catch (err) {
        print(err);
        yield FindingActiveCurrenciesFailed();
      }
    }

    if (event is FindMatchEvent) {
      try {
        yield FindingMatch();
        HttpClientResponse response = await _createOrderService.findMatch(
            createOrderObj: event.createOrderObj);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          if (result['entity'] == null) {
            yield FindingMatchSuccessful();
            yield MoveToMatchNotFoundScreen(
                createOrderObj: event.createOrderObj);
          } else {
            var res = result['entity'];
            log('FetchingListingById ${jsonEncode(res)}');
            yield FindingMatchSuccessful();
            MatchFoundObj matchFoundObj = MatchFoundObj(
                entityId: res['matchResult']['id'].toString(),
                customerId: res['matchResult']['customerId'].toString(),
                orderOwnerId: res['matchResult']['customerId'].toString(),
                orderNo: res['matchResult']['orderNo'].toString(),
                exchangeRate: res['matchResult']['rate'].toString(),
                convertedAmount:
                    res['paymentSummary']['myConvertedAmount'].toString(),
                convertedCurrency:
                    res['paymentSummary']['myConvertedCurrency'].toString(),
                myCurrency: res['paymentSummary']['myCurrency'].toString(),
                myAmount: res['paymentSummary']['myAmount'].toString(),
                totalAmount: res['paymentSummary']['totalAmount'].toString(),
                processingFee:
                    res['paymentSummary']['transactionFee'].toString(),
                status: res['matchResult']['status'].toString(),
                subTotal: res['convertedAmount'].toString(),
                userEmail: res['matchResult']['email'], // add to api
                userName: res['matchResult']['userName'], // add to api
                convertedPaymentChannelId:
                    res['matchResult']['convertedPaymentChannelId'].toString(),
                convertedPaymentReferenceNo: res['matchResult']
                        ['convertedPaymentReferenceNo']
                    .toString(),
                convertedPaymentResponse:
                    res['matchResult']['convertedPaymentResponse'].toString(),
                convertedPaymentError:
                    res['matchResult']['convertedPaymentError'].toString(),
                transactionFeeCurrency:
                    res['transactionFeeCurrency'].toString(),
                rates: [], // add to api
                ascDecValue: res['rateChange'] ?? 4.0, //add to api
                isRateAscending: true,
                orderStatus: res['matchResult']['orderStatus'] // add to api
                );
            yield MoveToFindMatchScreen(matchFoundObj: matchFoundObj);
          }
        } else {
          yield FindingMatchFailed();
        }
      } on SocketException {
        yield FindingMatchSocketException();
      } on HttpException {
        yield FindingMatchHttpException();
      } catch (err) {
        print(err);
        yield FindingMatchFailed();
      }
    }

    if (event is CreateOrderActionEvent) {
      try {
        yield CreatingOrder();
        HttpClientResponse response = await _createOrderService.createOrder(
            createOrderObj: event.createOrderObj);

        printWrapped('Create Order Status ${response.statusCode}');

        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        log('Create Order Result ${jsonEncode(result)}');

        if (response.statusCode == 200) {
          if (result['succeeded'] == false) {
            yield CreatingOrderFailed(
                message: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          } else {
            yield CreatingOrderSuccessful();
          }
        } else {
          yield CreatingOrderFailed(
              message: result['message'] ??
                  result['entity'] ??
                  result['messages'][0]);
        }
      } on SocketException {
        yield CreatingOrderSocketException();
      } on HttpException {
        yield CreatingOrderHttpException();
      } catch (err) {
        print(err);
        throw new Error();
      }
    }

    if (event is FetchTransactionLimitEvent) {
      try {
        yield FindingTransactionLimit();
        HttpClientResponse response =
        await _createOrderService.getTransactionLimits();

        Map<String, dynamic> result =
        jsonDecode(await response.transform(utf8.decoder).join());

        if (response.statusCode == 200) {
          if (result['entity'] == null) {
            yield FindingTransactionLimitSuccessful(transactionLimitArray: []);
          } else {
            List<dynamic> res = result['entity'];
            List<TransactionLimitResponseModelEntity> transactionLimitData = [];
            res.forEach((element) {
              transactionLimitData.add(TransactionLimitResponseModelEntity.fromJson(element));
            });

            AppConstants.transactionLimitArray = transactionLimitData;

            log('AppConstants.transactionLimitArray ${AppConstants.transactionLimitArray}');

            yield FindingTransactionLimitSuccessful(
                transactionLimitArray: transactionLimitData);
            // yield FindingTransactionLimitSuccessful();
          }
        } else {
          yield FindingTransactionLimitFailed();
        }
      } on SocketException {
        yield FindingTransactionLimitSocketException();
      } on HttpException {
        yield FindingTransactionLimitHttpException();
      } catch (err) {
        print(err);
        yield FindingTransactionLimitFailed();
      }
    }
  }
}
