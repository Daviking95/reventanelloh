import 'dart:io';

import 'package:anelloh_mobile/bloc/signup/signup_event.dart';
import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';

class SignUpService {
  Future<HttpClientResponse> createAccount(
      {SignUpRequestData requestData}) async {
    try {
      var deviceId;
      if (await readFromPref('__device__token')) {
        deviceId =
            await getFromPref('__device__token') ?? await getDeviceToken();
      } else {
        deviceId = await getDeviceToken();
      }

      Map<String, dynamic> signUpData = {
        'firstName': requestData.firstName,
        'userName': requestData.userName,
        'lastName': requestData.lastName,
        'email': requestData.emailAddress,
        'bvn': requestData.bvn.isNotEmpty ? int.parse(requestData.bvn) : 0,
        'gender': 0,
        'phoneNumber': requestData.phoneNumber.toString(),
        'address': 'Null',
        'dateOfBirth': DateTime.now().toIso8601String(),
        'password': requestData.password,
        'countryCode': requestData.countryCode,
        'documentType': requestData.documentType,
        'documentNumber': requestData.documentNumber,
        'documentExpiryDate': requestData.documentExpiryDate,
        'deviceId': deviceId,
        "applicationType": 1,
        "notificationId": await FirebaseMessaging.instance.getToken(),
        "ssn": requestData.ssn,
        'documentImage': requestData.documentImage,
      };

      // print("Account Creation Request ${jsonEncode(signUpData)}");

      HttpClientResponse response = await HttpService()
          .post(url: 'Customers/create', requestData: signUpData);
      return response;
    } on SocketException {
      throw new SocketException('Not Connected to the internet');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (error) {
      print(error);
      throw new Error();
    }
  }

  Future<HttpClientResponse> verifyEmailAndUserName(
      {@required String email, @required String userName}) async {
    try {
      // log('Customers/getbyUsernameAndEmail/$userName/$email');
      HttpClientResponse response = await HttpService().post(
          url: 'Customers/getbyUsernameAndEmail/$userName/$email',
          requestData: '');
      return response;
    } on SocketException {
      throw new SocketException('Not Connected to the internet');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }
}
