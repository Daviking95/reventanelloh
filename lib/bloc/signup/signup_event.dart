import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class SignUpScreenEvent extends Equatable {
  SignUpScreenEvent();
  @override
  List<Object> get props => [];
}

class MoveToLoginScreenEvent extends SignUpScreenEvent {}

class MoveToOTPScreenEvent extends SignUpScreenEvent {
  final String token;
  MoveToOTPScreenEvent({this.token}) : super();
}

class VerifyEmailAndUserName extends SignUpScreenEvent {
  final String email;
  final String userName;
  VerifyEmailAndUserName({@required this.email, @required this.userName})
      : super();
}

class CreateAccountEvent extends SignUpScreenEvent {
  final SignUpRequestData signUpRequestData;

  CreateAccountEvent({
    @required this.signUpRequestData,
  }) : super();
}

// CLASSES PERTAINING TO SIGN UP
class SignUpRequestData {
  final String firstName;
  final String lastName;
  final String userName;
  final String countryCode;
  final String emailAddress;
  final String phoneNumber;
  final String password;
  final String bvn;
  final int documentType;
  final String documentNumber;
  final dynamic documentImage;
  final String documentExpiryDate;
  final String ssn;

  SignUpRequestData(
      {@required this.firstName,
      @required this.lastName,
      this.userName,
      this.bvn,
      @required this.documentType,
      this.documentImage,
      this.documentExpiryDate,
      @required this.documentNumber,
      @required this.countryCode,
      @required this.emailAddress,
      @required this.phoneNumber,
      @required this.password,
      this.ssn});
}
