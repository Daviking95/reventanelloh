import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class SignUpScreenState extends Equatable {
  SignUpScreenState();
  @override
  List<Object> get props => [];
}

class CompletedSignUpAction extends SignUpScreenState {}

class RequestingSignUp extends SignUpScreenState {}

class FailedSignUpException extends SignUpScreenState {
  final String error;
  FailedSignUpException({@required this.error}) : super();
}

class SignUpScreenInitialState extends SignUpScreenState {}

class SignUpStateChanged extends SignUpScreenState {}

class SignUpNoInternetException extends SignUpScreenState {
  final String error;
  SignUpNoInternetException({this.error = "No Internet Error"}) : super();
}

class SignUpHttpException extends SignUpScreenState {
  final String error;
  SignUpHttpException({this.error}) : super();
}

class VerifyingEmailAndUsername extends SignUpScreenState {}

class EmailAndUsernameVerifySuccess extends SignUpScreenState {
  final String message;
  EmailAndUsernameVerifySuccess({this.message = ""}) : super();
}

class EmailAndUsernameVerifyFailed extends SignUpScreenState {
  final String error;
  EmailAndUsernameVerifyFailed({this.error = "Unknown Error"}) : super();
}

class MoveToLoginScreen extends SignUpScreenState {}

class MoveToOTPScreen extends SignUpScreenState {
  final String token;
  MoveToOTPScreen({this.token}) : super();
}

class SignUpAccountCreated extends SignUpScreenState {}
