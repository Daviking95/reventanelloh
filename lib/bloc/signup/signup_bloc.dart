import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/signup/signup_event.dart';
import 'package:anelloh_mobile/bloc/signup/signup_service.dart';
import 'package:anelloh_mobile/bloc/signup/signup_state.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SignUpScreenBloc extends Bloc<SignUpScreenEvent, SignUpScreenState> {
  SignUpScreenBloc() : super(SignUpScreenInitialState());

  SignUpService _signUpService = SignUpService();

  @override
  Stream<SignUpScreenState> mapEventToState(SignUpScreenEvent event) async* {
    try {
      if (event is CreateAccountEvent) {
        try {
          yield RequestingSignUp();
          HttpClientResponse response = await _signUpService.createAccount(
              requestData: event.signUpRequestData);
          yield SignUpStateChanged();

          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          if (response.statusCode == 200) {
            // print("Account Creation Response ${jsonEncode(result)}");

            if (result['succeeded'] == true) {
              Map<String, dynamic> resultEntity = result['entity'];
              await saveToPref(
                  '__anelloh_current_user__', jsonEncode(resultEntity));
              yield SignUpAccountCreated();
            } else {
              yield FailedSignUpException(
                  error: result['message'] ??
                      result['entity'] ??
                      result['messages'][0]);
            }
          }
        } catch (err) {
          if (err is SocketException) {
            yield SignUpNoInternetException(error: err.message);
            yield SignUpStateChanged();
          }
          if (err is HttpException) {
            yield SignUpHttpException(error: err.message);
            yield SignUpStateChanged();
          }
        }
      }

      if (event is MoveToLoginScreenEvent) {
        yield MoveToLoginScreen();
        yield SignUpStateChanged();
      }

      if (event is MoveToOTPScreenEvent) {
        yield MoveToOTPScreen(token: event.token);
        yield SignUpStateChanged();
      }

      if (event is VerifyEmailAndUserName) {
        yield VerifyingEmailAndUsername();
        HttpClientResponse response =
            await _signUpService.verifyEmailAndUserName(
                email: event.email, userName: event.userName);

        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            yield EmailAndUsernameVerifySuccess(
                message: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
            yield SignUpStateChanged();
          } else {
            yield EmailAndUsernameVerifyFailed(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
            yield SignUpStateChanged();
          }
        } else {
          yield EmailAndUsernameVerifyFailed(
              error: result['message'] ??
                  result['entity'] ??
                  result['messages'][0]);
          yield SignUpStateChanged();
        }
      }
    } on SocketException {
      yield SignUpNoInternetException();
      yield SignUpStateChanged();
    } on HttpException {
      yield SignUpHttpException(error: 'Service currently unavailable');
      yield SignUpStateChanged();
    } catch (error) {
      print(error);
      if (error is Error) {
        yield FailedSignUpException(error: error.toString());
      }
    }
  }
}
