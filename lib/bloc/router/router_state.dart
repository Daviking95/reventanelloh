import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class RouterState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialRouterState extends RouterState {}

class RouterMoveToCreateOrderScreen extends RouterState {
  final EditOrderObj editOrderObj;
  RouterMoveToCreateOrderScreen({this.editOrderObj}) : super();
}

class RouteNavigationChanged extends RouterState {}

class RouterMoveToDashboardScreen extends RouterState {}

class RouterMoveToCreateAccountScreen extends RouterState {}

class RouterMoveToAllListingScreen extends RouterState {}

class RouterMoveToProfileScreen extends RouterState {}

class RouterMoveToNotificationScreen extends RouterState {}

class RouterMoveToChangePasswordScreen extends RouterState {}

class RouterMoveToPinAndFaceIDScreen extends RouterState {}

class RouterMoveToPassportIdScreen extends RouterState {}

class RouterMoveToPaymentInfoScreen extends RouterState {
  final MatchFoundObj matchFoundObj;
  RouterMoveToPaymentInfoScreen({@required this.matchFoundObj}) : super();
}

class RouterMoveToPaymentSummaryScreen extends RouterState {
  final MatchFoundObj matchFoundObj;
  final EditOrderObj editOrderObj;
  RouterMoveToPaymentSummaryScreen(
      {@required this.matchFoundObj, @required this.editOrderObj})
      : super();
}
