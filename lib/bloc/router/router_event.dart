import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class RouterEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RouterMoveToCreateOrderScreenEvent extends RouterEvent {
  final EditOrderObj editOrderObj;

  RouterMoveToCreateOrderScreenEvent(
      {this.editOrderObj})
      : super();
}

class RouterMoveToDashboardScreenEvent extends RouterEvent {}

class RouterMoveToCreateAccountScreenEvent extends RouterEvent {}

class RouterMoveToMatchFoundScreenEvent extends RouterEvent {
  final MatchFoundObj matchFoundObj;
  RouterMoveToMatchFoundScreenEvent({@required this.matchFoundObj}) : super();
}

class RouterMoveToAllListingScreenEvent extends RouterEvent {}

class RouterMoveToPaymentInfoScreenEvent extends RouterEvent {
  final MatchFoundObj matchFoundObj;
  RouterMoveToPaymentInfoScreenEvent({@required this.matchFoundObj}) : super();
}

class RouterMoveToPaymentSummaryScreenEvent extends RouterEvent {
  final MatchFoundObj matchFoundObj;
  final EditOrderObj editOrderObj;

  RouterMoveToPaymentSummaryScreenEvent(
      {@required this.matchFoundObj, @required this.editOrderObj})
      : super();
}

class RouterMoveToProfileScreenEvent extends RouterEvent {}

class RouterMoveToNotificationScreenEvent extends RouterEvent {}

class RouterMoveToChangePasswordScreenEvent extends RouterEvent {}

class RouterMoveToPinFaceIdScreenEvent extends RouterEvent {}

class RouterMoveToPassportIdScreenEvent extends RouterEvent {}
