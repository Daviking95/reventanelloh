import 'package:anelloh_mobile/bloc/router/router_event.dart';
import 'package:anelloh_mobile/bloc/router/router_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class RouterBloc extends Bloc<RouterEvent, RouterState> {
  RouterBloc() : super(InitialRouterState());

  @override
  Stream<RouterState> mapEventToState(RouterEvent event) async* {
    if (event is RouterMoveToCreateOrderScreenEvent) {
      yield RouterMoveToCreateOrderScreen(editOrderObj : event.editOrderObj);
      yield RouteNavigationChanged();
    }
    if (event is RouterMoveToCreateAccountScreenEvent) {
      yield RouterMoveToCreateAccountScreen();
      yield RouteNavigationChanged();
    }
    if (event is RouterMoveToDashboardScreenEvent) {
      yield RouterMoveToDashboardScreen();
      yield RouteNavigationChanged();
    }
    if (event is RouterMoveToAllListingScreenEvent) {
      yield RouterMoveToAllListingScreen();
      yield RouteNavigationChanged();
    }
    if (event is RouterMoveToPaymentInfoScreenEvent) {
      yield RouterMoveToPaymentInfoScreen(matchFoundObj: event.matchFoundObj);
      yield RouteNavigationChanged();
    }
    if (event is RouterMoveToPaymentSummaryScreenEvent) {
      yield RouterMoveToPaymentSummaryScreen(matchFoundObj: event.matchFoundObj, editOrderObj : event.editOrderObj);
      yield RouteNavigationChanged();
    }
    if(event is RouterMoveToProfileScreenEvent) {
      yield RouterMoveToProfileScreen();
      yield RouteNavigationChanged();
    }
  }
}
