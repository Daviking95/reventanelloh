import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';

class ForgotPasswordService {
  Future<HttpClientResponse> forgotPassword({String email}) async {
    try {
      Map<String, dynamic> forgotEmailData = {
        'email': email,
      };

      HttpClientResponse result = await HttpService().post(
          url: 'Authentication/forgotPassword', requestData: forgotEmailData);
      return result;
    } on SocketException {
      throw new SocketException('No Internet Connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (error) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> resetPassword(
      {String email, String newPassword, String token}) async {
    try {
      Map<String, dynamic> newPasswordData = {
        'email': email,
        'token': token,
        "newPassword": newPassword
      };

      log("ResetPassword Result ${jsonEncode(newPasswordData)}");

      HttpClientResponse result = await HttpService().post(
          url: 'Authentication/resetPassword', requestData: newPasswordData);
      return result;
    } on SocketException {
      throw new SocketException('No Internet Connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (error) {
      throw new Error();
    }
  }
}
