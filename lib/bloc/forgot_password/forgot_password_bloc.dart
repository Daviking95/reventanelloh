import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/forgot_password/forgot_password_event.dart';
import 'package:anelloh_mobile/bloc/forgot_password/forgot_password_service.dart';
import 'package:anelloh_mobile/bloc/forgot_password/forgot_password_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ForgotPasswordScreenBloc
    extends Bloc<ForgotPasswordScreenEvent, ForgotPasswordScreenState> {
  ForgotPasswordScreenBloc() : super(ForgotPasswordInitialState());

  ForgotPasswordService _forgotPasswordService = ForgotPasswordService();

  @override
  Stream<ForgotPasswordScreenState> mapEventToState(
      ForgotPasswordScreenEvent event) async* {
    if (event is RecoverPasswordEvent) {
      try {
        yield LoadingPasswordRecovery();
        HttpClientResponse response = event.isChange
            ? await _forgotPasswordService.resetPassword(
                email: event.emailAddress,
                newPassword: event.newPassword,
                token: event.token)
            : await _forgotPasswordService.forgotPassword(
                email: event.emailAddress);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          log("ResetPassword Result $result");

          if (result['succeeded'] == true) {
            yield CompletedPasswordRecovery(
                message: event.isChange
                    ? 'Password changed successfully. Login with your new password'
                    : "Reset password successful. Go to your mail app to proceed with recovery");
          } else {
            yield FailedPasswordRecoveryException(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
          yield ForgotPasswordStateChanged();
        } else {
          yield ForgotPasswordStateChanged();
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          yield FailedPasswordRecoveryException(
              error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
        }
      } on SocketException {
        yield ForgotPasswordNoInternetException();
        yield ForgotPasswordStateChanged();
      } on HttpException {
        yield ForgotPasswordHttpException();
        yield ForgotPasswordStateChanged();
      } catch (error) {
        print(error);
        if (error is Error) {
          yield FailedPasswordRecoveryException(error: error.toString());
        }
      }
    }
  }
}
