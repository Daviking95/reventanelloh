import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class ForgotPasswordScreenEvent extends Equatable {
  ForgotPasswordScreenEvent();

  @override
  List<Object> get props => [];
}

class RecoverPasswordEvent extends ForgotPasswordScreenEvent {
  final String emailAddress;
  final bool isChange;
  final String newPassword;
  final String token;
  RecoverPasswordEvent(
      {@required this.emailAddress,
      this.isChange,
      this.newPassword,
      this.token})
      : super();
}

// CLASSES PERTAINING TO FORGOT PASSWORD
