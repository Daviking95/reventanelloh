
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class ForgotPasswordScreenState extends Equatable {
  ForgotPasswordScreenState();

  @override
  List<Object> get props => [];
}

class ForgotPasswordInitialState extends ForgotPasswordScreenState {}
class ForgotPasswordStateChanged extends ForgotPasswordScreenState {}

class CompletedPasswordRecovery extends ForgotPasswordScreenState {
  final String message;
  CompletedPasswordRecovery({
    @required this.message
  }):super();
}
class LoadingPasswordRecovery extends ForgotPasswordScreenState {}

class FailedPasswordRecoveryException extends ForgotPasswordScreenState {
  final String error;
  FailedPasswordRecoveryException({
    @required this.error
  }):super();
}

class ForgotPasswordNoInternetException extends ForgotPasswordScreenState {}

class ForgotPasswordHttpException extends ForgotPasswordScreenState {}


