import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/dashboard/dashboard_service.dart';
import 'package:anelloh_mobile/helpers/classes/order_listing.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

part 'dashboard_event.dart';
part 'dashboard_state.dart';

class DashboardBloc extends Bloc<DashboardScreenEvent, DashboardScreenState> {
  DashboardBloc() : super(DashboardInitialState());

  DashboardService _dashboardService = new DashboardService();

  @override
  Stream<DashboardScreenState> mapEventToState(
      DashboardScreenEvent event) async* {
    if (event is FetchAllOrdersEvent) {
      try {
        yield FetchingAllOrders();
        HttpClientResponse response = await _dashboardService
            .getTopTenOrderListings(customerId: event.customerId);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          List<OrderListing> listings = [];
          result['entity'].forEach((entity) {
            OrderListing orderListing = OrderListing(
              orderId: entity['order']['id'].toString(),
              orderNo: entity['order']['orderNo'].toString(),
              customerId: entity['order']['customerId'].toString(),
              convertedCurrency:
                  entity['order']['convertedCurrency'].toString(),
              myCurrency: entity['order']['myCurrency'].toString(),
              myAmount: entity['order']['myAmount'].toString(),
              convertedAmount: entity['order']['convertedAmount'].toString(),
              exchangeRate:
                  entity['order']['rate'].toString(), //add this to api response
              userName: entity['order']['userName'], //add this to api response
              userEmail: entity['order']['email'], //add this to api response
              rates: [], // add this to api response
              isRateAscending: double.parse(entity['rateChange'].toString()) > 0
                  ? true
                  : false,
              ascDecValue: entity['rateChange'],
              subTotal: entity['order']['myAmount'].toString(), // add to api
              processingFeeCurrency:
                  entity['order']['transactionFeeCurrency'].toString(),
              orderStatus: entity['order']['orderStatus'].toString(),
              processingFee: entity['order']['transactionFee'].toString(),
              totalAmount: entity['order']['totalAmount'].toString(),
              paymentStatus: entity['order']['paymentStatus'].toString(),
              refundFee: entity['order']['refundFee'],
              myPaymentChannelId:
                  entity['order']['myPaymentChannelId'].toString(),
            );
            listings.add(orderListing);
          });

          yield FetchingOrdersSuccessful(allOrders: listings);
        } else {
          yield FailedToFetchAllOrders();
        }
      } on SocketException {
        yield DashboardStateSocketException();
      } on HttpException {
        yield DashboardStateHttpException();
      } catch (err) {
        print(err);
        yield FailedToFetchAllOrders();
      }
    }
    if (event is AutoFetchAllOrdersEvent) {
      try {
        HttpClientResponse response = await _dashboardService
            .getTopTenOrderListings(customerId: event.customerId);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          List<OrderListing> listings = [];
          result['entity'].forEach((entity) {
            OrderListing orderListing = OrderListing(
              orderId: entity['order']['id'].toString(),
              orderNo: entity['order']['orderNo'].toString(),
              customerId: entity['order']['customerId'].toString(),
              convertedCurrency:
                  entity['order']['convertedCurrency'].toString(),
              myCurrency: entity['order']['myCurrency'].toString(),
              myAmount: entity['order']['myAmount'].toString(),
              convertedAmount: entity['order']['convertedAmount'].toString(),
              exchangeRate: entity['order']['rate'].toString(),
              //add this to api response
              userName: entity['order']['userName'],
              //add this to api response
              userEmail: entity['order']['email'],
              //add this to api response
              rates: [],
              // add this to api response
              isRateAscending: double.parse(entity['rateChange'].toString()) > 0
                  ? true
                  : false,
              ascDecValue: entity['rateChange'],
              subTotal: entity['order']['myAmount'].toString(),
              // add to api
              processingFeeCurrency:
                  entity['order']['transactionFeeCurrency'].toString(),
              orderStatus: entity['order']['orderStatus'].toString(),
              processingFee: entity['order']['transactionFee'].toString(),
              totalAmount: entity['order']['totalAmount'].toString(),
              paymentStatus: entity['order']['paymentStatus'].toString(),
              refundFee: entity['order']['refundFee'],
              myPaymentChannelId:
                  entity['order']['myPaymentChannelId'].toString(),
            );
            listings.add(orderListing);
          });

          yield DashboardStateChanged();
          yield FetchingOrdersSuccessful(allOrders: listings);
        }
      } catch (err) {
        print(err);
      }
    }
  }
}
