part of 'dashboard_bloc.dart';

class DashboardScreenEvent extends Equatable{
  DashboardScreenEvent();
  @override
  List<Object> get props => [];
}

class FetchAllOrdersEvent extends DashboardScreenEvent {
  final String customerId;
  FetchAllOrdersEvent({@required this.customerId}):super();
}

class AutoFetchAllOrdersEvent extends DashboardScreenEvent {
  final String customerId;
  AutoFetchAllOrdersEvent({@required this.customerId}):super();
}
