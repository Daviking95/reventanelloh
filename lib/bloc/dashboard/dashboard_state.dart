part of 'dashboard_bloc.dart';

class DashboardScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class DashboardInitialState extends DashboardScreenState {}

class DashboardStateChanged extends DashboardScreenState {}

class FetchingAllOrders extends DashboardScreenState {}

class FetchingOrdersSuccessful extends DashboardScreenState {
  final List<OrderListing> allOrders;
  FetchingOrdersSuccessful({@required this.allOrders}) : super();
}

class DashboardStateHttpException extends DashboardScreenState {}
class DashboardStateSocketException extends DashboardScreenState {}

class FailedToFetchAllOrders extends DashboardScreenState {}

