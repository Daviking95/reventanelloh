import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/foundation.dart';

class DashboardService {
  Future<HttpClientResponse> getTopTenOrderListings(
  {
  @required customerId,
}
      ) async {
    try {
      HttpClientResponse response =
          await HttpService().get(url: 'Orders/gettop10orderlistings/$customerId');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (error) {
      throw new Error();
    }
  }
}
