import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class AllListingScreenState extends Equatable {
  final List<ListingObj> listings;

  AllListingScreenState({this.listings}) : super();
  @override
  List<Object> get props => [];
}

class InitializeListingScreenState extends AllListingScreenState {
  final List<ListingObj> listings = [];
}

class FetchingListingData extends AllListingScreenState {}

class ErrorFetchingListingData extends AllListingScreenState {}

class SuccessfullyFetchedListingData extends AllListingScreenState {
  final List<ListingObj> listings;
  SuccessfullyFetchedListingData({@required this.listings})
      : super(listings: listings);
}

class ListingDataSocketException extends AllListingScreenState {}

class ListingDataHttpException extends AllListingScreenState {}

class MoveToListingMainScreen extends AllListingScreenState {}

// CLASSES ASSOCIATED WITH LISTING

class ListingObj {
  final MatchFoundObj matchFoundObj;

  ListingObj({@required this.matchFoundObj});
}
