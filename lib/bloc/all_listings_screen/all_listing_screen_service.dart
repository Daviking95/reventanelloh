import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';

class AllListingsService {
  // getOrderlistings
  Future<HttpClientResponse> fetchAllListings() async {
    try {
      HttpClientResponse response =
          await HttpService().get(url: 'Orders/getorderlistings');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }
}
