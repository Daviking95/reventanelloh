
import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_event.dart';
import 'package:anelloh_mobile/bloc/all_listings_screen/all_listing_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AllListingScreenBloc
    extends Bloc<AllListingScreenEvent, AllListingScreenState> {
  AllListingScreenBloc() : super(InitializeListingScreenState());

  @override
  Stream<AllListingScreenState> mapEventToState(
      AllListingScreenEvent event) async* {
    if (event is MoveToListingsMainScreenEvent) {
      yield MoveToListingMainScreen();
    }
  }
}
