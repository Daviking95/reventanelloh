
import 'package:equatable/equatable.dart';

class AllListingScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchListingDataEvent extends AllListingScreenEvent {}
class MoveToListingsMainScreenEvent extends AllListingScreenEvent {}