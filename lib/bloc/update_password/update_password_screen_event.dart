
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class UpdatePasswordScreenEvent extends Equatable {
  UpdatePasswordScreenEvent();
  @override
  List<Object> get props => [];
}

class SavePasswordEvent extends UpdatePasswordScreenEvent {
  final UpdatePasswordObj updatePasswordObj;
  SavePasswordEvent({
    @required this.updatePasswordObj,
}):super();
}


// CLASSES ASSOCIATED WITH PASSWORD UPDATE

class UpdatePasswordObj {
  final String userEmail;
  final String currentPassword;
  final String newPassword;

  UpdatePasswordObj({
    @required this.currentPassword,
    @required this.newPassword,
    @required this.userEmail,
});
}