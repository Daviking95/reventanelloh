import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/update_password/update_password_screen_event.dart';
import 'package:anelloh_mobile/bloc/update_password/update_password_screen_service.dart';
import 'package:anelloh_mobile/bloc/update_password/update_password_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UpdatePasswordScreenBloc
    extends Bloc<UpdatePasswordScreenEvent, UpdatePasswordScreenState> {
  UpdatePasswordScreenBloc()
      : super(InitialUpdatePasswordScreenState(isSavingPassword: false));

  UpdatePasswordService _updatePasswordService = UpdatePasswordService();

  @override
  Stream<UpdatePasswordScreenState> mapEventToState(
      UpdatePasswordScreenEvent event) async* {
    if (event is SavePasswordEvent) {
      try {
        yield SavingPassword();
        HttpClientResponse response = await _updatePasswordService
            .updatePasswordCall(updatePasswordObj: event.updatePasswordObj);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            yield SuccessfullySavedPassword();
          } else {
            yield FailedSavingPassword(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } else {
          yield FailedSavingPassword(
              error: result['message'] ??
                  result['entity'] ??
                  result['messages'][0]);
        }
      } on SocketException {
        yield UpdatePasswordNoInternetException();
      } on HttpException {
        yield UpdatePasswordHttpException();
      } catch (error) {
        yield FailedSavingPassword(error: error.toString());
      }
    }
  }
}
