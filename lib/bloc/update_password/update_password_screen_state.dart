
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class UpdatePasswordScreenState extends Equatable {
  UpdatePasswordScreenState();
  @override
  List<Object> get props => [];
}

class InitialUpdatePasswordScreenState extends UpdatePasswordScreenState {
  final bool isSavingPassword;
  InitialUpdatePasswordScreenState({
    @required this.isSavingPassword
}):super();
}
class UpdatePasswordScreenStateChanged extends UpdatePasswordScreenState {}
class SavingPassword extends UpdatePasswordScreenState {
  SavingPassword():super();
}
class SuccessfullySavedPassword extends UpdatePasswordScreenState {
  SuccessfullySavedPassword():super();
}

class FailedSavingPassword extends UpdatePasswordScreenState {
  final String error;
  FailedSavingPassword({
    @required this.error,
  }):super();
}

class UpdatePasswordNoInternetException extends UpdatePasswordScreenState{}
class UpdatePasswordHttpException extends UpdatePasswordScreenState {}
