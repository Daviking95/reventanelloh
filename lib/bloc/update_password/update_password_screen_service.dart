
import 'dart:io';

import 'package:anelloh_mobile/bloc/update_password/update_password_screen_event.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/cupertino.dart';

class UpdatePasswordService {
  UpdatePasswordService ();

  Future<HttpClientResponse>updatePasswordCall({@required UpdatePasswordObj updatePasswordObj}) async {
    try {
      Map<String, dynamic> requestData = {
        'email': updatePasswordObj.userEmail,
        'oldPassword': updatePasswordObj.currentPassword,
        'newPassword': updatePasswordObj.newPassword
      };
      HttpClientResponse response = await HttpService().post(url: 'Authentication/changePassword', requestData: requestData);
      return response;
    }catch(err) {
      print(err);
      return err;
    }
  }
}