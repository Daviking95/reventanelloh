

import 'package:equatable/equatable.dart';

class LaunchScreenEvent extends Equatable {
  LaunchScreenEvent();

  @override
  List<Object> get props => [];
}


class CheckHasOnboardedEvent extends LaunchScreenEvent{}
