
import 'package:equatable/equatable.dart';

class LaunchScreenState extends Equatable {
  LaunchScreenState();

  @override
  List<Object> get props => [];
}

class InitializeLaunchScreen extends LaunchScreenState{}

class UserHasOnboarded extends LaunchScreenState{
  UserHasOnboarded():super();
}

class UserHasNotOnboarded extends LaunchScreenState{
  UserHasNotOnboarded():super();
}

class UserShouldUsePin extends LaunchScreenState {}
