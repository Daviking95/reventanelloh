

import 'package:anelloh_mobile/bloc/launch/launch_event.dart';
import 'package:anelloh_mobile/bloc/launch/launch_service.dart';
import 'package:anelloh_mobile/bloc/launch/launch_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LaunchScreenBloc extends Bloc<LaunchScreenEvent, LaunchScreenState> {
  LaunchScreenBloc():super(InitializeLaunchScreen());
  LaunchScreenService _launchScreenService = LaunchScreenService();
  bool hasUserOnboarded;

  @override
  Stream<LaunchScreenState> mapEventToState(LaunchScreenEvent event) async*{
    if (event is CheckHasOnboardedEvent) {
      hasUserOnboarded = await _launchScreenService.checkHasOnboarded();
      if(hasUserOnboarded != null && hasUserOnboarded != false) {
        yield UserHasOnboarded();
      } else {
        yield UserHasNotOnboarded();
      }
    }
  }
}