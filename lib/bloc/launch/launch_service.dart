
import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LaunchScreenService {
  LaunchScreenService();

  checkHasOnboarded() async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    bool hasOnboarded = _pref.getBool('_ONBOARDING_COMPLETED_');
    return hasOnboarded;
  }

  setHasUserOnboarded() async {
    SharedPreferences _pref = await SharedPreferences.getInstance();
    await _pref.setBool('_ONBOARDING_COMPLETED_', true);
  }

  Future<HttpClientResponse> storeDeviceToken(
      {@required String deviceToken}) async {
    try {
      Map<String, dynamic> requestData = {
        'deviceToken': deviceToken
      };
      HttpClientResponse response = await HttpService().post(
          url: '', requestData: requestData);
      return response;
    } catch (err) {
      throw new Error();
    }
  }
}