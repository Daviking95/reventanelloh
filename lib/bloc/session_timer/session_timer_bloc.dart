
import 'dart:async';

import 'package:anelloh_mobile/bloc/session_timer/session_timer_event.dart';
import 'package:anelloh_mobile/bloc/session_timer/session_timer_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SessionTimerBloc extends Bloc<SessionTimerEvent, SessionTimerState> {
 SessionTimerBloc():super(InitialSessionTimer());

 @override
  Stream<SessionTimerState> mapEventToState(SessionTimerEvent event) async* {
   if (event is SessionTimerRestartEvent) {
    yield SessionTimerRestarted();
    yield SessionTimerChanged();
   }
   if (event is SessionTimerExpiredEvent) {
     yield SessionTimerExpired();
     yield SessionTimerChanged();
   }
 }
}