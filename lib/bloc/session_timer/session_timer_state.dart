
import 'package:equatable/equatable.dart';

class SessionTimerState extends Equatable {
  List<Object> get props => [];
}

class InitialSessionTimer extends SessionTimerState {}

class SessionTimerChanged extends SessionTimerState {}

class SessionTimerRestarted extends SessionTimerState {}

class SessionTimerExpired extends SessionTimerState {}
