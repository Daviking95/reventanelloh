
import 'package:equatable/equatable.dart';

class SessionTimerEvent extends Equatable {
  List<Object> get props => [];
}

class SessionTimerRestartEvent extends SessionTimerEvent {}
class ReInitializeSessionTimerEvent extends SessionTimerEvent {}
class SessionTimerExpiredEvent extends SessionTimerEvent {}
