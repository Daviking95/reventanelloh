import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_event.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_service.dart';
import 'package:anelloh_mobile/bloc/match_not_found/match_not_found_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MatchNotFoundBloc extends Bloc<MatchNotFoundEvent, MatchNotFoundState> {
  MatchNotFoundBloc() : super(InitialMatchNotFoundRateState());

  MatchNotFoundService _matchNotFoundService = MatchNotFoundService();

  @override
  Stream<MatchNotFoundState> mapEventToState(MatchNotFoundEvent event) async* {
    if (event is FetchOrderByRateEvent) {
      try {
        yield FetchingOrdersByRate();
        HttpClientResponse response =
            await _matchNotFoundService.getByMarketRates(
                rate: event.rate,
                currency: event.currency,
                customerId: event.customerId);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        // print('FetchingOrdersByRate $result');
        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            List orders = result['entity']['orders'];
            List<MatchFoundObj> listData = [];
            orders.forEach((e) {
              MatchFoundObj obj = MatchFoundObj(
                entityId: e['id'].toString(),
                customerId: e['customerId'].toString(),
                orderOwnerId: '',
                orderNo: e['orderNo'].toString(),
                convertedCurrency: e['convertedCurrency'],
                myCurrency: e['myCurrency'],
                myAmount: e['myAmount'].toString(),
                exchangeRate: e['rate'].toString(),
                userName: e['userName'],
                userEmail: e['email'],
                rates: [],
                isRateAscending: true,
                ascDecValue: 1.4,
                subTotal: '0',
                processingFee: '',
                convertedAmount: e['convertedAmount'].toString(),
                totalAmount: e['totalAmount'].toString(),
                orderStatus: e['orderStatus'],
              );
              listData.add(obj);
            });
            yield SuccessfullyFetchOrderByRate(matchFoundObjs: listData);
          }
        } else {
          yield ErrorFetchingOrderByRate(
              error: result['message'] ??
                  result['entity'] ??
                  result['messages'][0]);
        }
      } catch (err) {
        if (err is SocketException) {
          yield MatchNotFoundSocketException();
        }
        if (err is HttpException) {
          yield MatchNotFoundHttpException();
        }
        yield ErrorFetchingOrderByRate(error: 'An Error Occurred');
      }
    }
  }
}
