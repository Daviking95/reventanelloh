
import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class MatchNotFoundState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialMatchNotFoundRateState extends MatchNotFoundState {}

class FetchingOrdersByRate extends MatchNotFoundState {}

class ErrorFetchingOrderByRate extends MatchNotFoundState {
  final String error;
  ErrorFetchingOrderByRate({this.error}):super();
}

class SuccessfullyFetchOrderByRate extends MatchNotFoundState {
  final List<MatchFoundObj> matchFoundObjs;
  SuccessfullyFetchOrderByRate({@required this.matchFoundObjs}):super();
}

class MatchNotFoundSocketException extends MatchNotFoundState {}
class MatchNotFoundHttpException extends MatchNotFoundState {}