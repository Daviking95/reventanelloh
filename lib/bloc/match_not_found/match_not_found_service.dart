import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/cupertino.dart';

class MatchNotFoundService {
  Future<HttpClientResponse> getByMarketRates(
      {@required int rate, String customerId, String currency}) async {
    try {
      HttpClientResponse response = await HttpService().get(
          url:
              'Orders/getmyordersbyrateandcurrency/$customerId/$rate/$currency');

      // print('getmyordersbyrateandcurrency ${'Orders/getmyordersbyrateandcurrency/$customerId/$rate/$currency'}');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }
}
