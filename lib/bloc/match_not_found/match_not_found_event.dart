import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class MatchNotFoundEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class FetchOrderByRateEvent extends MatchNotFoundEvent {
  final int rate;
  final String currency;
  final String customerId;

  FetchOrderByRateEvent({@required this.rate, this.currency, this.customerId})
      : super();
}
