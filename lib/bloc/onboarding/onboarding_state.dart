
import 'package:equatable/equatable.dart';

class OnboardingScreenState extends Equatable{
  OnboardingScreenState();
  @override
  List<Object> get props => [];
}

class HasMovedScreen extends OnboardingScreenState{}

class InitialOnboardingScreen extends OnboardingScreenState{
  final int onboardingPage;
  InitialOnboardingScreen({this.onboardingPage}):super();
}

class NextOnboardingScreen extends OnboardingScreenState{
  final int onboardingPage;
  NextOnboardingScreen({this.onboardingPage}):super();
}

class PrevOnboardingScreen extends OnboardingScreenState{
  final int onboardingPage;
  PrevOnboardingScreen({this.onboardingPage}):super();
}

class MoveToSignUpScreen extends OnboardingScreenState {}

class SkippedOnboardingScreen extends OnboardingScreenState{}