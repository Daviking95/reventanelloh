
import 'package:anelloh_mobile/bloc/launch/launch_service.dart';
import 'package:anelloh_mobile/bloc/onboarding/onboarding_event.dart';
import 'package:anelloh_mobile/bloc/onboarding/onboarding_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class OnboardingScreenBloc extends Bloc<OnboardingScreenEvent, OnboardingScreenState> {
  OnboardingScreenBloc() : super(InitialOnboardingScreen(onboardingPage: 0));
  LaunchScreenService _launchScreenService = LaunchScreenService();
  int onboardingPage = 0;

  @override
  Stream<OnboardingScreenState> mapEventToState(OnboardingScreenEvent event)async*{
    if (event is NextOnboardingScreenEvent) {
      onboardingPage = onboardingPage + 1;
      yield NextOnboardingScreen(onboardingPage: onboardingPage);
      yield HasMovedScreen();
    }
    if (event is PrevOnboardingScreenEvent) {
      onboardingPage = onboardingPage - 1;
      yield PrevOnboardingScreen(onboardingPage:  onboardingPage);
      yield HasMovedScreen();
    }
    if (event is SkipOnboardingScreenEvent) {
      _launchScreenService.setHasUserOnboarded();
      yield SkippedOnboardingScreen();
    }
    if (event is MoveToSignUpScreenEvent) {
      _launchScreenService.setHasUserOnboarded();
      yield MoveToSignUpScreen();
    }
  }
}