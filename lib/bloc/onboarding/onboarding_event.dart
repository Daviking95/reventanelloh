
import 'package:equatable/equatable.dart';

class OnboardingScreenEvent extends Equatable {
  OnboardingScreenEvent();
  @override
  List<Object> get props => [];
}

class SkipOnboardingScreenEvent extends OnboardingScreenEvent{}

class NextOnboardingScreenEvent extends OnboardingScreenEvent{}

class PrevOnboardingScreenEvent extends OnboardingScreenEvent{}

class MoveToSignUpScreenEvent extends OnboardingScreenEvent {}
