
import 'package:equatable/equatable.dart';

class EditOrderScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class EditingOrder extends EditOrderScreenState {}

class InitialEditOrderState extends EditOrderScreenState {}

class EditOrderSuccessfully extends EditOrderScreenState {}

class EditOrderFailed extends EditOrderScreenState {
  final String error;
  EditOrderFailed({this.error}):super();
}

class EditOrderSocketException extends EditOrderScreenState {}

class EditOrderHttpException extends EditOrderScreenState {}