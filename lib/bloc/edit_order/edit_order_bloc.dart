import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/edit_order/edit_order_event.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_service.dart';
import 'package:anelloh_mobile/bloc/edit_order/edit_order_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class EditOrderScreenBloc
    extends Bloc<EditOrderScreenEvent, EditOrderScreenState> {
  EditOrderScreenBloc() : super(InitialEditOrderState());

  EditOrderService _editOrderService = new EditOrderService();

  @override
  Stream<EditOrderScreenState> mapEventToState(
      EditOrderScreenEvent event) async* {
    if (event is EditOrderEvent) {
      try {
        yield EditingOrder();

        log('event.isUploadReceipt ${event.isUploadReceipt}');

        HttpClientResponse response = event.isUploadReceipt
            ? await _editOrderService.uploadCustomerReceipt(
                editOrderObj: event.editOrderObj)
            : await _editOrderService.editOrder(
                editOrderObj: event.editOrderObj);

        log("Paying Locally ${response.statusCode}");

        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        print("Paying Locally ${jsonEncode(result)}");

        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            yield EditOrderSuccessfully();
          } else {
            yield EditOrderFailed(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } else {
          yield EditOrderFailed(
              error: result['message'] ??
                  result['entity'] ??
                  result['messages'][0]);
        }
      } catch (err) {
        if (err is SocketException) {
          yield EditOrderSocketException();
        }
        if (err is HttpException) {
          yield EditOrderHttpException();
        }
        yield EditOrderFailed();
      }
    }
  }
}
