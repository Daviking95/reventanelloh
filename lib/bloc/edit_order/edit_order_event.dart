import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class EditOrderScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class EditOrderEvent extends EditOrderScreenEvent {
  final EditOrderObj editOrderObj;
  final bool isUploadReceipt;

  EditOrderEvent({@required this.editOrderObj, this.isUploadReceipt = false})
      : super();
}
