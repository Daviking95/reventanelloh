import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/helpers/classes/edit_order_obj.dart';
import 'package:anelloh_mobile/helpers/convert_currency_code.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/foundation.dart';

class EditOrderService {
  Future<HttpClientResponse> editOrder(
      {@required EditOrderObj editOrderObj}) async {
    try {
      Map<String, dynamic> requestData = {
        'customerId': editOrderObj.customerId,
        'orderId': editOrderObj.orderId is String ? int.parse(editOrderObj.orderId) : editOrderObj.orderId,
        'myAmount': editOrderObj.myAmount,
        'myCurrency':
            convertCurrencyCode(currencyCode: editOrderObj.myCurrency),
        'convertedCurrency':
            convertCurrencyCode(currencyCode: editOrderObj.convertedCurrency),
        'convertedAmount': editOrderObj.convertedAmount,
        'rate': editOrderObj.rate,
        'myAccountNumber': editOrderObj.myAccountNumber,
        'myBankName': editOrderObj.myBankName,
        "address": editOrderObj?.address ?? "",
        "city": editOrderObj?.city ?? "",
        "state": editOrderObj?.state ?? "",
        "zipCode": editOrderObj?.zipCode ?? "",
        "country": editOrderObj?.country ?? "",
        'myPaymentChannelId': editOrderObj.myPaymentChannelId == 0
            ? 8
            : editOrderObj?.myPaymentChannelId ?? 8,
        'transactionFee': editOrderObj.transactionFee,
        'paymentReference': editOrderObj.paymentReference ?? 'n/a',
        'paymentStatus': editOrderObj.paymentStatus,
        'bankRouteNo':
            editOrderObj.bankRouteNo != null ? editOrderObj.bankRouteNo : 'n/a',
        "receipt": editOrderObj.receipt,
        'loggedInDevice': 1,
      };

      print("EditOrderService request ${jsonEncode(requestData)}");
      print('Orders/update/${editOrderObj.orderId}');

      HttpClientResponse response = await HttpService().put(
          url: 'Orders/update/${editOrderObj.orderId}',
          requestData: requestData);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  uploadCustomerReceipt({EditOrderObj editOrderObj}) async {
    try {
      Map<String, dynamic> requestData = {
        'customerId': editOrderObj.customerId,
        "orderNo": editOrderObj.orderNo,
        "customerReceipt": editOrderObj.receipt,
        'loggedInDevice': 1,
      };

      log("Paying Locally request ${jsonEncode(requestData)}");

      HttpClientResponse response = await HttpService()
          .post(url: 'Orders/uploadcustomerreceipt', requestData: requestData);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }
}
