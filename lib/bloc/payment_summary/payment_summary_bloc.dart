import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_event.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_service.dart';
import 'package:anelloh_mobile/bloc/payment_summary/payment_summary_state.dart';
import 'package:anelloh_mobile/helpers/classes/active_gateways.dart';
import 'package:anelloh_mobile/helpers/classes/bank_config.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class PaymentSummaryStateBloc
    extends Bloc<PaymentSummaryEvent, PaymentSummaryState> {
  PaymentSummaryStateBloc() : super(InitialPaymentSummaryState());

  PaymentSummaryService _paymentSummaryService = new PaymentSummaryService();

  @override
  Stream<PaymentSummaryState> mapEventToState(
      PaymentSummaryEvent event) async* {
    if (event is RetrieveActivePaymentGateway) {
      try {
        yield LoadingActivePaymentGateway();

        // HttpClientResponse response = await _paymentSummaryService
        //     .getActivePaymentChannelsByCurrency(event.myCurrency);

        HttpClientResponse response = event.isFromForeign
            ? await _paymentSummaryService
                .getActivePaymentChannelsByCurrency(event.myCurrency)
            : await _paymentSummaryService.getActivePaymentChannels();

        // print('Active Paymentt ${response.statusCode}');
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          if (result['succeeded']) {
            List<ActivePaymentGateways> activePaymentGatewaysList = [];
            List paymentChannels = event.isFromForeign
                ? result['entity']
                : result['entity']['paymentChannels'];

            log("My Currency Cide ${event.myCurrency}");

            paymentChannels.forEach((element) {
              if (event.myCurrency == element['currencyCode']) {
                activePaymentGatewaysList
                    .add(ActivePaymentGateways.fromJson(element));
              }
            });

            yield ActivePaymentGatewaysState(
                activePaymentGatewayList: activePaymentGatewaysList);
          } else {
            yield ErrorListingActivePayments(
                errorMessage: result['message'] ?? "");
          }
        } else {
          yield ErrorListingActivePayments(errorMessage: "");
        }
      } on SocketException {
        yield ActivePaymentSocketException();
      } on HttpException {
        yield ActivePaymentHttpException();
      } catch (err) {
        print(err);
        yield ErrorListingActivePayments();
      }
    }

    if (event is RetrieveBankPaymentConfig) {
      try {
        yield LoadingBankPaymentConfig();

        HttpClientResponse response =
            await _paymentSummaryService.getBankConfig(event.myCurrency);

        print('BankPaymentConfig ${response.statusCode}');
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          print('BankPaymentConfig $result');

          if (result['succeeded']) {
            BankConfigEntity bankConfigEntity;

            bankConfigEntity = BankConfigEntity.fromJson(result['entity']);

            yield ActiveBankPaymentConfigState(
                bankConfigEntity: bankConfigEntity);
          } else {
            yield ErrorListingBankPaymentConfig(
                errorMessage: result['message'] ?? "");
          }
        } else {
          yield ErrorListingBankPaymentConfig(errorMessage: "");
        }
      } on SocketException {
        yield BankPaymentConfigSocketException();
      } on HttpException {
        yield BankPaymentConfigHttpException();
      } catch (err) {
        print(err);
        yield ErrorListingBankPaymentConfig();
      }
    }
    if (event is PayWithStripeEvent) {
      try {
        yield MakingPayments();
        // call backend url, get a response
        HttpClientResponse response =
            await _paymentSummaryService.payWithStripe(
                orderAmount: event.orderAmount,
                orderCurrency: event.orderCurrency,
                customerId: event.customerId,
                orderNo: event.orderNo,
                orderTransactionFee: event.orderTransactionFee);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          if (result['succeeded'] == true) {
            yield SuccessfullyMadeStripePayments(
                paymentIntentClientSecret: result['entity']);
          } else {
            yield ErrorRequestingInitialPayments();
          }
        } else {
          yield ErrorRequestingInitialPayments();
        }
      } on SocketException {
        yield PaymentMakingSocketException();
      } on HttpException {
        yield PaymentMakingHttpException();
      } catch (err) {
        print(err);
        yield ErrorRequestingInitialPayments();
      }
    }

    if (event is PayWithPaystackEvent) {
      try {
        yield MakingPayments();

        HttpClientResponse response =
            await _paymentSummaryService.payWithPaystack(
                orderAmount: event.orderAmount,
                orderCurrency: event.orderCurrency,
                customerId: event.customerId,
                orderNo: event.orderNo,
                orderTransactionFee: event.orderTransactionFee);

        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          log('PayWithPaystackEvent $result');

          if (result['succeeded'] == false) {
            yield ErrorRequestingInitialPayments(
                errorMessage: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          } else {
            yield SuccessfullyMadePayStackPayments(
                accessCode: result['entity']['data']['access_Code']);
          }
        } else {
          yield ErrorRequestingInitialPayments();
        }
      } on SocketException {
        yield PaymentMakingSocketException();
      } on HttpException {
        yield PaymentMakingHttpException();
      } catch (err) {
        print(err);
        yield ErrorRequestingInitialPayments();
      }
    }

    if (event is CheckPaymentIntentStatusEvent) {
      int retryCount = event.retryCount - 1;
      try {
        yield CheckingPaymentIntentStatus();
        HttpClientResponse response =
            await _paymentSummaryService.checkStripePaymentStatus(
                orderNo: event.orderNo,
                sessionId: event.sessionId,
                customerId: event.customerId);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          if (result['succeeded'] == true) {
            yield SuccessfullyConfirmedPayment();
          } else {
            yield ErrorAfterMakingInitialPayments(
                orderNo: event.orderNo,
                customerId: event.customerId,
                sessionId: event.sessionId,
                retryCount: retryCount);
          }
        } else {
          yield ErrorAfterMakingInitialPayments(
              orderNo: event.orderNo,
              customerId: event.customerId,
              sessionId: event.sessionId,
              retryCount: retryCount);
        }
      } catch (err) {
        yield ErrorAfterMakingInitialPayments(
            orderNo: event.orderNo,
            customerId: event.customerId,
            sessionId: event.sessionId,
            retryCount: retryCount);
      }
    }

    if (event is VerifyPaystackPaymentEvent) {
      int retryCount = event.retryCount - 1;
      try {
        yield VerifyingPayment();
        HttpClientResponse response =
            await _paymentSummaryService.confirmPaystackPayment(
                customerId: event.customerId,
                orderNo: event.orderNo,
                sessionId: event.sessionId);

        // print('Verify Paystack response ${response.statusCode}');

        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          // print('Verify Paystack response ${result}');

          if (result['succeeded'] == true) {
            yield PaymentVerifiedSuccessfully();
          } else {
            yield PaymentVerificationFailed(
                message: result['message'] ??
                    result['entity'] ??
                    result['messages'][0],
                customerId: event.customerId,
                orderNo: event.orderNo,
                sessionId: event.sessionId,
                retryCount: retryCount);
          }
        } else {
          yield PaymentVerificationFailed(
              message: "Bad Request",
              customerId: event.customerId,
              orderNo: event.orderNo,
              sessionId: event.sessionId,
              retryCount: retryCount);
        }
      } catch (err) {
        yield PaymentVerificationFailed(
          message: "Request Failed",
          customerId: event.customerId,
          orderNo: event.orderNo,
          sessionId: event.sessionId,
          retryCount: retryCount,
        );
      }
    }
  }
}
