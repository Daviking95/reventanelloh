import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/helpers/convert_currency_code.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class PaymentSummaryService {
  Future<HttpClientResponse> payWithStripe({
    @required String orderAmount,
    @required String orderCurrency,
    @required String customerId,
    @required String orderNo,
    @required String orderTransactionFee,
  }) async {
    try {
      Map<String, dynamic> requestData = {
        'customerId': customerId,
        'orderNo': orderNo,
        'currencyCode': convertCurrencyCode(currencyCode: orderCurrency),
        'callBackUrl': 'https://app.anelloh.com/payment_summary',
        'loggedInDeviceId': 1,
      };
      HttpClientResponse response = await HttpService()
          .post(url: 'Stripe/createcardpayment', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  Future<HttpClientResponse> payWithPaystack({
    @required String orderAmount,
    @required String orderCurrency,
    @required String customerId,
    @required String orderNo,
    @required String orderTransactionFee,
  }) async {
    try {
      Map<String, dynamic> requestData = {
        'customerId': customerId,
        'orderNo': orderNo,
        'currencyCode': convertCurrencyCode(currencyCode: orderCurrency),
        'callBackUrl': 'https://app.anelloh.com/payment_summary',
      };

      log('payWithPaystack ${jsonEncode(requestData)}');

      HttpClientResponse response = await HttpService()
          .post(url: 'Paystack/createpayment', requestData: requestData);
      return response;
    } catch (err) {
      print(err);
      throw new Error();
    }
  }

  Future<HttpClientResponse> checkStripePaymentStatus({
    @required String orderNo,
    @required String sessionId,
    @required String customerId,
  }) async {
    try {
      Map<String, dynamic> requestData = {
        'orderNo': orderNo,
        'sessionId': sessionId,
        'customerId': customerId,
        'loggedInDevice': 1,
      };
      HttpClientResponse response = await HttpService()
          .post(url: 'Stripe/getpaymentintentstatus', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> confirmPaystackPayment({
    @required String customerId,
    @required String orderNo,
    @required sessionId,
  }) async {
    try {
      Map<String, dynamic> requestData = {
        'customerId': customerId,
        'sessionId': sessionId,
        'orderNo': orderNo,
        'applicationType': 1
      };
      // print('Verify Paystack request ${jsonEncode(requestData)}');
      HttpClientResponse response = await HttpService()
          .post(url: 'Paystack/verifypayment', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  getActivePaymentChannels() async {
    try {
      HttpClientResponse response = await HttpService()
          .get(url: 'PaymentChannels/getactivepaymentchannels');

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  getBankConfig(String myCurrency) async {
    try {
      print('Getting here ${myCurrency ?? AppConstants.instantMatchCurrency}');

      HttpClientResponse response = await HttpService().put(
          url: 'Configurations/getbankconfigurationbycurrency',
          requestData: {"currencyCode": myCurrency.contains("NGN") ? 1 : 2});
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }

  getActivePaymentChannelsByCurrency(String myCurrency) async {
    try {
      HttpClientResponse response = await HttpService().get(
          url:
              'PaymentChannels/getactivepaymentchannelsbycurrency/${myCurrency.contains('NGN') ? 1 : 2}');

      log('PayCurrency ${'PaymentChannels/getactivepaymentchannelsbycurrency/${myCurrency.contains('NGN') ? 1 : 2}'}');

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }
}
