import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class PaymentSummaryEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class RetrieveActivePaymentGateway extends PaymentSummaryEvent {
  final String myCurrency;
  final bool isFromForeign;

  RetrieveActivePaymentGateway({this.myCurrency, this.isFromForeign = false});
}

class RetrieveBankPaymentConfig extends PaymentSummaryEvent {
  final String myCurrency;

  RetrieveBankPaymentConfig({this.myCurrency});
}

class PayWithStripeEvent extends PaymentSummaryEvent {
  final String orderAmount;
  final String orderCurrency;
  final String customerId;
  final String orderNo;
  final String orderTransactionFee;

  PayWithStripeEvent({
    @required this.orderAmount,
    @required this.orderCurrency,
    @required this.customerId,
    @required this.orderNo,
    @required this.orderTransactionFee,
  }) : super();
}

class PayWithPaystackEvent extends PaymentSummaryEvent {
  final String orderAmount;
  final String orderCurrency;
  final String customerId;
  final String orderNo;
  final String orderTransactionFee;

  PayWithPaystackEvent({
    @required this.orderAmount,
    @required this.orderCurrency,
    @required this.customerId,
    @required this.orderNo,
    @required this.orderTransactionFee,
  }) : super();
}

class CheckPaymentIntentStatusEvent extends PaymentSummaryEvent {
  final String orderNo;
  final String customerId;
  final String sessionId;
  final int retryCount;

  CheckPaymentIntentStatusEvent({
    @required this.orderNo,
    @required this.customerId,
    @required this.sessionId,
    @required this.retryCount,
  }) : super();
}

class VerifyPaystackPaymentEvent extends PaymentSummaryEvent {
  final String orderNo;
  final String customerId;
  final String sessionId;
  final int retryCount;
  VerifyPaystackPaymentEvent({
    @required this.orderNo,
    @required this.customerId,
    @required this.sessionId,
    @required this.retryCount,
  }) : super();
}
