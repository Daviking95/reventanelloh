import 'package:anelloh_mobile/helpers/classes/active_gateways.dart';
import 'package:anelloh_mobile/helpers/classes/bank_config.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/foundation.dart';

class PaymentSummaryState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitialPaymentSummaryState extends PaymentSummaryState {}

class MakingPayments extends PaymentSummaryState {}

class LoadingActivePaymentGateway extends PaymentSummaryState {}

class ActivePaymentGatewaysState extends PaymentSummaryState {
  final List<ActivePaymentGateways> activePaymentGatewayList;

  ActivePaymentGatewaysState({this.activePaymentGatewayList}) : super();
}

class ActivePaymentSocketException extends PaymentSummaryState {}

class ActivePaymentHttpException extends PaymentSummaryState {}

class LoadingBankPaymentConfig extends PaymentSummaryState {}

class ActiveBankPaymentConfigState extends PaymentSummaryState {
  final BankConfigEntity bankConfigEntity;

  ActiveBankPaymentConfigState({this.bankConfigEntity}) : super();
}

class BankPaymentConfigSocketException extends PaymentSummaryState {}

class BankPaymentConfigHttpException extends PaymentSummaryState {}

class CheckingPaymentIntentStatus extends PaymentSummaryState {}

class ErrorRequestingInitialPayments extends PaymentSummaryState {
  final String errorMessage;
  ErrorRequestingInitialPayments({this.errorMessage = ''}) : super();
}

class ErrorListingActivePayments extends PaymentSummaryState {
  final String errorMessage;
  ErrorListingActivePayments({this.errorMessage = ''}) : super();
}

class ErrorListingBankPaymentConfig extends PaymentSummaryState {
  final String errorMessage;
  ErrorListingBankPaymentConfig({this.errorMessage = ''}) : super();
}

class ErrorAfterMakingInitialPayments extends PaymentSummaryState {
  final String orderNo;
  final String customerId;
  final String sessionId;
  final int retryCount;
  ErrorAfterMakingInitialPayments({
    @required this.orderNo,
    @required this.customerId,
    @required this.sessionId,
    @required this.retryCount,
  }) : super();
}

class SuccessfullyConfirmedPayment extends PaymentSummaryState {}

class SuccessfullyMadeStripePayments extends PaymentSummaryState {
  final String paymentIntentClientSecret;
  SuccessfullyMadeStripePayments({@required this.paymentIntentClientSecret})
      : super();
}

class SuccessfullyMadePayStackPayments extends PaymentSummaryState {
  final String accessCode;

  SuccessfullyMadePayStackPayments({
    @required this.accessCode,
  }) : super();
}

class VerifyingPayment extends PaymentSummaryState {}

class PaymentVerifiedSuccessfully extends PaymentSummaryState {}

class PaymentVerificationFailed extends PaymentSummaryState {
  final String orderNo;
  final String customerId;
  final String sessionId;
  final int retryCount;
  final String message;

  PaymentVerificationFailed(
      {@required this.orderNo,
      @required this.customerId,
      @required this.sessionId,
      @required this.retryCount,
      @required this.message})
      : super();
}

class PaymentMakingSocketException extends PaymentSummaryState {}

class PaymentMakingHttpException extends PaymentSummaryState {}
