import 'dart:io';

import 'package:anelloh_mobile/bloc/profile/profile_screen_event.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/foundation.dart';

class ProfileScreenService {
  Future<HttpClientResponse> handleEditingUsers({
    @required String customerId,
    @required EditUserData editUserData,
  }) async {
    Map<String, dynamic> requestData = {
      'id': customerId,
      'firstName': editUserData.firstName,
      'userName': editUserData.userName,
      'lastName': editUserData.lastName,
      'phoneNumber': editUserData.phoneNumber,
      'email': editUserData.emailAddress,
      'countryCode': editUserData.countryCode,
      'bvn': editUserData.bvn,
      'gender': editUserData.gender,
      'address': editUserData.address,
      'dateOfBirth': editUserData.dob,
      'documentType': editUserData.documentType,
      'documentNumber': editUserData.documentNumber,
      'deviceId': editUserData.deviceId,
      'documentExpiryDate': editUserData.documentExpiryDate,
      'documentImage': editUserData.documentImage,
      'loggedInDevice': editUserData.loggedInDevice,
    };
    HttpClientResponse response = await HttpService()
        .put(url: 'Customers/update/$customerId', requestData: requestData);
    return response;
  }
}
