
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:photo_manager/photo_manager.dart';

class ProfileScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class MoveToUpdatePasswordScreenEvent extends ProfileScreenEvent {}
class MoveToChangePinScreenEvent extends ProfileScreenEvent {}
class MoveToEditProfileScreenEvent extends ProfileScreenEvent {}
class MoveToSelectProfileImageScreenEvent extends ProfileScreenEvent {
  final List<AssetEntity> imageList;
  MoveToSelectProfileImageScreenEvent({
    @required this.imageList,
}):super();
}

class EditUserEvent extends ProfileScreenEvent {
  final EditUserData editUserData;
  final User user;
  EditUserEvent({
    @required this.editUserData,
    @required this.user,
}):super();
}

// CLASSES ASSOCIATED WITH USER PROFILE

class EditUserData {
  final String firstName;
  final String lastName;
  final String userName;
  final String emailAddress;
  final String phoneNumber;
  final String countryCode;
  final int bvn;
  final int gender;
  final String address;
  final String dob;
  final int documentType;
  final int documentNumber;
  final String deviceId;
  final String documentExpiryDate;
  final String documentImage;
  final int loggedInDevice;

  EditUserData({
    @required this.firstName,
    @required this.lastName,
    @required this.userName,
    @required this.emailAddress,
    @required this.phoneNumber,
    @required this.countryCode,
    @required this.bvn,
    @required this.gender,
    @required this.address,
    @required this.dob,
    @required this.documentType,
    @required this.documentNumber,
    @required this.deviceId,
    @required this.documentExpiryDate,
    @required this.documentImage,
    @required this.loggedInDevice,
  });
}
