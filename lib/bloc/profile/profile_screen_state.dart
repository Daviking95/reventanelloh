
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';
import 'package:photo_manager/photo_manager.dart';

class ProfileScreenState extends Equatable {
  @override
  List<Object> get props => [];
}


class InitialProfileScreenState extends ProfileScreenState {}
class MoveToUpdatePasswordScreen extends ProfileScreenState {}
class MoveToChangePinScreen extends ProfileScreenState {}
class ProfileStateChanged extends ProfileScreenState {}
class MoveToEditProfileScreen extends ProfileScreenState {}
class MoveToSelectProfileImageScreen extends ProfileScreenState {
  final List<AssetEntity> imageList;
  MoveToSelectProfileImageScreen({
    @required this.imageList
  }):super();
}
class EditingUserData extends ProfileScreenState {}
class EditingUserSuccessful extends ProfileScreenState {}
class EditingUserFailed extends ProfileScreenState {
  final String error;
  EditingUserFailed({
    @required this.error,
}):super();
}
class EditingUserNoInternetException extends ProfileScreenState{}
class EditingUserHttpException extends ProfileScreenState {}
