import 'dart:convert';
import 'dart:io';

import 'package:anelloh_mobile/bloc/profile/profile_screen_event.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_service.dart';
import 'package:anelloh_mobile/bloc/profile/profile_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileScreenBloc extends Bloc<ProfileScreenEvent, ProfileScreenState> {
  ProfileScreenBloc() : super(InitialProfileScreenState());

  ProfileScreenService _profileScreenService = ProfileScreenService();

  @override
  Stream<ProfileScreenState> mapEventToState(ProfileScreenEvent event) async* {
    if (event is MoveToUpdatePasswordScreenEvent) {
      yield MoveToUpdatePasswordScreen();
      yield ProfileStateChanged();
    }
    if (event is MoveToChangePinScreenEvent) {
      yield MoveToChangePinScreen();
      yield ProfileStateChanged();
    }
    if (event is MoveToEditProfileScreenEvent) {
      yield MoveToEditProfileScreen();
      yield ProfileStateChanged();
    }
    if (event is MoveToSelectProfileImageScreenEvent) {
      yield MoveToSelectProfileImageScreen(imageList: event.imageList);
      yield ProfileStateChanged();
    }
    if (event is EditUserEvent) {
      try {
        yield EditingUserData();
        HttpClientResponse response =
            await _profileScreenService.handleEditingUsers(
                customerId: event.user.customerId,
                editUserData: event.editUserData);
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());
        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            yield EditingUserSuccessful();
          } else {
            yield EditingUserFailed(
                error: result['message'] ??
                    result['entity'] ??
                    result['messages'][0]);
          }
        } else {
          yield EditingUserFailed(error: 'An error occurred');
        }
      } on SocketException {
        yield EditingUserNoInternetException();
      } on HttpException {
        yield EditingUserHttpException();
      } catch (error) {
        yield EditingUserFailed(error: 'Service unavailable');
      }
    }
  }
}
