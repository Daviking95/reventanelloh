import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class CurrencyConversionEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class ConvertCurrency extends CurrencyConversionEvent {
  final int convertedAmount;
  ConvertCurrency({@required this.convertedAmount}) : super();
}

// CLASSES ASSOCIATED WITH CURRENCY CONVERSION
