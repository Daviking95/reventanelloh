import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class CurrencyConversionService {
  Future<Response> currencyConversion(
      {@required String fromCurrency,
      @required String toCurrency,
      @required fromCurrencyRate}) async {
    try {
      Response response = await Dio().get(
          'https://free.currconv.com/api/v7/convert?q=${fromCurrency}_$toCurrency&compact=ultra&apiKey=${env['CURRENCY_CONVERTER_API_KEY']}');
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (error) {
      throw new Error();
    }
  }
}
