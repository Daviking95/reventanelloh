import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class CurrencyConversionState extends Equatable {
  final int convertedAmount;
  CurrencyConversionState({@required this.convertedAmount});
  @override
  List<Object> get props => [];
}

class InitialCurrencyConversion extends CurrencyConversionState {
  final int rate = 0;
}

class CurrencyConversionSuccessful extends CurrencyConversionState {
  final int convertedAmount;
  CurrencyConversionSuccessful({@required this.convertedAmount})
      : super(convertedAmount: convertedAmount);
}

class ConvertingCurrency extends CurrencyConversionState {}
