import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_event.dart';
import 'package:anelloh_mobile/bloc/currency_conversion/currency_conversion_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CurrencyConversionBloc
    extends Bloc<CurrencyConversionEvent, CurrencyConversionState> {
  CurrencyConversionBloc() : super(InitialCurrencyConversion());
  @override
  Stream<CurrencyConversionState> mapEventToState(
      CurrencyConversionEvent event) async* {
    if (event is ConvertCurrency) {
      yield ConvertingCurrency();
      yield CurrencyConversionSuccessful(
          convertedAmount: event.convertedAmount);
    }
  }
}
