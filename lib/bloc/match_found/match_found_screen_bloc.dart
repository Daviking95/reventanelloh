import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/match_found/match_found_screen_event.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_service.dart';
import 'package:anelloh_mobile/bloc/match_found/match_found_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class MatchFoundScreenBloc
    extends Bloc<MatchFoundScreenEvent, MatchFoundScreenState> {
  MatchFoundScreenBloc() : super(InitializeMatchFoundState());

  MatchFoundScreenService _matchFoundScreenService = MatchFoundScreenService();

  @override
  Stream<MatchFoundScreenState> mapEventToState(
      MatchFoundScreenEvent event) async* {
    if (event is AcceptMatchFoundEvent) {
      yield AcceptMatchFound();
      yield MatchFoundStateChanged();
    }
    if (event is RejectMatchFoundEvent) {
      yield RejectMatchFound();
      yield MatchFoundStateChanged();
    }
    if (event is MoveToCreateOrderScreenEvent) {
      yield MoveToCreateOrderScreen();
    }
    if (event is MoveToDashboardScreenEvent) {
      yield MoveToDashboardScreen();
    }
    if (event is CreateAndMatchOrderEvent) {
      try {
        yield CreatingAndMatchingOrder();

        HttpClientResponse response =
            await _matchFoundScreenService.createAndMatchOrder(
                matchFoundObj: event.matchFoundObj,
                user: event.user,
                address: event.address,
                city: event.city,
                state: event.state,
                zipCode: event.zipCode,
                myAccountNumber: event.myAccountNumber,
                myBankName: event.myBankName,
                country: event.country,
                myPaymentChannelId: event.myPaymentChannelId);

        log("CreatingAndMatchingOrder REs ${response.statusCode}");
        Map<String, dynamic> result =
            jsonDecode(await response.transform(utf8.decoder).join());

        log("CreatingAndMatchingOrder REs 2 ${jsonEncode(result)}");

        if (response.statusCode == 200) {
          if (result['succeeded'] == true) {
            log('result entity orderNo ${result['entity']['entity']['orderNo']}');
            log('result entity customerId ${result['entity']['entity']['customerId']}');
            yield CreateAndMatchOrderSuccessful(
                matchFoundObj: event.matchFoundObj,
                customerOrderNo: result['entity']['entity']['orderNo'],
                customerId: result['entity']['entity']['customerId']);
          } else {
            yield CreateAndMatchOrderFailed(
                errorMessage: result['message'] ??
                    result['entity']['entity'] ??
                    result['messages'][0]);
          }
        } else {
          yield CreateAndMatchOrderFailed(
              errorMessage: result['message'] ??
                  result['entity']['entity'] ??
                  result['messages'][0]);
        }
      } on SocketException {
        yield CreateAndMatchOrderSocketException();
      } on HttpException {
        yield CreateAndMatchOrderHttpException();
      } catch (err) {
        print(err);
      }
    }
  }
}
