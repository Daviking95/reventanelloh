import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/helpers/convert_currency_code.dart';
import 'package:anelloh_mobile/helpers/device_info.dart';
import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter/material.dart';

class MatchFoundScreenService {
  Future<HttpClientResponse> createAndMatchOrder({
    @required MatchFoundObj matchFoundObj,
    @required User user,
    String address,
    String city,
    String state,
    String zipCode,
    String country,
    int myPaymentChannelId,
    String myAccountNumber,
    String myBankName,
  }) async {
    try {
      var deviceId;
      if (await readFromPref('__device__token')) {
        deviceId =
            await getFromPref('__device__token') ?? await getDeviceToken();
      } else {
        deviceId = await getDeviceToken();
      }

      Map<String, dynamic> requestData = {
        'customerId': user.customerId,
        'myAmount': double.parse(matchFoundObj.myAmount),
        'myCurrency':
            convertCurrencyCode(currencyCode: matchFoundObj.myCurrency),
        'rate': double.parse(matchFoundObj.exchangeRate),
        'myAccountNumber': matchFoundObj?.myAccountNumber ?? myAccountNumber,
        'myBankName': matchFoundObj?.myBankName ?? myBankName,
        'bankRouteNo': matchFoundObj?.bankRouteNo ?? "",
        "address": address ?? "",
        "city": city ?? "",
        "state": state ?? "",
        "zipCode": zipCode ?? "",
        "country": country ?? "",
        "applicationType": 1,
        "myPaymentChannelId": myPaymentChannelId ?? 8,
        'convertedCurrency':
            convertCurrencyCode(currencyCode: matchFoundObj.convertedCurrency),
        'convertedAmount': double.parse(matchFoundObj.convertedAmount),
        'findMatchResult': {
          'orderId': int.parse(matchFoundObj.entityId),
          'orderNo': matchFoundObj.orderNo,
          'customerId': matchFoundObj.customerId,
          'transactionFee': double.parse(matchFoundObj.processingFee),
          'orderStatus': matchFoundObj.orderStatus
        },
        "deviceId": deviceId,
        "loggedInDevice": 1
      };

      log("CreatingAndMatchingOrder ${jsonEncode(requestData)}");

      HttpClientResponse response = await HttpService()
          .post(url: 'Orders/createandmatchorder', requestData: requestData);

      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    } catch (err) {
      throw new Error();
    }
  }
}
