import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class MatchFoundScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class AcceptMatchFoundEvent extends MatchFoundScreenEvent {}

class RejectMatchFoundEvent extends MatchFoundScreenEvent {}

class MoveToCreateOrderScreenEvent extends MatchFoundScreenEvent {}

class MoveToDashboardScreenEvent extends MatchFoundScreenEvent {}

class MoveToPaymentDetailsScreenEvent extends MatchFoundScreenEvent {}

class CreateAndMatchOrderEvent extends MatchFoundScreenEvent {
  final MatchFoundObj matchFoundObj;
  final User user;
  final String address;
  final String city;
  final String state;
  final String zipCode;
  final String country;
  final int applicationType;
  final int myPaymentChannelId;
  final String myBankName;
  final String myAccountNumber;

  CreateAndMatchOrderEvent(
      {@required this.matchFoundObj,
      @required this.user,
      @required this.address,
      this.city,
      this.state,
      this.zipCode,
      this.country,
      this.applicationType,
      this.myPaymentChannelId,
      this.myBankName,
      this.myAccountNumber})
      : super();
}
