import 'package:anelloh_mobile/bloc/create_order_screen/create_order_screen_event.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class MatchFoundScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class InitializeMatchFoundState extends MatchFoundScreenState {}

class AcceptMatchFound extends MatchFoundScreenState {}

class RejectMatchFound extends MatchFoundScreenState {}

class MatchFoundStateChanged extends MatchFoundScreenState {}

class MoveToCreateOrderScreen extends MatchFoundScreenState {}

class CreatingAndMatchingOrder extends MatchFoundScreenState {}

class CreateAndMatchOrderSuccessful extends MatchFoundScreenState {
  final MatchFoundObj matchFoundObj;
  final dynamic customerOrderNo;
  final dynamic customerId;
  CreateAndMatchOrderSuccessful(
      {@required this.matchFoundObj, this.customerOrderNo, this.customerId})
      : super();
}

class CreateAndMatchOrderFailed extends MatchFoundScreenState {
  final String errorMessage;
  CreateAndMatchOrderFailed({@required this.errorMessage}) : super();
}

class CreateAndMatchOrderSocketException extends MatchFoundScreenState {}

class CreateAndMatchOrderHttpException extends MatchFoundScreenState {}

class MoveToDashboardScreen extends MatchFoundScreenState {}

class MoveToPaymentDetailsScreen extends MatchFoundScreenState {}
