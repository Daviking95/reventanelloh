import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/protected_screen/protected_screen_event.dart';
import 'package:anelloh_mobile/bloc/protected_screen/protected_screen_service.dart';
import 'package:anelloh_mobile/bloc/protected_screen/protected_screen_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProtectedScreenBloc
    extends Bloc<ProtectedScreenEvent, ProtectedScreenState> {
  ProtectedScreenBloc() : super(ProtectedScreenInitialState());

  ProtectedScreenService _protectedScreenService = new ProtectedScreenService();

  Stream<ProtectedScreenState> mapEventToState(
      ProtectedScreenEvent event) async* {
    if (event is SessionExpiredEvent) {
      yield SessionExpired();
    }

    if (event is ValidateTokenEvent) {
      try {
        yield ValidatingToken();
        HttpClientResponse response =
            await _protectedScreenService.validateToken(token: event.token);
        if (response.statusCode != 200) {
          yield FailedToValidateToken();
        } else {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          log("ValidatingToken ${jsonEncode(result)}");

          if (result['succeeded'] == false) {
            yield FailedToValidateToken();
          }
        }
      } on SocketException {
        yield FailedToValidateToken();
      } on HttpException {
        yield FailedToValidateToken();
      } catch (err) {
        if (err is SocketException) {
          yield FailedToValidateToken();
        }
        if (err is HttpException) {
          yield FailedToValidateToken();
        }
        yield FailedToValidateToken();
      }
    }
  }
}
