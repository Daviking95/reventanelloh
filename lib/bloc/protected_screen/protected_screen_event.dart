import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class ProtectedScreenEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SessionExpiredEvent extends ProtectedScreenEvent {}

class ValidateTokenEvent extends ProtectedScreenEvent {
  final String token;
  ValidateTokenEvent({@required this.token}):super();
}


