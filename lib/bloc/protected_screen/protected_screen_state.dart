import 'package:equatable/equatable.dart';

class ProtectedScreenState extends Equatable {
  @override
  List<Object> get props => [];
}

class ProtectedScreenInitialState extends ProtectedScreenState {}

class SessionExpired extends ProtectedScreenState {}

class IsSettingUpCurrentUser extends ProtectedScreenState {}

class FailedToValidateToken extends ProtectedScreenState {}

class ValidatingToken extends ProtectedScreenState {}