
import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/cupertino.dart';

class ProtectedScreenService {
  Future<HttpClientResponse> validateToken({@required String token}) async {
    try {
      Map<String, dynamic> requestData = {
        'token': token
      };
      HttpClientResponse response = await HttpService().post(url: 'Authentication/validateToken', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('');
    } on HttpException {
      throw new HttpException('');
    }
    catch(err) {
      throw new Error();
    }
  }
}