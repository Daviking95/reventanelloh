import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/bloc/user/user_event.dart';
import 'package:anelloh_mobile/bloc/user/user_service.dart';
import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:anelloh_mobile/constants.dart';
import 'package:anelloh_mobile/helpers/classes/transaction_limit.dart';
import 'package:anelloh_mobile/helpers/shared_pref_helper.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserBloc extends Bloc<UserEvent, UserState> {
  UserBloc()
      : super(InitialUserState(
            user: User(
          fullNames: '',
          firstName: '',
          lastName: '',
          userName: '',
          emailAddress: '',
          profileImage: null,
          phoneNumber: '',
          bvn: '',
          homeAddress: '',
          gender: '',
          customerId: '',
          verificationStatus: '',
          countryCode: '',
        )));
  UserService _userService = UserService();

  @override
  Stream<UserState> mapEventToState(UserEvent event) async* {
    // if (event is SaveCurrentUserEvent) {
    //   yield SavingCurrentUserLoader();
    //
    //   Map<String, dynamic> resultEntity;
    //   if (await readFromPref('__anelloh_current_user__')) {
    //     resultEntity =
    //         jsonDecode(await getFromPref('__anelloh_current_user__'));
    //   }
    //
    //   var image;
    //   if (resultEntity['customerImageFileLocation'] != null) {
    //     image = resultEntity['customerImageFileLocation'];
    //   }
    //   User user = User(
    //       fullNames: '${resultEntity['firstName']} ${resultEntity['lastName']}',
    //       firstName: resultEntity['firstName'],
    //       lastName: resultEntity['lastName'],
    //       homeAddress: resultEntity['address'],
    //       gender: resultEntity['gender'].toString(),
    //       customerId: resultEntity['customerId'].toString(),
    //       verificationStatus: resultEntity['status'].toString(),
    //       userName: resultEntity['userName'],
    //       emailAddress: resultEntity['email'],
    //       phoneNumber: resultEntity['phoneNumber'].toString(),
    //       profileImage: image,
    //       countryCode: resultEntity['countryCode'],
    //       documentImage: resultEntity['documentImage'],
    //       documentExpiryDate: resultEntity['documentExpiryDate'],
    //       documentNumber: resultEntity['documentNumber'],
    //       documentType: resultEntity['documentType']);
    //
    //   // AppConstants.userData = user;
    //
    //   yield SaveCurrentUser(user: user);
    // }
    if (event is LogOutUserEvent) {
      try {
        yield RequestingLogout();
        HttpClientResponse response =
            await _userService.logOut(customerEmail: event.user.emailAddress);
        if (response.statusCode == 200) {
          AppConstants.userData = null;
          yield LogOutSuccessful();
        } else {
          yield LogOutFailed();
        }
      } on SocketException {} on HttpException {} catch (err) {
        print(err);
      }
    }
    if (event is GetCustomerByIdEvent) {
      try {
        yield FetchingCustomerById();
        HttpClientResponse response =
            await _userService.getSwapCustomerById(customerId: event.customerId);

        log('FetchingCustomerById Result ${response.statusCode}');

        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());

          var res = result['entity'];

          log('FetchingCustomerById Result ${jsonEncode(res)}');

          if (result['succeeded'] == true) {
            var image;
            if (res['customerImageFileLocation'] != null) {
              image = res['customerImageFileLocation'];
            }
            User user = User(
                fullNames: '${res['firstName']} ${result['lastName']}',
                firstName: res['firstName'],
                lastName: res['lastName'],
                homeAddress: res['address'],
                gender: res['gender'].toString(),
                customerId: res['customerId'].toString(),
                verificationStatus: res['status'].toString(),
                userName: res['userName'],
                emailAddress: res['email'],
                phoneNumber: res['phoneNumber'].toString(),
                profileImage: image,
                countryCode: res['countryCode']);

            // AppConstants.userData = user;
            yield SuccessfullyFetchedCustomerById(orderCustomer: user);
          } else {
            yield ErrorFetchingCustomerById();
          }
        } else {
          yield ErrorFetchingCustomerById();
        }
      } on SocketException {
        yield UserStateNoInternetException();
      } on HttpException {
        yield UserStateHttpException();
      } catch (err) {
        print(err);
        yield ErrorFetchingCustomerById();
      }
    }

    if (event is GetCurrentCustomerByIdEvent) {
      try {
        yield FetchingCurrentCustomer();

        Map<String, dynamic> resultEntity;
        // if (await readFromPref('__anelloh_current_user__')) {
        //   resultEntity =
        //       jsonDecode(await getFromPref('__anelloh_current_user__'));
        // } else {
        HttpClientResponse response =
            await _userService.getCustomerById(customerId: event.customerId);
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          resultEntity = result['entity'];

          log('AppConstants.userData resultEntity ${resultEntity}');

          await saveToPref(
              '__anelloh_current_user__', jsonEncode(resultEntity));
        }
        // }

        var image;
        if (resultEntity['customerImageFileLocation'] != null) {
          image = resultEntity['customerImageFileLocation'];
        }
        User user = User(
            fullNames:
                '${resultEntity['firstName']} ${resultEntity['lastName']}',
            firstName: resultEntity['firstName'],
            lastName: resultEntity['lastName'],
            homeAddress: resultEntity['address'],
            gender: resultEntity['gender'].toString(),
            customerId: resultEntity['customerId'].toString(),
            verificationStatus: resultEntity['status'].toString(),
            userName: resultEntity['userName'],
            emailAddress: resultEntity['email'],
            phoneNumber: resultEntity['phoneNumber'].toString(),
            profileImage: image,
            countryCode: resultEntity['countryCode'],
            documentImage: resultEntity['documentImage'],
            documentExpiryDate: resultEntity['documentExpiryDate'],
            documentNumber: resultEntity['documentNumber'],
            documentType: resultEntity['documentType']);

        AppConstants.userData = user;

        log('AppConstants.userData Bloc ${AppConstants.userData}');

        yield SaveCurrentUser(user: user);

        // HttpClientResponse response =
        //     await _userService.getCustomerById(customerId: event.customerId);
        // if (response.statusCode == 200) {
        //   Map<String, dynamic> result =
        //       jsonDecode(await response.transform(utf8.decoder).join());
        //   Map<String, dynamic> resultEntity = result['entity'];
        //   await saveToPref(
        //       '__anelloh_current_user__', jsonEncode(resultEntity));
        //   var image;
        //   if (resultEntity['customerImageFileLocation'] != null) {
        //     image = resultEntity['customerImageFileLocation'];
        //   }
        //   User user = User(
        //       fullNames:
        //           '${resultEntity['firstName']} ${resultEntity['lastName']}',
        //       firstName: resultEntity['firstName'],
        //       lastName: resultEntity['lastName'],
        //       homeAddress: resultEntity['address'],
        //       gender: resultEntity['gender'].toString(),
        //       customerId: resultEntity['customerId'].toString(),
        //       verificationStatus: resultEntity['status'].toString(),
        //       userName: resultEntity['userName'],
        //       emailAddress: resultEntity['email'],
        //       phoneNumber: resultEntity['phoneNumber'].toString(),
        //       profileImage: image,
        //       countryCode: resultEntity['countryCode'],
        //       documentImage: resultEntity['documentImage'],
        //       documentExpiryDate: resultEntity['documentExpiryDate'],
        //       documentNumber: resultEntity['documentNumber'],
        //       documentType: resultEntity['documentType']);
        //
        //   AppConstants.userData = user;
        //   yield FetchingCurrentCustomer();
        //   yield SaveCurrentUser(user: user);
        // }
      } on SocketException {
        yield UserStateNoInternetException();
      } on HttpException {
        yield UserStateHttpException();
      } catch (err) {
        yield ErrorOccurredLogUserOut();
      }
    }

    if (event is GetAllTransactionLimitEvent) {
      try {
        var resultEntity;

        HttpClientResponse response =
            await _userService.getAllTransactionLimit();
        if (response.statusCode == 200) {
          Map<String, dynamic> result =
              jsonDecode(await response.transform(utf8.decoder).join());
          resultEntity = result['entity'];

          List<TransactionLimitResponseModelEntity> listings = [];

          resultEntity.forEach((entity) {
            TransactionLimitResponseModelEntity transactionListing =
                TransactionLimitResponseModelEntity.fromJson(entity);

            listings.add(transactionListing);
          });

          AppConstants.transactionLimitArray = listings;

          // log('AppConstants.transactionLimitArray ${AppConstants.transactionLimitArray}');
        }
      } on SocketException {
        yield UserStateNoInternetException();
      } on HttpException {
        yield UserStateHttpException();
      } catch (err) {
        yield ErrorOccurredLogUserOut();
      }
    }
  }
}
