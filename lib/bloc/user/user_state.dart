import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class UserState extends Equatable {
  final User user;
  UserState({this.user});
  @override
  List<Object> get props => [];
}

class UserStateChanged extends UserState {}

class InitialUserState extends UserState {
  final User user;
  final User orderCustomer;
  InitialUserState({this.user, this.orderCustomer}) : super(user: user);
}

class FetchingCustomerById extends UserState {}

class FetchingCurrentCustomer extends UserState {}

class ResetCustomerTokenDueToTokenExpiredError extends UserState {}

class ErrorOccurredLogUserOut extends UserState {}

class ReSettingCurrentCustomerToken extends UserState {}

class SuccessfullyFetchedCurrentCustomer extends UserState {}

class SuccessfullyResetCustomerToken extends UserState {}

class SuccessfullyFetchedCustomerById extends UserState {
  final User orderCustomer;
  SuccessfullyFetchedCustomerById({@required this.orderCustomer}) : super();
}

class ErrorFetchingCustomerById extends UserState {}

class SavingCurrentUserLoader extends UserState {}

class SaveCurrentUser extends UserState {
  final User user;
  SaveCurrentUser({@required this.user}) : super(user: user);

  SaveCurrentUser copyWith({
    String fullNames,
    String firstName,
    String lastName,
    String middleName,
    String bvn,
    String homeAddress,
    String gender,
    String customerId,
    String verificationStatus,
    String userName,
    String emailAddress,
    String phoneNumber,
    String profileImage,
    String countryCode,
  }) {
    User user = User(
      fullNames: fullNames ?? this.user.fullNames,
      firstName: firstName ?? this.user.firstName,
      lastName: lastName ?? this.user.lastName,
      homeAddress: homeAddress ?? this.user.homeAddress,
      gender: gender ?? this.user.gender,
      customerId: customerId ?? this.user.customerId,
      verificationStatus: verificationStatus ?? this.user.verificationStatus,
      userName: userName ?? this.user.userName,
      emailAddress: emailAddress ?? this.user.emailAddress,
      phoneNumber: phoneNumber ?? this.user.phoneNumber,
      profileImage: profileImage ?? this.user.profileImage,
      countryCode: countryCode ?? this.user.countryCode,
    );
    return SaveCurrentUser(user: user);
  }
}

class RequestingLogout extends UserState {}

class LogOutSuccessful extends UserState {}

class LogOutFailed extends UserState {}

class UserStateNoInternetException extends UserState {}

class UserStateHttpException extends UserState {}

// CLASSES ASSOCIATED WITH USER

class User {
  final String fullNames;
  final String firstName;
  final String lastName;
  final String middleName;
  final String bvn;
  final String homeAddress;
  final String gender;
  final String customerId;
  final String verificationStatus;
  final String userName;
  final String emailAddress;
  final String phoneNumber;
  final String profileImage;
  final String countryCode;
  final String rating;
  final int noOfSuccessfulTransactions;
  final String documentImage;
  final int documentType;
  final String documentNumber;
  final String documentExpiryDate;

  User({
    @required this.fullNames,
    @required this.firstName,
    @required this.lastName,
    this.middleName,
    this.bvn,
    this.rating,
    this.noOfSuccessfulTransactions,
    this.documentImage,
    this.documentType,
    this.documentNumber,
    this.documentExpiryDate,
    @required this.homeAddress,
    @required this.gender,
    @required this.customerId,
    @required this.verificationStatus,
    @required this.userName,
    @required this.emailAddress,
    @required this.phoneNumber,
    @required this.profileImage,
    @required this.countryCode,
  });
}
