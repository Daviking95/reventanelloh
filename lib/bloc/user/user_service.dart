import 'dart:developer';
import 'dart:io';

import 'package:anelloh_mobile/helpers/dio.dart';
import 'package:flutter/material.dart';

class UserService {
  Future<HttpClientResponse> logOut({@required String customerEmail}) async {
    try {
      Map<String, dynamic> requestData = {'email': customerEmail};
      HttpClientResponse response = await HttpService()
          .post(url: 'Authentication/logout', requestData: requestData);
      return response;
    } on SocketException {
      throw new SocketException('No Internet connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (err) {
      throw new Error();
    }
  }

  // api to get user by id
  Future<HttpClientResponse> getCustomerById(
      {@required String customerId}) async {
    try {
      log('FetchingCustomerById Customers/getbyid/$customerId');

      HttpClientResponse response = await HttpService()
          .post(url: 'Customers/getbyid/$customerId', requestData: '');

      return response;
    } on SocketException {
      throw new SocketException('No Internet connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> getAllTransactionLimit() async {
    try {
      HttpClientResponse response = await HttpService()
          .post(url: 'Configurations/getalltransactionlimits', requestData: '');
      return response;
    } on SocketException {
      throw new SocketException('No Internet connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (err) {
      throw new Error();
    }
  }

  Future<HttpClientResponse> getSwapCustomerById({String customerId}) async {
    try {
      log('FetchingCustomerById Customers/getswapcustomerbyid/$customerId');

      HttpClientResponse response = await HttpService()
          .post(url: 'Customers/getswapcustomerbyid/$customerId', requestData: '');

      return response;
    } on SocketException {
      throw new SocketException('No Internet connection');
    } on HttpException {
      throw new HttpException('Service currently unavailable');
    } catch (err) {
      throw new Error();
    }
  }
}
