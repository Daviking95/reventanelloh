import 'package:anelloh_mobile/bloc/user/user_state.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/cupertino.dart';

class UserEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class SaveCurrentUserEvent extends UserEvent {
  final User user;
  SaveCurrentUserEvent({@required this.user}) : super();
}

class LogOutUserEvent extends UserEvent {
  final User user;
  LogOutUserEvent({
    @required this.user,
  }) : super();
}

class GetCustomerByIdEvent extends UserEvent {
  final String customerId;
  GetCustomerByIdEvent({@required this.customerId}) : super();
}

class GetCurrentCustomerByIdEvent extends UserEvent {
  final String customerId;
  GetCurrentCustomerByIdEvent({@required this.customerId}) : super();
}

class GetAllTransactionLimitEvent extends UserEvent {}
