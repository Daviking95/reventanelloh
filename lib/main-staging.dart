
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart' as DotEnv;
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intercom_plugin/intercom_plugin.dart';
import 'dynamic_link_service.dart';
import 'flavors.dart';
import 'helpers/fcm_service.dart';
import 'main.dart';

void main() async{
  F.appFlavor = Flavor.STAGING;
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  checkDeepLinking();
  DynamicLinkService().handleDynamicLinks();

  // Set the background messaging handler early on, as a named top-level function
  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);

  /// Create an Android Notification Channel.
  ///
  /// We use this channel in the `AndroidManifest.xml` file to override the
  /// default FCM channel to enable heads up notifications.
  await flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.createNotificationChannel(channel);

  await DotEnv.load(fileName: '.env');
  await Intercom.initialize(
      appId: 'pwta5071',
      iOSApiKey: 'ios_sdk-3b929daff10f72d82e3cb2aa89b4d65dc468a2a2',
      androidApiKey: 'android_sdk-6d6bf312895de81dd592b3995404d12be0019a1b');
  Intercom.registerUnidentifiedUser();
  runApp(AnellohApp());
}
